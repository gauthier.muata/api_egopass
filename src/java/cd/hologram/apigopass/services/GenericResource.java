package cd.hologram.apigopass.services;

import cd.hologram.apigopass.constants.GeneralConst;
import cd.hologram.apigopass.constants.PropertiesConst;
import cd.hologram.apigopass.business.AssujBusiness;
import cd.hologram.apigopass.business.GenericBusiness;
import cd.hologram.apigopass.business.GisBusiness;
import cd.hologram.apigopass.business.DataAccess;
import cd.hologram.apigopass.business.DocumentBusiness;
import cd.hologram.apigopass.mock.Periode;
import cd.hologram.apigopass.properties.PropertiesConfig;
import cd.hologram.apigopass.properties.PropertiesMessage;
import cd.hologram.apigopass.utils.Tools;
import cd.hologram.apigopass.utils.ResponseMessage;
import cd.hologram.apigopass.entities.Personne;
import cd.hologram.apigopass.entities.ArticleBudgetaire;
import cd.hologram.apigopass.entities.Bien;
import cd.hologram.apigopass.entities.LoginWeb;
import cd.hologram.apigopass.entities.DetailsGoPass;
import cd.hologram.apigopass.entities.Compagnie;
import cd.hologram.apigopass.entities.AdressePersonne;
import cd.hologram.apigopass.entities.ComplementBien;
import cd.hologram.apigopass.entities.Aeroport;
import cd.hologram.apigopass.entities.Assujeti;
import cd.hologram.apigopass.entities.Tarif;
import cd.hologram.apigopass.pojo.RDeclaration;
import cd.hologram.apigopass.pojo.TraitementBien;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author moussa.toure
 */
@Path("")
public class GenericResource {

    @Context
    private UriInfo context;

    ResponseMessage responseMessage;
    PropertiesConfig propertiesConfig;
    PropertiesMessage propertiesMessage;

    /**
     * Creates a new instance of GenericResource
     *
     * @throws java.io.IOException
     */
    public GenericResource() throws IOException {

        propertiesConfig = new PropertiesConfig();
        propertiesMessage = new PropertiesMessage();
        responseMessage = new ResponseMessage();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("v1/login")
    public String login(@FormParam("username") String login, @FormParam("password") String password, @Context HttpHeaders headers) throws IOException {
        String reponse;

        try {

            reponse = DocumentBusiness.login(login, password);

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralException("en", "application/json");
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest("0", "v1/login", reponse);
//        System.out.println(String.format("Opération : login \n Requete du client N° %s. \n Réponse : \n %s", 0, reponse));

        return reponse;
    }

    /**
     * Retrieves representation of an instance of
     * cd.hologram.apidgrk.business.GenericResource
     *
     * @param file
     * @param headers
     * @return an instance of java.lang.String
     * @throws java.io.IOException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
//    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("bank/v1/payment")
    public String pushPayement(String file, @Context HttpHeaders headers) throws IOException {

        String reponse = null;
        String lang = checkLangHeader(headers);
        String format = checkFormatHeader(headers);
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);

            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = DocumentBusiness.payment(file, "", "");
//            }

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest("", "bank/v1/payment", reponse);
//        System.out.println(String.format("Opération : payment \n Requete du client N° %s. \n Réponse : \n %s", id, reponse));

        return reponse;
    }

    /**
     * Retrieves representation of an instance of
     * cd.hologram.apidgrk.business.GenericResource
     *
     * @param numero
     * @param headers
     * @return an instance of java.lang.String
     * @throws java.io.IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("v1/noteperception/{numero}")
    public Object getNotePerceptionByNumero(@PathParam("numero") String numero, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = checkFormatHeader(headers);
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);

            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = DocumentBusiness.getNotePerceptionById(numero, lang, format);
//            }

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest(id, "v1/noteperception/" + numero, reponse);
//        System.out.println(String.format("Opération : noteperception \n Requete du client N° %s. \n Réponse : \n %s", id, reponse));

        return reponse;
    }

    /**
     * Retrieves representation of an instance of
     * cd.hologram.apidgrk.business.GenericResource
     *
     * @param numero
     * @param headers
     * @return an instance of java.lang.String
     * @throws java.io.IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("v1/amr/{numero}")
    public String getAmrByNumero(@PathParam("numero") String numero, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = checkFormatHeader(headers);
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);

            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {

                reponse = DocumentBusiness.getAmrByNumero(numero, lang, format);
            }

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest(id, "v1/amr/" + numero, reponse);
//        System.out.println(String.format("Opération : amr \n Requete du client N° %s. \n Réponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     * Retrieves representation of an instance of
     * cd.hologram.apidgrk.business.GenericResource
     *
     * @param numero
     * @param headers
     * @return an instance of java.lang.String
     * @throws java.io.IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("v2/declaration/{numero}")
    public String getDeclarationV2ByNumero(@PathParam("numero") String numero, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = checkFormatHeader(headers);
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);

            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = DocumentBusiness.getDeclaration(numero, lang, format, 2);
//            }

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest(id, "v1/declaration/" + numero, reponse);
//        System.out.println(String.format("Opération : declaration \n Requete du client N° %s. \n Réponse : \n %s", id, reponse));

        return reponse;
    }

    /**
     * Retrieves representation of an instance of
     * cd.hologram.apidgrk.business.GenericResource
     *
     * @param numero
     * @param headers
     * @return an instance of java.lang.String
     * @throws java.io.IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("v1/declaration/{numero}")
    public String getDeclarationByNumero(@PathParam("numero") String numero, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = checkFormatHeader(headers);
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);

            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = DocumentBusiness.getDeclaration(numero, lang, format, 1);
            }

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest(id, "v1/declaration/" + numero, reponse);
//        System.out.println(String.format("Opération : declaration \n Requete du client N° %s. \n Réponse : \n %s", id, reponse));

        return reponse;
    }

    /**
     * Retrieves representation of an instance of
     * cd.hologram.apidgrk.business.GenericResource ro
     *
     * @param nif
     * @param headers
     * @return an instance of java.lang.String
     * @throws java.io.IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("v3/declaration/{nif}")
    public String getDeclarationByAssujetti(@PathParam("nif") String nif, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = checkFormatHeader(headers);
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);

//            reponse = checkAutorization(headers, appCode);
//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = DocumentBusiness.getDeclarationByNif(nif, lang, format, 1);
//            }

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest(id, "v1/declaration/" + nif, reponse);
//        System.out.println(String.format("Opération : declaration \n Requete du client N° %s. \n Réponse : \n %s", id, reponse));

        return reponse;
    }

    /**
     * Retrieves representation of an instance of
     * cd.hologram.apidgrk.business.GenericResource
     *
     * @param headers
     * @return an instance of java.lang.String
     * @throws java.io.IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("v3/journal/{personne}")
    public String getJournalByNom(@PathParam("personne") String personne, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = checkFormatHeader(headers);
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);

//            reponse = checkAutorization(headers, appCode);
//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            //  reponse = DocumentBusiness.getDeclarationByNif(nif, lang, format, 1);
            // reponse = DocumentBusiness.getBordereauCode(nomcomplet, lang, format, 1);
            reponse = DocumentBusiness.getJournalCode(personne, lang, format, 1);
//            }

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest(id, "v1/journal/" + personne, reponse);
//        System.out.println(String.format("Opération : declaration \n Requete du client N° %s. \n Réponse : \n %s", id, reponse));

        return reponse;
    }

    /**
     *
     * @param numero
     * @param codeAb
     * @param version
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v3/getAssujetti/{numero}/{codeAb}/{version}")
    public String getAssujettiV3(@PathParam("numero") String numero, @PathParam("codeAb") String codeAb, @PathParam("version") String version, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getAssujetti_v3(numero, codeAb, 3, version.equals("1"));

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getAssujetti\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v2/getAssujetti")
    public String getAssujettiV2(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = AssujBusiness.getLoginWeb(objet);
//            }

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        DocumentBusiness.logRequest(id, "v2/getAssujetti/" + ntd + "/" + password + "/" + operation, reponse);
//        System.out.println(String.format("Opération : v2/getAssujetti \n Requete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param file
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getAssujetti")
    public String getAssujetti(String file, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getAssujetti(file, 1, true);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getAssujetti\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    String checkTokenHeader(HttpHeaders bankHeader) {

        String token = GeneralConst.ResultCode.EXCEPTION_OPERATION;

        for (String headerKeySet : bankHeader.getRequestHeaders().keySet()) {

            if (headerKeySet.equalsIgnoreCase("token")) {
                token = bankHeader.getRequestHeader(headerKeySet).get(0);
                break;
            }
        }

        return token;
    }

    String checkIDHeader(HttpHeaders bankHeader) {

        String id = GeneralConst.ResultCode.EXCEPTION_OPERATION;

        for (String headerKeySet : bankHeader.getRequestHeaders().keySet()) {

            if (headerKeySet.equalsIgnoreCase("id")) {
                id = bankHeader.getRequestHeader(headerKeySet).get(0);
                break;
            }
        }

        return id;
    }

    String checkFormatHeader(HttpHeaders bankHeader) {

        String format = GeneralConst.Format_Response.JSON;

        for (String headerKeySet : bankHeader.getRequestHeaders().keySet()) {
            if (headerKeySet.equalsIgnoreCase("accept")) {
                format = bankHeader.getRequestHeader(headerKeySet).get(0);
                break;
            }
        }

        return format;
    }

    String checkLangHeader(HttpHeaders bankHeader) {

        String lang = GeneralConst.Lang.FR;

        for (String headerKeySet : bankHeader.getRequestHeaders().keySet()) {

            if (headerKeySet.equalsIgnoreCase("accept-language")) {
                lang = bankHeader.getRequestHeader(headerKeySet).get(0);
                break;
            }
        }

        return lang;
    }

    String checkAutorization(HttpHeaders bankHeaders, String appCode) throws Exception {

        String id = checkIDHeader(bankHeaders);
        String token = checkTokenHeader(bankHeaders);
        String lang = checkLangHeader(bankHeaders);
        String format = checkFormatHeader(bankHeaders);

        String response;

        try {

            if (id.equals(GeneralConst.ResultCode.EXCEPTION_OPERATION)) {

                response = responseMessage.getHeaderMissingException(lang, GeneralConst.ParamName.id, format);

            } else if (token.equals(GeneralConst.ResultCode.EXCEPTION_OPERATION)) {

                response = responseMessage.getHeaderMissingException(lang, GeneralConst.ParamName.token, format);

            } else {

                boolean isAutorize = Tools.isCustumerAuthorize(token, id, appCode);

                response = isAutorize
                        ? GeneralConst.NumberString.ONE
                        : responseMessage.getTokenInvalidException(lang, format);

            }

        } catch (Exception e) {

            throw e;
        }

        return response;

    }

    /**
     *
     * @param cityId
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/getAdministrativeEntities/{cityId}")
    public Object getAdministrativeEntities(@PathParam("cityId") String cityId, @Context HttpHeaders headers) throws IOException {

        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String districtCode = propertiesConfig.getContent(PropertiesConst.Config.DISTRICT_CODE);
            String communeCode = propertiesConfig.getContent(PropertiesConst.Config.COMMUNE_CODE);
            String filterByVille = propertiesConfig.getContent(PropertiesConst.Config.FILTER_BY_VILLE);
            String filterByDistrict = propertiesConfig.getContent(PropertiesConst.Config.FILTER_BY_DISTRICT);
            String filterByCommune = propertiesConfig.getContent(PropertiesConst.Config.FILTER_BY_COMMUNE);
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);

//            reponse = checkAutorization(headers, appCode);
//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = GisBusiness.getAdministrativeEntitiesByCity(cityId, districtCode, communeCode, filterByVille, filterByDistrict, filterByCommune);
//            }

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralExceptionV2(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getAdministrativeEntities\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param communeId
     * @param quarterId
     * @param avenueId
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/getOwnerProperties/{communeId}/{quarterId}/{avenueId}")
    public Object getOwnerProperties(@PathParam("communeId") String communeId, @PathParam("quarterId") String quarterId,
            @PathParam("avenueId") String avenueId, @Context HttpHeaders headers) throws IOException {

        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String codeImmobilier = propertiesConfig.getContent(PropertiesConst.Config.CODE_IMMOBILIER);
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);

            reponse = checkAutorization(headers, appCode);
            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.getOwnerProperties(communeId, quarterId, avenueId, codeImmobilier);
            }

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralExceptionV2(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getOwnerProperties\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param communeId
     * @param quarterId
     * @param avenueId
     * @param periodeDate
     * @param impotCode
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/getOwnerPropertiesAndTransactions/{communeId}/{quarterId}/{avenueId}/{periodeDate}/{impotCode}")
    public Object getOwnerPropertiesAndTransactions(@PathParam("communeId") String communeId, @PathParam("quarterId") String quarterId, @PathParam("avenueId") String avenueId,
            @PathParam("periodeDate") String periodeDate, @PathParam("impotCode") String impotCode, @Context HttpHeaders headers) throws IOException {

        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String codeImmobilier = propertiesConfig.getContent(PropertiesConst.Config.CODE_IMMOBILIER);
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);

            reponse = checkAutorization(headers, appCode);
            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.getOwnerPropertiesAndTransactions(communeId, quarterId, avenueId, codeImmobilier, impotCode, periodeDate);
            }

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralExceptionV2(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getOwnerPropertiesAndTransactions\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param ab
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/getTarif/{ab}")
    public Object getTarif(@PathParam("ab") String ab, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
//            reponse = checkAutorization(headers, appCode);
//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = DocumentBusiness.getPaliers(ab);
//            }

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralExceptionV2(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getTarif\nRequete du client N° %s.\nResponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/getTax")
    public Object getTax(@Context HttpHeaders headers) throws IOException {

        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
//            reponse = checkAutorization(headers, appCode);
//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = GisBusiness.getArticlesBudgetaires();
//            }

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralExceptionV2(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getTax\nRequete du client N° %s.\nResponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/getUnits")
    public Object getUnits(@Context HttpHeaders headers) throws IOException {

        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);

            reponse = checkAutorization(headers, appCode);
            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.getUnits();
            }

        } catch (Exception ex) {

            reponse = responseMessage.getGeneralExceptionV2(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getUnits\nRequete du client N° %s.\nResponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param file
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/createPerson")
    public String createPerson(String file, @Context HttpHeaders headers) throws IOException {

        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.newPersonAndProperties(file);
            }
        } catch (Exception ex) {

            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : createPerson\n Requete du client N° %s.\n Réponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param file
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/create_person")
    public String create_person(String file, @Context HttpHeaders headers) throws IOException {

        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.newPerson(file);
            }
        } catch (Exception ex) {

            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : create_person\n Requete du client N° %s.\n Response : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param file
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/create_property")
    public String create_property(String file, @Context HttpHeaders headers) throws IOException {
        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.newBien(file);
            }
        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : create_property\n Requete du client N° %s.\n Response : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/getPersonType")
    public String getPersonType(@Context HttpHeaders headers) throws IOException {
        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.getTypePersonne();
            }
        } catch (Exception e) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(e.toString());
        }

//        System.out.println(String.format("Opération : getPersonType\n Requete du client N° %s.\n Réponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/getPropertyType")
    public String getPropertyType(@Context HttpHeaders headers) throws IOException {
        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.getTypeProperty();
            }
        } catch (Exception e) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(e.toString());
        }

//        System.out.println(String.format("Opération : getPropertyType\n Requete du client N° %s.\n Response : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param param
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/getPersonInformation/{param}")
    public String getPersonInformation(@PathParam("param") String param, @Context HttpHeaders headers) throws IOException {
        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.getInformationPersonne(param);
            }
        } catch (Exception e) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(e.toString());
        }

//        System.out.println(String.format("Opération : getPersonneByParam\nRequete du client N° %s.\nResponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param code
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/getPersonById/{code}")
    public String getPersonById(@PathParam("code") String code, @Context HttpHeaders headers) throws IOException {
        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.getPersonneById(code);
            }
        } catch (Exception e) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(e.toString());
        }

//        System.out.println(String.format("Opération : getPersonById \nRequete du client N° %s.\nResponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param file
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/saveDeclaration")
    public String retraitDeclaration(String file, @Context HttpHeaders headers) throws IOException {
        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.newRetraitDeclaration(file);
            }
        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : saveDeclaration\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param mail
     * @return
     * @throws java.io.IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/checkMailValid/{mail}")
    public String checkMailValid(@Context HttpHeaders headers, @PathParam("mail") String mail) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
//            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = AssujBusiness.getPersonne(mail);
//            }

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : checkMailValid\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param object
     * @return
     * @throws java.io.IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/initPassword")
    public String initPassword(@Context HttpHeaders headers, String object) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;

        try {

            reponse = AssujBusiness.initPassword(object);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        return reponse;
    }

    /**
     *
     * @param agentId
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getTransactions/{agentId}")
    public String getTransactions(@PathParam("agentId") String agentId, @Context HttpHeaders headers) throws IOException {
        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.getTransactions(agentId);
            }
        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getTransactions\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param taxId
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getRates/{taxId}")
    public String getRates(@PathParam("taxId") String taxId, @Context HttpHeaders headers) throws IOException {
        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.getRates(taxId);
            }
        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getRates\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param rNumber
     * @param Id
     * @param taxID
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("v1/checkProperty/{rNumber}/{Id}/{taxID}")
    public Object checkCarRegistrationNumber(@PathParam("rNumber") String rNumber, @PathParam("Id") String Id, @PathParam("taxID") String taxID, @Context HttpHeaders headers) throws IOException {

        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
//        String id = checkIDHeader(headers);

        JSONObject jsonResult = new JSONObject();

        JSONObject data = new JSONObject();
        JSONObject dataComplement = new JSONObject();
        List<JSONObject> dataPeriodList = new ArrayList<>();

        JSONObject jsona;

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
//            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            String codeComplement = propertiesConfig.getContent(PropertiesConst.Config.CONFIG_TYPE_COMPLEMENT_BIEN);
            jsona = new JSONObject(codeComplement);
            String complement = jsona.get(Id).toString();

            ComplementBien complementBien = DataAccess.getComplementBienImmatriculation(rNumber, complement);

            if (complementBien != null) {

                Bien bien = complementBien.getBien();

                List<Assujeti> assujetiList = bien.getAssujetiList();

                data.put("personCode", bien.getFkAdressePersonne().getPersonne().getCode());
                data.put("dateAcquisition", bien.getAcquisitionList() == null
                        ? "" : bien.getAcquisitionList().get(0).getDateAcquisition());

                dataComplement = TraitementBien.getJSONComplement(Id, bien);
                data.put("complement", dataComplement);

                if (!assujetiList.isEmpty()) {

                    for (Assujeti assujeti : assujetiList) {

                        if (assujeti.getArticleBudgetaire().getCode().equals(taxID)) {

                            ArticleBudgetaire ab = assujeti.getArticleBudgetaire();
                            Tarif tarif = assujeti.getTarif();

                            DataAccess dataAccess = new DataAccess();

                            List<RDeclaration> declarations = dataAccess.getDeclarationByAssujettissement(assujeti.getId());

                            List<Periode> periodeList;

                            data.put("tarif", assujeti.getTarif().getCode());

                            if (!declarations.isEmpty()) {

                                periodeList = DocumentBusiness.getPeriodeInformation(ab.getCode(), tarif.getCode(), assujeti.getId(), assujeti.getValeur().intValue());
                                dataPeriodList = TraitementBien.getJSONPeriodes(periodeList);
                                data.put("periodeList", dataPeriodList);

                            } else {

                                jsonResult.put("message", "declaration not found");
                                data.put("periodeList", "");
                            }

                        }

                    }

                } else {
                    //Aucun assujettissement trouvé
                    jsonResult.put("message", "Assuj Not found");
                    data.put("periodeList", "");
                }

                jsonResult.put("code", "100");
                jsonResult.put("data", data);

            } else {
                //Le complement immatriculation est introuvable
                jsonResult.put("code", "400");
                jsonResult.put("message", "Bien Not found");
                jsonResult.put("data", "");
            }

            reponse = jsonResult.toString();

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : checkProperty \nRequete du client N° %s. \nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param file
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v2/saveProperty")
    public String saveProperty2(String file, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
//        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
//            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = AssujBusiness.newBienAndAssujV2(file);
//            }
        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : newCarTaxing\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param file
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/saveProperty")
    public String saveProperty(String file, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
//        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
//            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = AssujBusiness.newBienAndAssuj(file);
//            }
        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : newCarTaxing\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/savecustumer")
    public String savecustumer(String file, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = AssujBusiness.saveAssujetti(file);
//            }
        } catch (Exception ex) {

            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        //  DocumentBusiness.logRequest(id, "v1/savecustumer/", reponse);
//        System.out.println(String.format("Opération :v1/savecustumer/ \n Requete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param locationId
     * @param headers
     * @return
     * @throws java.io.IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getDetailsByLocationId/{locationId}")
    public String getDetailsByLocationId(@PathParam("locationId") String locationId, @Context HttpHeaders headers) throws IOException {
        String reponse = GeneralConst.EMPTY_STRING;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            String typeBien = propertiesConfig.getContent(PropertiesConst.Config.CODE_IMMOBILIER);
            String typeComplementBien = propertiesConfig.getContent(PropertiesConst.Config.TYPE_COMPLEMENT_BIEN_LOCATION_ID);

            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.getDetailsByLocationId(locationId, typeComplementBien, typeBien);
            }
        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }
        DocumentBusiness.logRequest(id, "v1/getDetailsByLocationId/" + locationId, reponse);
//        System.out.println(String.format("Opération : getDetailsByLocationId\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @return
     * @throws java.io.IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getAllLocationData")
    public String getAllKinshasaDashboardData(@Context HttpHeaders headers) throws IOException {
        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            String typeBien = propertiesConfig.getContent(PropertiesConst.Config.CODE_IMMOBILIER);
            String typeComplementBien = propertiesConfig.getContent(PropertiesConst.Config.TYPE_COMPLEMENT_BIEN_LOCATION_ID);

            reponse = checkAutorization(headers, appCode);

            if (reponse.equals(GeneralConst.NumberString.ONE)) {
                reponse = GisBusiness.getAllLocationData(typeComplementBien, typeBien);
            }
        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }
        DocumentBusiness.logRequest(id, "v1/getAllLocationData/", reponse);
//        System.out.println(String.format("Opération : getAllLocationData\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @return
     * @throws java.io.IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v2/getAllLocationData")
    public String getAllLocation(@Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;

        try {
            String typeBien = propertiesConfig.getContent(PropertiesConst.Config.CODE_IMMOBILIER);
            String typeComplementBien = propertiesConfig.getContent(PropertiesConst.Config.TYPE_COMPLEMENT_BIEN_LOCATION_ID);

            reponse = GisBusiness.getAllLocationData(typeComplementBien, typeBien);
        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest("", "v1/getAllLocationData/", reponse);
//        System.out.println(String.format("Opération : getAllLocationData\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @return
     * @throws java.io.IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v2/getAllLocationPayementData")
    public String getAllKinshasaDashboardData_V3(@Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;

        try {
            String typeBien = propertiesConfig.getContent(PropertiesConst.Config.CODE_IMMOBILIER);
            String typeComplementBien = propertiesConfig.getContent(PropertiesConst.Config.TYPE_COMPLEMENT_BIEN_LOCATION_ID);

            reponse = GisBusiness.getAllLocationPayementData(typeComplementBien, typeBien);
        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest("", "v1/getAllLocationPayementData/", reponse);
//        System.out.println(String.format("Opération : getAllLocationPayementData\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @return
     * @throws java.io.IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getService")
    public String getAllSerice(@Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;

        try {

            reponse = DocumentBusiness.getService();

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest("", "v1/getService/", reponse);
//        System.out.println(String.format("Opération : getService\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param service
     * @return
     * @throws java.io.IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getArticle/{service}")
    public String getAbService(@Context HttpHeaders headers, @PathParam("service") String service) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;

        try {

            reponse = DocumentBusiness.getABService(service);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest("", "v1/getArticle/", reponse);
//        System.out.println(String.format("Opération : getArticle\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param file
     * @param headers
     * @return
     * @throws java.io.IOException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/setValidationCode")
    public String setValidationCode(String file, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            JSONObject data = new JSONObject(file);
            Personne personne = new Personne();

            personne.setNif(data.getString("nif"));
            personne.setNom(data.getString("noms"));

            LoginWeb loginWeb = new LoginWeb();

            loginWeb.setMail(data.getString("mail"));
            loginWeb.setTelephone(data.getString("telephone"));

            personne.setLoginWeb(loginWeb);

            JSONObject dataResult = new JSONObject();

            if (loginWeb.getMail() != null || !"".equals(loginWeb.getMail())) {

                String code = Tools.generateRandomValue(5);
                int result = AssujBusiness.sendValidationCode(personne, code);
                if (result == 1) {
                    dataResult.put("code", "100");
                    dataResult.put("code_validation", code);
                } else {
                    dataResult.put("code", "150");
                    dataResult.put("message", "Problème de connexion");
                }

            } else {
                dataResult.put("code", "200");
                dataResult.put("message", "Veuillez configurer l'adresse mail pour ce compte.");
            }

            reponse = dataResult.toString();

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        DocumentBusiness.logRequest(id, "v1/setValidationCode/", reponse);
//        System.out.println(String.format("Opération : setValidationCode\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param file
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/updatePassword")
    public String updatePassword(String file, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            boolean result;

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

            JSONObject data = new JSONObject(file);
            String idUser = data.getString("idUser");
            String newPassword = data.getString("password");
            String email = data.getString("email");
            LoginWeb checkPw = DataAccess.checkLoginWeb(email, newPassword);

            if (checkPw == null) {
//                if (reponse.equals(GeneralConst.NumberString.ONE)) {

                result = DataAccess.updatePassword(idUser, newPassword);

                if (result) {
                    reponse = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    reponse = GeneralConst.ResultCode.FAILED_OPERATION;
                }
//               return reponse;
//                }
            } else {
                reponse = GeneralConst.ResultCode.NOT_EXIST_OPERATION;
            }

        } catch (Exception ex) {
            reponse = GeneralConst.ResultCode.EXCEPTION_OPERATION;
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : updatePassword\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param file
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/taxation")
    public String taxation(String file, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {
            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = AssujBusiness.saveTaxation(file);
//            }
        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        DocumentBusiness.logRequest(id, "v1/taxation/", reponse);
//        System.out.println(String.format("Opération :v1/taxation/\n Requete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param nif
     * @param adress
     * @return
     * @throws java.io.IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("v1/newadress")
    public String addnewAdress(@Context HttpHeaders headers, @FormParam("nif") String nif, @FormParam("adress") String adress) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            String result = DataAccess.saveNewAdress(nif, adress);
            JSONObject jsonResult = new JSONObject();
            if (!result.isEmpty()) {
                List<AdressePersonne> ap = DataAccess.getAdressePersonneById(result);
                jsonResult.put("code", 100);
                jsonResult.put("adressID", result);
                jsonResult.put("message", ap.isEmpty() ? GeneralConst.EMPTY_STRING : ap.get(0).getAdresse().toString());
            } else {
                jsonResult.put("code", 200);
                jsonResult.put("message", "failed");
            }
            reponse = jsonResult.toString();
//            }

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        DocumentBusiness.logRequest(id, "v1/newadress/" + nif, reponse);
//        System.out.println(String.format("Opération : newadress\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param nif
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getTaxation/{nif}")
    public String getTaxation(@Context HttpHeaders headers, @PathParam("nif") String nif) throws IOException {
        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            reponse = AssujBusiness.getTaxation(nif);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest("", "v1/getTaxation/", reponse);
//        System.out.println(String.format("Opération : getTaxation\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param idNc
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getDetailsTaxation/{idNc}")
    public String getDetailsTaxation(@Context HttpHeaders headers, @PathParam("idNc") String idNc) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            reponse = AssujBusiness.getDetailTaxation(idNc);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest("", "v1/getDetailsTaxation/", reponse);
//        System.out.println(String.format("Opération : getMontantPalier\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getEntiteAdministrative")
    public String getEntiteAdministrative(@Context HttpHeaders headers) throws IOException {
        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            reponse = AssujBusiness.getEntiteAdministrative();

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest("", "v1/getEntiteAdministrative/", reponse);
//        System.out.println(String.format("Opération : getEntiteAdministrative\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param nif
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getMontantPalier/{nif}")
    public String getMontantPalier(@Context HttpHeaders headers, @PathParam("nif") String nif) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            reponse = AssujBusiness.getPalier(nif);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        DocumentBusiness.logRequest("", "v1/getMontantPalier/", reponse);
//        System.out.println(String.format("Opération : getMontantPalier\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param objet
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/authenticationMpata")
    public String authenticationMpata(@Context HttpHeaders headers, String objet) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = DocumentBusiness.authenticationMpataProcess(objet);
//            }

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : authenticationMpata\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param objet
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/transfertMpata")
    public String transfertMpata(@Context HttpHeaders headers, String objet) throws IOException {
        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = DocumentBusiness.transfertMpata(objet);
//            }

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : transfertMpata\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/saveRetraitDeclaration")
    public String saveRetrait(String objet, @Context HttpHeaders headers) throws IOException {
        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = checkAutorization(headers, appCode);

//            if (reponse.equals(GeneralConst.NumberString.ONE)) {
            reponse = AssujBusiness.saveRetraitDeclaration(objet);
//            }

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : saveRetraitDeclaration\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getDivision")
    public String getDivision(@Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            reponse = AssujBusiness.getDivisionList();

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getDivision\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param code
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getPrintNoteTaxation/{code}")
    public String getPrintNoteTaxation(@Context HttpHeaders headers, @PathParam("code") String code) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            reponse = AssujBusiness.printNoteTaxation(code);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getPrintNoteTaxation\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param code
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getPrintNotePerception/{code}")
    public String getPrintNotePerception(@Context HttpHeaders headers, @PathParam("code") String code) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            reponse = AssujBusiness.printNotePerception(code);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getPrintNoteTaxation\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param libelle
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getAdresses/{libelle}")
    public String getAdresses(@Context HttpHeaders headers, @PathParam("libelle") String libelle) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            reponse = AssujBusiness.loadAdresses(libelle);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getAdresses\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param type
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getTypeBienByService/{type}")
    public String getTypeBienByService(@Context HttpHeaders headers, @PathParam("type") String type) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            reponse = AssujBusiness.loadTypeBienByService(Integer.valueOf(type));

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getTypeBienByService\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;
    }

    /**
     *
     * @param nom
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getAssujettiByName/{nom}")
    public String getAssujettiByName(@PathParam("nom") String nom, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getLoginWebByNom(nom);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getAssujettiByName\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/setDemandeInscription")
    public String setDemandeInscription(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.setDemandeInscription(objet);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : setDemandeInscription\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param codeTypeBien
     * @param idBien
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getTypeComplementBiens/{codeTypeBien}/{idBien}")
    public String getTypeComplementBiens(
            @PathParam("codeTypeBien") String codeTypeBien,
            @PathParam("idBien") String idBien,
            @Context HttpHeaders headers
    ) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.loadTypeComplementBiens(codeTypeBien, idBien);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getTypeComplementBiens\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param codePersonne
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getAdressePersonne/{codePersonne}")
    public String getAdressePersonne(
            @PathParam("codePersonne") String codePersonne,
            @Context HttpHeaders headers
    ) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getAdressePersonneByPersonne(codePersonne);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getAdressePersonne\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param valueSearch
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getProprietaires/{valueSearch}")
    public String getProprietaires(
            @PathParam("valueSearch") String valueSearch,
            @Context HttpHeaders headers
    ) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.loadPropriétaires(valueSearch);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getAdressePersonne\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/saveBien")
    public String saveBien(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.createBien(objet);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : saveBien\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param nif
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v2/getBiensPersonne/{nif}")
    public String getBiensPersonne(@PathParam("nif") String nif, @Context HttpHeaders headers
    ) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getBiensPersonne(nif);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getBiensPersonne\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param codePersonne
     * @param forLocation
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getBiensPersonne/{codePersonne}/{forLocation}")
    public String getBiensPersonne(@PathParam("codePersonne") String codePersonne,
            @PathParam("forLocation") String forLocation, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.loadBiensPersonne(codePersonne, forLocation);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getBiensPersonne\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/setNewPassword")
    public String setNewPassword(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.setNewPassword(objet);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : setNewPassword\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/setLocation")
    public String createAcquisition(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.setLocation(objet);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : setLocation\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/setPermutation")
    public String setPermutation(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.setPermutation(objet);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : setPermutation\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param idBien
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/deleteBienByCode/{idBien}")
    public String deleteBienByCode(
            @PathParam("idBien") String idBien,
            @Context HttpHeaders headers
    ) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.deleteBienByCode(idBien);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : deleteBienByCode\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getArticleBudgetaireAssujettissable")
    public String getArticleBudgetaireAssujettissable(@Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getArticleBudgetaireAssujettissable();

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getArticleBudgetaireAssujettissable\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param codeBien
     * @param codeAb
     * @param headers
     * @param formeJuridique
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/loadArticlesBudgetaires/{codeBien}/{codeAb}/{formeJuridique}")
    public String loadArticlesBudgetaires(@PathParam("codeBien") String codeBien, @PathParam("codeAb") String codeAb,
            @PathParam("formeJuridique") String formeJuridique, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.loadArticlesBudgetaires(codeBien, codeAb, formeJuridique);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : loadArticlesBudgetaires\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param codeAb
     * @param headers
     * @param codeTarif
     * @param codeBien
     * @param formeJuridique
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/loadPeriodiciteAndTarifs/{codeAb}/{formeJuridique}/{codeTarif}/{codeBien}")
    public String loadPeriodiciteAndTarifs(@PathParam("codeAb") String codeAb,
            @PathParam("formeJuridique") String formeJuridique, @PathParam("codeTarif") String codeTarif,
            @PathParam("codeBien") String codeBien, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.loadPeriodiciteAndTarifs(codeAb, formeJuridique, codeTarif, codeBien);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : loadPeriodiciteAndTarifs\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/generatePeriodesDeclarations")
    public String generatePeriodesDeclarations(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getPeriodesDeclarations(objet);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : generatePeriodesDeclarations\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/saveAssujettissement")
    public String saveAssujettissement(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.createAssujettissement(objet);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : saveAssujettissement\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getListSitesPeage")
    public String getListSitesPeage(@Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getListSitesPeage();

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getListSitesPeage\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getListTarifsGoPass")
    public String getListTarifsGoPass(@Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getListTarifsGoPass();

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getListTarifsPeage\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/saveCommandeGoPass")
    public String saveCommandeGoPass(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.saveCommandeGoPass(objet);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : saveCommandeGoPass\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/saveCommandeGoPassSpontanee")
    public String saveCommandeGoPassSpontanee(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.saveCommandeGoPassSpontanee(objet);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : saveCommandeGoPass\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/saveCommandeCarte")
    public String saveCommandeCarte(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.saveCommandeCarte(objet);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : saveCommandeCarte\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param nif
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getCommandesGoPass/{nif}")
    public String getCommandesGoPass(@PathParam("nif") String nif, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getCommandesGoPass(nif);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getCommandesGoPass\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }
    
    /**
     *
     * @param Param_Object
     * @param headers
     * @return
     * @throws IOException //
     * @throws org.json.JSONException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/GetAllCommandes/")
    public Object GetAllCommandes(String Param_Object, @Context HttpHeaders headers) throws IOException, JSONException {

        String reponse;
        JSONObject request = new JSONObject(Param_Object);
        JsonObject jsonreturnresponse = new JsonObject(); 
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;

        String DateDebut = request.get("DateDebut").toString();
        String DateFin = request.get("DateFin").toString();

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getCommandesGoPassAll(DateDebut,DateFin);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getCommandesGoPass\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }


    /**
     *
     * @param reference
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getCommandesGoPassByReference/{reference}")
    public String getCommandesGoPassByReference(@PathParam("reference") String reference, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getCommandesGoPassByReference(reference);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getCommandesGoPassByReference\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     * /**
     *
     * @param idCmd
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getDetailsGoPassByCommandeId/{id}")
    public String getDetailsGoPassByCommandeId(@PathParam("id") int idCmd, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getDetailsGoPassByCommandeId(idCmd);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getVoucherByCommandeId\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param reference
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/setEtatPayCommande/{reference}")
    public String setEtatPayCommande(@PathParam("reference") String reference, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.setEtatPayCommande(reference);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : setEtatPayCommande\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param nif
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getListDetailVoucher/{nif}")
    public String getListDetailVoucher(@PathParam("nif") String nif, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getListDetailVoucher(nif);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getListDetailVoucher\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param reference
     * @param nif
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/printNotePaiement/{reference}/{nif}")
    public String printNotePaiement(@PathParam("reference") String reference, @PathParam("nif") String nif, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.printNotePaiement(reference, nif);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : printNotePaiement\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/setPreuvePaiementCommande")
    public String setPreuvePaiementCommande(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.setPreuvePaiementCommande(objet);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : setPreuvePaiementCommande\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param nif
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getListComandesCartes/{nif}")
    public String getListComandesCartes(@PathParam("nif") String nif, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getListComandesCartes(nif);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getListComandesCartes\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param objet
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/attributeCarteGopass")
    public String attributeCarteGopass(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.attributeCarteGopass(objet);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : AttributeCarteVoucher\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param nif
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getListCartesGopass/{nif}")
    public String getListCartesGopass(@PathParam("nif") String nif, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getListCartesGopass(nif);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getListCartesVoucher\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param nif
     * @param idCarte
     * @param type
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getGopassCarte/{nif}/{idCarte}/{type}")
    public String getGopassCarte(
            @PathParam("nif") String nif,
            @PathParam("idCarte") int idCarte,
            @PathParam("type") int type,
            @Context HttpHeaders headers
    ) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getGoPassCarte(nif, idCarte, type);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getVouchersCarte\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getDashboard/{dateDebut}/{dateFin}/{nif}")
    public String getDashboard(@Context HttpHeaders headers, @PathParam("dateDebut") String dateDebut,
            @PathParam("dateFin") String dateFin, @PathParam("nif") String nif) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            //reponse = checkAutorization(headers, appCode);
            reponse = AssujBusiness.getDataDashBoard(dateDebut, dateFin, nif);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getDashboard\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getDataDashBoardGopass/{dateDebut}/{dateFin}/{nif}/{isDateFiltre}")
    public String getDataDashBoardGopass(
            @Context HttpHeaders headers,
            @PathParam("dateDebut") String dateDebut,
            @PathParam("dateFin") String dateFin,
            @PathParam("nif") String nif,
            @PathParam("isDateFiltre") int isDateFiltre
    ) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            //reponse = checkAutorization(headers, appCode);
            reponse = AssujBusiness.getDataDashBoardGopass(dateDebut, dateFin, nif, isDateFiltre);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getDashboard\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    /**
     *
     * @param headers
     * @param fkAxe
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getListSiteAxePeage/{fkAxe}")
    public String getListSiteAxePeage(
            @Context HttpHeaders headers, @PathParam("fkAxe") int fkAxe) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            //reponse = checkAutorization(headers, appCode);
            reponse = AssujBusiness.getListSiteAxePeage(fkAxe);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }
        return reponse;
    }

    /**
     *
     * @param headers
     * @param idCmd
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getDetailCommandeVoucher/{idCmd}")
    public String getDetailCommandeVoucher(
            @Context HttpHeaders headers, @PathParam("idCmd") int idCmd) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = checkIDHeader(headers);

        try {

            String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            //reponse = checkAutorization(headers, appCode);
            reponse = AssujBusiness.getDetailCommandeVoucher(idCmd);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getDetailCommandeVoucher\nRequete du client N° %s.\nRéponse : \n %s", "", reponse));
        return reponse;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/updateUserInfos")
    public String updateUserInfos(String objet, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.updateUserInfos(objet);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : updateUserInfos\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     *
     * @param code
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/verifyNoteTaxation/{reference}")
    public String verifyNoteTaxation(@Context HttpHeaders headers, @PathParam("reference") String code) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.verifyNoteTaxation(code);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        return reponse;
    }

    /**
     *
     * @param headers
     * @param reference
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/verifyCommandeByReference/{reference}")
    public String verifyCommandeByReference(@Context HttpHeaders headers, @PathParam("reference") String reference) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.verifyCommandeByReference(reference);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        return reponse;
    }

    /**
     *
     * @param file
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/savePaiementCommandeVoucher")
    public String savePaiementCommandeVoucher(String file, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.savePaiementCommandeVoucher(file);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        return reponse;
    }

    /**
     *
     * @param file
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v2/saveDeclaration")
    public String saveDeclaration(String file, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.saveDeclaration(file);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        return reponse;
    }

    /**
     *
     * @param headers
     * @param reference
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/generateVoucher/{reference}")
    public String generateVoucher(@Context HttpHeaders headers, @PathParam("reference") String reference) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.generateVoucher(reference);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        return reponse;
    }

    /**
     *
     * @param headers
     * @param nif
     * @param typeBien
     * @param dateDebut
     * @param dateFin
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/rechercheAvanceeBien/{nif}/{typeBien}/{dateDebut}/{dateFin}")
    public String rechercheAvanceeBien(@Context HttpHeaders headers, @PathParam("nif") String nif,
            @PathParam("typeBien") String typeBien, @PathParam("dateDebut") String dateDebut,
            @PathParam("dateFin") String dateFin) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            reponse = AssujBusiness.rechercheAvanceeBien(nif, typeBien, dateDebut, dateFin);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        return reponse;
    }

    /**
     *
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getTypeBien")
    public String getTypeBien(@Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            reponse = AssujBusiness.getTypeBien();

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        return reponse;
    }

    /**
     *
     * @param headers
     * @param reference
     * @param montant
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/generateVouchers/{reference}/{montant}")
    public String generateVouchersWithDirectSwift(@Context HttpHeaders headers,
            @PathParam("reference") String reference, @PathParam("montant") double montant) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            reponse = AssujBusiness.generateVouchersWithDirectSwift(reference, montant);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        return reponse;
    }

    /**
     *
     * @param headers
     * @param code
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/activateAccount/{id}")
    public String activateAccount(@Context HttpHeaders headers, @PathParam("id") String code) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            reponse = AssujBusiness.activateAccount(code);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        return reponse;
    }

    /**
     * /**
     *
     * @param nif
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getGoPassBlancs/{nif}")
    public String getGoPassBlancs(@PathParam("nif") String nif, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getGoPassBlancs(nif);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getVoucherByCommandeId\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    /**
     * /**
     *
     * @param Param_Object
     * @param headers
     * @return
     * @throws IOException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/getGoPassBlancsByCommande/")
    public String getGoPassBlancsByCommande(String Param_Object, @Context HttpHeaders headers) throws IOException, JSONException {

        String reponse;
        JSONObject request = new JSONObject(Param_Object);
        String commande = request.get("commande").toString();
        
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            //String appCode = propertiesConfig.getContent(PropertiesConst.Config.APP_CODE);
            reponse = AssujBusiness.getGoPassBlancsByCommande(commande);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

//        System.out.println(String.format("Opération : getVoucherByCommandeId\nRequete du client N° %s.\nRéponse : \n %s", id, reponse));
        return reponse;

    }

    
    /**
     * /**
     *
     * @param nif
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("v1/getGoPassByCode/{reference}")
    public String getGoPassByCode(@PathParam("reference") String reference, @Context HttpHeaders headers) throws IOException {

        String reponse;
        String lang = checkLangHeader(headers);
        String format = GeneralConst.Format_Response.JSON;
        String id = "0";

        try {

            reponse = AssujBusiness.getGoPassByCode(reference);

        } catch (Exception ex) {
            reponse = responseMessage.getGeneralException(lang, format);
            System.out.println(ex.toString());
        }

        return reponse;

    }

    /**
     *
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/getaeroportList/")
    public Object getaeroportList(@Context HttpHeaders headers) throws IOException {

        String jsonreturnresponse = "0";
        List<Aeroport> listaeroport = new ArrayList<>();
        GenericBusiness compagnie = new GenericBusiness();
        List<JsonObject> listAeroportJson = new ArrayList<>();
        try {
            listaeroport = compagnie.getAeroportList();
            JsonObject jsonListAeroports;
            if (!listaeroport.isEmpty()) {
                for (Aeroport ListAeroport : listaeroport) {
                    jsonListAeroports = new JsonObject();
                    jsonListAeroports.addProperty("IdAeroport", ListAeroport.getIdAeroport());
                    jsonListAeroports.addProperty("Code", ListAeroport.getCodeAeroport());
                    jsonListAeroports.addProperty("Intitule", ListAeroport.getIntitule());
                    jsonListAeroports.addProperty("VilleCode", ListAeroport.getFkEntite().getIdEntite());
                    jsonListAeroports.addProperty("VilleIntitule", ListAeroport.getFkEntite().getIntituleEntite());
                    if (ListAeroport.getFkEntite().getFkEntiteMere().getFkTypeEntite().getIdTypeEntite().equals("TEA00000000022019")) {
                        jsonListAeroports.addProperty("ProvinceCode", ListAeroport.getFkEntite().getFkEntiteMere().getIdEntite());
                        jsonListAeroports.addProperty("ProvinceIntitule", ListAeroport.getFkEntite().getFkEntiteMere().getIntituleEntite());
                        jsonListAeroports.addProperty("PaysCode", ListAeroport.getFkEntite().getFkEntiteMere().getFkEntiteMere().getIdEntite());
                        jsonListAeroports.addProperty("PaysIntitule", ListAeroport.getFkEntite().getFkEntiteMere().getFkEntiteMere().getIntituleEntite());
                        jsonListAeroports.addProperty("ContinentCode", ListAeroport.getFkEntite().getFkEntiteMere().getFkEntiteMere().getFkEntiteMere().getIdEntite());
                        jsonListAeroports.addProperty("ContinentIntitule", ListAeroport.getFkEntite().getFkEntiteMere().getFkEntiteMere().getFkEntiteMere().getIntituleEntite());
                    } else if (ListAeroport.getFkEntite().getFkEntiteMere().getFkTypeEntite().getIdTypeEntite().equals("TEA00000000012019")) {
                        jsonListAeroports.addProperty("ProvinceCode", "");
                        jsonListAeroports.addProperty("ProvinceIntitule", "");
                        jsonListAeroports.addProperty("PaysCode", ListAeroport.getFkEntite().getFkEntiteMere().getIdEntite());
                        jsonListAeroports.addProperty("PaysIntitule", ListAeroport.getFkEntite().getFkEntiteMere().getIntituleEntite());
                        jsonListAeroports.addProperty("ContinentCode", ListAeroport.getFkEntite().getFkEntiteMere().getFkEntiteMere().getIdEntite());
                        jsonListAeroports.addProperty("ContinentIntitule", ListAeroport.getFkEntite().getFkEntiteMere().getFkEntiteMere().getIntituleEntite());
                    }

                    listAeroportJson.add(jsonListAeroports);
                }
                jsonreturnresponse = listAeroportJson.toString();
            }

        } catch (Exception ex) {
            jsonreturnresponse = "-1";
        }

        return jsonreturnresponse;
    }

    /**
     *
     * @param Param_Object
     * @param headers
     * @return
     * @throws IOException //
     * @throws org.json.JSONException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/savecompagnie/")
    public Object savecompagnie(String Param_Object, @Context HttpHeaders headers) throws IOException, JSONException {

        JSONObject request = new JSONObject(Param_Object);
        String jsonreturnresponse = "";
        String PathSinglePhoto = "";

        String intitule = request.get("intitule").toString();
        String sigle = request.get("sigle").toString();
        String pathPhoto = request.get("pathPhoto").toString();
        String ExtPhoto = request.get("ExtPhoto").toString();
        String AgentCreat = request.get("AgentCreat").toString();
        GenericBusiness compagnie = new GenericBusiness();

        PathSinglePhoto = Tools.saveFile(pathPhoto, 1, AgentCreat);

        Compagnie param = new Compagnie();
        param.setIntitule(intitule);
        param.setSigle(sigle);
        param.setLogo(PathSinglePhoto);

        jsonreturnresponse = compagnie.savecompagnieBusiness(param);
        return jsonreturnresponse;
    }

    /**
     *
     * @param ab
     * @param headers
     * @return
     * @throws IOException
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("v1/getcompagnieList/")
    public Object getcompagnieList(@Context HttpHeaders headers) throws IOException {

        String jsonreturnresponse = "0";
        List<Compagnie> listCompagnie = new ArrayList<>();
        GenericBusiness compagnie = new GenericBusiness();
        List<JsonObject> listCompagnieJson = new ArrayList<>();
        try {
            listCompagnie = compagnie.getCompagnieList();
            JsonObject jsonListCompagnies;
            if (!listCompagnie.isEmpty()) {
                for (Compagnie ListCompagnie : listCompagnie) {
                    jsonListCompagnies = new JsonObject();
                    jsonListCompagnies.addProperty("IdCompagnie", ListCompagnie.getIdCompagnie());
                    jsonListCompagnies.addProperty("CodeAita", ListCompagnie.getCodeAita());
                    jsonListCompagnies.addProperty("CodeOaci", ListCompagnie.getCodeOaci());
                    jsonListCompagnies.addProperty("Intitule", ListCompagnie.getIntitule());
                    jsonListCompagnies.addProperty("Sigle", ListCompagnie.getSigle());
                    jsonListCompagnies.addProperty("Logo", Tools.encoderAll(ListCompagnie.getLogo(), "jpg"));
                    listCompagnieJson.add(jsonListCompagnies);
                }
                jsonreturnresponse = listCompagnieJson.toString();
            }

        } catch (Exception ex) {
            jsonreturnresponse = "-1";
        }

        return jsonreturnresponse;
    }

    /**
     *
     * @param Param_Object
     * @param headers
     * @return
     * @throws IOException //
     * @throws org.json.JSONException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/activegopass/")
    public Object activegopass(String Param_Object, @Context HttpHeaders headers) throws IOException, JSONException, ParseException {

        JSONObject request = new JSONObject(Param_Object);
        String jsonreturnresponse = "";
        String PathSinglePhoto = "";

        String CodeGoPass = request.get("CodeGoPass").toString();
        String Passager = request.get("Passager").toString();
        String SexePassager = request.get("SexePassager").toString();
        String FkCompagnie = request.get("FkCompagnie").toString();
        String AgentCreat = request.get("AgentCreat").toString();
        String DateVol = request.get("DateVol").toString();
        String NumeroVol = request.get("NumeroVol").toString();
        String FkAeroportProvenance = request.get("FkAeroportProvenance").toString();
        String FkAeroportDestination = request.get("FkAeroportDestination").toString();
        GenericBusiness detailGoPass = new GenericBusiness();

        Compagnie compagnie = new Compagnie(Integer.parseInt(FkCompagnie));
        Aeroport aeroport_provenance = new Aeroport(Integer.parseInt(FkAeroportProvenance));
        Aeroport aeroport_destination = new Aeroport(Integer.parseInt(FkAeroportDestination));

        DetailsGoPass param = new DetailsGoPass();
        param.setCodeGoPass(CodeGoPass);
        param.setPassager(Passager);
        param.setSexePassager(SexePassager);
        param.setDateVol(DateVol);
        param.setNumeroVol(NumeroVol);
        param.setFkCompagnie(compagnie);
        param.setFkAeroportProvenance(aeroport_provenance);
        param.setFkAeroportDestination(aeroport_destination);

        jsonreturnresponse = detailGoPass.activeGoPassBusiness(param);
        return jsonreturnresponse;
    }

    /**
     *
     * @param Param_Object
     * @param headers
     * @return
     * @throws IOException //
     * @throws org.json.JSONException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/sendMail/")
    public Object sendMail(String Param_Object, @Context HttpHeaders headers) throws IOException, JSONException {

        JSONObject request = new JSONObject(Param_Object);
        String jsonreturnresponse = "";
        GenericBusiness formatMail = new GenericBusiness();

        String Paragraph1 = request.get("Paragraph1").toString();
        String Paragraph2 = request.get("Paragraph2").toString();
        String MailDestinataire = request.get("MailDestinataire").toString();
        String HeaderMessage = request.get("HeaderMessage").toString();
        String ObjectMessage = request.get("ObjectMessage").toString();

        formatMail.sendNotification(formatMail.FormatMail(ObjectMessage, Paragraph1, Paragraph2), HeaderMessage, MailDestinataire);
        jsonreturnresponse = "1";
        return jsonreturnresponse;
    }

    /**
     *
     * @param Param_Object
     * @param headers
     * @return
     * @throws IOException //
     * @throws org.json.JSONException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/GetDashBoardReporting/")
    public Object GetDashBoardReporting(String Param_Object, @Context HttpHeaders headers) throws IOException, JSONException {

        JSONObject request = new JSONObject(Param_Object);
        JsonObject jsonreturnresponse = new JsonObject(); 

        String DateDebut = request.get("DateDebut").toString();
        String DateFin = request.get("DateFin").toString();
        String Compagnnie = request.get("Compagnie").toString();
        String Aeroport = request.get("Aeroport").toString();

        GenericBusiness dashboard = new GenericBusiness();
        JSONObject param = new JSONObject();
        param.put("DateDebut",DateDebut);
        param.put("DateFin",DateFin);
        param.put("Compagnie",Compagnnie);
        param.put("Aeroport",Aeroport);
        jsonreturnresponse.addProperty("Aeroport",dashboard.GetDashBoard(param));
        jsonreturnresponse.addProperty("Compagnie", dashboard.GetDashBoardCompagnie(param));
        return jsonreturnresponse.toString();
    }
    
    /**
     *
     * @param Param_Object
     * @param headers
     * @return
     * @throws IOException //
     * @throws org.json.JSONException
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("v1/GetVoyageurList/")
    public Object GetVoyageurList(String Param_Object, @Context HttpHeaders headers) throws IOException, JSONException {

        JSONObject request = new JSONObject(Param_Object);
        JsonObject jsonreturnresponse = new JsonObject(); 

        String DateDebut = request.get("DateDebut").toString();
        String DateFin = request.get("DateFin").toString();
        String Compagnnie = request.get("Compagnie").toString();
        String Aeroport = request.get("Aeroport").toString();

        GenericBusiness dashboard = new GenericBusiness();
        JSONObject param = new JSONObject();
        param.put("DateDebut",DateDebut);
        param.put("DateFin",DateFin);
        param.put("Compagnie",Compagnnie);
        param.put("Aeroport",Aeroport);
        jsonreturnresponse.addProperty("Voyageur",dashboard.GetVoyageurList(param));
        return jsonreturnresponse.toString();
    }

}
