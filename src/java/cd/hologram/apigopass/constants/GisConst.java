/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.constants;

/**
 *
 * @author juslin.tshiamua
 */
public class GisConst {

    public class paramName {

        public static final String CITY_ID = "cityId";
        public static final String CITY_NAME = "cityName";
        public static final String DISTRICT_ID = "districtId";
        public static final String DISTRICT_NAME = "districtName";
        public static final String COMMUNE_ID = "communeId";
        public static final String COMMUNE_NAME = "communeName";
        public static final String QUARTER_ID = "quarterId";
        public static final String QUARTER_NAME = "quarterName";
        public static final String AVENUE_ID = "avenueId";
        public static final String AVENUE_NAME = "avenueName";
        public static final String CITY_LIST = "cityList";
        public static final String DISTRICT_LIST = "districtList";
        public static final String COMMUNE_LIST = "communeList";
        public static final String QUARTER_LIST = "quarterList";
        public static final String AVENUE_LIST = "avenueList";
    }
    
    public class status {
        public static final String ERROR_CODE = "300";
        public static final String EMPTY_CODE = "200";
        public static final String SUCCESS_CODE = "100";
        public static final String ERROR_MESSAGE = "Error:";
        public static final String EMPTY_MESSAGE = "Not found";
        public static final String SUCCESS_MESSAGE = "Success";
    }
    
    public class ComplementBien{
        public static final String BUILT_AREA = "00000000000000032015";
        public static final String MONTANT_LOYER = "00000000000000072015";
        public static final String OPEN_SPACE = "00000000000000102015";
        public static final String MONTANT_LOYER_ANNUEL = "00000000000001472015";
        public static final String LATTITUDE = "00000000000001652015";
        public static final String LONGITUDE = "00000000000001662015";
    }
}
