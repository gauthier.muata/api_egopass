/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.constants;

/**
 *
 * @author moussa.toure
 */
public class PropertiesConst {

    public class Config {

        public static final String PROPERTIES_FILE_PATH = "/cd/hologram/apigopass/properties/CONFIG.properties";

        public static final String SERVER_PORT = "SERVER_PORT";
        public static final String IP_SMTP = "IP_SMTP";
        public static final String MAIL_SMTP = "MAIL_SMTP";
        public static final String PASSWORD_ADRESS = "PASSWORD_ADRESS";
        public static final String USE_ONLINE_MAIL = "USE_ONLINE_MAIL";
        public static final String SMS_SERVICE = "SMS_SERVICE";
        public static final String SMS_URL = "SMS_URL";
        public static final String SMS_URL2 = "SMS_URL2";
        
        public static final String SMS_SUBSCRIBE = "SMS_SUBSCRIBE";
        public static final String MAILJET_USERNAME = "MAILJET_USERNAME";
        public static final String MAILJET_PASSWORD = "MAILJET_PASSWORD";
        public static final String MAILJET_URL = "MAILJET_URL";
        public static final String MAILJET_MAIL_BODY = "MAILJET_MAIL_BODY";

        public static final String MAIL_SUBSCRIBE_CLIENT_SUBJECT = "MAIL_SUBSCRIBE_CLIENT_SUBJECT";
        public static final String MAIL_SUBSCRIBE_CLIENT_CONTENT = "MAIL_SUBSCRIBE_CLIENT_CONTENT";
        public static final String ERECETTES_LINK = "ERECETTES_LINK";

        public static final String APP_CODE = "APP_CODE";

        public static final String MODE_PAIEMENT = "MODE_PAIEMENT";
        public static final String CODE_AGENT_SERVICE_WEB = "CODE_AGENT_SERVICE_WEB";
        public static final String URL_HICONNECT_LOGIN = "URL_HICONNECT_LOGIN";

        public static final String CITY_CODE = "CITY_CODE";
        public static final String DISTRICT_CODE = "DISTRICT_CODE";
        public static final String COMMUNE_CODE = "COMMUNE_CODE";
        public static final String FILTER_BY_VILLE = "FILTER_BY_VILLE";
        public static final String FILTER_BY_DISTRICT = "FILTER_BY_DISTRICT";
        public static final String FILTER_BY_COMMUNE = "FILTER_BY_COMMUNE";

        public static final String CODE_IMMOBILIER = "CODE_IMMOBILIER";
        public static final String SUPERFICIE_BATIE = "SUPERFICIE_BATIE";

        public static final String CONFIG_TYPE_COMPLEMENT_BIEN = "CONFIG_TYPE_COMPLEMENT_BIEN";
        public static final String CONFIG_KEYS_COMPLEMENTS = "CONFIG_KEYS_COMPLEMENTS";

        public static final String BUILT_AREA = "BUILT_AREA";
        public static final String SUPERFICIE_NON_BATIE = "SUPERFICIE_NON_BATIE";
        public static final String NOMBRE_ETAGE = "NOMBRE_ETAGE";
        public static final String NOMBRE_APPARTEMENT = "NOMBRE_APPARTEMENT";
        public static final String TYPE_MAISON = "TYPE_MAISON";
        public static final String TYPE_COMPLEMENT_BIEN_LOCATION_ID = "TYPE_COMPLEMENT_BIEN_LOCATION_ID";
        public static final String CODE_SITE = "CODE_SITE";
        public static final String CODE_PROVINCE_HK = "CODE_PROVINCE_HK";
        public static final String BANQUE_TELE_DEC = "BANQUE_TELE_DEC";
        public static final String COMPTE_BANCAIRE_TELE_DEC = "COMPTE_BANCAIRE_TELE_DEC";
        public static final String CODE_SITE_TELE_DECLARATION = "CODE_SITE_TELE_DECLARATION";
        
        public final static String MPATA_APPLICATION = "MPATA_APPLICATION";
        public final static String MPATA_PASSWORD = "MPATA_PASSWORD";
        public final static String MPATA_USERNAME = "MPATA_USERNAME";
        public final static String E_MPATA_URL_AUTH = "E_MPATA_URL_AUTH";
        public final static String E_MPATA_URL_CHECK_PIN = "E_MPATA_URL_CHECK_PIN";
        public final static String E_MPATA_URL_TRANSACTION = "E_MPATA_URL_TRANSACTION";
        
        public final static String TAUX_ICM = "TAUX_ICM";
        public final static String PERIODE_GRACE_EN_COURS = "PERIODE_GRACE_EN_COURS";
        
        public final static String CODE_IMPOT_VIGNETTE = "CODE_IMPOT_VIGNETTE";
        public final static String CODE_DIVISION = "CODE_DIVISION";
        
        public final static String IMPOT_ASSUJETTISSEMENT = "IMPOT_ASSUJETTISSEMENT";
    }

    public class Mail {

        public static final String PROPERTIES_FILE_PATH = "/cd/hologram/apigopass/properties/FR_MAIL.properties";
        public static final String MAIL_CODE_VALIDATION_FR = "MAIL_CODE_VALIDATION_FR";
        public static final String MAIL_CODE_VALIDATION_EN = "MAIL_CODE_VALIDATION_EN";
        public static final String MAIL_CODE_VALIDATION_FR_SUBJECT = "MAIL_CODE_VALIDATION_FR_SUBJECT";
        public static final String MAIL_CODE_VALIDATION_TITLE = "MAIL_CODE_VALIDATION_TITLE";
        public static final String MAIL_CODE_VALIDATION = "MAIL_CODE_VALIDATION";

    }

    public class Message {

        public static final String PROPERTIES_FILE_PATH = "/cd/hologram/apigopass/properties/FR_Message.properties";

        public static final String NO_HEADER_CODE = "NO_HEADER_CODE";
        public static final String NO_HEADER_MESSAGE_FR = "NO_HEADER_MESSAGE_FR";
        public static final String NO_HEADER_MESSAGE_EN = "NO_HEADER_MESSAGE_EN";

        public static final String TOKEN_IS_INVALID_CODE = "TOKEN_IS_INVALID_CODE";
        public static final String TOKEN_IS_INVALID_MESSAGE_FR = "TOKEN_IS_INVALID_MESSAGE_FR";
        public static final String TOKEN_IS_INVALID_MESSAGE_EN = "TOKEN_IS_INVALID_MESSAGE_EN";

        public static final String ERROR_FR = "ERROR_FR";
        public static final String ERROR_EN = "ERROR_EN";

        public static final String SUCCESS_FR = "SUCCESS_FR";
        public static final String SUCCESS_EN = "SUCCESS_EN";

        public static final String EXCEPTION_CODE = "EXCEPTION_CODE";
        public static final String EXCEPTION_MESSAGE_FR = "EXCEPTION_MESSAGE_FR";
        public static final String EXCEPTION_MESSAGE_EN = "EXCEPTION_MESSAGE_EN";

        public static final String DOCUMENT_NOT_FOUND_CODE = "DOCUMENT_NOT_FOUND_CODE";
        public static final String DOCUMENT_NOT_FOUND_MESSAGE_FR = "DOCUMENT_NOT_FOUND_MESSAGE_FR";
        public static final String DOCUMENT_NOT_FOUND_MESSAGE_EN = "DOCUMENT_NOT_FOUND_MESSAGE_EN";

        public static final String BODY_ERROR_CODE = "BODY_ERROR_CODE";
        public static final String BODY_ERROR_MESSAGE_1_FR = "BODY_ERROR_MESSAGE_1_FR";
        public static final String BODY_ERROR_MESSAGE_1_EN = "BODY_ERROR_MESSAGE_1_EN";
        public static final String BODY_ERROR_MESSAGE_2_FR = "BODY_ERROR_MESSAGE_2_FR";
        public static final String BODY_ERROR_MESSAGE_2_EN = "BODY_ERROR_MESSAGE_2_EN";

        public static final String SAVE_PAYMENT_OK_CODE = "SAVE_PAYMENT_OK_CODE";
        public static final String SAVE_PAYEMENT_KO_CODE = "SAVE_PAYEMENT_KO_CODE";

        public static final String SAVE_PAYMENT_OK_FR = "SAVE_PAYMENT_OK_FR";
        public static final String SAVE_PAYMENT_OK_EN = "SAVE_PAYMENT_OK_EN";
        public static final String SAVE_PAYMENT_KO_FR = "SAVE_PAYMENT_KO_FR";
        public static final String SAVE_PAYMENT_KO_EN = "SAVE_PAYMENT_KO_EN";

        public static final String BANK_ERROR_CODE = "BANK_ERROR_CODE";
        public static final String BANK_ACCOUNT_ERROR_FR = "BANK_ACCOUNT_ERROR_FR";
        public static final String BANK_ACCOUNT_ERROR_EN = "BANK_ACCOUNT_ERROR_EN";

        public static final String BANK_REF_NUMEBER_CODE = "BANK_REF_NUMEBER_CODE";
        public static final String BANK_REF_NUMEBER_ERROR_FR = "BANK_REF_NUMEBER_ERROR_FR";
        public static final String BANK_REF_NUMEBER_ERROR_EN = "BANK_REF_NUMEBER_ERROR_EN";

    }

}
