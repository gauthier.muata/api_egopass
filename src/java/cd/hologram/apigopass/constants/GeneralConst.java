/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.constants;

/**
 *
 * @author gauthier.muata
 */
public class GeneralConst {

    public static String EMPTY_STRING = "";
    public static String EMPTY_NULL = "null";
    public static String SPACE = " ";
    public static String BRAKET_OPEN = "(";
    public static String BRAKET_CLOSE = ")";
    public static String SEPARATOR_SLASH = " / ";
    public static String SEPARATOR_SLASH_NO_SPACE = "/";
    public static String OPERATION = "operation";
    public static final String COMMA = ";";
    public static String DASH_SEPARATOR = "-";
    public static String DASH_SEPARATOR_2 = "_";
    public static String VIRGULE = ",";
    public static String DOT = ".";
    public static int EMPTY_ZERO = 0;
    public static String SPACE_HTML = "&nbsp;";
    public static String TWO_SPACE_HTML = "&nbsp;&nbsp;";
    public static String TWO_POINTS = " : ";
    public static String NON_DEFINI = "non defini";

    public static final String ALL = "*";

    public static String KEY_SESSION_USER = "emairie_user";

    public static final String DATA_IMAGE_JPEG_BASE64_TEXT = "data:image/jpeg;base64,";
    public static final String BASE64_TEXT = "base64";

    public final static String PERSONNE_NAME_STEP_DYNAMIC = "Personne (%s)";

    public final static String DEFAULT_ANCHOR_PERSONNE_GENERATE = "ListePersonneGeneree";

    public final static String PERSONNE_GENERATE_FORMAT = "<p> %s. %s de sexe %s né(e) le %s à %s <p/>";

    public final static String IS_DEAD_TEXT = " (est décedée)";

    public static String PREFIX_IDENTIFIANT_KEY = "IDENT_";

    public static String TEXT_SELECTED_VALUE = "-- S&eacute;lectionner --";

    public final static String COMPLEMENT_LIST = "<div class=\"form-group row\">\n"
            + " <label class=\"col-lg-3 col-form-label\" for=\"%s\">%s %s</label>\n"
            + " <div class=\"col-lg-8\">\n"
            + " <select class=\"form-control\" id=\"%s\" placeholder=\"%s\" name=\"%s\">%s</select>\n"
            + " </div>\n"
            + " </div>";

    public final static String INPUT_CONTROL = "<div class=\"form-group row\">\n"
            + " <label class=\"col-lg-3 control-label\" for=\"%s\">%s %s</label>\n"
            + " <div class=\"col-lg-8\">\n"
            + " <input type=\"%s\" class=\"form-control\" id=\"%s\" placeholder=\"%s\" name=\"%s\" value=\"%s\"/>\n"
            + " </div>\n"
            + " </div>";

    public final static String BOX_CONTROL = "<div class=\"form-group row\">\n"
            + " <label class=\"col-lg-3 control-label\" for=\"%s\">%s %s</label>\n"
            + " <div class=\"col-lg-5\">\n"
            + " <input type=\"%s\" class=\"form-control\" id=\"%s\" placeholder=\"%s\" name=\"%s\" value=\"%s\"/>\n"
            + " </div>\n"
            + " <div class=\"col-lg-3\">\n"
            + " <select class=\"form-control\" id=\"%s\" placeholder=\"\" name=\"\">%s</select>\n"
            + " </div>\n"
            + " </div>";

    public class Operation {

    }

    public class ResultCode {

        public final static String SUCCES_OPERATION = "1";
        public final static String FAILED_OPERATION = "0";
        public final static String EXCEPTION_OPERATION = "-1";
        public final static String EXIST_OPERATION = "6";
        public final static String NOT_EXIST_OPERATION = "-6";
        public final static String INVALID_PASSWORD = "3";
        public final static String EMAIL_ADRESS_EXIST = "2";

        public final static String PHONE_NUMBER_EXIST = "4";
        public final static String DOCUMENT_HTML_NOT_EXISTS = "2";

        public final static String USER_NOT_AUTORISE = "10";
        public final static String IVALID_CODE = "5";
        public final static String GOPASS_NOT_GENERATED = "-2";
        public final static String CMD_PAYED = "2";

    }

    public class NumberString {

        public static final String ZERO = "0";
        public static final String ONE = "1";
        public static final String TWO = "2";
        public static final String THREE = "3";
        public static final String FOUR = "4";
        public static final String FIVE = "5";
        public static final String MOINS_UN = "-1";
    }

    public class NumberNumeric {

        public static final int ZERO = 0;
        public static final int ONE = 1;
        public static final int TWO = 2;
        public static final int THREE = 3;
        public static final int FOUR = 4;
        public static final int FIVE = 5;
        public static final int SIX = 6;
        public static final int SEVEN = 7;
        public static final int HEIGHT = 8;
        public static final int NINE = 9;
        public static final int TEN = 10;
        public static final int ELEVEN = 11;
        public static final int CENT = 100;
        public static final int MOINS_UN = -1;

    }

    public class FormatDate {

        public final static String FORMAT_DMY = "dd/MM/yyyy";
        public final static String FORMAT_DMY_V2 = "dd-MM-yyyy";

        public final static String FORMAT_YMD = "yyyy/MM/dd";
        public final static String FORMAT_YMD_V2 = "yyyy-MM-dd";

        public final static String FORMAT_DMY_NO_SEPARATOR = "ddMMyyyy";
        public final static String FORMAT_YDM_NO_SEPARATOR = "yyyyMMdd";

        public final static String FORMAT_DMY_HOUR = "dd/MM/yyyy HH:mm:ss";
        public final static String FORMAT_DMY_HOUR_V1 = "dd/MM/yyyy à HH:mm:ss";

        public final static String FORMAT_YMD_WITH_HOUR = "yyyy-MM-dd HH:mm:ss";

    }

    public class ParamName {

        public final static String id = "id";
        public final static String token = "token";

        public final static String etat = "etat";
        public final static String firstName = "firstname";
        public final static String lastname = "lastname";
        public final static String middleName = "middlename";
        public final static String birthday = "birthdate";
        public final static String phoneNumber = "phonenumber";
        public final static String modeSearch = "modesearch";
        public final static String fullname = "fullname";
        public final static String email = "email";
        public final static String nif = "nif";
        public final static String ntd = "ntd";
        public final static String type = "type";
        public final static String LibelleType = "Libelletype";
        public final static String Libelle = "Libelle";
        public final static String LibelleAdresse = "LibelleAdresse";
        public final static String AdresseList = "AdresseList";
        public final static String BienList = "BienList";
        public final static String PeriodeList = "PeriodeList";

        public final static String periode = "periode";
        public final static String ab = "ab";
        public final static String Assuj = "Assujetti";
        public final static String SiteList = "SiteList";
    }

    public class CalendarMonth {

        public final static String JANVIER_CODE_1 = "1";
        public final static String JANVIER_CODE_2 = "01";
        public final static String JANVIER_NAME = "Janvier";

        public final static String FEVRIER_CODE_1 = "2";
        public final static String FEVRIER_CODE_2 = "02";
        public final static String FEVRIER_NAME = "Février";

        public final static String MARS_CODE_1 = "3";
        public final static String MARS_CODE_2 = "03";
        public final static String MARS_NAME = "Mars";

        public final static String AVRIL_CODE_1 = "4";
        public final static String AVRIL_CODE_2 = "04";
        public final static String AVRIL_NAME = "Avril";

        public final static String MAI_CODE_1 = "5";
        public final static String MAI_CODE_2 = "05";
        public final static String MAI_NAME = "Mai";

        public final static String JUIN_CODE_1 = "6";
        public final static String JUIN_CODE_2 = "06";
        public final static String JUIN_NAME = "Juin";

        public final static String JUILLET_CODE_1 = "7";
        public final static String JUILLET_CODE_2 = "07";
        public final static String JUILLET_NAME = "Juillet";

        public final static String AOUT_CODE_1 = "8";
        public final static String AOUT_CODE_2 = "08";
        public final static String AOUT_NAME = "Août";

        public final static String SEPTEMBRE_CODE_1 = "9";
        public final static String SEPTEMBRE_CODE_2 = "09";
        public final static String SEPTEMBRE_NAME = "Septembre";

        public final static String OCTOBRE_CODE = "10";
        public final static String OCTOBRE_NAME = "Octobre";

        public final static String NOVEMEBRE_CODE = "11";
        public final static String NOVEMEBRE_NAME = "Novembre";

        public final static String DECMEBRE_CODE = "12";
        public final static String DECMEBRE_NAME = "Décembre";
    }

    public class Devise {

        public final static String DEVISE_CDF = "CDF";
        public final static String DEVISE_USD = "USD";
    }

    public class LogParam {

        public final static String MAC_ADRESSE = "MAC_ADRESSE";
        public final static String IP_ADRESSE = "IP_ADRESSE";
        public final static String HOST_NAME = "HOST_NAME";
        public final static String FK_EVENEMENT = "FK_EVENEMENT";
        public final static String CONTEXT_BROWSER = "CONTEXT_BROWSER";
    }

    public class Boolean {

        public final static String TRUE = "true";
        public final static String FALSE = "false";

    }

    public class TablesNames {

    }

    public class SearchCode {

    }

    public class Module {

    }

    public class FormatComplement {

        public final static String PERSONNE_FORMAT = "Personne";
        public final static String STRING_FORMAT = "String";
        public final static String TEXT_FORMAT = "Text";
        public final static String INTEGER = "Integer";
        public final static String DATE_FORMAT = "Date";
        public final static String TIME_FORMAT = "Time";
        public final static String DATE_TIME_FORMAT = "DateTime";
        public final static String COLOR_FORMAT = "Color";
        public final static String PICTURE_FORMAT = "Picture";
        public final static String DOCUMENT_FORMAT = "Document";
        public final static String ADRESS_FORMAT = "Adress";
        public final static String LIST_FORMAT = "List";

        public final static String COMPLEMENT_ENFANT_KEY = "complementenfant";
        public final static String NOM_COMPLET_KEY = "nomcomplet";
        public final static String NATIONALITY_KEY = "nationalite";
        public final static String ORIGINE_KEY = "origine";
        public final static String NOM_KEY = "nom";
        public final static String POST_NOM_KEY = "postnom";
        public final static String PRENOM_KEY = "prenom";

        public final static String PRINT_DATE_TIME_KEY = "printdatetime";
        public final static String PRINT_DATE_KEY = "printdate";

        public final static String DEMANDE_DATE_ACTE_TEXT_KEY = "datedemandetextacte";
        public final static String DEMANDE_DATE_ATTESTATION_TEXT_KEY = "datedemandetextattestation";
        public final static String DEMANDE_DATE_KEY = "datedemande";

        public final static String SITE_DEMANDE_KEY = "sitedemande";
        public final static String VILLE_DEMANDE_KEY = "ville";
        public final static String USER_AUTHORITY_KEY = "siteauthority";
        public final static String DOCUMENT_SERVICE_DEMANDE = "servicedemande";
        public final static String CODE_QR = "qrcode";
        public final static String USER_DEMANDE_KEY = "userdemande";
        public final static String TITLE_DOCUMENT_KEY = "titledocument";
        public final static String DOCUMENT_NUMBER_KEY = "documentnumber";
        public final static String DOCUMENT_NUMBERPAPIER_KEY = "numeropapier";
        public final static String DOCUMENT_NUMBERHOLO_KEY = "numerohologramme";
        public final static String DOCUMENT_NUMBERACTE_KEY = "numeroacte";
        public final static String DOCUMENT_NUMVOLUME_KEY = "numerovolume";
        public final static String MENTION_KEY = "mentionmarginale";
        public final static String DOCUMENT_NUMFOLIO_KEY = "numerofolio";

        public final static String DOCUMENT_LISTE_PERSONNE_GENERATE_KEY = "listepersonnegeneree";
        public final static String DOCUMENT_REQUIS_KEY = "documentRequis";

        public final static String TYPE_KEY = "type";
        public final static String FIELD_KEY = "field";
        public final static String FUNCTION_KEY = "function";

        public final static String FUNCTION_DATE_TO_STRING_ACTE = "datetotextacte";
        public final static String FUNCTION_DATE_TO_STRING_ATTESTATION = "datetotextattestation";

    }

    public class Numeric {

        public static final int ZERO = 0;
        public static final int ONE = 1;
        public static final int TWO = 2;
        public static final int THREE = 3;
        public static final int FOUR = 4;
        public static final int FIVE = 5;
        public static final int SIX = 6;
        public static final int SEVEN = 7;
        public static final int HEIGHT = 8;
        public static final int NINE = 9;
        public static final int TEN = 10;
        public static final int ELEVEN = 11;
        public static final int CENT = 100;

    }

    public class Format {

        public final static String FORMAT_DATE = "dd/MM/yyyy";
        public final static String FORMAT_DATE2 = "ddMMyyyy";
        public final static String FORMAT_DATE3 = "yyyy-MM-dd";
        public final static String FORMAT_DATE4 = "dd-MM-yyyy";
        public final static String FORMAT_DATE_WITH_HOUR_V1 = "dd/MM/yyyy à HH:mm:ss";
        public final static String FORMAT_DATE_WITH_HOUR_V2 = "yyyy-MM-dd HH:mm:ss";
        public final static String FORMAT_IMAGE = "image/png";
        public final static String FORMAT_IMAGE_BASE64 = "BASE64";
        public static final String ZERO_FORMAT = ",00";
        public static final String FORMAT_HTML = "HTML";
        public static final String FORMAT_PDF = "PDF";
        public static final String FORMAT_AMOUNT = "###,###.###";
        public static final String FORMAT_DATE_TEXT_ATTESTATION = "Le %s jour du mois de %s de l'an %s";
        public static final String FORMAT_DATE_TEXT_ACTE = "L'an %s, le %s jour du mois de %s";

    }

    public class Contrainte {

        public final static String CONTRAINTE_DATA = "contrainteData";
        public final static String FUNCTION_NAME = "fonction";
        public final static String FAIl_MESSAGE = "failMessage";
        public final static String PARAMS = "params";

    }

    public class COMPLEMENT_LIST_ACTION {

        public final static String ADD = "add";
        public final static String REMOVE = "remove";

    }

    public class NumeralString {

        public final static String UN = "Premier";
        public final static String DEUX = "Deuxième";
        public final static String TROIS = "Troisième";
        public final static String QUATRE = "Quatrième";
        public final static String CINQ = "Cinquième";
        public final static String SIX = "Sixième";
        public final static String SEPT = "Septième";
        public final static String HUIT = "Huitième";
        public final static String NEUF = "Neuvième";
        public final static String DIX = "Dixième";
        public final static String ONZE = "Onzième";
        public final static String DOUZE = "Douzième";
        public final static String TREIZE = "Treizième";
        public final static String QUATORZE = "Quatorzième";
        public final static String QUIZE = "Quinzième";
        public final static String SIEZE = "Seizième";
        public final static String DIX_SEPT = "Dix-septième";
        public final static String DIX_HUIT = "Dix-huitième";
        public final static String DIX_NEUF = "Dix-neuvième";
        public final static String VINGT = "Vingtième";
        public final static String VINGT_UN = "Vingt et unième";
        public final static String VINGT_DEUXIEME = "Vingt-deuxième";
        public final static String VINGT_TROIS = "Vingt-troisième";
        public final static String VINGT_QUATRE = "Vingt-quatrième";
        public final static String VINGT_CINQ = "Vingt-cinquième";
        public final static String VINGT_SIX = "Vingt-sixième";
        public final static String VINGT_SEPT = "Vingt-septième";
        public final static String VINGT_HUIT = "Vingt-huitième";
        public final static String VINGT_NEUF = "Vingt-neuvième";
        public final static String TRENTE = "Trentième";
        public final static String TRENTE_UN = "Trente et unième";

    }

    public class Mail {

        public final static String CRYPTO_TLSv1 = "TLSv1";
        public final static String STATUS = "Status";
        public final static String MESSAGE = "Messages";
        public final static String SUCCESS = "success";
        public final static String MAIL_PARAM_JSON = "{\"Messages\":[{\"From\":{\"Email\":\"%s\",\"Name\":\"ePéage\"},\"To\":[{\"Email\":\"%s\",\"Name\":\"%s\"}],\"Subject\":\"%s\",\"TextPart\":\"%s\",\"HTMLPart\":\"%s\"}]}";

    }

    public class API_Response {

        public final static String CODE = "code";
        public final static String Message = "message";
        public final static String Status_EN = "status";
        public final static String status_FR = "statut";
    }

    public class Lang {

        public final static String FR = "fr";
        public final static String EN = "en";

    }

    public class Format_Response {

        public final static String JSON = "application/json";
        public final static String XML = "application/xml";

    }

    public class TypeDocument {

        public final static String NP = "NP";
        public final static String BAP = "BAP";
        public final static String AMR = "AMR";
        public final static String AMR1 = "AMR1";
        public final static String AMR2 = "AMR2";
        public final static String DEC = "DEC";

    }

    public class Periodicite {

//        public final static String PONC = "PONC";
//        public final static String JOUR = "JOUR";
//        public final static String YEAR = "ANNEE";
//        public final static String MONTH = "MENS";
//        public final static String BIMENS = "BIMEN";
//        public final static String TRIME = "TRIME";
//        public final static String SEMS = "SEMS";
        public final static String PONC = "PR0012015";
        public final static String JOUR = "PR0022015";
        public final static String YEAR = "PR0042015";
        public final static String MONTH = "PR0032015";
        public final static String BIMENS = "PR0102015";
        public final static String TRIME = "PR0052015";
        public final static String SEMS = "PR0072015";
    }

    public class Periodicite_NbreJour {

        public final static String PONC = "0";
        public final static String JOUR = "1";
        public final static String YEAR = "365";
        public final static String MONTH = "31";
        public final static String BIMENS = "BIMEN";
        public final static String TRIME = "TRIME";
        public final static String SEMS = "SEMS";
    }

    public class TypeTaux {

        public final static String P = "%";
        public final static String F = "F";

    }

    public class paymentKEY {

        public static final String BORDEREAU = "bankRefNumber";
        public static final String PAIEMENT_DATE = "paymentDate";
        public static final String MONTANT = "amountPaid";
        public static final String CURRENCY = "currency";
        public static final String ACCOUNT = "bankAccount";
        public static final String DOCUMENT_TYPE = "documentType";
        public static final String DOCUMENT_NUMBER = "documentNumber";
    }

    public class ParamMpata {

        public final static String PARAM_JSON = "{\"application\":\"%s\",\"password\":\"%s\",\"username\":\"%s\"}";
        public final static String PARAM_TRANSFERT_JSON = "{\"adminId\":0,\"amount\":%s,\"currency\":\"%s\",\"date\":\"%s\",\"description\":\"%s\",\"feesIn\":true,\"operationType\":3,\"receiver\":\"0821416216\",\"sender\":\"%s\"}";
        public final static String PARAM_JSON_V2 = "{\"reference\":\"%s\",\"amount\":%s}";

    }

    public class Plage {

        public final static String PLAGE = "50:-AA-,100:-BB-,150:-CC-,200:-DD-,250:-EE-,300:-FF-,350:-GG-,400:-HH-,450:-II-,500:-JJ-,550:-KK-,600:-LL-,650:-MM-,700:-NN-,750:-OO-";
    }
}
