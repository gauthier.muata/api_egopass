/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.utils;

import cd.hologram.apigopass.constants.GeneralConst;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 *
 * @author gauthier.muata
 */
public class DateUtil {

    public static int daysBetween(Date d1, Date d2) {
        return (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }

    public static Date addDayOfDate(Date date, int day) {
        GregorianCalendar calendar = new java.util.GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, day);
        return calendar.getTime();
    }

    public static long getMonthsBetween(Date date1, Date dateJour) {
        Calendar otherDay = new GregorianCalendar();
        otherDay.setTime(date1);
        Calendar today = new GregorianCalendar();
        today.setTime(dateJour);
        int yearsInBetween = today.get(Calendar.YEAR) - otherDay.get(Calendar.YEAR);
        int monthsDiff = today.get(Calendar.MONTH) - otherDay.get(Calendar.MONTH);
        long months = (yearsInBetween * 12 + monthsDiff);
        return months;
    }

    public static long getYearsBetween(Date date1, Date date2) {
        Calendar otherDay = new GregorianCalendar();
        otherDay.setTime(date1);
        Calendar today = new GregorianCalendar();
        today.setTime(date2);
        int yearsInBetween = today.get(Calendar.YEAR) - otherDay.get(Calendar.YEAR);
        long years = yearsInBetween;
        return years;
    }

    public static int dateToInt(Date date) {
        int result = -1;
        try {
            DateFormat dateFormat = new SimpleDateFormat(GeneralConst.FormatDate.FORMAT_YDM_NO_SEPARATOR);
            result = Integer.parseInt(dateFormat.format(date));
        } catch (Exception e) {
        }
        return result;
    }

    public static String formatToDayMonthYear(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat(GeneralConst.FormatDate.FORMAT_DMY);
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String formatToDayMonthYearV2(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat(GeneralConst.FormatDate.FORMAT_DMY_V2);
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String formatToDayMonthYearTime(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat(GeneralConst.FormatDate.FORMAT_DMY_HOUR);
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String formatToDayMonthYearTimeV2(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat(GeneralConst.FormatDate.FORMAT_DMY_HOUR_V1);
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static Date formatToDayMonthYear(String date) {

        DateFormat formatter;
        Date formattedDate = null;
        formatter = new SimpleDateFormat(GeneralConst.FormatDate.FORMAT_DMY_V2, Locale.US);
        try {
            formattedDate = formatter.parse(date);
        } catch (ParseException e) {
        }
        return formattedDate;
    }

    public static Date formatToDayMonthYearV2(String date) {

        DateFormat formatter;
        Date formattedDate = null;
        formatter = new SimpleDateFormat(GeneralConst.FormatDate.FORMAT_DMY);
        try {
            formattedDate = formatter.parse(date);
        } catch (ParseException e) {
        }
        return formattedDate;
    }

    /**
     * Cette méthode permet de convertir une date en timeStamp
     *
     * @param date xxx
     * @return xxx
     *
     */
    public static long convertDateToTimestamp(Date date) {
        long output = date.getTime() / 1000L;
        String str = Long.toString(output);
        long timestamp = Long.parseLong(str) * 1000;
        return timestamp;
    }

    public static String formatToDayMonthYearTimeV3(String date) {
        String result = GeneralConst.EMPTY_STRING;

        if (date != null && !date.isEmpty()) {

            String[] parts = date.split("-");
            String part1 = parts[0];
            String part2 = parts[1];
            String part3 = parts[2];

            result = part3.concat("/").concat(part2).concat("/").concat(part1);
        }

        return result;
    }

    public static String formatToDayMonthYearTimeV4(String date) {
        String result = GeneralConst.EMPTY_STRING;

        if (date != null && !date.isEmpty()) {

            String[] parts = date.split("-");
            String part1 = parts[0];
            String part2 = parts[1];
            String part3 = parts[2];

            //result = part3.concat("/").concat(part2).concat("/").concat(part1);
            result = part1.concat("/").concat(part2).concat("/").concat(part3);

        }

        return result;
    }

    public static String formatToYearMonthDay(String date) {
        String result = GeneralConst.EMPTY_STRING;

        if (date != null && !date.isEmpty()) {

            String[] parts = date.split("/");
            String part1 = parts[0];
            String part2 = parts[1];
            String part3 = parts[2];

            result = part1.concat("-").concat(part2).concat("-").concat(part3);

        }

        return result;
    }

    public static String formatDateToString(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat(GeneralConst.Format.FORMAT_DATE);
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String formatDateToStringV2(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat(GeneralConst.Format.FORMAT_DATE4);
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String formatDateToStringV3(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat(GeneralConst.Format.FORMAT_DATE_WITH_HOUR_V2);
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String formatDateWithTimeToString(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat(GeneralConst.Format.FORMAT_DATE_WITH_HOUR_V1);
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static Date formatStringFullToDate(String date) {

        DateFormat formatter;
        Date formattedDate = null;
        formatter = new SimpleDateFormat(GeneralConst.Format.FORMAT_DATE3, Locale.US);
        try {
            formattedDate = formatter.parse(date);
        } catch (ParseException e) {
        }
        return formattedDate;
    }

    public static Date formatStringFullToDateV2(String date) {

        DateFormat formatter;
        Date formattedDate = null;
        formatter = new SimpleDateFormat(GeneralConst.Format.FORMAT_DATE, Locale.FRENCH);
        try {
            formattedDate = formatter.parse(date);
        } catch (ParseException e) {
        }
        return formattedDate;
    }

    public static Date formatStringFullToDateV3(String date) {

        DateFormat formatter;
        Date formattedDate = null;
        formatter = new SimpleDateFormat(GeneralConst.Format.FORMAT_DATE, Locale.US); //dd-MM-yyyy
        try {
            formattedDate = formatter.parse(date);
        } catch (ParseException e) {
        }
        return formattedDate;
    }
    
      public static Date formatDate(String date) {
        try {
            SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
            Date dt = dateformat.parse(date);
            return dt;
        } catch (Exception e) {
            return null;
        }
    }


}
