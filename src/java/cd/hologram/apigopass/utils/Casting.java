package cd.hologram.apigopass.utils;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.logging.*;

/**
 *
 * @author tony.lundja
 * @param <T>
 */
public class Casting<T> {
      private static Casting instance = null;

    private Casting() {
    }

    public static Casting getInstance() {
        if (instance == null) {
            instance = new Casting();
        }
        return instance;
    }

    public Class convertIntoClassType(Class<T> entity) {
        Class classe = null;
        try {
            String nomClasseCanonique = entity.getCanonicalName();
            classe = Class.forName(nomClasseCanonique);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Casting.class.getName()).log(Level.SEVERE, null, ex);
        }
        return classe;
    }
}
