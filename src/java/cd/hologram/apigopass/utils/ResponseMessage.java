/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.utils;

import cd.hologram.apigopass.constants.GeneralConst;
import cd.hologram.apigopass.constants.PropertiesConst;
import cd.hologram.apigopass.mock.Erreur;
import cd.hologram.apigopass.mock.Success;
import cd.hologram.apigopass.properties.PropertiesConfig;
import cd.hologram.apigopass.properties.PropertiesMessage;
import com.google.gson.JsonObject;
import java.io.IOException;

/**
 *
 * @author moussa.toure
 */
public class ResponseMessage {

    PropertiesConfig propertiesConfig;
    PropertiesMessage propertiesMessage;

    public ResponseMessage() throws IOException {

        propertiesConfig = new PropertiesConfig();
        propertiesMessage = new PropertiesMessage();
    }

    public String getHeaderMissingException(String lang, String param, String format) throws IOException {

        String message;

        if (format.equals(GeneralConst.Format_Response.JSON)) {

            JsonObject error = new JsonObject();

            error.addProperty(GeneralConst.API_Response.CODE, propertiesMessage.getContent(PropertiesConst.Message.NO_HEADER_CODE));

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {

                error.addProperty(GeneralConst.API_Response.Status_EN, propertiesMessage.getContent(PropertiesConst.Message.ERROR_EN));
                error.addProperty(GeneralConst.API_Response.Message, String.format(propertiesMessage.getContent(PropertiesConst.Message.NO_HEADER_MESSAGE_EN), param));
            } else {

                error.addProperty(GeneralConst.API_Response.status_FR, propertiesMessage.getContent(PropertiesConst.Message.ERROR_FR));
                error.addProperty(GeneralConst.API_Response.Message, String.format(propertiesMessage.getContent(PropertiesConst.Message.NO_HEADER_MESSAGE_FR), param));
            }

            message = error.toString();

        } else {

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {

                cd.hologram.apigopass.mock.Error error = new cd.hologram.apigopass.mock.Error();
                error.setCode(propertiesMessage.getContent(PropertiesConst.Message.NO_HEADER_CODE));
                error.setMessage(String.format(propertiesMessage.getContent(PropertiesConst.Message.NO_HEADER_MESSAGE_EN), param));

                message = Tools.convertToXML(cd.hologram.apigopass.mock.Error.class, error);

            } else {

                Erreur erreur = new Erreur();

                erreur.setCode(propertiesMessage.getContent(PropertiesConst.Message.NO_HEADER_CODE));
                erreur.setMessage(String.format(propertiesMessage.getContent(PropertiesConst.Message.NO_HEADER_MESSAGE_EN), param));

                message = Tools.convertToXML(Erreur.class, erreur);
            }
        }

        return message;
    }

    public String getTokenInvalidException(String lang, String format) throws IOException {

        String message;

        if (format.equals(GeneralConst.Format_Response.JSON)) {

            JsonObject error = new JsonObject();

            error.addProperty(GeneralConst.API_Response.CODE, propertiesMessage.getContent(PropertiesConst.Message.TOKEN_IS_INVALID_CODE));

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {
                error.addProperty(GeneralConst.API_Response.Status_EN, propertiesMessage.getContent(PropertiesConst.Message.ERROR_EN));
                error.addProperty(GeneralConst.API_Response.Message, propertiesMessage.getContent(PropertiesConst.Message.TOKEN_IS_INVALID_MESSAGE_EN));
            } else {
                error.addProperty(GeneralConst.API_Response.status_FR, propertiesMessage.getContent(PropertiesConst.Message.ERROR_FR));
                error.addProperty(GeneralConst.API_Response.Message, propertiesMessage.getContent(PropertiesConst.Message.TOKEN_IS_INVALID_MESSAGE_FR));
            }

            message = error.toString();

        } else {

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {

                cd.hologram.apigopass.mock.Error error = new cd.hologram.apigopass.mock.Error();
                error.setCode(propertiesMessage.getContent(PropertiesConst.Message.EXCEPTION_CODE));
                error.setMessage(propertiesMessage.getContent(PropertiesConst.Message.TOKEN_IS_INVALID_MESSAGE_EN));

                message = Tools.convertToXML(cd.hologram.apigopass.mock.Error.class, error);

            } else {

                Erreur erreur = new Erreur();

                erreur.setCode(propertiesMessage.getContent(PropertiesConst.Message.EXCEPTION_CODE));
                erreur.setMessage(propertiesMessage.getContent(PropertiesConst.Message.TOKEN_IS_INVALID_MESSAGE_FR));

                message = Tools.convertToXML(Erreur.class, erreur);
            }

        }

        return message;

    }

    public String getGeneralException(String lang, String format) throws IOException {

        String message;

        if (format.equals(GeneralConst.Format_Response.JSON)) {

            JsonObject error = new JsonObject();

            error.addProperty(GeneralConst.API_Response.CODE, Integer.valueOf(propertiesMessage.getContent(PropertiesConst.Message.EXCEPTION_CODE)));

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {

                error.addProperty(GeneralConst.API_Response.Status_EN, propertiesMessage.getContent(PropertiesConst.Message.ERROR_EN));
                error.addProperty(GeneralConst.API_Response.Message, propertiesMessage.getContent(PropertiesConst.Message.EXCEPTION_MESSAGE_EN));

            } else {

                error.addProperty(GeneralConst.API_Response.status_FR, propertiesMessage.getContent(PropertiesConst.Message.ERROR_FR));
                error.addProperty(GeneralConst.API_Response.Message, propertiesMessage.getContent(PropertiesConst.Message.EXCEPTION_MESSAGE_FR));

            }

            message = error.toString();

        } else {

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {

                cd.hologram.apigopass.mock.Error error = new cd.hologram.apigopass.mock.Error();
                error.setCode(propertiesMessage.getContent(PropertiesConst.Message.EXCEPTION_CODE));
                error.setMessage(propertiesMessage.getContent(PropertiesConst.Message.EXCEPTION_MESSAGE_EN));

                message = Tools.convertToXML(cd.hologram.apigopass.mock.Error.class, error);

            } else {

                Erreur erreur = new Erreur();

                erreur.setCode(propertiesMessage.getContent(PropertiesConst.Message.EXCEPTION_CODE));
                erreur.setMessage(propertiesMessage.getContent(PropertiesConst.Message.EXCEPTION_MESSAGE_FR));

                message = Tools.convertToXML(Erreur.class, erreur);
            }
        }

        return message;
    }

    public String getGeneralExceptionV2(String lang, String format) throws IOException {

        String message;

        JsonObject error = new JsonObject();

        error.addProperty(GeneralConst.API_Response.CODE, Integer.valueOf(propertiesMessage.getContent(PropertiesConst.Message.EXCEPTION_CODE)));

        if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {

            error.addProperty(GeneralConst.API_Response.Status_EN, propertiesMessage.getContent(PropertiesConst.Message.ERROR_EN));
            error.addProperty(GeneralConst.API_Response.Message, propertiesMessage.getContent(PropertiesConst.Message.EXCEPTION_MESSAGE_EN));

        } else {

            error.addProperty(GeneralConst.API_Response.status_FR, propertiesMessage.getContent(PropertiesConst.Message.ERROR_FR));
            error.addProperty(GeneralConst.API_Response.Message, propertiesMessage.getContent(PropertiesConst.Message.EXCEPTION_MESSAGE_FR));

        }

        message = error.toString();

        return message;
    }

    public String getDocumentException(String lang, String numero, String type, String format) throws IOException {

        String message;

        if (format.equals(GeneralConst.Format_Response.JSON)) {

            JsonObject error = new JsonObject();

            error.addProperty(GeneralConst.API_Response.CODE, propertiesMessage.getContent(PropertiesConst.Message.DOCUMENT_NOT_FOUND_CODE));

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {
                error.addProperty(GeneralConst.API_Response.Message, String.format(propertiesMessage.getContent(PropertiesConst.Message.DOCUMENT_NOT_FOUND_MESSAGE_EN), numero, type));
            } else {
                error.addProperty(GeneralConst.API_Response.Message, String.format(propertiesMessage.getContent(PropertiesConst.Message.DOCUMENT_NOT_FOUND_MESSAGE_FR), numero, type));
            }

            message = error.toString();

        } else {

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {

                cd.hologram.apigopass.mock.Error error = new cd.hologram.apigopass.mock.Error();
                error.setCode(propertiesMessage.getContent(PropertiesConst.Message.DOCUMENT_NOT_FOUND_CODE));
                error.setMessage(String.format(propertiesMessage.getContent(PropertiesConst.Message.DOCUMENT_NOT_FOUND_MESSAGE_EN), numero, type));

                message = Tools.convertToXML(cd.hologram.apigopass.mock.Error.class, error);

            } else {

                Erreur erreur = new Erreur();

                erreur.setCode(propertiesMessage.getContent(PropertiesConst.Message.DOCUMENT_NOT_FOUND_CODE));
                erreur.setMessage(String.format(propertiesMessage.getContent(PropertiesConst.Message.DOCUMENT_NOT_FOUND_MESSAGE_FR), numero, type));

                message = Tools.convertToXML(Erreur.class, erreur);
            }
        }
        return message;
    }

    public String getBodyException(String lang, String format) throws IOException {

        String message;

        if (format.equals(GeneralConst.Format_Response.JSON)) {

            JsonObject error = new JsonObject();

            error.addProperty(GeneralConst.API_Response.CODE, propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_CODE));

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {
                error.addProperty(GeneralConst.API_Response.Status_EN, propertiesMessage.getContent(PropertiesConst.Message.ERROR_EN));
                error.addProperty(GeneralConst.API_Response.Message, propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_MESSAGE_1_EN));
            } else {
                error.addProperty(GeneralConst.API_Response.Status_EN, propertiesMessage.getContent(PropertiesConst.Message.ERROR_FR));
                error.addProperty(GeneralConst.API_Response.Message, propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_MESSAGE_1_FR));
            }

            message = error.toString();

        } else {

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {

                cd.hologram.apigopass.mock.Error error = new cd.hologram.apigopass.mock.Error();
                error.setCode(propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_CODE));
                error.setMessage(propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_MESSAGE_1_EN));

                message = Tools.convertToXML(cd.hologram.apigopass.mock.Error.class, error);

            } else {

                Erreur erreur = new Erreur();

                erreur.setCode(propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_CODE));
                erreur.setMessage(propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_MESSAGE_1_FR));

                message = Tools.convertToXML(Erreur.class, erreur);
            }
        }

        return message;

    }

    public String getBodyException(String lang, String column, String format) throws IOException {

        String message;

        if (format.equals(GeneralConst.Format_Response.JSON)) {

            JsonObject error = new JsonObject();

            error.addProperty(GeneralConst.API_Response.CODE, propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_CODE));

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {
                error.addProperty(GeneralConst.API_Response.Status_EN, propertiesMessage.getContent(PropertiesConst.Message.ERROR_EN));
                error.addProperty(GeneralConst.API_Response.Message, String.format(propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_MESSAGE_2_EN), column));
            } else {
                error.addProperty(GeneralConst.API_Response.Status_EN, propertiesMessage.getContent(PropertiesConst.Message.ERROR_FR));
                error.addProperty(GeneralConst.API_Response.Message, String.format(propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_MESSAGE_2_FR), column));
            }

            message = error.toString();

        } else {

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {

                cd.hologram.apigopass.mock.Error error = new cd.hologram.apigopass.mock.Error();
                error.setCode(propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_CODE));
                error.setMessage(String.format(propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_MESSAGE_2_EN), column));

                message = Tools.convertToXML(cd.hologram.apigopass.mock.Error.class, error);

            } else {

                Erreur erreur = new Erreur();

                erreur.setCode(propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_CODE));
                erreur.setMessage(String.format(propertiesMessage.getContent(PropertiesConst.Message.BODY_ERROR_MESSAGE_2_FR), column));

                message = Tools.convertToXML(Erreur.class, erreur);
            }
        }

        return message;

    }

    public String getSavePayment(String lang, boolean result, String format, String document) throws IOException {

        String message;

        if (format.equals(GeneralConst.Format_Response.JSON)) {

            JsonObject objectMessage = new JsonObject();

            objectMessage.addProperty(GeneralConst.API_Response.CODE, result
                    ? propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYMENT_OK_CODE)
                    : propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYEMENT_KO_CODE));

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {

                objectMessage.addProperty(GeneralConst.API_Response.Status_EN,
                        result ? propertiesMessage.getContent(PropertiesConst.Message.SUCCESS_EN)
                                : propertiesMessage.getContent(PropertiesConst.Message.ERROR_EN));

                objectMessage.addProperty(GeneralConst.API_Response.Message, result
                        ? String.format(propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYMENT_OK_EN), document)
                        : String.format(propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYMENT_KO_EN), document));

            } else {

                objectMessage.addProperty(GeneralConst.API_Response.status_FR,
                        result ? propertiesMessage.getContent(PropertiesConst.Message.SUCCESS_FR)
                                : propertiesMessage.getContent(PropertiesConst.Message.ERROR_FR));

                objectMessage.addProperty(GeneralConst.API_Response.status_FR, result
                        ? propertiesMessage.getContent(PropertiesConst.Message.SUCCESS_FR)
                        : propertiesMessage.getContent(PropertiesConst.Message.ERROR_FR));

                objectMessage.addProperty(GeneralConst.API_Response.Message, result
                        ? String.format(propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYMENT_OK_FR), document)
                        : String.format(propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYMENT_KO_FR), document));
            }

            message = objectMessage.toString();

        } else {

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {

                if (result) {

                    Success success = new Success();

                    success.setCode(propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYMENT_OK_CODE));
                    success.setMessage(String.format(propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYMENT_OK_EN), document));

                    message = Tools.convertToXML(Success.class, success);

                } else {

                    cd.hologram.apigopass.mock.Error error = new cd.hologram.apigopass.mock.Error();
                    error.setCode(propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYEMENT_KO_CODE));
                    error.setMessage(String.format(propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYMENT_KO_EN), document));

                    message = Tools.convertToXML(cd.hologram.apigopass.mock.Error.class, error);
                }

            } else {

                if (result) {

                    Success success = new Success();

                    success.setCode(propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYMENT_OK_CODE));
                    success.setMessage(String.format(propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYMENT_OK_FR), document));

                    message = Tools.convertToXML(Success.class, success);

                } else {

                    cd.hologram.apigopass.mock.Error error = new cd.hologram.apigopass.mock.Error();
                    error.setCode(propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYEMENT_KO_CODE));
                    error.setMessage(String.format(propertiesMessage.getContent(PropertiesConst.Message.SAVE_PAYMENT_KO_FR), document));

                    message = Tools.convertToXML(cd.hologram.apigopass.mock.Error.class, error);
                }

            }

        }

        return message;

    }

    public String geBankAccountException(String lang, String format) throws IOException {

        String message;

        if (format.equals(GeneralConst.Format_Response.JSON)) {

            JsonObject error = new JsonObject();

            error.addProperty(GeneralConst.API_Response.CODE, propertiesMessage.getContent(PropertiesConst.Message.BANK_ERROR_CODE));

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {
                error.addProperty(GeneralConst.API_Response.Message, propertiesMessage.getContent(PropertiesConst.Message.BANK_ACCOUNT_ERROR_EN));
            } else {
                error.addProperty(GeneralConst.API_Response.Message, propertiesMessage.getContent(PropertiesConst.Message.BANK_ACCOUNT_ERROR_FR));
            }

            message = error.toString();

        } else {

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {

                cd.hologram.apigopass.mock.Error error = new cd.hologram.apigopass.mock.Error();
                error.setCode(propertiesMessage.getContent(PropertiesConst.Message.BANK_ERROR_CODE));
                error.setMessage(propertiesMessage.getContent(PropertiesConst.Message.BANK_ACCOUNT_ERROR_EN));

                message = Tools.convertToXML(cd.hologram.apigopass.mock.Error.class, error);

            } else {

                Erreur erreur = new Erreur();

                erreur.setCode(propertiesMessage.getContent(PropertiesConst.Message.BANK_ERROR_CODE));
                erreur.setMessage(propertiesMessage.getContent(PropertiesConst.Message.BANK_ACCOUNT_ERROR_FR));

                message = Tools.convertToXML(Erreur.class, erreur);
            }
        }
        return message;
    }

    public String geBankRefNmberException(String lang, String format) throws IOException {

        String message;

        if (format.equals(GeneralConst.Format_Response.JSON)) {

            JsonObject error = new JsonObject();

            error.addProperty(GeneralConst.API_Response.CODE, propertiesMessage.getContent(PropertiesConst.Message.BANK_REF_NUMEBER_CODE));

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {
                error.addProperty(GeneralConst.API_Response.Message, propertiesMessage.getContent(PropertiesConst.Message.BANK_REF_NUMEBER_ERROR_EN));
            } else {
                error.addProperty(GeneralConst.API_Response.Message, propertiesMessage.getContent(PropertiesConst.Message.BANK_REF_NUMEBER_ERROR_FR));
            }

            message = error.toString();

        } else {

            if (lang.equalsIgnoreCase(GeneralConst.Lang.EN)) {

                cd.hologram.apigopass.mock.Error error = new cd.hologram.apigopass.mock.Error();
                error.setCode(propertiesMessage.getContent(PropertiesConst.Message.BANK_REF_NUMEBER_CODE));
                error.setMessage(propertiesMessage.getContent(PropertiesConst.Message.BANK_REF_NUMEBER_ERROR_EN));

                message = Tools.convertToXML(cd.hologram.apigopass.mock.Error.class, error);

            } else {

                Erreur erreur = new Erreur();

                erreur.setCode(propertiesMessage.getContent(PropertiesConst.Message.BANK_REF_NUMEBER_CODE));
                erreur.setMessage(propertiesMessage.getContent(PropertiesConst.Message.BANK_REF_NUMEBER_ERROR_FR));

                message = Tools.convertToXML(Erreur.class, erreur);
            }
        }
        return message;
    }
}
