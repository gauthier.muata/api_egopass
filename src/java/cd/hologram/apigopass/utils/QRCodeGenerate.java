/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.utils;

import cd.hologram.apigopass.constants.GeneralConst;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64.Decoder;
import javax.imageio.ImageIO;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

/**
 *
 * @author Administrateur
 */
public class QRCodeGenerate {

    public static String generateQRCode(String code, String reference, String dir) throws Exception {
        String qrBase64 = GeneralConst.EMPTY_STRING;
        try {
            try (ByteArrayOutputStream out = QRCode.from(code).to(ImageType.PNG).stream()) {
                qrBase64 = Tools.getByteaToBase64String(out.toByteArray());
                String imageString = qrBase64.split(",")[1];
                BufferedImage image;
                byte[] imageByte;
                Decoder decoder = java.util.Base64.getDecoder();
                imageByte = decoder.decode(imageString);
                try (ByteArrayInputStream bis = new ByteArrayInputStream(imageByte)) {
                    image = ImageIO.read(bis);
                }
                File outputfile = new File(dir + "/".concat(code.concat(".png")));
                ImageIO.write(image, "png", outputfile);
            }
        } catch (Exception e) {
            throw e;
        }
        return qrBase64;
    }

    public static BufferedImage generateQRCodeImage(String code, String reference, String dir) throws Exception {
        ByteArrayOutputStream stream = QRCode.from(code).withSize(250, 250).stream();
        ByteArrayInputStream bis = new ByteArrayInputStream(stream.toByteArray());
        File outputfile = new File(dir + "/".concat(code.concat(".png")));
        ImageIO.write(ImageIO.read(bis), "png", outputfile);

        return ImageIO.read(bis);
    }

//    public static BufferedImage generateQRCodeImage(String code, String reference, String dir) throws Exception {
//        ByteArrayOutputStream stream = QRCode.from(code).withSize(250, 250).stream();
//        ByteArrayInputStream bis = new ByteArrayInputStream(stream.toByteArray());
//        File outputfile = new File(dir + "/".concat(code.concat(".png")));
//        ImageIO.write(ImageIO.read(bis), "png", outputfile);
//
//        return ImageIO.read(bis);
//    }
    public static void saveComponentAsJPEG(String code, String dir) {

        try {
            // create QR code
            BitMatrix bitMatrix = new QRCodeWriter().encode(code, BarcodeFormat.QR_CODE, 250, 250);
            // save as image
            File img = new File(dir.concat("/").concat(code.concat(".png")));
            try (FileOutputStream fos = new FileOutputStream(img)) {
                MatrixToImageWriter.writeToStream(bitMatrix, "png", fos);
            }
        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }
    }

}
