/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Administrateur
 */
public class ZippHelp {

    public static String convertZipFileToBaseEncodeString(String path) {
        File originalFile = new File(path);
        String encodedBase64 = null;
        try {
            try (FileInputStream fileInputStreamReader = new FileInputStream(originalFile)) {
                byte[] bytes = new byte[(int) originalFile.length()];
                fileInputStreamReader.read(bytes);
                encodedBase64 = new String(Base64.encodeBase64(bytes));
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        return encodedBase64;

    }
}
