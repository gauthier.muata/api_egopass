/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.utils;

import cd.hologram.apigopass.constants.GeneralConst;
import static cd.hologram.apigopass.utils.ConvertDate.getDayOfMonthByYear;
import java.util.*;
import org.apache.commons.lang.StringUtils;
import org.json.*;

/**
 *
 * @author emmanuel.tsasa
 */
public class PeriodiciteData {

    public static List<JSONObject> getPeriodeJournaliere(Date dateJour, int length, String codePeriodicite, String nbreJourLimitePaiement) {

        List<JSONObject> jsonPeriodes = new ArrayList();

//        try {
//
//            for (int i = 0; i < length; i++) {
//
//                String date = ConvertDate.formatDateToStringOfFormat(dateJour, GeneralConst.Format.FORMAT_DATE);
//
//                JSONObject jsonPeriode = new JSONObject();
//                jsonPeriode.put("ordrePeriode", (i + 1));
//                jsonPeriode.put("dateDebut", date);
//                jsonPeriode.put("dateFin", date);
//                jsonPeriode.put("dateLimite", date);
//                jsonPeriode.put("dateLimitePaiement", nbreJourLimitePaiement.isEmpty() ? nbreJourLimitePaiement : date);
//                jsonPeriode.put("periode",
//                        Tools.getPeriodeIntitule(dateJour, codePeriodicite));
//
//                jsonPeriodes.add(jsonPeriode);
//
//                dateJour = ConvertDate.addDayOfDate(dateJour, 1);
//            }
//
//        } catch (JSONException | NumberFormatException e) {
//
//        }
        return jsonPeriodes;
    }

    public static List<JSONObject> getPeriodesMensuelles(
            int nombreJour,
            boolean forTaxation, Date dateDepart, int moisDepart, int anneeDepart,
            final String day_echeance, String day_echeance_paiement, String codePeriodicite, String periodeEcheance) {

        List<JSONObject> jsonPeriodes = new ArrayList();

        try {

            int i = 0;

            while (i < 12) {

                i++;

                JSONObject jsonPeriode = new JSONObject();

                jsonPeriode.put("ordrePeriode", i);

                String dateDebut = "01/" + StringUtils.leftPad(String.valueOf(moisDepart), 2, "0") + "/" + anneeDepart;
                jsonPeriode.put("dateDebut", dateDebut);
                jsonPeriode.put("periode",
                        Tools.getPeriodeIntitule(ConvertDate.formatDate(dateDebut), codePeriodicite));

                switch (codePeriodicite) {
                    case GeneralConst.Periodicite.BIMENS:
                        moisDepart += 1;
                        if (moisDepart > 12) {
                            moisDepart = (moisDepart - 12);
                            anneeDepart += 1;
                        }
                        break;
                    case GeneralConst.Periodicite.TRIME:
                        moisDepart += 2;
                        if (moisDepart > 12) {
                            moisDepart = (moisDepart - 12);
                            anneeDepart += 1;
                        }
                        break;
                    case GeneralConst.Periodicite.SEMS:
                        moisDepart += 5;
                        if (moisDepart > 12) {
                            moisDepart = (moisDepart - 12);
                            anneeDepart += 1;
                        }
                }

                String dateFin = getDayOfMonthByYear(anneeDepart, moisDepart) + "/" + StringUtils.leftPad(String.valueOf(moisDepart), 2, "0") + "/" + anneeDepart;
                jsonPeriode.put("dateFin", dateFin);

                if (!day_echeance.equals(GeneralConst.EMPTY_STRING) && !day_echeance.equals(GeneralConst.NumberString.ZERO)) {

                    String dateLimite;
                    String dateLimitePaiement;

                    int moisFin, anneeFin;
                    if (!periodeEcheance.equals(GeneralConst.EMPTY_STRING)) {
                        if (periodeEcheance.equals(GeneralConst.NumberString.ONE)) {
                            moisFin = (moisDepart == 12) ? 1 : moisDepart + 1;
                            anneeFin = (moisDepart == 12) ? (anneeDepart + 1) : anneeDepart;
                        } else {
                            moisFin = moisDepart;
                            anneeFin = anneeDepart;
                        }
                    } else {
                        moisFin = moisDepart;
                        anneeFin = anneeDepart;
                    }

                    int numberDayNextMonth = getDayOfMonthByYear(anneeFin, moisFin);

                    if (numberDayNextMonth < Integer.parseInt(day_echeance)) {
                        dateLimite = numberDayNextMonth + "/" + StringUtils.leftPad(String.valueOf(moisFin), 2, "0") + "/" + anneeFin;
                    } else {
                        dateLimite = day_echeance + "/" + StringUtils.leftPad(String.valueOf(moisFin), 2, "0") + "/" + anneeFin;

                    }

                    jsonPeriode.put("dateLimite", takeEcheanceDate(dateLimite, true));
                    jsonPeriode.put("isEchus", Compare.before(Tools.formatStringFullToDateV2(dateLimite), new Date()));

                    if (!day_echeance_paiement.equals(GeneralConst.EMPTY_STRING) && !day_echeance_paiement.equals(GeneralConst.NumberString.ZERO)) {

                        if (numberDayNextMonth < Integer.parseInt(day_echeance_paiement)) {
                            dateLimitePaiement = numberDayNextMonth + "/" + StringUtils.leftPad(String.valueOf(moisFin), 2, "0") + "/" + anneeFin;
                        } else {
                            dateLimitePaiement = day_echeance_paiement + "/" + StringUtils.leftPad(String.valueOf(moisFin), 2, "0") + "/" + anneeFin;
                        }

                        jsonPeriode.put("dateLimitePaiement", takeEcheanceDate(dateLimitePaiement, true));
                        jsonPeriode.put("isEchusPaiement", Compare.before(Tools.formatStringFullToDateV2(dateLimitePaiement), new Date()));

                    } else {

                        jsonPeriode.put("dateLimitePaiement", GeneralConst.NON_DEFINI);
                        jsonPeriode.put("isEchusPaiement", false);

                    }

                } else if ((day_echeance.equals(GeneralConst.EMPTY_STRING) || day_echeance.equals(GeneralConst.NumberString.ZERO)) && (!day_echeance_paiement.equals(GeneralConst.EMPTY_STRING) && !day_echeance_paiement.equals(GeneralConst.NumberString.ZERO))) {

                    String dateLimitePaiement;

                    int moisFin, anneeFin;
                    if (!periodeEcheance.equals(GeneralConst.EMPTY_STRING)) {
                        if (periodeEcheance.equals(GeneralConst.NumberString.ONE)) {
                            moisFin = (moisDepart == 12) ? 1 : moisDepart + 1;
                            anneeFin = (moisDepart == 12) ? (anneeDepart + 1) : anneeDepart;
                        } else {
                            moisFin = moisDepart;
                            anneeFin = anneeDepart;
                        }
                    } else {
                        moisFin = moisDepart;
                        anneeFin = anneeDepart;
                    }

                    int numberDayNextMonth = getDayOfMonthByYear(anneeFin, moisFin);

                    if (numberDayNextMonth < Integer.parseInt(day_echeance_paiement)) {
                        dateLimitePaiement = numberDayNextMonth + "/" + StringUtils.leftPad(String.valueOf(moisFin), 2, "0") + "/" + anneeFin;
                    } else {
                        dateLimitePaiement = day_echeance_paiement + "/" + StringUtils.leftPad(String.valueOf(moisFin), 2, "0") + "/" + anneeFin;
                    }

                    jsonPeriode.put("dateLimitePaiement", takeEcheanceDate(dateLimitePaiement, true));
                    jsonPeriode.put("isEchusPaiement", Compare.before(Tools.formatStringFullToDateV2(dateLimitePaiement), new Date()));

                    jsonPeriode.put("dateLimite", GeneralConst.NON_DEFINI);
                    jsonPeriode.put("isEchus", false);
                    jsonPeriode.put("isEchusPaiement", false);

                } else {

                    jsonPeriode.put("dateLimite", GeneralConst.NON_DEFINI);
                    jsonPeriode.put("dateLimitePaiement", GeneralConst.NON_DEFINI);
                    jsonPeriode.put("isEchus", false);
                    jsonPeriode.put("isEchusPaiement", false);
                }

                jsonPeriodes.add(jsonPeriode);

                //Pour incrementer la date
                dateDepart = ConvertDate.addDayOfDate(dateDepart, nombreJour);

                if (moisDepart == 12) {
                    anneeDepart++;
                    moisDepart = 0;
                }

                moisDepart++;

                //Regeneration des 2 periodes en cas de manque
                if (forTaxation) {
                    if (i == 2) {
                        break;
                    }
                }

            }
        } catch (JSONException | NumberFormatException e) {

        }
        return jsonPeriodes;
    }

    public static List<JSONObject> getPeriodesAnnuelles(
            int nombreJour,
            boolean forTaxation, Date dateDepart, int anneeDepart, final String day_echeance,
            final String month_eheance, final String day_echeance_paiement,
            final String month_eheance_paiement, String codePeriodicite, String periodeEcheance) {

        List<JSONObject> jsonPeriodes = new ArrayList();

        try {

            int i = 0;

            while (dateDepart.before(new Date()) || forTaxation) {
                i++;

                JSONObject jsonPeriode = new JSONObject();

                jsonPeriode.put("ordrePeriode", i);

                String dateDebut = "01/01/" + anneeDepart;
                jsonPeriode.put("dateDebut", dateDebut);
                jsonPeriode.put("periode",
                        Tools.getPeriodeIntitule(ConvertDate.formatDate(dateDebut), codePeriodicite));

                switch (codePeriodicite) {
                    case "2ANS":
                        anneeDepart += 1;
                        break;
                    case "5ANS":
                        anneeDepart += 4;
                }

                String dateFin = "31/12/" + anneeDepart;
                jsonPeriode.put("dateFin", dateFin);

                if (!day_echeance.equals(GeneralConst.EMPTY_STRING) && !day_echeance.equals(GeneralConst.NumberString.ZERO)) {

                    int anneeFin;
                    if (!periodeEcheance.equals(GeneralConst.EMPTY_STRING)) {
                        if (periodeEcheance.equals(GeneralConst.NumberString.ONE)) {
                            anneeFin = (anneeDepart + 1);
                        } else {
                            anneeFin = anneeDepart;
                        }
                    } else {
                        anneeFin = anneeDepart;
                    }

                    String dateLimite = day_echeance + "/" + month_eheance + "/" + anneeFin;

                    jsonPeriode.put("dateLimite", takeEcheanceDate(dateLimite, true));
                    jsonPeriode.put("isEchus", Compare.before(Tools.formatStringFullToDateV2(dateLimite), new Date()));

                    if (!day_echeance_paiement.equals(GeneralConst.EMPTY_STRING) && !day_echeance_paiement.equals(GeneralConst.NumberString.ZERO)) {

                        String dateLimitePaiement = day_echeance_paiement + "/" + month_eheance_paiement + "/" + anneeFin;

                        jsonPeriode.put("dateLimitePaiement", takeEcheanceDate(dateLimitePaiement, true));
                        jsonPeriode.put("isEchusPaiement", Compare.before(Tools.formatStringFullToDateV2(dateLimitePaiement), new Date()));

                    } else {

                        jsonPeriode.put("dateLimitePaiement", GeneralConst.NON_DEFINI);
                        jsonPeriode.put("isEchusPaiement", false);
                    }
                } else if ((day_echeance.equals(GeneralConst.EMPTY_STRING) || day_echeance.equals(GeneralConst.NumberString.ZERO)) && (!day_echeance_paiement.equals(GeneralConst.EMPTY_STRING) && !day_echeance_paiement.equals(GeneralConst.NumberString.ZERO))) {

                    int anneeFin;
                    if (!periodeEcheance.equals(GeneralConst.EMPTY_STRING)) {
                        if (periodeEcheance.equals(GeneralConst.NumberString.ONE)) {
                            anneeFin = (anneeDepart + 1);
                        } else {
                            anneeFin = anneeDepart;
                        }
                    } else {
                        anneeFin = anneeDepart;
                    }

                    jsonPeriode.put("isEchus", false);
                    jsonPeriode.put("isEchusPaiement", false);

                    String dateLimitePaiement = day_echeance_paiement + "/" + month_eheance_paiement + "/" + anneeFin;

                    jsonPeriode.put("dateLimitePaiement", takeEcheanceDate(dateLimitePaiement, true));
                    jsonPeriode.put("isEchusPaiement", Compare.before(Tools.formatStringFullToDateV2(dateLimitePaiement), new Date()));

                } else {

                    jsonPeriode.put("dateLimite", GeneralConst.NON_DEFINI);
                    jsonPeriode.put("dateLimitePaiement", GeneralConst.NON_DEFINI);
                    jsonPeriode.put("isEchus", false);
                    jsonPeriode.put("isEchusPaiement", false);
                }

                jsonPeriodes.add(jsonPeriode);

                //Pour incrementer la date
                dateDepart = ConvertDate.addDayOfDate(dateDepart, nombreJour);

                anneeDepart++;

                //Regeneration des 2 periodes en cas de manque
                if (forTaxation) {
                    if (i == 2) {
                        break;
                    }
                }

            }
        } catch (JSONException | NumberFormatException e) {

        }
        return jsonPeriodes;
    }

    public static String takeEcheanceDate(String echeanceDate, boolean isLegal) {

        Date dateEcheance = ConvertDate.formatDate(echeanceDate);
//        dateEcheance = Tools.getEcheanceDate(dateEcheance, 1, isLegal);
//        echeanceDate = ConvertDate.formatDateToStringOfFormat(dateEcheance, GeneralConst.Format.FORMAT_DATE);
        return echeanceDate;
    }

}
