package cd.hologram.apigopass.utils;

import java.sql.Connection;
import java.util.logging.Logger;

/**
 *
 * @author moussa.toure
 */
public class CustumException {

    private static final Logger LOG = Logger.getLogger(Connection.class.getName());

    public static void LogException(Exception exception) {

        try {
            LOG.log(java.util.logging.Level.SEVERE,
                    "EXCEPTION : {0} "
                    + "// MESSAGE : {1} "
                    + "// CLASS : {2} "
                    + "// METHOD : {3} "
                    + "// LINE : {4} ",
                    new Object[]{
                        exception.getClass(),
                        exception.getCause().toString(),
                        exception.getStackTrace()[0].getClassName(),
                        exception.getStackTrace()[0].getMethodName(),
                        exception.getStackTrace()[0].getLineNumber()
                    });
        } catch (Exception e) {
            CustumException.BasicLogException(e);

        }

    }

    public static void LogException(Exception exception, String codeEven, Integer codeAgent) {

        try {

            LOG.log(java.util.logging.Level.SEVERE,
                    "EXCEPTION : {0} "
                    + "// MESSAGE : {1} "
                    + "// CLASS : {2} "
                    + "// METHOD : {3} "
                    + "// LINE : {4} ",
                    new Object[]{
                        exception.getClass(),
                        exception.getMessage(),
                        exception.getStackTrace()[0].getClassName(),
                        exception.getStackTrace()[0].getMethodName(),
                        exception.getStackTrace()[0].getLineNumber()
                    });

        } catch (Exception e) {
            CustumException.BasicLogException(e);
        }

    }

    public static void BasicLogException(Exception exception) {

        LOG.log(java.util.logging.Level.SEVERE,
                "EXCEPTION : {0} "
                + "// MESSAGE : {1} "
                + "// CLASS : {2} "
                + "// METHOD : {3} "
                + "// LINE : {4} ",
                new Object[]{
                    exception.getClass(),
                    exception.getMessage(),
                    exception.getStackTrace()[0].getClassName(),
                    exception.getStackTrace()[0].getMethodName(),
                    exception.getStackTrace()[0].getLineNumber()
                });

    }

}
