/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

/**
 *
 * @author WILLY
 */
public class SMSSender {

    public static int sendSMS(String url) {

        try {

            SSLContext sc = SSLContext.getInstance("TLSv1");
            TrustManager[] trustAllCerts = {new InsecureTrustManager()};
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HostnameVerifier allHostsValid = new InsecureHostnameVerifier();

            Client client = ClientBuilder.newBuilder().sslContext(sc).hostnameVerifier(allHostsValid).build();

            WebTarget target;
            
            target = client.target(url);
            
            System.out.println("url : " + url);
            
            String response = target.request().get(String.class);
            
               System.out.println("response : " + response); 

            client.close();

        } catch (NoSuchAlgorithmException | KeyManagementException ex) {
            return -1;
        }

        return 200;
    }

}
