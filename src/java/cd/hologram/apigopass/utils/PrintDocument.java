/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.utils;

import cd.hologram.apigopass.business.DataAccess;
import static cd.hologram.apigopass.business.DataAccess.getArchiveByRefDocument;
import cd.hologram.apigopass.constants.GeneralConst;
import cd.hologram.apigopass.entities.Acquisition;
import cd.hologram.apigopass.entities.Adresse;
import cd.hologram.apigopass.entities.Agent;
import cd.hologram.apigopass.entities.Archive;
import cd.hologram.apigopass.entities.ArticleBudgetaire;
import cd.hologram.apigopass.entities.Banque;
import cd.hologram.apigopass.entities.Bien;
import cd.hologram.apigopass.entities.Commande;
import cd.hologram.apigopass.entities.CompteBancaire;
import cd.hologram.apigopass.entities.PeriodeDeclaration;
import cd.hologram.apigopass.entities.Personne;
import cd.hologram.apigopass.entities.RetraitDeclaration;
import cd.hologram.apigopass.entities.Site;
import cd.hologram.apigopass.etat.DocumentReturnString;
import cd.hologram.apigopass.mock.NotePerceptionPrint;
import cd.hologram.apigopass.mock.NoteTaxationMock;
import cd.hologram.apigopass.mock.VoucherMock;
import cd.hologram.apigopass.properties.PropertiesConfig;
import cd.hologram.apigopass.properties.PropertiesMail;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author moussa.toure
 */
public class PrintDocument {

    PropertiesConfig propertiesConfig;
    PropertiesMail propertiesMail;
    boolean saveArchive;
    boolean existArchive;
    Date echeanceMep;
    int xfrom;

    public PrintDocument() throws IOException {

        propertiesConfig = new PropertiesConfig();
    }

    String generateQRCode(String contenu) {
        String qrBase64 = GeneralConst.EMPTY_STRING;
        try {
            ByteArrayOutputStream out = QRCode.from(contenu).to(ImageType.PNG).stream();
            ByteArrayInputStream qrIn = new ByteArrayInputStream(out.toByteArray());
            file = new DefaultStreamedContent(qrIn, GeneralConst.Format.FORMAT_IMAGE);
            qrBase64 = Tools.getByteaToBase64String(out.toByteArray());
        } catch (Exception e) {
            throw e;
        }
        return qrBase64;
    }

    public String createNoteTaxationDeclaration(RetraitDeclaration retraitDeclaration, String userId, BigDecimal amountDeclaration, String periodeNames) throws Exception {

        String dataXhtm = GeneralConst.EMPTY_STRING;
        NoteTaxationMock noteTaxationMock = new NoteTaxationMock();

        Personne personne = new Personne();
        Adresse adresse = new Adresse();
        Agent agent = new Agent();
        Banque banque = new Banque();
        CompteBancaire compteBancaire = new CompteBancaire();

        noteTaxationMock.setFlag(propertiesConfig.getContent("LOGO_DRHKAT"));

        String url = propertiesConfig.getContent("URL_TRACKING_DOC").concat(retraitDeclaration.getId() + "");

        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            noteTaxationMock.setCodeQR(codeQR);
        } else {
            noteTaxationMock.setCodeQR(GeneralConst.EMPTY_STRING);
        }

        BigDecimal amountTaxation = new BigDecimal(0);

        noteTaxationMock.setDateTaxation(ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));
        noteTaxationMock.setDateEcheance(ConvertDate.formatDateToStringOfFormat(retraitDeclaration.getDateEcheancePaiement(), "dd/MM/YYYY"));
        noteTaxationMock.setNumeroNoteTaxation(retraitDeclaration.getId() + "");

        personne = DataAccess.getPersonneByCode_V2(retraitDeclaration.getFkAssujetti());

        String nomProprietaire = GeneralConst.EMPTY_STRING;

        if (retraitDeclaration.getFkAb().equals(propertiesConfig.getContent("CODE_IMPOT_RL"))) {

            PeriodeDeclaration pd = DataAccess.getPeriodeDeclarationByCode(retraitDeclaration.getFkPeriode());

            if (pd != null) {

                Bien bien = pd.getAssujetissement().getBien();

                if (bien.getAcquisitionList().size() > 0) {

                    for (Acquisition acquisition : bien.getAcquisitionList()) {

                        if (acquisition.getProprietaire()) {
                            nomProprietaire = acquisition.getPersonne() != null ? acquisition.getPersonne().toString().toUpperCase() : "";
                        }
                    }
                }
            }
        }

        if (personne != null) {

            if (retraitDeclaration.getFkAb().equals(propertiesConfig.getContent("CODE_IMPOT_RL"))) {

                noteTaxationMock.setContribuable(nomProprietaire);
                noteTaxationMock.setNif(personne.getNif() == null ? GeneralConst.EMPTY_STRING : personne.getNif().toUpperCase());
                noteTaxationMock.setRedevable(personne.toString().toUpperCase());

            } else {

                noteTaxationMock.setContribuable(personne.toString().toUpperCase());
                noteTaxationMock.setNif(personne.getNif() == null ? GeneralConst.EMPTY_STRING : personne.getNif().toUpperCase());
                noteTaxationMock.setRedevable(GeneralConst.EMPTY_STRING);

            }

            adresse = DataAccess.getAdresseDefaultByAssujetti(personne.getCode());

            if (adresse != null) {
                noteTaxationMock.setAdresseAssujetti(adresse.toString().toUpperCase());
            } else {
                noteTaxationMock.setAdresseAssujetti(GeneralConst.EMPTY_STRING);
            }

        } else {
            noteTaxationMock.setContribuable(GeneralConst.EMPTY_STRING);
            noteTaxationMock.setNif(GeneralConst.EMPTY_STRING);
            noteTaxationMock.setRedevable(GeneralConst.EMPTY_STRING);
        }

        agent = DataAccess.getAgentByCode(userId);

        Site site = DataAccess.getSiteByCode(retraitDeclaration.getFkSite());
        noteTaxationMock.setEntiteEmetrice(site.getIntitule());
        noteTaxationMock.setLieuTaxation(site.getAdresse().getChaine());

        if (agent != null) {
            noteTaxationMock.setNomFonctionTaxateur(agent.toString().toUpperCase() + "<br/>" + agent.getFonction().getIntitule().toUpperCase());
        } else {
            noteTaxationMock.setNomFonctionTaxateur(GeneralConst.EMPTY_STRING);
        }

        if (retraitDeclaration.getRetraitDeclarationMere() == null) {
            noteTaxationMock.setTypeNoteTaxation("PRINCIPALE");
        } else {

            if (retraitDeclaration.getEtat() == 3) {
                noteTaxationMock.setTypeNoteTaxation("ECHELONNEMENT");
            } else {
                noteTaxationMock.setTypeNoteTaxation("PENALITE");
            }

        }

        ArticleBudgetaire articleBudgetaire = DataAccess.getArticleBudgetaireByCode(retraitDeclaration.getFkAb());

        noteTaxationMock.setImpot(articleBudgetaire == null ? GeneralConst.EMPTY_STRING : articleBudgetaire.getIntitule().toUpperCase());

        PeriodeDeclaration periodeDeclaration = DataAccess.getPeriodeDeclarationByCode(retraitDeclaration.getFkPeriode());
        if (periodeNames.equals(GeneralConst.EMPTY_STRING)) {
            periodeDeclaration = null;
        }
        //String periodeName = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(), articleBudgetaire.getPeriodicite().getCode());
        String periodeName = periodeNames;

        noteTaxationMock.setPeriodeDeclaration(periodeName.toUpperCase());

        noteTaxationMock.setAdresseBien(periodeDeclaration == null ? GeneralConst.EMPTY_STRING : periodeDeclaration.getAssujetissement().getBien().getFkAdressePersonne().getAdresse().toString().toUpperCase());

        noteTaxationMock.setNumeroDeclaration(retraitDeclaration.getCodeDeclaration().toUpperCase());

        String deviseLetter = GeneralConst.EMPTY_STRING;

        switch (retraitDeclaration.getDevise().toUpperCase()) {
            case GeneralConst.Devise.DEVISE_CDF:
                deviseLetter = propertiesConfig.getContent("TXT_CDF_LETTRE");
                break;
            case GeneralConst.Devise.DEVISE_USD:
                deviseLetter = propertiesConfig.getContent("TXT_USD_LETTRE");
                break;
        }

        String letterOne;
        String letterTwo;
        String chiffreEnLettre;

        BigDecimal partInteger = new BigDecimal("0");
        BigDecimal partVirgule = new BigDecimal("0");

        boolean virguleExist;

        if (retraitDeclaration.getEstPenalise() == GeneralConst.Numeric.ONE) {

            amountTaxation = amountTaxation.add(amountDeclaration.floatValue() == 0 ? retraitDeclaration.getMontant() : amountDeclaration);

        } else {

            if (retraitDeclaration.getPenaliteRemise().floatValue() > 0) {
                //amountTaxation = amountTaxation.add(retraitDeclaration.getPenaliteRemise());
                amountTaxation = amountTaxation.add(amountDeclaration.floatValue() == 0 ? retraitDeclaration.getMontant() : amountDeclaration);
            } else {
                amountTaxation = amountTaxation.add(amountDeclaration.floatValue() == 0 ? retraitDeclaration.getMontant() : amountDeclaration);
            }
        }

        String amount = String.format("%.2f", amountTaxation);

        amount = amount.replace(".", ",");

        if (amount != null && !amount.isEmpty()) {

            if (amount.contains(GeneralConst.VIRGULE)) {

                String[] amountSplit = amount.split(GeneralConst.VIRGULE);
                partInteger = BigDecimal.valueOf(Double.valueOf(amountSplit[0]));
                partVirgule = BigDecimal.valueOf(Double.valueOf(amountSplit[1]));
            }
        }

        if (partInteger.floatValue() > 0) {

            letterOne = FrenchNumberToWords.convert(partInteger);

            if (partVirgule.floatValue() > 0) {

                letterTwo = FrenchNumberToWords.convert(partVirgule);
                chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getContent("TXT_VIRGULE")
                        + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLetter;
                virguleExist = true;
            } else {
                chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLetter;
                virguleExist = false;
            }

            noteTaxationMock.setMontantLettre(chiffreEnLettre);

        } else {
            noteTaxationMock.setMontantLettre(GeneralConst.EMPTY_STRING);
            virguleExist = false;
        }

        if (virguleExist) {

            noteTaxationMock.setMontantChiffre(Tools.formatNombreToString(propertiesConfig.getContent("NUMBER_FORMAT"),
                    amountTaxation) + GeneralConst.SPACE + retraitDeclaration.getDevise());
        } else {
            noteTaxationMock.setMontantChiffre(Tools.formatNombreToString(propertiesConfig.getContent("NUMBER_FORMAT"),
                    amountTaxation)
                    + GeneralConst.Format.ZERO_FORMAT
                    + GeneralConst.SPACE + retraitDeclaration.getDevise());
        }

        banque = DataAccess.getBanqueByCode(retraitDeclaration.getFkBanque());

        noteTaxationMock.setBanque(banque == null ? GeneralConst.EMPTY_STRING : banque.getIntitule().toUpperCase());

        compteBancaire = DataAccess.getCompteBancaireByCode_V2(retraitDeclaration.getFkCompteBancaire());

        noteTaxationMock.setCompteBancaire(compteBancaire == null ? GeneralConst.EMPTY_STRING : compteBancaire.getIntitule().toUpperCase());

        return dataXhtm = createDocument(noteTaxationMock, "NOTE_TAXATION_DECLARATION", false);
    }

    StreamedContent file = null;

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    String createDocument(Object printObject, String typeDocument, boolean exist) {

        String contenuXhtml = null;
        String reImpression;
        String documentContent = GeneralConst.SPACE;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;

        try {
            switch (typeDocument.trim()) {

                case "NP":

                    NotePerceptionPrint np = (NotePerceptionPrint) printObject;
                    if (np != null) {
                        if (!exist) {

                            contenuXhtml = propertiesConfig.getContent("MODEL_NP_DRHKAT");
                            reImpression = GeneralConst.NumberString.ONE;

                            documentContent = DocumentReturnString.getDocument(
                                    "",
                                    printObject,
                                    contenuXhtml, reImpression,
                                    GeneralConst.NumberString.ONE);

                            counter++;
                            bulkQuery.put(counter + ":EXEC F_NEW_ARCHIVE_WEB ?1,?2,?3,?4,?5,?6", new Object[]{
                                np.getNumeroNotePerception().trim(),
                                "NOTE DE PERCEPTION",
                                documentContent,
                                "Impression de la note de perception : " + np.getNumeroNotePerception().trim(),
                                np.getNumeroNotePerception().trim(),
                                "NOTE DE PERCEPTION"
                            });

                            saveArchive = DataAccess.executeQueryBulkInsert(bulkQuery);

                        } else {
                            Archive archive = getArchiveByRefDocument(np.getNumeroNotePerception().trim());
                            if (archive != null) {
                                documentContent = archive.getDocumentString();
                            }

                        }
                    }

                    break;

                case "NOTE_TAXATION_DECLARATION":

                    NoteTaxationMock noteTaxationMock = (NoteTaxationMock) printObject;

                    if (noteTaxationMock != null) {

                        contenuXhtml = propertiesConfig.getContent("MODEL_NOTE_TAXATION_BIEN_IMMOBILIER");
                        reImpression = GeneralConst.NumberString.ONE;

                        documentContent = DocumentReturnString.getDocument(
                                "ntDoc",
                                printObject,
                                contenuXhtml, reImpression,
                                GeneralConst.NumberString.ONE);

                        counter++;
                        bulkQuery.put(counter + ":EXEC F_NEW_ARCHIVE_WEB ?1,?2,?3,?4,?5,?6", new Object[]{
                            noteTaxationMock.getNumeroDeclaration(),
                            "NOTE DE PAIEMENT",
                            documentContent,
                            "Impression de la note de paiement : " + noteTaxationMock.getNumeroNoteTaxation(),
                            noteTaxationMock.getNumeroNoteTaxation(),
                            "NOTE DE PAIEMENT DE COMMANDE VOUCHER"
                        });

                        saveArchive = DataAccess.executeQueryBulkInsert(bulkQuery);
                    }
                    break;

                case "NPCV":

                    VoucherMock voucherMock = (VoucherMock) printObject;

                    if (voucherMock != null) {

                        switch (Integer.valueOf(propertiesConfig.getContent("IS_PEAGE_LUALABA"))) {
                            case 0:
                                contenuXhtml = propertiesConfig.getContent("MODEL_NOTE_PAIEMENT_COMMANDE");
                                break;
                            case 1:
                                contenuXhtml = propertiesConfig.getContent("MODEL_NOTE_PAIEMENT_COMMANDE_LUALABA");
                                break;
                            case 2:
                                contenuXhtml = propertiesConfig.getContent("MODEL_NOTE_PAIEMENT_COMMANDE_ACGT");
                                break;
                        }

                        reImpression = GeneralConst.NumberString.ONE;

                        documentContent = DocumentReturnString.getDocument(
                                "ntDoc",
                                printObject,
                                contenuXhtml, reImpression,
                                GeneralConst.NumberString.ONE);

                        counter++;
                        bulkQuery.put(counter + ":EXEC F_NEW_ARCHIVE_TRACKINGDOC_VOUCHER ?1,?2", new Object[]{
                            documentContent,
                            voucherMock.getReference()
                        });
                        saveArchive = DataAccess.executeQueryBulkInsert(bulkQuery);
                    }
                    break;

            }
        } catch (Exception e) {
            throw e;
        }

        return documentContent;
    }

    public String createNotePaiementVoucher(Commande commande) {

        VoucherMock voucherMock = new VoucherMock();

        String url = propertiesConfig.getContent("URL_TRACKING_DOC").concat(commande.getReference() + "");
        String typeNote = propertiesConfig.getContent("TYPE_NOTE_DPGP");

        switch (Integer.valueOf(propertiesConfig.getContent("IS_PEAGE_LUALABA"))) {
            case 0:
                voucherMock.setFlag(propertiesConfig.getContent("LOGO_DPGP"));
                break;
            case 1:
                voucherMock.setFlag(propertiesConfig.getContent("LOGO_SOPEL"));
                url = propertiesConfig.getContent("URL_TRACKING_DOC_LUALABA").concat(commande.getReference() + "");
                typeNote = propertiesConfig.getContent("TYPE_NOTE_SOPEL");
                break;
            case 2:
                voucherMock.setFlag(propertiesConfig.getContent("LOGO_ACGT"));
                url = propertiesConfig.getContent("URL_TRACKING_DOC_ACGT").concat(commande.getReference() + "");
                typeNote = propertiesConfig.getContent("TYPE_NOTE_ACGT");
                break;
        }

        voucherMock.setNumeroNote(commande.getId());

        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            voucherMock.setCodeQR(codeQR);
        } else {
            voucherMock.setCodeQR(GeneralConst.EMPTY_STRING);
        }

        voucherMock.setDateNote(ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));
        voucherMock.setReference(commande.getReference());

        Personne personne = DataAccess.getPersonneByCode_V2(commande.getFkPersonne().getCode());

        if (personne != null) {

            voucherMock.setContribuable(personne.toString().toUpperCase());

            voucherMock.setNif(personne.getNif() == null ? GeneralConst.EMPTY_STRING : personne.getNif().toUpperCase());

            Adresse adresse = DataAccess.getAdresseDefaultByAssujetti(personne.getCode());

            if (adresse != null) {
                voucherMock.setAdresse(adresse.toString().toUpperCase());
            } else {
                voucherMock.setAdresse(GeneralConst.EMPTY_STRING);
            }

        } else {
            voucherMock.setContribuable(GeneralConst.EMPTY_STRING);
            voucherMock.setNif(GeneralConst.EMPTY_STRING);
        }

        voucherMock.setFkAb(typeNote.toUpperCase());

        String deviseLetter = GeneralConst.EMPTY_STRING;

        switch (commande.getDevise().getCode().toUpperCase()) {
            case GeneralConst.Devise.DEVISE_CDF:
                deviseLetter = propertiesConfig.getContent("TXT_CDF_LETTRE");
                break;
            case GeneralConst.Devise.DEVISE_USD:
                deviseLetter = propertiesConfig.getContent("TXT_USD_LETTRE");
                break;
        }

        String letterOne;
        String letterTwo;
        String chiffreEnLettre;

        BigDecimal partInteger = new BigDecimal("0");
        BigDecimal partVirgule = new BigDecimal("0");

        boolean virguleExist;

        BigDecimal amountTaxation = commande.getMontant();

        String amount = String.format("%.2f", amountTaxation);

        amount = amount.replace(".", ",");

        if (amount != null && !amount.isEmpty()) {

            if (amount.contains(GeneralConst.VIRGULE)) {

                String[] amountSplit = amount.split(GeneralConst.VIRGULE);
                partInteger = BigDecimal.valueOf(Double.valueOf(amountSplit[0]));
                partVirgule = BigDecimal.valueOf(Double.valueOf(amountSplit[1]));
            }
        }

        if (partInteger.floatValue() > 0) {

            letterOne = FrenchNumberToWords.convert(partInteger);

            if (partVirgule.floatValue() > 0) {

                letterTwo = FrenchNumberToWords.convert(partVirgule);
                chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getContent("TXT_VIRGULE")
                        + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLetter;
                virguleExist = true;
            } else {
                chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLetter;
                virguleExist = false;
            }

            voucherMock.setMontantLettre(chiffreEnLettre);

        } else {
            voucherMock.setMontantLettre(GeneralConst.EMPTY_STRING);
            virguleExist = false;
        }

        if (virguleExist) {

            voucherMock.setMontantChiffre(Tools.formatNombreToString(propertiesConfig.getContent("NUMBER_FORMAT"),
                    amountTaxation) + GeneralConst.SPACE + commande.getDevise().getCode());
        } else {
            voucherMock.setMontantChiffre(Tools.formatNombreToString(propertiesConfig.getContent("NUMBER_FORMAT"),
                    amountTaxation)
                    + GeneralConst.Format.ZERO_FORMAT
                    + GeneralConst.SPACE + commande.getDevise().getCode());
        }

        Banque banque = DataAccess.getBanqueByCode(commande.getFkCompteBancaire().getBanque().getCode());

        voucherMock.setBanque(banque == null ? GeneralConst.EMPTY_STRING : banque.getIntitule().toUpperCase());

        CompteBancaire compteBancaire = DataAccess.getCompteBancaireByCode_V3(commande.getFkCompteBancaire().getCode());

        voucherMock.setCompteBancaire(compteBancaire == null ? GeneralConst.EMPTY_STRING : compteBancaire.getIntitule().toUpperCase());

        return createDocument(voucherMock, "NPCV", false);

    }

}
