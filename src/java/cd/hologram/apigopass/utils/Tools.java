/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.utils;

import cd.hologram.apigopass.business.DataAccess;
import cd.hologram.apigopass.constants.GeneralConst;
import cd.hologram.apigopass.entities.Agent;
import cd.hologram.apigopass.entities.DetailsNc;
import cd.hologram.apigopass.entities.NoteCalcul;
import cd.hologram.apigopass.entities.PeriodeDeclaration;
import cd.hologram.apigopass.entities.Site;
import cd.hologram.apigopass.mock.ComplementsPersonnePhysique;
import static cd.hologram.apigopass.utils.ConvertDate.formatDateToStringOfFormat;
import cd.hologram.tokengenerator.business.Token;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Base64.Decoder;
import java.util.Locale;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author moussa.toure
 */
public class Tools {

    static final String DATA_IMAGE_JPEG_BASE64_TEXT = "data:image/jpeg;base64,";
    static final String BASE64_TEXT = GeneralConst.Format.FORMAT_IMAGE_BASE64;

    public static String sha1Encode(String token) {
        String result = org.apache.commons.codec.digest.DigestUtils.sha1Hex(token);
        return result;
    }

    public static Date addDayTodate(Date date, Integer day, boolean islegale) {

        Date newDate = null;

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            if (islegale) {
                calendar.add(Calendar.DATE, -day);
            } else {
                calendar.add(Calendar.DATE, day);
            }
            newDate = calendar.getTime();

        } catch (Exception e) {
            throw e;
        }

        return newDate;
    }

    public static Date addDayTodate_V2(Date date, Integer day) {

        Date newDate = null;

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, day);
            newDate = calendar.getTime();

        } catch (Exception e) {
            throw e;
        }

        return newDate;
    }

    public static String getLibelleByLangue(String contenu, String langue) {

        String result = GeneralConst.EMPTY_STRING;
        try {
            JSONObject jsonContenu = new JSONObject(contenu);
            result = jsonContenu.getString(langue);

        } catch (JSONException e) {
            e.getStackTrace();
        }
        return result;

    }

    public static String getLibelleByLangue(String contenu, String langue, boolean hasHabitant) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            JSONObject jsonContenu = new JSONObject(contenu);

            if (!hasHabitant) {

                String intituleJson = jsonContenu.getString("intitule");
                JSONObject jsonIntitule = new JSONObject(intituleJson);
                result = jsonIntitule.getString(langue);
            } else {

                String habitantJson = jsonContenu.getString("habitant");
                JSONObject jsonHabitant = new JSONObject(habitantJson);
                result = jsonHabitant.getString(langue);
            }

        } catch (JSONException e) {
            e.getStackTrace();
        }
        return result;

    }

    public static String getByteaToBase64String(byte[] bytea) {
        String photoStr = GeneralConst.EMPTY_STRING;
        try {
            if (bytea != null) {
                StringBuilder sb = new StringBuilder();
                sb.append(GeneralConst.DATA_IMAGE_JPEG_BASE64_TEXT);
                sb.append(StringUtils.newStringUtf8(Base64.encodeBase64(bytea, false)));
                photoStr = sb.toString();
            }
        } catch (Exception e) {
            return GeneralConst.EMPTY_STRING;
        }
        return photoStr;
    }

    public static String getNumeralByNumber(int number) {

        String numeral = GeneralConst.EMPTY_STRING;

        switch (number) {

            case 1:
                numeral = GeneralConst.NumeralString.UN;
                break;
            case 2:
                numeral = GeneralConst.NumeralString.DEUX;
                break;
            case 3:
                numeral = GeneralConst.NumeralString.TROIS;
                break;
            case 4:
                numeral = GeneralConst.NumeralString.QUATRE;
                break;
            case 5:
                numeral = GeneralConst.NumeralString.CINQ;
                break;
            case 6:
                numeral = GeneralConst.NumeralString.SIX;
                break;
            case 7:
                numeral = GeneralConst.NumeralString.SEPT;
                break;
            case 8:
                numeral = GeneralConst.NumeralString.HUIT;
                break;
            case 9:
                numeral = GeneralConst.NumeralString.NEUF;
                break;
            case 10:
                numeral = GeneralConst.NumeralString.DIX;
                break;
            case 11:
                numeral = GeneralConst.NumeralString.ONZE;
                break;
            case 12:
                numeral = GeneralConst.NumeralString.DOUZE;
                break;
            case 13:
                numeral = GeneralConst.NumeralString.TREIZE;
                break;
            case 14:
                numeral = GeneralConst.NumeralString.QUATORZE;
                break;
            case 15:
                numeral = GeneralConst.NumeralString.QUIZE;
                break;
            case 16:
                numeral = GeneralConst.NumeralString.SIEZE;
                break;
            case 17:
                numeral = GeneralConst.NumeralString.DIX_SEPT;
                break;
            case 18:
                numeral = GeneralConst.NumeralString.DIX_HUIT;
                break;
            case 19:
                numeral = GeneralConst.NumeralString.DIX_NEUF;
                break;
            case 20:
                numeral = GeneralConst.NumeralString.VINGT;
                break;
            case 21:
                numeral = GeneralConst.NumeralString.VINGT_UN;
                break;
            case 22:
                numeral = GeneralConst.NumeralString.VINGT_DEUXIEME;
                break;
            case 23:
                numeral = GeneralConst.NumeralString.VINGT_TROIS;
                break;
            case 24:
                numeral = GeneralConst.NumeralString.VINGT_QUATRE;
                break;
            case 25:
                numeral = GeneralConst.NumeralString.VINGT_CINQ;
                break;
            case 26:
                numeral = GeneralConst.NumeralString.VINGT_SIX;
                break;
            case 27:
                numeral = GeneralConst.NumeralString.VINGT_SEPT;
                break;
            case 28:
                numeral = GeneralConst.NumeralString.VINGT_HUIT;
                break;
            case 29:
                numeral = GeneralConst.NumeralString.VINGT_NEUF;
                break;
            case 30:
                numeral = GeneralConst.NumeralString.TRENTE;
                break;
            case 31:
                numeral = GeneralConst.NumeralString.TRENTE_UN;
                break;

        }

        return numeral;
    }

    public static String getMoisByCode(String code) {

        String mois = GeneralConst.EMPTY_STRING;

        switch (code.trim()) {
            case GeneralConst.CalendarMonth.JANVIER_CODE_1:
            case GeneralConst.CalendarMonth.JANVIER_CODE_2:
                mois = GeneralConst.CalendarMonth.JANVIER_NAME;
                break;
            case GeneralConst.CalendarMonth.FEVRIER_CODE_1:
            case GeneralConst.CalendarMonth.FEVRIER_CODE_2:
                mois = GeneralConst.CalendarMonth.FEVRIER_NAME;
                break;
            case GeneralConst.CalendarMonth.MARS_CODE_1:
            case GeneralConst.CalendarMonth.MARS_CODE_2:
                mois = GeneralConst.CalendarMonth.MARS_NAME;
                break;
            case GeneralConst.CalendarMonth.AVRIL_CODE_1:
            case GeneralConst.CalendarMonth.AVRIL_CODE_2:
                mois = GeneralConst.CalendarMonth.AVRIL_NAME;
                break;
            case GeneralConst.CalendarMonth.MAI_CODE_1:
            case GeneralConst.CalendarMonth.MAI_CODE_2:
                mois = GeneralConst.CalendarMonth.MAI_NAME;
                break;
            case GeneralConst.CalendarMonth.JUIN_CODE_1:
            case GeneralConst.CalendarMonth.JUIN_CODE_2:
                mois = GeneralConst.CalendarMonth.JUIN_NAME;
                break;
            case GeneralConst.CalendarMonth.JUILLET_CODE_1:
            case GeneralConst.CalendarMonth.JUILLET_CODE_2:
                mois = GeneralConst.CalendarMonth.JUILLET_NAME;
                break;
            case GeneralConst.CalendarMonth.AOUT_CODE_1:
            case GeneralConst.CalendarMonth.AOUT_CODE_2:
                mois = GeneralConst.CalendarMonth.AOUT_NAME;
                break;
            case GeneralConst.CalendarMonth.SEPTEMBRE_CODE_1:
            case GeneralConst.CalendarMonth.SEPTEMBRE_CODE_2:
                mois = GeneralConst.CalendarMonth.SEPTEMBRE_NAME;
                break;
            case GeneralConst.CalendarMonth.OCTOBRE_CODE:
                mois = GeneralConst.CalendarMonth.OCTOBRE_NAME;
                break;
            case GeneralConst.CalendarMonth.NOVEMEBRE_CODE:
                mois = GeneralConst.CalendarMonth.NOVEMEBRE_NAME;
                break;
            case GeneralConst.CalendarMonth.DECMEBRE_CODE:
                mois = GeneralConst.CalendarMonth.DECMEBRE_NAME;
                break;
        }

        return mois;
    }

    public static String getValidFormat(String date) {
        String result;
        String[] parts = date.split("-");
        String part1 = parts[GeneralConst.Numeric.ZERO];
        String part2 = parts[GeneralConst.Numeric.ONE];
        String part3 = parts[GeneralConst.Numeric.TWO];
        result = part3 + part2 + part1;
        return result;
    }

    public static String formatDateToString(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat(GeneralConst.Format.FORMAT_DATE);
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String formatDateToStringV1(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat(GeneralConst.Format.FORMAT_DATE_WITH_HOUR_V1);
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }
    public static String formatDateToStringV3(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat(GeneralConst.Format.FORMAT_DATE_WITH_HOUR_V2);
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String formatCloseWhereForIn(List<String> params) {

        int size = params.size();
        String in = "(";
        for (int i = 0; i < size; i++) {

            in += ((i + 1) == size)
                    ? "'" + params.get(i) + "')"
                    : "'" + params.get(i) + "',";
        }
        return in;
    }

    public static boolean isCustumerAuthorize(String token, String custumerCode, String AppCode) throws FileNotFoundException {
        try {
            Token objToken = new Token();
            boolean isCustumerCodeValid = objToken.getCustumerCode(token).equals(custumerCode);
            boolean isTokenValid = objToken.verifyToken(token, AppCode);
            return isTokenValid == isCustumerCodeValid;
        } catch (Exception e) {
            return false;
        }
    }

//    public static String getPeriodeIntitule(Date date, String periodeCode) {
//
//        try {
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(date);
//
//            int day, year, month;
//
//            String periode = GeneralConst.EMPTY_STRING;
//
//            day = calendar.get(Calendar.DATE);
//            month = calendar.get(Calendar.MONTH);
//            year = calendar.get(Calendar.YEAR);
//            month++;
//            String monthName = Tools.getMoisByCode(month + GeneralConst.EMPTY_STRING);
//
//            switch (periodeCode) {
//
//                case GeneralConst.Periodicite_NbreJour.JOUR:
//                    periode = String.format("%s/%s/%s", day, month, year);
//                    break;
//                case GeneralConst.Periodicite_NbreJour.MONTH:
//                    periode = String.format("%s %s", monthName, year);
//                    break;
//                case GeneralConst.Periodicite.TRIME:
//                    periode = String.format("%s %s", monthName, year);
//                    break;
//                case GeneralConst.Periodicite.BIMENS:
//                    month += GeneralConst.Numeric.ONE;
//                    periode = String.format("%s %s <br/> %s %s",
//                            monthName, year,
//                            Tools.getMoisByCode(month + GeneralConst.EMPTY_STRING), year);
//                    break;
//                case GeneralConst.Periodicite.SEMS:
//                    periode = String.format("%s %s", monthName, year);
//                    break;
//                case GeneralConst.Periodicite_NbreJour.YEAR:
//                    periode = String.format("%s", year);
//                    break;
//                default:
//
//            }
//
//            return periode.toUpperCase();
//        } catch (Exception e) {
//            throw e;
//        }
//    }
    public static String getPeriodeIntitule(Date date, String periodeCode) {

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            int day, year, month;

            String periode = GeneralConst.EMPTY_STRING;

            day = calendar.get(Calendar.DATE);
            month = calendar.get(Calendar.MONTH);
            year = calendar.get(Calendar.YEAR);
            month++;
            String monthName = Tools.getMoisByCode(month + GeneralConst.EMPTY_STRING);

            switch (periodeCode) {

                case GeneralConst.Periodicite.JOUR:
                case GeneralConst.Periodicite.PONC:
                    periode = String.format("%s/%s/%s", day, month, year);
                    break;
                case GeneralConst.Periodicite.MONTH:
                    periode = String.format("%s %s", monthName, year);
                    break;
                case GeneralConst.Periodicite.TRIME:
                    periode = String.format("%s %s", monthName, year);
                    break;
                case GeneralConst.Periodicite.BIMENS:
                    month += GeneralConst.Numeric.ONE;
                    periode = String.format("%s %s <br/> %s %s",
                            monthName, year,
                            Tools.getMoisByCode(month + GeneralConst.EMPTY_STRING), year);
                    break;
                case GeneralConst.Periodicite.SEMS:
                    periode = String.format("%s %s", monthName, year);
                    break;
                case GeneralConst.Periodicite.YEAR:
                    periode = String.format("%s", year);
                    break;
                default:

            }

            return periode.toUpperCase();
        } catch (Exception e) {
            throw e;
        }
    }

    public static String convertToXML(Class c, Object o) {
        String xml;
        try {

            JAXBContext context = JAXBContext.newInstance(c);
            Marshaller m = context.createMarshaller();

            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            StringWriter sw = new StringWriter();
            m.marshal(o, sw);
            return sw.toString();

        } catch (Exception e) {
            xml = GeneralConst.EMPTY_STRING;
        }

        return xml;
    }

    public static String getValidFormatDatePrint(String date) {
        String result = GeneralConst.EMPTY_STRING;

        if (date != null && !date.isEmpty()) {
            String[] parts = date.split("-");
            String part1 = parts[0];
            String part2 = parts[1];
            String part3 = parts[2];
            result = part3 + "/" + part2 + "/" + part1;
        }

        return result;
    }

    public static String getValidFormatV2(String date) {
        String result;
        String[] parts = date.split("-");
        String part1 = parts[0];
        String part2 = parts[1];
        String part3 = parts[2];
        result = part3 + part2 + part1;
        return result;
    }

    public static String getNameFile(List<?> currentObject) {
        for (int j = 0; j < currentObject.size(); j++) {
            List<Field> currentField = getPublicFields(currentObject.get(j).getClass());
            for (int i = 0; i < currentField.size(); i++) {
                String fieldStr = currentField.get(i).getName();
                String formatedFieldName = String.valueOf(fieldStr.charAt(0)).toUpperCase() + fieldStr.substring(1);
            }
        }
        return null;
    }

    public static String getNameFile(Object currentObject) throws InvocationTargetException {

        List<Field> currentField = getPrivateFields(currentObject.getClass());
        for (int i = 0; i < currentField.size(); i++) {
            Field field = currentField.get(i);
            String fieldStr = field.getName();
            String valeur = getValue(currentObject, fieldStr);
            System.out.println("");

        }

        return null;

    }

    public static List<Field> getPublicFields(Class<?> theClass) {
        List<Field> publicFields = new ArrayList<>();
        Field[] fields = theClass.getDeclaredFields();

        for (Field field : fields) {
            if (Modifier.isPublic(field.getModifiers())) {
                publicFields.add(field);
            }
        }
        return publicFields;
    }

    public static List<Field> getPrivateFields(Class<?> theClass) {
        List<Field> privateFields = new ArrayList<>();
        Field[] fields = theClass.getDeclaredFields();

        for (Field field : fields) {
            if (Modifier.isPrivate(field.getModifiers())) {
                privateFields.add(field);
            }
        }
        return privateFields;
    }

    public static String getValue(Object currentObject, String fieldName) {
        String valueStr = "";
        try {

            Method m = currentObject.getClass().getMethod("get" + fieldName.trim());
            valueStr = String.valueOf(m.invoke(currentObject));
        } catch (NoSuchMethodException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
            valueStr = "Aucune donnée";
        }
        return valueStr;
    }

    public static void main(String[] args) {

//        String mail = "<html><head></head><body style=\"background-color: #f6f6f6;font-family: sans-serif;font-size:14px\"><div style=\"background-color: #ffffff;box-sizing: border-box;display: block;margin: 0 auto;max-width: 580px;padding: 20px;border-radius: 3px;margin-top:40px;box-shadow:8px 10px 0px rgba(0,0,0,0.1)\"><hr style=\"background-color:#0085c7;height:1px;margin-bottom:1px;margin-top:10px\" /> <br/> Bonjour <b>%s</b>, <br/> <br/> Nous vous confirmons la création de votre compte avec succès. <br/> <br/> Dorénavant, vous pouvez faire la demande de vos documents d'état-civil et de la population en ligne. <br/> <br/> Ci-dessous vos informations de connexion à l'application. <br/> <br/> Login : <b>%s</b> <br/> Votre code de validation : <b>%s</b> <br/> <br/> Merci, l'équipe eMairie <br/> <br/></div><div style=\"clear: both;margin-top: 10px;text-align: center;color: #999999;font-size:12px\"> <br/> <span>&copy; Copyright 2019&nbsp;&nbsp;</span><a href=\"https://hologram.cd\">Hologram Identification Services</a></div></body></html>";
//        mail = String.format(mail, "ab", "45", 54);
        ComplementsPersonnePhysique cpp = new ComplementsPersonnePhysique();
        cpp.setCF000000000000052015_birthDay("18/06/1989");
        try {
            getNameFile(cpp);

            //System.out.println(mail);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(Tools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Date getEcheanceDate(NoteCalcul noteCalcul, boolean addEcheance,
            int nbreJour) {

        Date dateEchance = null;

        try {

            if (!noteCalcul.getDetailsNcList().isEmpty()) {

                DetailsNc detailsNc = noteCalcul.getDetailsNcList().get(GeneralConst.Numeric.ZERO);

                if (detailsNc.getPeriodeDeclaration() != null) {

                    if (detailsNc.getPenaliser() == GeneralConst.Numeric.ZERO) {

                        PeriodeDeclaration periodeDeclaration = noteCalcul.getDetailsNcList().get(
                                GeneralConst.Numeric.ZERO).getPeriodeDeclaration();

                        if (periodeDeclaration.getDateLimite() != null) {

                            dateEchance = periodeDeclaration.getDateLimite();

                        } else {

                            if (addEcheance) {

                                dateEchance = getEcheanceDate(new Date(), nbreJour);

                            }
                        }

                    } else {

                        if (addEcheance) {

                            dateEchance = getEcheanceDate(new Date(), nbreJour);

                        }

                    }

                } else {

                    if (addEcheance) {

                        dateEchance = getEcheanceDate(new Date(), nbreJour);

                    }

                }

            }

        } catch (Exception e) {
            throw e;
        }

        return dateEchance;
    }

    public static Date getEcheanceDate(Date dateEcheance, int NbrJour, boolean isLegal) {

        Date dateUser = dateEcheance;

        boolean canGo = true, isDayOff, isSunday;

        while (canGo) {

            isDayOff = checkDayOff(dateUser);
            isSunday = checkSunday(dateUser);

            canGo = isDayOff || isSunday;

            if (canGo) {

                dateUser = addDayTodate(dateUser, NbrJour, isLegal);

            } else {

                canGo = false;

            }

        }

        return dateUser;
    }

    public static Date getEcheanceDate(Date date, int nbreJour) {

        Date dateEchance = null;

        try {

            dateEchance = addDayTodate(date, nbreJour, false);

            dateEchance = getEcheanceDate(dateEchance, GeneralConst.Numeric.ONE, false);

        } catch (Exception e) {
            throw e;
        }

        return dateEchance;
    }

    static boolean checkSunday(Date date) {

        boolean isSundy = false;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            isSundy = true;
        }

        return isSundy;
    }

    static boolean checkDayOff(Date date) {

        boolean isDayOff = false;

//        List<JourFerie> dates = GeneralBusiness.getDaysOff();
//
//        for (JourFerie jourFerie : dates) {
//
//            if (jourFerie.getType().equals(GeneralConst.Number.ONE)) {
//
//                String userDate = getDatepartv2(date);
//                String dayOff = getDatepartv2(jourFerie.getDateJourFerie());
//
//                isDayOff = userDate.equals(dayOff);
//                if (isDayOff) {
//                    break;
//                }
//
//            } else {
//
//                isDayOff = jourFerie.getDateJourFerie().compareTo(date) == GeneralConst.Numeric.ZERO;
//
//                if (isDayOff) {
//                    break;
//                }
//
//            }
//
//        }
        return isDayOff;

    }

    public static String generateRandomValue(int size) {
        String generatedString = RandomStringUtils.randomAlphanumeric(size);
        return generatedString;
    }

    public static String getListToStringV2(String siteValueList) {

        String result = GeneralConst.EMPTY_STRING;
        result = siteValueList.replace("\"", "")
                .replace("[", GeneralConst.EMPTY_STRING)
                .replace("]", GeneralConst.EMPTY_STRING);

        return result;
    }

    public static String generateDocumentNumber(String codeAgent, String typeDocument, String codeDocument) {

        String document_Incr = codeDocument;
        String requestIncr = GeneralConst.EMPTY_STRING;

        String sqlQuery = GeneralConst.EMPTY_STRING;

        String documentNumero = GeneralConst.EMPTY_STRING;
        Agent agent = DataAccess.getAgentByCode(codeAgent);
        String codeSite = agent.getSite().getCode();
        String documentPrefix = GeneralConst.EMPTY_STRING;

        switch (typeDocument.trim()) {
            case "AMR":
            case "AMR_OF_INVIT_PAYER":
                documentPrefix = "AMR";
                sqlQuery = "UPDATE T_AMR SET NUMERO_DOCUMENT = '%s' WHERE NUMERO = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_AMR('" + codeSite + "')";
                break;
            case "COMMANDEMENT":
                documentPrefix = "CMD";
                sqlQuery = "UPDATE T_COMMANDEMENT SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_COMMANDEMENT('" + codeSite + "')";
                break;
            case "CONTRAINTE":
                documentPrefix = "CNT";
                sqlQuery = "UPDATE T_CONTRAINTE SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_CONTRAINTE('" + codeSite + "')";
                break;
            case "MED":
            case "INVITATION_SERVICE":
                documentPrefix = "ISA";
                sqlQuery = "UPDATE T_MED SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_MED('INVITATION_SERVICE','" + codeSite + "')";
                break;
            case "INVITATION_PAIEMENT":
                documentPrefix = "IAP";
                sqlQuery = "UPDATE T_MED SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_MED('INVITATION_PAIEMENT','" + codeSite + "')";
                break;
            case "SERVICE":
                documentPrefix = "ISB";
                sqlQuery = "UPDATE T_MED SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_MED('SERVICE','" + codeSite + "')";
                break;
            case "RELANCE":
                documentPrefix = "RL";
                sqlQuery = "UPDATE T_MED SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_MED('RELANCE','" + codeSite + "')";
                break;
            case "PAIEMENT":
                sqlQuery = "UPDATE T_MED SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_MED('PAIEMENT','" + codeSite + "')";
                break;
            case "ATD":
                documentPrefix = "ATD";
                sqlQuery = "UPDATE T_ATD SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_ATD('" + codeSite + "')";
                break;
        }

        document_Incr = incr(DataAccess.getLastIncrement(requestIncr), 4);
        Site site = DataAccess.getSiteByCode(codeSite);
        documentNumero = "DRHKAT" + "/" + site.getFkDivision().getSigle().trim() + "/" + site.getSigle() + "/" + documentPrefix.concat(document_Incr);

        sqlQuery = String.format(sqlQuery, documentNumero, codeDocument);

        if (DataAccess.updateNumeroDoc(sqlQuery)) {
            return documentNumero;
        } else {
            return codeDocument;
        }
    }

    public static String incr(int intCode, int nbrOrdrSize) {

        String key = GeneralConst.EMPTY_STRING;
        String zero = GeneralConst.EMPTY_STRING;;
        try {
            intCode += 1;
            int taille = String.valueOf(intCode).length();

            for (int i = 1; i < nbrOrdrSize - (taille - 1); i++) {
                zero += "0";
            }
            key = zero + intCode;

        } catch (Exception e) {

            for (int i = 1; i < nbrOrdrSize; i++) {
                zero += "0";
            }
            key = zero + 1;
        }
        return key;
    }

    static public String formatNombreToString(String pattern, BigDecimal value) {

        NumberFormat nf = NumberFormat.getNumberInstance(new Locale("fr", "FR"));
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern(pattern);
        String output = df.format(value);
        return output;

    }

    public static String getDeviseByNC(NoteCalcul noteCalcul) {

        String devise = GeneralConst.EMPTY_STRING;

        try {
            List<DetailsNc> detailsNcList = noteCalcul.getDetailsNcList();
            if (detailsNcList != null && !detailsNcList.isEmpty()) {
                devise = detailsNcList.get(0).getDevise().getCode();
                if (devise == null) {
                    devise = GeneralConst.EMPTY_STRING;
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return devise;
    }

    public static Date formatStringFullToDateV2(String date) {

        DateFormat formatter;
        Date formattedDate = null;
        formatter = new SimpleDateFormat(GeneralConst.Format.FORMAT_DATE, Locale.FRENCH);
        try {
            formattedDate = formatter.parse(date);
        } catch (ParseException e) {
        }
        return formattedDate;
    }

    public static String getListToString(String args) {

        String result = GeneralConst.EMPTY_STRING;
        result = args.replace("[", "").replace("]", "").replace("\"", "");

        return result;
    }

    public static String encoder(String origin) {
        java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
        String encodedString = encoder.encodeToString(origin.getBytes());
        return encodedString;
    }
    
    public static String decoder(String encodedString) {
        Decoder decoder = java.util.Base64.getDecoder();
        byte[] bytes = decoder.decode(encodedString);
        return new String(bytes);
    }
    
    public static String saveFile(String imageBase64, int reference, String userId) {

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String result = "", fileName, pathMedia, pathMediaName = null, extension = null, date = formatDateToStringOfFormat(new Date(), "dd-MM-yyyy");
        File file = null, domainFile = new File(System.getProperty("com.sun.aas.instanceRoot"));

        try {

            final String PATH_ALL_MEDIAS = "/";
            pathMedia = domainFile.getAbsolutePath() + PATH_ALL_MEDIAS;

            String tstamp1 = randomTenNumber();
            String tstamp2 = String.valueOf(timestamp.getTime());

            if (reference == -3) {

                file = new File(pathMedia +"/ARCHIVE");
                pathMediaName = tstamp1 + "/" + userId + "/" + date + "/" + tstamp2;
                extension = (reference == -3) ? ".pdf" : ".jpg";

            } else if (reference == 1 || reference == 2)  {

                file = new File(pathMedia + "ARCHIVE");
               pathMediaName = tstamp1 + "/" + userId + "/" + date + "/" + tstamp2;
                extension = ".jpg";

            }  

            boolean isOk = true;
            if (!file.exists()) {
                isOk = file.mkdirs();
            }

            if (isOk) {

                fileName = YbgCrypt.getCrypto(pathMediaName).concat(extension);
                String absolutePath = file.getAbsolutePath().concat("/").concat(fileName);
                imageBase64 = imageBase64.split(";")[1].split(",")[1];
                byte[] data = Base64.decodeBase64(imageBase64);
                try (OutputStream stream = new FileOutputStream(absolutePath)) {
                    stream.write(data);
                }

                result = absolutePath;
            }

        } catch (Exception e) {
            e.getStackTrace();
            result = "";
        }
        return result;
    }
    
    public static String randomTenNumber() {

        Random r = new Random();
        int low = 1000000000;
        int high = 2147483647;
        int result = r.nextInt(high - low) + low;
        return String.valueOf(result);

    }
    
    public static String encoderAll(String filePath, String Extension) {
        String base64File = "";
        if (Extension.equals("jpg") || Extension.equals("jpeg") || Extension.equals("png")) {
            base64File = "data:image/png;base64,";
        }

        File file = new File(filePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            byte fileData[] = new byte[(int) file.length()];
            imageInFile.read(fileData);
            base64File += Base64.encodeBase64String(fileData);
        } catch (FileNotFoundException e) {
            return "";
        } catch (IOException ioe) {
            return "";
        }
        if (Extension.equals("pdf")) {
            base64File = "data:application/pdf;base64," + base64File;
        }
        return base64File;
    }

}
