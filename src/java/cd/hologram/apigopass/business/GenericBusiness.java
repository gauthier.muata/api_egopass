/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.business;

import cd.hologram.apigopass.dao.Dao;
import cd.hologram.apigopass.entities.Aeroport;
import cd.hologram.apigopass.entities.Compagnie;
import cd.hologram.apigopass.entities.DetailsGoPass;
import cd.hologram.apigopass.utils.Casting;
import cd.hologram.apigopass.utils.MailSender;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author Lenovo
 */
public class GenericBusiness {

    private static final Dao myDao = new Dao();

    public static List<Compagnie> getCompagnieList() {
        try {
            String query = "select * from t_compagnie where etat=1 order by intitule asc ";
            List<Compagnie> datatransaction = (List<Compagnie>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Compagnie.class), false);
            return datatransaction;
        } catch (Exception e) {
            throw e;
        }
    }

    public String savecompagnieBusiness(Compagnie compagnie) {
        try {
            String result;
            HashMap<String, Object> storedProcedureParams = new HashMap<>();

            storedProcedureParams.put("Intitule", compagnie.getIntitule());
            storedProcedureParams.put("Sigle", compagnie.getSigle());
            storedProcedureParams.put("Logo", compagnie.getLogo());

            result = myDao.getDaoImpl().getStoredProcedureOutput("F_NEW_COMPAGNIE", "ret", storedProcedureParams);
            return result;
        } catch (Exception error) {
            return "-1";
        }
    }

    public static List<Aeroport> getAeroportList() {
        try {
            String query = "select * from T_AEROPORT  where etat=1 order by intitule asc ";
            List<Aeroport> dataaeroport = (List<Aeroport>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Aeroport.class), false);
            return dataaeroport;
        } catch (Exception e) {
            throw e;
        }
    }

    public String activeGoPassBusiness(DetailsGoPass detail) {
        try {
            String result;
            HashMap<String, Object> storedProcedureParams = new HashMap<>();

            storedProcedureParams.put("CodeGoPass", detail.getCodeGoPass());
            storedProcedureParams.put("Passager", detail.getPassager());
            storedProcedureParams.put("SexePassager", detail.getSexePassager());
            storedProcedureParams.put("compagnie", detail.getFkCompagnie().getIdCompagnie());
            storedProcedureParams.put("DateVol", detail.getDateVol());
            storedProcedureParams.put("NumeroVol", detail.getNumeroVol());
            storedProcedureParams.put("aeroport_provenance", detail.getFkAeroportProvenance().getIdAeroport());
            storedProcedureParams.put("aeroport_destination", detail.getFkAeroportDestination().getIdAeroport());

            result = myDao.getDaoImpl().getStoredProcedureOutput("F_ACTIVE_GO_PASS", "ret", storedProcedureParams);
            return result;
        } catch (Exception error) {
            return "-1";
        }
    }
    
    public static String sendNotification(String... objet) {
        String ret = "0";
        try {
            String mailContent = objet[0];
            String mailSubjet = objet[1];
            String emailReceiver = objet[2];
//        String emailCc = objet[3];

            MailSender mailSender = new MailSender();
            mailSender.setDate(new Date());
            mailSender.setHost("https://api.mailjet.com/v3.1/send");
            mailSender.setSubject(mailSubjet);
            mailSender.setMessage(mailContent);
            mailSender.setReciever(emailReceiver);
            mailSender.setSender("christianbrowser@gmail.com");

            mailSender.sendMailWithAPI("871f9b49e6e1b2b97effb44343d070df", "064a4d19b59f3934182fbfcd19e4eae5");
//            mailSender.sendMailWithAPI("2d99ce4a704360a9f4677d64dd071a99", "0484b6789801328ed3c5be1a623b8157");
            ret = "1";

        } catch (Exception e) {
            System.out.println(e.fillInStackTrace() +" Message : " + e.getMessage() + " --Cause : "+ e.getCause());    
            ret = "0";
        }
        return ret;

    }
    
     public String FormatMail(String TitleMessage, String Paragraphe1, String Paragraphe2) {
        String messageFormat = "";
        String Header = "<html><link rel='stylesheet' href='css/mm_travel2.css type='text/css' />";

        String HeaderMessage = "<table width='47%' border='0' align='center' cellpadding='0' cellspacing='0'><tr bgcolor='#3366CC'><td width='593' height='63' id='logo' valign='bottom' align='center' nowrap='nowrap'><strong style ='font:24px Verdana, Arial, Helvetica, sans-serif;color: #CCFF99;letter-spacing:.2em;line-height:30px;'>" + TitleMessage + "</strong> </td></tr><tr bgcolor='#3366CC'><td height='19' id='tagline' valign='top' align='center'></td></tr><tr bgcolor='#3366CC'></tr><tr></tr><tr>";

        String Body = "<td colspan='2' valign='top'><br /><br /><table width='500' border='0' align='center' cellpadding='2' cellspacing='0'><tr><td class='bodyText'><p style='font:11px Verdana, Arial, Helvetica, sans-serif;color:#003366;line-height:20px;margin-top:0px;'>" + Paragraphe1 + "</p><p style='font:11px Verdana, Arial, Helvetica, sans-serif;color:#003366;line-height:20px;margin-top:0px;'>" + Paragraphe2 + "</p></td></tr></table></td></tr><br />";

        String Footer = "<br><tr bgcolor='#FFFFFF'><td height='19'></td></tr><tr bgcolor='#FFFFFF'><td height='19'></td></tr><tr bgcolor='#3366CC'><td height='19'><p class='Style1'><center>Hologram Identification Services - RVA </p></td></tr></table></body></html>";

        messageFormat = Header + HeaderMessage + Body + Footer;
//        messageFormat = "<html><table width='47%' border='0' align='center' cellpadding='0' cellspacing='0'><tr bgcolor='#3366CC'><td height='19' id='tagline' valign='top' align='center'><p>June 2021</p></td></tr></table></html>";

        return messageFormat;
    }
     
     public String GetDashBoard(JSONObject param) {
        try {

            String result;
            HashMap<String, Object> storedProcedureParams = new HashMap<>();

            storedProcedureParams.put("DateDebut", param.get("DateDebut"));
            storedProcedureParams.put("DateFin", param.get("DateFin"));
            storedProcedureParams.put("Compagnie", param.get("Compagnie"));
            storedProcedureParams.put("Aeroport", param.get("Aeroport"));

            result = myDao.getDaoImpl().getStoredProcedureOutput("GetDashBoard", "ret", storedProcedureParams);
            return result;
        } catch (Exception error) {
            return "-1";
        }
    }
     
     public String GetDashBoardCompagnie(JSONObject param) {
        try {

            String result;
            HashMap<String, Object> storedProcedureParams = new HashMap<>();

            storedProcedureParams.put("DateDebut", param.get("DateDebut"));
            storedProcedureParams.put("DateFin", param.get("DateFin"));
            storedProcedureParams.put("Compagnie", param.get("Compagnie"));
            storedProcedureParams.put("Aeroport", param.get("Aeroport"));

            result = myDao.getDaoImpl().getStoredProcedureOutput("GetDashBoardCompagnie", "ret", storedProcedureParams);
            return result;
        } catch (Exception error) {
            return "-1";
        }
    }
     
     public String GetVoyageurList(JSONObject param) {
        try {

            String result;
            HashMap<String, Object> storedProcedureParams = new HashMap<>();

            storedProcedureParams.put("DateDebut", param.get("DateDebut"));
            storedProcedureParams.put("DateFin", param.get("DateFin"));
            storedProcedureParams.put("Compagnie", param.get("Compagnie"));
            storedProcedureParams.put("Aeroport", param.get("Aeroport"));

            result = myDao.getDaoImpl().getStoredProcedureOutput("GetListVoyageurCompagnie", "ret", storedProcedureParams);
            return result;
        } catch (Exception error) {
            return "-1";
        }
    }

}
