/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.business;

import static cd.hologram.apigopass.business.GisBusiness.responseMessage;
import cd.hologram.apigopass.constants.GeneralConst;
import cd.hologram.apigopass.constants.PropertiesConst;
import cd.hologram.apigopass.entities.AbComplementBien;
import cd.hologram.apigopass.entities.Acquisition;
import cd.hologram.apigopass.entities.Adresse;
import cd.hologram.apigopass.entities.AdressePersonne;
import cd.hologram.apigopass.entities.Agent;
import cd.hologram.apigopass.entities.Archive;
import cd.hologram.apigopass.entities.ArticleBudgetaire;
import cd.hologram.apigopass.entities.Assujeti;
import cd.hologram.apigopass.entities.AxePeage;
import cd.hologram.apigopass.entities.Bien;
import cd.hologram.apigopass.entities.Bordereau;
import cd.hologram.apigopass.entities.CarteGoPass;
import cd.hologram.apigopass.entities.Commande;
import cd.hologram.apigopass.entities.CommandeCarte;
import cd.hologram.apigopass.entities.Complement;
import cd.hologram.apigopass.entities.ComplementBien;
import cd.hologram.apigopass.entities.ComplementForme;
import cd.hologram.apigopass.entities.CompteBancaire;
import cd.hologram.apigopass.entities.DetailsGoPass;
import cd.hologram.apigopass.entities.DetailsNc;
import cd.hologram.apigopass.entities.Devise;
import cd.hologram.apigopass.entities.Division;
import cd.hologram.apigopass.entities.EntiteAdministrative;
import cd.hologram.apigopass.entities.Etat;
import cd.hologram.apigopass.entities.FormeJuridique;
import cd.hologram.apigopass.entities.GoPass;
import cd.hologram.apigopass.entities.IcmParam;
import cd.hologram.apigopass.entities.LoginWeb;
import cd.hologram.apigopass.entities.NoteCalcul;
import cd.hologram.apigopass.entities.NotePerception;
import cd.hologram.apigopass.entities.Palier;
import cd.hologram.apigopass.entities.PeriodeDeclaration;
import cd.hologram.apigopass.entities.Periodicite;
import cd.hologram.apigopass.entities.Personne;
import cd.hologram.apigopass.entities.RetraitDeclaration;
import cd.hologram.apigopass.entities.Service;
import cd.hologram.apigopass.entities.Site;
import cd.hologram.apigopass.entities.Tarif;
import cd.hologram.apigopass.entities.TarifSite;
import cd.hologram.apigopass.entities.Taux;
import cd.hologram.apigopass.entities.TypeBien;
import cd.hologram.apigopass.entities.TypeBienService;
import cd.hologram.apigopass.entities.TypeComplement;
import cd.hologram.apigopass.entities.TypeComplementBien;
import cd.hologram.apigopass.entities.Unite;
import cd.hologram.apigopass.entities.UsageBien;
import cd.hologram.apigopass.entities.ValeurPredefinie;
import cd.hologram.apigopass.entities.Voucher;
import cd.hologram.apigopass.mock.CommandeGoPassSpontanneeMock;
import cd.hologram.apigopass.mock.ComplementMockIF;
import cd.hologram.apigopass.mock.DeclarationMock;
import cd.hologram.apigopass.mock.DeclarationPenaliteMock;
import cd.hologram.apigopass.mock.DetailCommande;
import cd.hologram.apigopass.mock.LogUser;
import cd.hologram.apigopass.mock.Periode;
import cd.hologram.apigopass.mock.PeriodeDeclarationSelection;
import cd.hologram.apigopass.mock.VoucherMock;
import cd.hologram.apigopass.properties.PropertiesConfig;
import cd.hologram.apigopass.properties.PropertiesMail;
import cd.hologram.apigopass.utils.Compare;
import cd.hologram.apigopass.utils.ConvertDate;
import cd.hologram.apigopass.utils.MailSender;
import cd.hologram.apigopass.utils.PeriodiciteData;
import cd.hologram.apigopass.utils.PrintDocument;
import cd.hologram.apigopass.utils.QRCodeGenerate;
import cd.hologram.apigopass.utils.ResponseMessage;
import cd.hologram.apigopass.utils.SMSSender;
import cd.hologram.apigopass.utils.Tools;
import cd.hologram.apigopass.utils.ZipFolder;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author emmanuel.tsasa
 */
public class AssujBusiness implements ServletContextListener {

    static DataAccess dataAccess;
    static JSONArray complementKeys;
    static List<ComplementBien> complementBienTempList;
    static PropertiesConfig propertiesConfig;
    static PropertiesMail propertiesMail;
    final static String PROPERTY_TYPE = "PropertyType";
    final static String REQUERANT = "Requerant";
    final static String PROPERTY_NAME = "PropertyName";
    final static String COMPLEMENTS = "Complements";
    final static String ADRESS = "Adress";
    final static String COLOR = "Color";
    final static String HORSE_POWER = "Horsepower";
    final static String WEIGHT = "Weight";
    final static String BRAND = "Brand";
    final static String REGISTRATION_NUMBER = "RegistrationNumber";
    final static String LAND_REGISTRY_NUMBER = "LandRegistryNumber";
    final static String CHASSI_NUMBER = "ChassiNumber";
    final static String CUSTUMER_ID = "CustumerId";
    final static String BIEN_ID = "BienId";
    final static String AMOUNT = "Amount";
    final static String CURRENCY = "Currency";
    final static String ACQUISITION_DATE = "AcquisitionDate";
    final static String TAX_ID = "TaxId";
    final static String TARIF_ID = "TarifId";
    final static String CONFIG_KEYS_COMPLEMENTS = "CONFIG_KEYS_COMPLEMENTS";
    final static String KEY = "key";
    final static String ASSUJETTISABLE = "assujettisable";
    final static String CODE = "code";
    final static String OPERATION = "Operation";
    final static String YEAR = "Year";
    final static String MONTH = "Month";
    final static String CENTER = "center";

    static List<ComplementBien> infoComplementaires;
    private ScheduledExecutorService scheduler;

    public static String getLoginWeb(String file) throws Exception {

        JSONObject responseJson = new JSONObject();
        JSONObject object = new JSONObject(file);

        try {

            String operation = object.getString("operation");
            String username = object.getString("ntd");
            String password = object.getString("password");

            if (operation.equals(GeneralConst.NumberString.ONE)) {

                LoginWeb loginWeb = DataAccess.getLoginWeb(username, password);

                if (loginWeb != null) {

                    Personne personne = loginWeb.getPersonne();

                    responseJson.put("code", 100);
                    responseJson.put(GeneralConst.ParamName.etat, loginWeb.getEtat());
                    responseJson.put(GeneralConst.ParamName.nif, loginWeb.getFkPersonne());
                    responseJson.put(GeneralConst.ParamName.ntd, personne.getLoginWeb().getUsername());
                    responseJson.put(GeneralConst.ParamName.fullname, personne.toString());
                    responseJson.put(GeneralConst.ParamName.phoneNumber, personne.getLoginWeb() != null ? personne.getLoginWeb().getTelephone() : GeneralConst.EMPTY_STRING);
                    responseJson.put(GeneralConst.ParamName.email, personne.getLoginWeb() != null ? personne.getLoginWeb().getMail() : GeneralConst.EMPTY_STRING);
                    responseJson.put(GeneralConst.ParamName.type, personne.getFormeJuridique() != null ? personne.getFormeJuridique().getCode() : GeneralConst.EMPTY_STRING);
                    responseJson.put(GeneralConst.ParamName.LibelleType, personne.getFormeJuridique() != null ? personne.getFormeJuridique().getIntitule() : GeneralConst.EMPTY_STRING);

                } else {

                    responseJson.put("code", 200);
                    responseJson.put("message", "not found");
                }

            } else {

                LoginWeb loginWeb = DataAccess.getLoginWeb(username);

                if (loginWeb != null) {

                    responseJson.put("Authorize", true);

                } else {

                    responseJson.put("Authorize", false);
                }

            }

        } catch (Exception e) {
            throw e;
        }

        return responseJson.toString();
    }

    public static String getLoginWebByNom(String nom) throws Exception {

        List<JSONObject> listPersone = new ArrayList<>();
        String response = GeneralConst.EMPTY_STRING;

        try {
            List<LoginWeb> lws = DataAccess.getLoginWebByNom(nom);
            if (!lws.isEmpty()) {
                for (LoginWeb lw : lws) {
                    JSONObject objet = new JSONObject();
                    objet.put("code", lw.getPersonne().getCode());
                    objet.put("nom", lw.getPersonne().getNom());
                    objet.put("postnom", lw.getPersonne().getPostnom());
                    objet.put("prenom", lw.getPersonne().getPrenoms());
                    objet.put("mail", lw.getMail());
                    objet.put("phone", lw.getTelephone());
                    objet.put("etat", lw.getEtat());
                    objet.put("ntd", lw.getUsername());
                    listPersone.add(objet);
                }
                response = listPersone.toString();
            } else {
                JSONObject json = new JSONObject();
                json.put("code", 200);
                response = json.toString();
            }
        } catch (Exception e) {
            JSONObject json = new JSONObject();
            json.put("code", 300);
            response = json.toString();
        }

        return response;
    }

    public static String setDemandeInscription(String object) throws Exception {

        JSONObject jobject = new JSONObject(object);

        try {

            String email = jobject.getString("mail");
            String phone = jobject.getString("phone");
            String nif = jobject.getString("code");

            LoginWeb loginWeb = DataAccess.getLoginWebByTelephoneOrEmail(email, phone);
            if (loginWeb != null) {
                if (!loginWeb.getFkPersonne().equals(nif)) {
                    if (email.equals(loginWeb.getMail()) && phone.equals(loginWeb.getTelephone())) {
                        return "3";
                    } else {
                        if (email.equals(loginWeb.getMail())) {
                            return GeneralConst.ResultCode.EMAIL_ADRESS_EXIST;
                        } else if (phone.equals(loginWeb.getTelephone())) {
                            return GeneralConst.ResultCode.PHONE_NUMBER_EXIST;
                        }
                    }
                }
            }

            if (DataAccess.setDemandeInscription(email, phone, nif)) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String getAssujetti(String file, int version, boolean all) throws Exception {

        JSONObject objGetPersonne;
        String response = GeneralConst.EMPTY_STRING;
        propertiesConfig = new PropertiesConfig();

        BigDecimal tauxHA = new BigDecimal(propertiesConfig.getContent(PropertiesConst.Config.TAUX_ICM));

        try {

            List<Personne> personnes;

            if (version == 1) {

                objGetPersonne = new JSONObject(file);

                String firstName = objGetPersonne.getString(GeneralConst.ParamName.firstName);
                String lastname = objGetPersonne.getString(GeneralConst.ParamName.lastname);
                String middleName = objGetPersonne.getString(GeneralConst.ParamName.middleName);
                String birthday = objGetPersonne.getString(GeneralConst.ParamName.birthday);
                String phoneNumber = objGetPersonne.getString(GeneralConst.ParamName.phoneNumber);
                String searchMode = objGetPersonne.getString(GeneralConst.ParamName.modeSearch);

                String complementForme = "CF000000000000052015";

                personnes = DataAccess.getListPersonne(lastname, middleName, firstName, birthday, complementForme, "1");

            } else {

                personnes = DataAccess.getPersonneById(file);
            }

            List<JSONObject> jSONObjects = new ArrayList<>();
            List<JSONObject> jsonAdressList = new ArrayList<>();
            List<JSONObject> jsonBienList = new ArrayList<>();

            for (Personne personne : personnes) {

                if (personne.getLoginWeb() == null) {
                    continue;
                }

                JSONObject jsono = new JSONObject();

                jsono.put(GeneralConst.ParamName.nif, personne.getCode());
                jsono.put(GeneralConst.ParamName.fullname, personne.toString());
                jsono.put(GeneralConst.ParamName.phoneNumber, personne.getLoginWeb() != null ? personne.getLoginWeb().getTelephone() : GeneralConst.EMPTY_STRING);
                jsono.put(GeneralConst.ParamName.email, personne.getLoginWeb() != null ? personne.getLoginWeb().getMail() : GeneralConst.EMPTY_STRING);
                jsono.put(GeneralConst.ParamName.type, personne.getFormeJuridique() != null ? personne.getFormeJuridique().getCode() : GeneralConst.EMPTY_STRING);
                jsono.put(GeneralConst.ParamName.LibelleType, personne.getFormeJuridique() != null ? personne.getFormeJuridique().getIntitule() : GeneralConst.EMPTY_STRING);

                List<AdressePersonne> adressePersonnes = personne.getAdressePersonneList();

                for (AdressePersonne adressePersonne : adressePersonnes) {

                    JSONObject jsonAdress = new JSONObject();

                    jsonAdress.put(GeneralConst.ParamName.id, adressePersonne.getCode());
                    jsonAdress.put(GeneralConst.ParamName.Libelle, adressePersonne.getAdresse().toString());

                    jsonAdressList.add(jsonAdress);

                }

                if (all) {

                    List<Acquisition> acquisitions = DataAccess.getBiensPersonne(personne.getCode().trim());

                    for (Acquisition acquisition : acquisitions) {

                        Bien bien = acquisition.getBien();

                        JSONObject jsonBien = new JSONObject();

                        jsonBien.put(GeneralConst.ParamName.id, bien.getId());
                        jsonBien.put(GeneralConst.ParamName.Libelle, bien.getIntitule());
                        jsonBien.put(GeneralConst.ParamName.LibelleAdresse, bien.getFkAdressePersonne().getAdresse().toString());
                        jsonBien.put(GeneralConst.ParamName.LibelleAdresse, bien.getFkAdressePersonne().getAdresse().toString());

                        List<Assujeti> assujetiList = bien.getAssujetiList();
                        List<JSONObject> assujetiListJson = new ArrayList<>();

                        for (Assujeti assujeti : assujetiList) {

                            JSONObject jsonAss = new JSONObject();
                            List<JSONObject> periodeList = new ArrayList<>();

                            jsonAss.put(GeneralConst.ParamName.ab, assujeti.getArticleBudgetaire().getCode());

                            List<PeriodeDeclaration> listPeriodeDeclarations = DataAccess.getListPeriodeDeclarationByAssuj(assujeti.getId());

                            long moisRetard = 0;

                            int annee = 0;

                            if (listPeriodeDeclarations.size() > 0) {

                                for (PeriodeDeclaration periode : listPeriodeDeclarations) {

                                    annee++;

                                    IcmParam icmParam = DataAccess.getIcmParamByTarifAndAnnee(periode.getAssujetissement().getBien().getFkTarif(), annee);

                                    JSONObject jsonPeriode = new JSONObject();

                                    jsonPeriode.put("periodeId", periode.getId());
                                    jsonPeriode.put("periode", Tools.getPeriodeIntitule(periode.getDebut(),
                                            periode.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode()));

                                    if (icmParam != null) {

                                        jsonPeriode.put("tauxPeriode", icmParam.getTaux());
                                        jsonPeriode.put("devise", icmParam.getDevise());
                                        jsonPeriode.put("icmParamId", icmParam.getId());

                                        BigDecimal valeurCuConvertiEnHA = new BigDecimal("0");

                                        valeurCuConvertiEnHA = valeurCuConvertiEnHA.add(periode.getAssujetissement().getValeur().multiply(tauxHA));

                                        BigDecimal valeurICM = new BigDecimal("0");

                                        valeurICM = valeurICM.add(valeurCuConvertiEnHA.multiply(icmParam.getTaux()));

                                        jsonPeriode.put("tauxCumule", valeurICM);

                                    } else {

                                        jsonPeriode.put("tauxPeriode", 0);
                                        jsonPeriode.put("devise", GeneralConst.EMPTY_STRING);
                                        jsonPeriode.put("icmParamId", GeneralConst.EMPTY_STRING);
                                    }

                                    if (periode.getDateLimite() != null) {

                                        if (Compare.before(periode.getDateLimite(), new Date())) {

                                            moisRetard = DataAccess.getDateDiffBetwenTwoDates(ConvertDate.formatDateToString(periode.getDateLimite()));

                                        } else {
                                            moisRetard = 0;
                                        }

                                    } else {

                                        if (Compare.before(periode.getFin(), new Date())) {
                                            moisRetard = DataAccess.getDateDiffBetwenTwoDates(ConvertDate.formatDateToString(periode.getFin()));

                                        } else {
                                            moisRetard = 0;
                                        }
                                    }

                                    jsonPeriode.put("moisRetard", moisRetard);
                                    jsonPeriode.put("estPenalise", moisRetard == 0 ? 0 : 1);

                                    int resultRecidiviste = DataAccess.checkRecidivisteDeclaration(
                                            periode.getAssujetissement().getId(), periode.getId());

                                    jsonPeriode.put("estRecidiviste", resultRecidiviste == 0 ? 0 : 1);

                                    periodeList.add(jsonPeriode);
                                }
                            }

                            jsonAss.put(GeneralConst.ParamName.PeriodeList, periodeList.toString());

                            assujetiListJson.add(jsonAss);
                        }

                        jsonBien.put(GeneralConst.ParamName.Assuj, assujetiListJson.toString());

                        jsonBienList.add(jsonBien);
                    }
                }

                jsono.put(GeneralConst.ParamName.AdresseList, jsonAdressList.toString());
                jsono.put(GeneralConst.ParamName.BienList, jsonBienList.toString());

                jSONObjects.add(jsono);
            }

            response = jSONObjects.toString();

        } catch (Exception e) {
            throw e;
        }

        return response;
    }

    public static String getAssujetti_v2(String file, String codeAb, int version, boolean all) throws Exception {

        JSONObject objGetPersonne;
        String response = GeneralConst.EMPTY_STRING;
        propertiesConfig = new PropertiesConfig();

        BigDecimal tauxHA = new BigDecimal(propertiesConfig.getContent(PropertiesConst.Config.TAUX_ICM));

        try {

            List<Personne> personnes;

            if (version == 1) {

                objGetPersonne = new JSONObject(file);

                String firstName = objGetPersonne.getString(GeneralConst.ParamName.firstName);
                String lastname = objGetPersonne.getString(GeneralConst.ParamName.lastname);
                String middleName = objGetPersonne.getString(GeneralConst.ParamName.middleName);
                String birthday = objGetPersonne.getString(GeneralConst.ParamName.birthday);
                String phoneNumber = objGetPersonne.getString(GeneralConst.ParamName.phoneNumber);
                String searchMode = objGetPersonne.getString(GeneralConst.ParamName.modeSearch);

                String complementForme = "CF000000000000052015";

                personnes = DataAccess.getListPersonne(lastname, middleName, firstName, birthday, complementForme, "1");

            } else {

                personnes = DataAccess.getPersonneById(file);
            }

            List<JSONObject> jSONObjects = new ArrayList<>();
            List<JSONObject> jsonAdressList = new ArrayList<>();
            List<JSONObject> jsonBienList = new ArrayList<>();

            for (Personne personne : personnes) {

                if (personne.getLoginWeb() == null) {
                    continue;
                }

                JSONObject jsono = new JSONObject();

                jsono.put(GeneralConst.ParamName.nif, personne.getCode());
                jsono.put(GeneralConst.ParamName.fullname, personne.toString());
                jsono.put(GeneralConst.ParamName.phoneNumber, personne.getLoginWeb() != null ? personne.getLoginWeb().getTelephone() : GeneralConst.EMPTY_STRING);
                jsono.put(GeneralConst.ParamName.email, personne.getLoginWeb() != null ? personne.getLoginWeb().getMail() : GeneralConst.EMPTY_STRING);
                jsono.put(GeneralConst.ParamName.type, personne.getFormeJuridique() != null ? personne.getFormeJuridique().getCode() : GeneralConst.EMPTY_STRING);
                jsono.put(GeneralConst.ParamName.LibelleType, personne.getFormeJuridique() != null ? personne.getFormeJuridique().getIntitule() : GeneralConst.EMPTY_STRING);

                List<AdressePersonne> adressePersonnes = personne.getAdressePersonneList();

                for (AdressePersonne adressePersonne : adressePersonnes) {

                    JSONObject jsonAdress = new JSONObject();

                    jsonAdress.put(GeneralConst.ParamName.id, adressePersonne.getCode());
                    jsonAdress.put(GeneralConst.ParamName.Libelle, adressePersonne.getAdresse().toString());

                    jsonAdressList.add(jsonAdress);

                }

                if (all) {

                    List<Acquisition> acquisitions = DataAccess.getBiensPersonne(personne.getCode().trim());

                    for (Acquisition acquisition : acquisitions) {

                        Bien bien = acquisition.getBien();

                        List<Assujeti> assujetiList = bien.getAssujetiList();
                        List<JSONObject> assujetiListJson = new ArrayList<>();

                        for (Assujeti assujeti : assujetiList) {

                            if (assujeti.getArticleBudgetaire().getCode().equals(codeAb)) {

                                JSONObject jsonAss = new JSONObject();
                                List<JSONObject> periodeList = new ArrayList<>();

                                JSONObject jsonBien = new JSONObject();

                                jsonBien.put(GeneralConst.ParamName.id, bien.getId());
                                jsonBien.put(GeneralConst.ParamName.Libelle, bien.getIntitule());
                                jsonBien.put(GeneralConst.ParamName.LibelleAdresse, bien.getFkAdressePersonne().getAdresse().toString());
                                jsonBien.put("codeAb", codeAb);

                                jsonAss.put(GeneralConst.ParamName.ab, assujeti.getArticleBudgetaire().getCode());

                                List<PeriodeDeclaration> listPeriodeDeclarations = DataAccess.getListPeriodeDeclarationByAssuj(assujeti.getId());

                                long moisRetard = 0;

                                int annee = 0;

                                if (listPeriodeDeclarations.size() > 0) {

                                    for (PeriodeDeclaration periode : listPeriodeDeclarations) {

                                        annee++;

                                        IcmParam icmParam = DataAccess.getIcmParamByTarifAndAnnee(periode.getAssujetissement().getBien().getFkTarif(), annee);

                                        JSONObject jsonPeriode = new JSONObject();

                                        jsonPeriode.put("periodeId", periode.getId());
                                        jsonPeriode.put("periode", Tools.getPeriodeIntitule(periode.getDebut(),
                                                periode.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode()));

                                        if (icmParam != null) {

                                            jsonPeriode.put("tauxPeriode", icmParam.getTaux());
                                            jsonPeriode.put("devise", icmParam.getDevise());
                                            jsonPeriode.put("icmParamId", icmParam.getId());

                                            BigDecimal valeurCuConvertiEnHA = new BigDecimal("0");

                                            valeurCuConvertiEnHA = valeurCuConvertiEnHA.add(periode.getAssujetissement().getValeur().multiply(tauxHA));

                                            BigDecimal valeurICM = new BigDecimal("0");

                                            valeurICM = valeurICM.add(valeurCuConvertiEnHA.multiply(icmParam.getTaux()));

                                            jsonPeriode.put("tauxCumule", valeurICM);

                                        } else {

                                            jsonPeriode.put("tauxPeriode", 0);
                                            jsonPeriode.put("devise", GeneralConst.EMPTY_STRING);
                                            jsonPeriode.put("icmParamId", GeneralConst.EMPTY_STRING);
                                        }

                                        if (periode.getDateLimite() != null) {

                                            if (Compare.before(periode.getDateLimite(), new Date())) {

                                                moisRetard = DataAccess.getDateDiffBetwenTwoDates(ConvertDate.formatDateToString(periode.getDateLimite()));

                                            } else {
                                                moisRetard = 0;
                                            }

                                        } else {

                                            if (Compare.before(periode.getFin(), new Date())) {
                                                moisRetard = DataAccess.getDateDiffBetwenTwoDates(ConvertDate.formatDateToString(periode.getFin()));

                                            } else {
                                                moisRetard = 0;
                                            }
                                        }

                                        jsonPeriode.put("moisRetard", moisRetard);
                                        jsonPeriode.put("estPenalise", moisRetard == 0 ? 0 : 1);

                                        int resultRecidiviste = DataAccess.checkRecidivisteDeclaration(
                                                periode.getAssujetissement().getId(), periode.getId());

                                        jsonPeriode.put("estRecidiviste", resultRecidiviste == 0 ? 0 : 1);

                                        periodeList.add(jsonPeriode);
                                    }
                                }

                                jsonAss.put(GeneralConst.ParamName.PeriodeList, periodeList.toString());

                                assujetiListJson.add(jsonAss);

                                jsonBien.put(GeneralConst.ParamName.Assuj, assujetiListJson.toString());

                                jsonBienList.add(jsonBien);

                            }
                        }

                    }
                }

                jsono.put(GeneralConst.ParamName.AdresseList, jsonAdressList.toString());
                jsono.put(GeneralConst.ParamName.BienList, jsonBienList.toString());

                jSONObjects.add(jsono);
            }

            response = jSONObjects.toString();

        } catch (Exception e) {
            throw e;
        }

        return response;
    }

    public static String getAssujetti_v3(String file, String codeAb, int version, boolean all) throws Exception {

        List<JSONObject> jSONObjects = new ArrayList<>();
        List<JSONObject> jsonAdressList = new ArrayList<>();
        List<JSONObject> jsonBienList = new ArrayList<>();
        List<JSONObject> jsonCompteBancaireList = new ArrayList<>();

        JSONObject objGetPersonne;

        String response = GeneralConst.EMPTY_STRING;
        propertiesConfig = new PropertiesConfig();

        BigDecimal tauxHA = new BigDecimal(propertiesConfig.getContent(PropertiesConst.Config.TAUX_ICM));
        int periodeGrace = new Integer(propertiesConfig.getContent(PropertiesConst.Config.PERIODE_GRACE_EN_COURS));
        String banqueCode = propertiesConfig.getContent("COMPTE_TDT");

        try {

            List<CompteBancaire> cbs = DataAccess.getCompteBancaireByBanqueCode(banqueCode);
            if (!cbs.isEmpty()) {
                for (CompteBancaire cb : cbs) {
                    JSONObject object = new JSONObject();
                    object.put("code", cb.getCode());
                    object.put("intitule", cb.getIntitule());
                    object.put("devise", cb.getDevise().getCode());
                    object.put("intitule_devise", cb.getDevise().getIntitule());
                    object.put("codeBanque", cb.getBanque().getCode());
                    jsonCompteBancaireList.add(object);
                }
            }

            Taux tauxChange = DataAccess.getTauxByDevise("USD");

            List<Personne> personnes;

            if (version == 1) {

                objGetPersonne = new JSONObject(file);

                String firstName = objGetPersonne.getString(GeneralConst.ParamName.firstName);
                String lastname = objGetPersonne.getString(GeneralConst.ParamName.lastname);
                String middleName = objGetPersonne.getString(GeneralConst.ParamName.middleName);
                String birthday = objGetPersonne.getString(GeneralConst.ParamName.birthday);
                String phoneNumber = objGetPersonne.getString(GeneralConst.ParamName.phoneNumber);
                String searchMode = objGetPersonne.getString(GeneralConst.ParamName.modeSearch);

                String complementForme = "CF000000000000052015";

                personnes = DataAccess.getListPersonne(lastname, middleName, firstName, birthday, complementForme, "1");

            } else {

                personnes = DataAccess.getPersonneById(file);
            }

            for (Personne personne : personnes) {

                if (personne.getLoginWeb() == null) {
                    continue;
                }

                JSONObject jsono = new JSONObject();

                jsono.put(GeneralConst.ParamName.nif, personne.getCode());
                jsono.put(GeneralConst.ParamName.fullname, personne.toString());
                jsono.put(GeneralConst.ParamName.phoneNumber, personne.getLoginWeb() != null ? personne.getLoginWeb().getTelephone() : GeneralConst.EMPTY_STRING);
                jsono.put(GeneralConst.ParamName.email, personne.getLoginWeb() != null ? personne.getLoginWeb().getMail() : GeneralConst.EMPTY_STRING);
                jsono.put(GeneralConst.ParamName.type, personne.getFormeJuridique() != null ? personne.getFormeJuridique().getCode() : GeneralConst.EMPTY_STRING);
                jsono.put(GeneralConst.ParamName.LibelleType, personne.getFormeJuridique() != null ? personne.getFormeJuridique().getIntitule() : GeneralConst.EMPTY_STRING);

                Adresse adress = DataAccess.getAdresseDefaultByAssujetti(personne.getCode());

                jsono.put("adresse", adress == null ? DataAccess.getAdressePersonneByPersonne(personne.getCode()).toString() : adress.toString());

//                List<AdressePersonne> adressePersonnes = personne.getAdressePersonneList();
//
//                for (AdressePersonne adressePersonne : adressePersonnes) {
//
//                    JSONObject jsonAdress = new JSONObject();
//
//                    jsonAdress.put(GeneralConst.ParamName.id, adressePersonne.getCode());
//                    jsonAdress.put(GeneralConst.ParamName.Libelle, adressePersonne.getAdresse().toString());
//
//                    jsonAdressList.add(jsonAdress);
//
//                }
                if (all) {

                    List<Assujeti> assujetis = DataAccess.getAssujettissement(personne.getCode(), codeAb);

                    for (Assujeti assujeti : assujetis) {

                        Bien bien = assujeti.getBien();

                        List<JSONObject> assujetiListJson = new ArrayList<>();

                        JSONObject jsonBien = new JSONObject();

                        jsonBien.put("codeAssujetti", assujeti.getId());
                        jsonBien.put(GeneralConst.ParamName.id, bien.getId());
                        jsonBien.put(GeneralConst.ParamName.Libelle, bien.getIntitule());
                        jsonBien.put(GeneralConst.ParamName.LibelleAdresse, bien.getFkAdressePersonne().getAdresse().toString());
                        jsonBien.put("codeNature", bien.getTypeBien().getCode());
                        jsonBien.put("libelleNature", bien.getTypeBien().getIntitule());
                        jsonBien.put("valeurBase", assujeti.getValeur());
                        jsonBien.put("uniteValeurBase", assujeti.getUniteValeur());
                        jsonBien.put("tauxChange", tauxChange.getTaux());

                        String uniteCode, deviseCode;

                        switch (codeAb) {
                            case "00000000000002312021": //ICM
                            case "00000000000002312020": //ICM
                                jsonBien.put("taux", 0);
                                jsonBien.put("devise", "USD");

                                Tarif tarif = DataAccess.getTarifByCode(assujeti.getBien().getFkTarif());

                                jsonBien.put("tarif", tarif == null ? "" : tarif.getIntitule().toUpperCase());
                                jsonBien.put("tauxCumule", 0);
                                jsonBien.put("typeTaux", "F");
                                jsonBien.put("valeurBase", assujeti.getValeur());
                                jsonBien.put("uniteAssuj", assujeti.getUniteValeur());
                                break;
                            default:
                                boolean isPalier = assujeti.getArticleBudgetaire().getPalier();

                                Palier palier = DataAccess.getPalierForBienImmobilier(
                                        assujeti.getArticleBudgetaire().getCode().trim(),
                                        assujeti.getBien().getFkTarif(),
                                        assujeti.getPersonne().getFormeJuridique().getCode(),
                                        assujeti.getBien().getFkQuartier(),
                                        assujeti.getValeur().floatValue(), isPalier,
                                        assujeti.getBien().getTypeBien().getCode());

                                uniteCode = palier == null ? "" : palier.getUnite();

                                if (assujeti.getArticleBudgetaire().getCode().equals("00000000000002272020")) {
                                    deviseCode = palier == null ? "CDF" : palier.getDevise();
                                } else {
                                    deviseCode = assujeti.getUniteValeur().toUpperCase();
                                }
                                jsonBien.put("uniteAssuj", deviseCode);

                                BigDecimal taux = new BigDecimal(BigInteger.ZERO);

                                BigDecimal valeurBase = new BigDecimal(BigInteger.ONE);

                                if (palier != null) {

                                    if (palier.getTypeTaux().equals("F")) {

                                        if (assujeti.getValeur().floatValue() == 0) {

                                            taux = palier.getMultiplierValeurBase() == 0 ? palier.getTaux()
                                                    : palier.getTaux().multiply(valeurBase);

                                        } else {

                                            taux = palier.getMultiplierValeurBase() == 0 ? palier.getTaux()
                                                    : palier.getTaux().multiply(assujeti.getValeur());
                                        }

                                    } else {

                                        taux = assujeti.getValeur().multiply(palier.getTaux()).divide(BigDecimal.valueOf(GeneralConst.Numeric.CENT));
                                    }

                                    jsonBien.put("taux", palier.getTaux());

                                }

                                jsonBien.put("tarif", assujeti.getTarif().getIntitule());
                                jsonBien.put("tauxCumule", taux);
                                jsonBien.put("typeTaux", palier.getTypeTaux());

                                if (palier.getTypeTaux().equals("%")) {

                                    if (assujeti.getValeur().floatValue() > 0) {

                                        jsonBien.put("valeurBase", assujeti.getValeur());
                                    } else {
                                        jsonBien.put("valeurBase", 0);
                                    }

                                } else {
                                    jsonBien.put("valeurBase", 0);
                                }

                                jsonBien.put("devise", palier.getDevise());
                                break;
                        }

                        long moisRetard = 0;
                        int annee = 0;
                        int count = 0;

                        String partYearDateToday = ConvertDate.getYearPartDateFromDateParam(ConvertDate.formatDateToString(new Date()));

                        List<PeriodeDeclaration> listPeriodeDeclarations = DataAccess.getListPeriodeDeclarationByAssuj(assujeti.getId());

                        List<JSONObject> periodeList = new ArrayList<>();

                        if (listPeriodeDeclarations.size() > 0) {

                            for (PeriodeDeclaration periode : listPeriodeDeclarations) {
                                count++;
                                annee++;

                                IcmParam icmParam = DataAccess.getIcmParamByTarifAndAnnee(periode.getAssujetissement().getBien().getFkTarif(), annee);

                                JSONObject jsonPeriode = new JSONObject();

                                jsonPeriode.put("periodeId", periode.getId());
                                jsonPeriode.put("periode", Tools.getPeriodeIntitule(periode.getDebut(),
                                        periode.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode()));

                                if (icmParam != null) {

                                    jsonPeriode.put("tauxPeriode", icmParam.getTaux());
                                    jsonPeriode.put("devise", icmParam.getDevise());
                                    jsonPeriode.put("icmParamId", icmParam.getId());

                                    BigDecimal valeurCuConvertiEnHA = new BigDecimal("0");

                                    valeurCuConvertiEnHA = valeurCuConvertiEnHA.add(periode.getAssujetissement().getValeur().multiply(tauxHA));

                                    BigDecimal valeurICM = new BigDecimal("0");

                                    valeurICM = valeurICM.add(valeurCuConvertiEnHA.multiply(icmParam.getTaux()));

                                    jsonPeriode.put("tauxCumule", valeurICM);

                                } else {

                                    jsonPeriode.put("tauxPeriode", 0);
                                    jsonPeriode.put("devise", GeneralConst.EMPTY_STRING);
                                    jsonPeriode.put("icmParamId", GeneralConst.EMPTY_STRING);
                                }

                                if (periode.getDateLimite() != null) {

                                    if (Compare.before(periode.getDateLimite(), new Date())) {

                                        //moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates(ConvertDate.formatDateToString(periode.getDateLimite()));
                                        String partYearDateParm = ConvertDate.getYearPartDateFromDateParam(ConvertDate.formatDateToString(periode.getFin()));

                                        if (partYearDateParm.equals(partYearDateToday)) {
                                            moisRetard = DataAccess.getDateDiffBetwenTwoDates_V2(
                                                    ConvertDate.formatDateToString(periode.getDebut()), ConvertDate.formatDateToString(new Date()));
                                        } else {
                                            moisRetard = DataAccess.getDateDiffBetwenTwoDates_V2(
                                                    ConvertDate.formatDateToString(periode.getDebut()), ConvertDate.formatDateToString(periode.getFin()));
                                        }

                                        if (moisRetard == 11 && count > 1) {
                                            moisRetard = moisRetard + 1;
                                        }

                                    } else {
                                        moisRetard = 0;
                                    }

                                } else {

                                    if (Compare.before(periode.getFin(), new Date())) {

                                        //moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates(ConvertDate.formatDateToString(periode.getFin()));
                                        String partYearDateParm = ConvertDate.getYearPartDateFromDateParam(ConvertDate.formatDateToString(periode.getFin()));

                                        if (partYearDateParm.equals(partYearDateToday)) {
                                            moisRetard = DataAccess.getDateDiffBetwenTwoDates_V2(
                                                    ConvertDate.formatDateToString(periode.getDebut()), ConvertDate.formatDateToString(new Date()));
                                        } else {
                                            moisRetard = DataAccess.getDateDiffBetwenTwoDates_V2(
                                                    ConvertDate.formatDateToString(periode.getDebut()), ConvertDate.formatDateToString(periode.getFin()));
                                        }

                                        if (moisRetard == 11 && count > 1) {
                                            moisRetard = moisRetard + 1;
                                        }

                                    } else {
                                        moisRetard = 0;
                                    }
                                }

                                jsonPeriode.put("moisRetard", moisRetard);
                                jsonPeriode.put("estPenalise", moisRetard == 0 ? GeneralConst.NumberNumeric.ZERO : GeneralConst.NumberNumeric.ONE);

                                int resultRecidiviste = DataAccess.checkRecidivisteDeclaration(
                                        periode.getAssujetissement().getId(), periode.getId());

                                jsonPeriode.put("estRecidiviste", resultRecidiviste == 0 ? GeneralConst.NumberNumeric.ZERO : GeneralConst.NumberNumeric.ONE);

                                periodeList.add(jsonPeriode);

                            }

                        } else {
                            response = GeneralConst.ResultCode.FAILED_OPERATION;
                        }

                        jsonBien.put(GeneralConst.ParamName.PeriodeList, periodeList.toString());
                        jsonBien.put(GeneralConst.ParamName.Assuj, assujetiListJson.toString());

                        jsonBienList.add(jsonBien);

                    }
                }

                jsono.put(GeneralConst.ParamName.AdresseList, jsonAdressList.toString());
                jsono.put(GeneralConst.ParamName.BienList, jsonBienList.toString());
                jsono.put("compteBancaireList", jsonCompteBancaireList.toString());

                jSONObjects.add(jsono);
            }

            response = jSONObjects.toString();

        } catch (Exception e) {
            throw e;
        }

        return response;
    }

    public static String newBienAndAssujV2(String file) throws IOException, JSONException {

        String response = GeneralConst.EMPTY_STRING;
        responseMessage = new ResponseMessage();
        dataAccess = new DataAccess();
        propertiesConfig = new PropertiesConfig();

        try {

            JSONObject data = new JSONObject(file);

            String codePersonne = data.getString(CUSTUMER_ID);
            String taxID = data.getString(TAX_ID);
            String bienID = data.getString(BIEN_ID);
            String amount = data.getString(AMOUNT);
            String currency = data.getString(CURRENCY);
            String requerant = data.getString(REQUERANT);
//            String center = data.getString(CENTER);
            String codeAgent = propertiesConfig.getContent(PropertiesConst.Config.CODE_AGENT_SERVICE_WEB);
            String banque = propertiesConfig.getContent(PropertiesConst.Config.BANQUE_TELE_DEC);
            String compteBancaire = propertiesConfig.getContent(PropertiesConst.Config.COMPTE_BANCAIRE_TELE_DEC);

            ArticleBudgetaire articleBudgetaire = DataAccess.getArticleBudgetaireByCode(taxID);
            Periodicite periodicite = articleBudgetaire.getPeriodicite();

            String dateDebut = GeneralConst.EMPTY_STRING, dateFin = GeneralConst.EMPTY_STRING;

            String period = periodicite.getCode();

            int year = Integer.valueOf(data.getString(YEAR));
            int month = period.equals("PR0032015") ? Integer.valueOf(data.getString(MONTH)) : 0;

            switch (period) {
                case "PR0032015": //MENSUEL
                    int maxDay = ConvertDate.getDayOfMonthByYear(year, month);
                    dateDebut = "01/" + StringUtils.leftPad(String.valueOf(month), 2, "0") + "/" + year;
                    dateFin = maxDay + "/" + StringUtils.leftPad(String.valueOf(month), 2, "0") + "/" + year;
                    break;
                case "PR0042015": //ANNUEL
                    dateDebut = "01/01/" + year;
                    dateFin = "31/12/" + year;
            }

            Date dateLimite = ConvertDate.formatDate(dateDebut);
            int nbrJourLimite = articleBudgetaire.getNbrJourLimite();

            Assujeti assujeti = DataAccess.getAssujettiByTaxeID(taxID, bienID);

            PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();
            periodeDeclaration.setId(0);
            periodeDeclaration.setAssujetissement(assujeti);
            periodeDeclaration.setDebut(ConvertDate.formatDate(dateDebut));
            periodeDeclaration.setFin(ConvertDate.formatDate(dateFin));
            periodeDeclaration.setDateLimite(ConvertDate.addDayTodate(dateLimite, nbrJourLimite, false));

            boolean isyear = month == 0;

            PeriodeDeclaration pd = DataAccess.getPeriodeDeclarationRequerant(assujeti.getId(), year, month, period, articleBudgetaire.getCode(), isyear);

            if (pd != null) {
                periodeDeclaration = pd;
            }

            RetraitDeclaration declaration = new RetraitDeclaration();

            declaration.setRequerant(requerant);
            declaration.setDevise(currency);
            declaration.setFkAb(taxID);
            declaration.setValeurBase(new BigDecimal(Double.valueOf(amount)));
            declaration.setMontant(new BigDecimal(Double.valueOf(amount)));
            declaration.setFkAssujetti(codePersonne);

            String codePeriode = DataAccess.createBien(periodeDeclaration, declaration, "", codeAgent, compteBancaire, banque);

            JSONObject dataResult = new JSONObject();
            List<JSONObject> periodResultList = new ArrayList<>();

            if (!codePeriode.equals(GeneralConst.EMPTY_STRING)) {

                dataResult.put("code", "100");
                dataResult.put("message", "Success");

                String periode = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(), articleBudgetaire.getPeriodicite().getNbrJour().toString());

                RetraitDeclaration rd = DataAccess.getRetraitByPeriode(Integer.valueOf(codePeriode));
                JSONObject perdiodResult = new JSONObject();

                perdiodResult.put("id", codePeriode);
                perdiodResult.put("code", rd.getCodeDeclaration());
                perdiodResult.put("title", periode);
                perdiodResult.put("amount", amount);
                perdiodResult.put("devise", currency);
                periodResultList.add(perdiodResult);

                dataResult.put("data", periodResultList);

            } else {

                dataResult.put("code", "200");
                dataResult.put("message", "Error");
                dataResult.put("data", "");
            }

            response = dataResult.toString();

        } catch (JSONException | NumberFormatException e) {
            throw e;
        }

        return response;
    }

    public static String newBienAndAssuj(String file) throws IOException, Exception {

        String response = GeneralConst.EMPTY_STRING;
        responseMessage = new ResponseMessage();
        dataAccess = new DataAccess();
        propertiesConfig = new PropertiesConfig();

        try {

            JSONObject data = new JSONObject(file);

            String amount = data.has(AMOUNT) ? data.getString(AMOUNT) : GeneralConst.EMPTY_STRING;
            String typePersonneCode = data.getString(PROPERTY_TYPE);
            String intitule = getDataPropertyType(data.getString(OPERATION));
            String requerant = data.getString(REQUERANT);
            String currency = data.has(CURRENCY) ? data.getString(CURRENCY) : GeneralConst.EMPTY_STRING;
            Personne personne = new Personne(data.getString(CUSTUMER_ID));
            AdressePersonne adressePersonne = new AdressePersonne(data.getString(ADRESS));
            String codeComplement = propertiesConfig.getContent(PropertiesConst.Config.CONFIG_TYPE_COMPLEMENT_BIEN);
            String codeAgent = propertiesConfig.getContent(PropertiesConst.Config.CODE_AGENT_SERVICE_WEB);
            JSONObject jsona = new JSONObject(codeComplement);
            String complement = jsona.get(data.getString(OPERATION)).toString();
            String center = data.getString(CENTER);

            String operation = data.getString(OPERATION);
            String param = operation.equals("1")
                    ? data.getString(REGISTRATION_NUMBER)
                    : data.getString(LAND_REGISTRY_NUMBER);

            ComplementBien complementBien = DataAccess.getComplementBienImmatriculation(param, complement);
            boolean exist = (complementBien != null);

            Bien bien;
            if (exist) {
                bien = complementBien.getBien();
                complementBienTempList = bien.getComplementBienList();
            } else {
                bien = new Bien();
                bien.setId(GeneralConst.EMPTY_STRING);
                bien.setTypeBien(new TypeBien(typePersonneCode));
                complementBienTempList = null;
            }

            bien.setIntitule(intitule);
            bien.setDescription(null);
            bien.setFkAdressePersonne(adressePersonne);

            ComplementBien cbAssuj = null;
            List<ComplementBien> complementBienList = new ArrayList<>();

            for (int i = 0; i < complementKeys.length(); i++) {
                JSONObject dataJson = complementKeys.getJSONObject(i);
                String key = dataJson.getString(KEY);
                String assujettisable = dataJson.getString(ASSUJETTISABLE);
                if (assujettisable.equals("1")) {
                    cbAssuj = getNewComplementBien(bien, data, key, exist);
                    complementBienList.add(cbAssuj);
                } else {
                    complementBienList.add(getNewComplementBien(bien, data, key, exist));
                }
            }

            bien.setComplementBienList(complementBienList);

            Acquisition acquisition = new Acquisition();
            acquisition.setId(GeneralConst.EMPTY_STRING);
            acquisition.setBien(bien);
            acquisition.setPersonne(personne);
            // acquisition.setDateAcquisition(data.getString(ACQUISITION_DATE));
            acquisition.setDateAcquisition(null);

            acquisition.setProprietaire(false);
            if (!operation.equals("3")) {
                acquisition.setProprietaire(true);
            }

            if (exist) {
                List<Acquisition> acquisitionList = bien.getAcquisitionList();
                for (Acquisition a : acquisitionList) {
                    if (a.getPersonne().getCode().equals(personne.getCode())) {
                        acquisition = a;
                        acquisition.setDateAcquisition(data.getString(ACQUISITION_DATE));
                        break;
                    }
                }
            }

            bien.setAcquisitionList(new ArrayList<>());
            bien.getAcquisitionList().add(acquisition);

            String codeAB = data.getString(TAX_ID);
            ArticleBudgetaire articleBudgetaire = DataAccess.getArticleBudgetaireByCode(codeAB);
            Periodicite periodicite = articleBudgetaire.getPeriodicite();

            String dateDebut = GeneralConst.EMPTY_STRING, dateFin = GeneralConst.EMPTY_STRING;

            int year = Integer.valueOf(data.getString(YEAR));
            int month = operation.equals("3") ? Integer.valueOf(data.getString(MONTH)) : 0;

            String period = periodicite.getCode();

            switch (period) {
                case "PR0032015": //MENSUEL
                    int maxDay = ConvertDate.getDayOfMonthByYear(year, month);
                    dateDebut = "01/" + StringUtils.leftPad(String.valueOf(month), 2, "0") + "/" + year;
                    dateFin = maxDay + "/" + StringUtils.leftPad(String.valueOf(month), 2, "0") + "/" + year;
                    break;
                case "PR0042015": //ANNUEL
                    dateDebut = "01/01/" + year;
                    dateFin = "31/12/" + year;
            }

            Date dateLimite = ConvertDate.formatDate(dateDebut);
            int nbrJourLimite = articleBudgetaire.getNbrJourLimite();
            String tarifCode = data.getString(TARIF_ID);

            Assujeti assujeti = new Assujeti();
            assujeti.setPersonne(personne);
            assujeti.setBien(bien);
            assujeti.setFkAdressePersonne(adressePersonne);
            assujeti.setArticleBudgetaire(articleBudgetaire);
            assujeti.setComplementBien(cbAssuj.getId());
            assujeti.setDuree(Short.valueOf("1"));
            assujeti.setTarif(new Tarif(tarifCode));
            assujeti.setValeur(BigDecimal.valueOf(Double.valueOf(cbAssuj.getValeur())));
            assujeti.setDateDebut(dateDebut);
            assujeti.setDateFin(dateFin);
            assujeti.setNbrJourLimite(nbrJourLimite);
            assujeti.setPeriodicite(articleBudgetaire.getPeriodicite().getCode());

            PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();
            periodeDeclaration.setId(0);
            periodeDeclaration.setAssujetissement(assujeti);
            periodeDeclaration.setDebut(ConvertDate.formatDate(dateDebut));
            periodeDeclaration.setFin(ConvertDate.formatDate(dateFin));
            periodeDeclaration.setDateLimite(ConvertDate.addDayTodate(dateLimite, nbrJourLimite, false));

            assujeti.setPeriodeDeclarationList(new ArrayList<>());
            assujeti.getPeriodeDeclarationList().add(periodeDeclaration);

            if (exist) {
                List<Assujeti> assujettiList = bien.getAssujetiList();
                for (Assujeti a : assujettiList) {
                    String abCode = a.getArticleBudgetaire().getCode();
                    String bienId = a.getBien().getId();
                    String bienId2 = bien.getId();
                    String complement_bien = a.getComplementBien();
                    String cb_assuj = cbAssuj.getId();
                    if (abCode.equals(codeAB) && bienId.equals(bienId2) && complement_bien.equals(cb_assuj)) {

                        assujeti = a;
                        assujeti.setBien(bien);

                        boolean isyear = month == 0;

                        PeriodeDeclaration pd = DataAccess.getPeriodeDeclarationRequerant(a.getId(), year, month, period, codeAB, isyear);
                        assujeti.setPeriodeDeclarationList(new ArrayList<>());
                        if (pd == null) {
                            assujeti.getPeriodeDeclarationList().add(periodeDeclaration);
                        } else {
                            assujeti.getPeriodeDeclarationList().add(pd);
                        }
                        break;
                    }
                }
            }

            JSONObject dataResult = new JSONObject();
            List<JSONObject> periodResultList = new ArrayList<>();

            PeriodeDeclaration pd = assujeti.getPeriodeDeclarationList().get(0);

            RetraitDeclaration declaration = DataAccess.getRetraitByPeriode(pd.getId());

            if (declaration != null) {

                dataResult.put("code", "300");
                dataResult.put("message", "period already declared");

            } else {

                String assCode = DataAccess.createBien(assujeti, cbAssuj.getTypeComplement().getCode(), tarifCode, requerant, amount, currency, center, codeAgent);

                if (!assCode.equals(GeneralConst.EMPTY_STRING)) {

                    dataResult.put("code", "100");
                    dataResult.put("message", "Success");

                    List<Periode> list = DocumentBusiness.getPeriodeInformation(codeAB, assCode, true);

                    if (list != null) {
                        for (Periode p : list) {
                            JSONObject perdiodResult = new JSONObject();
                            perdiodResult.put("id", p.getId());
                            perdiodResult.put("code", p.getCode());
                            perdiodResult.put("title", p.getTitle());
                            perdiodResult.put("amount", p.getAmount());
                            perdiodResult.put("devise", p.getDevise());
                            perdiodResult.put("taux", p.getTaux());
                            perdiodResult.put("date", p.getDateCreate());
                            periodResultList.add(perdiodResult);
                        }
                        dataResult.put("data", periodResultList);
                    } else {
                        dataResult.put("data", "");
                    }

                } else {

                    dataResult.put("code", "200");
                    dataResult.put("message", "Error");
                    dataResult.put("data", "");
                }
            }

            response = dataResult.toString();

        } catch (Exception e) {
            throw e;
        }

        return response;
    }

    private static ComplementBien getNewComplementBien(Bien bien, JSONObject data, String key, boolean exist) {

        ComplementBien complementBien = new ComplementBien();
        try {

            if (exist) {
                String code = getComplementCode(key);
                boolean find = false;
                for (ComplementBien cb : complementBienTempList) {
                    if (cb.getTypeComplement().getCode().equals(code)) {
                        complementBien = cb;
                        complementBien.setExist(true);
                        find = true;
                        break;
                    }
                }
                if (!find) {
                    complementBien.setBien(bien);
                    complementBien.setTypeComplement(new TypeComplementBien(getComplementCode(key)));
                    complementBien.setExist(false);
                }
            } else {
                complementBien.setBien(bien);
                complementBien.setExist(false);
                complementBien.setTypeComplement(new TypeComplementBien(getComplementCode(key)));
            }
            complementBien.setDevise(GeneralConst.EMPTY_STRING);
            complementBien.setValeur(data.getString(key));
        } catch (JSONException ex) {
            complementBien = null;
        }
        return complementBien;
    }

    private static String getComplementCode(String complementKey) {

        String code = GeneralConst.EMPTY_STRING;
        for (int i = 0; i < complementKeys.length(); i++) {
            try {
                JSONObject data = complementKeys.getJSONObject(i);
                String key = data.getString(KEY);
                if (key.equals(complementKey)) {
                    code = data.getString(CODE);
                    break;
                }
            } catch (JSONException ex) {
            }
        }

        return code;
    }

    public static String getDataPropertyType(String operation) {

        try {

            JSONArray dataKeys = new JSONArray(propertiesConfig.getContent(CONFIG_KEYS_COMPLEMENTS));

            for (int i = 0; i < dataKeys.length(); i++) {
                JSONObject propertyTypeJson = dataKeys.getJSONObject(i);
                if (propertyTypeJson.getString(OPERATION).equals(operation)) {
                    complementKeys = propertyTypeJson.getJSONArray(COMPLEMENTS);
                    return propertyTypeJson.getString(PROPERTY_NAME);
                }
            }

        } catch (JSONException ex) {
        }
        return GeneralConst.EMPTY_STRING;
    }

    public static String saveTaxation(String file) throws Exception {

        String response = GeneralConst.EMPTY_STRING;
        dataAccess = new DataAccess();
        propertiesConfig = new PropertiesConfig();

        try {

            JSONArray data = new JSONArray(file);

            NoteCalcul noteCalcul = new NoteCalcul();
            List<DetailsNc> listDetailsNcs = new ArrayList<>();

            String codePersonne = GeneralConst.EMPTY_STRING;
            String codeservice = GeneralConst.EMPTY_STRING;
            String codeAdresse;

            for (int i = 0; i < data.length(); i++) {

                JSONObject jo = data.getJSONObject(i);

                DetailsNc detailsNc = new DetailsNc();

                codePersonne = jo.getString("nif");
                codeservice = jo.getString("service");

                detailsNc.setArticleBudgetaire(new ArticleBudgetaire(jo.getString("article")));
                detailsNc.setTarif(jo.getString("tarif"));
                detailsNc.setValeurBase(new BigDecimal(jo.getDouble("base")));
                detailsNc.setMontantDu(new BigDecimal(jo.getDouble("montant")));
                detailsNc.setQte(jo.getInt("quantite"));
                detailsNc.setTauxArticleBudgetaire(new BigDecimal(jo.getDouble("taux")));
                detailsNc.setTypeTaux(jo.getString("type"));
                detailsNc.setDevise(new Devise(jo.getString("devise")));
                detailsNc.setPenaliser(Short.valueOf("0"));
                detailsNc.setPeriodeDeclaration(new PeriodeDeclaration(0));

                listDetailsNcs.add(detailsNc);

            }

            String agentPaiement = propertiesConfig.getContent(PropertiesConst.Config.CODE_AGENT_SERVICE_WEB);
            String siteCode = propertiesConfig.getContent(PropertiesConst.Config.CODE_SITE);

            Personne personne = DataAccess.getPersonById(codePersonne);
            codeAdresse = personne.getAdressePersonneList().get(0).getAdresse().getId();

            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            noteCalcul.setPersonne(codePersonne);
            noteCalcul.setDateCreat(new Date());
            noteCalcul.setFkAdressePersonne(codeAdresse);
            noteCalcul.setAgentCreat(agentPaiement);
            noteCalcul.setSite(siteCode);
            noteCalcul.setDetailsNcList(listDetailsNcs);
            noteCalcul.setService(codeservice);
            noteCalcul.setExercice(String.valueOf(calendar.get(Calendar.YEAR)));

            response = DataAccess.saveNC(noteCalcul);

            JSONObject dataResult = new JSONObject();

            if (!response.isEmpty()) {

                dataResult.put("code", "100");
                dataResult.put("message", "Success");
                dataResult.put("nc", response);

            } else {

                dataResult.put("code", "200");
                dataResult.put("message", "failed");
            }

            response = dataResult.toString();

        } catch (JSONException | NumberFormatException e) {
            throw e;
        }

        return response;
    }

    public static String getTaxation(String nif) throws Exception {
        String response = GeneralConst.EMPTY_STRING;
        List<JSONObject> listNC = new ArrayList<>(), listDNC = new ArrayList<>();
        dataAccess = new DataAccess();

        try {
            List<NoteCalcul> ncs = DataAccess.getTaxation(nif);
            if (!ncs.isEmpty()) {
                for (NoteCalcul nc : ncs) {
                    JSONObject jsona = new JSONObject();
                    double montant = 0;
//                    jsona.put("code", 100);
                    jsona.put("numero", nc.getNumero());
                    jsona.put("etat", nc.getEtat());
                    jsona.put("dateCreate", Tools.formatDateToStringV3(nc.getDateCreat()));
                    jsona.put("dateCloture", nc.getDateCloture() == null
                            ? GeneralConst.EMPTY_STRING : Tools.formatDateToStringV3(nc.getDateCloture()));
                    jsona.put("dateValidation", nc.getDateValidation() == null
                            ? GeneralConst.EMPTY_STRING : Tools.formatDateToStringV3(nc.getDateValidation()));
                    jsona.put("agentCloture", nc.getAgentCloture() == null
                            ? GeneralConst.EMPTY_STRING : nc.getAgentCloture());
                    jsona.put("agentValidation", nc.getAgentValidation() == null
                            ? GeneralConst.EMPTY_STRING : nc.getAgentValidation());
                    jsona.put("codeAvis", nc.getAvis());
                    jsona.put("codeService", GeneralConst.EMPTY_STRING);
                    jsona.put("intituleService", GeneralConst.EMPTY_STRING);
                    Service service = dataAccess.getServiceByCode(nc.getService());
                    if (service != null) {
                        jsona.put("codeService", service.getCode());
                        jsona.put("intituleService", service.getIntitule());
                    }
                    jsona.put("existNc", GeneralConst.EMPTY_STRING);
                    NotePerception np = DataAccess.getNP(nc.getNumero());
                    jsona.put("existNc", np != null ? "1" : GeneralConst.EMPTY_STRING);
                    jsona.put("compteBancaire", np != null ? np.getCompteBancaire() == null
                            ? GeneralConst.EMPTY_STRING : np.getCompteBancaire() : GeneralConst.EMPTY_STRING);
                    jsona.put("numeroNp", np != null ? np.getNumero() : GeneralConst.EMPTY_STRING);
                    List<DetailsNc> dncs = DataAccess.getDetailsNC(nc.getNumero());
                    montant = dncs.stream().map((dn) -> dn.getMontantDu().doubleValue()).reduce(montant, (accumulator, _item) -> accumulator + _item);
                    jsona.put("montantNc", montant);
                    jsona.put("devise", nc.getDetailsNcList().get(0).getDevise().getCode());
                    listNC.add(jsona);
                }
                response = listNC.toString();
            } else {
                JSONObject json = new JSONObject();
                json.put("code", 200);
                json.put("message", "not found");
                response = json.toString();
            }

        } catch (Exception e) {
            throw e;
        }
        return response;
    }

    public static String getDetailTaxation(String nc) {
        List<JSONObject> listDNC = new ArrayList<>();

        try {
            List<DetailsNc> dncs = DataAccess.getDetailsNC(nc);
            if (dncs.size() > 0) {
                for (DetailsNc dnc : dncs) {
                    JSONObject object = new JSONObject();
                    object.put("montant", dnc.getMontantDu().doubleValue());
                    object.put("devise", dnc.getDevise().getCode());
                    object.put("base", dnc.getValeurBase());
                    object.put("quantite", dnc.getQte());
                    object.put("codeArticleBudgetaire", dnc.getArticleBudgetaire().getCode());
                    object.put("intituleArticle", dnc.getArticleBudgetaire().getIntitule());
                    object.put("typeTaux", dnc.getTypeTaux());
                    object.put("id", dnc.getId());
                    object.put("numero", dnc.getNoteCalcul().getNumero());
                    listDNC.add(object);
                }
                return listDNC.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String getPersonne(String mail) throws Exception {

        String response, url = "";
        propertiesConfig = new PropertiesConfig();
        String mailTitle = propertiesConfig.getContent("MAIL_CODE_INIT_PW_TITLE");
        String mailContent = propertiesConfig.getContent("MAIL_CODE_INIT_PW_CONTENT");

        try {

            JSONObject dataResult = new JSONObject();
            int isPeageLualaba = Integer.valueOf(propertiesConfig.getContent("IS_PEAGE_LUALABA"));
            switch (isPeageLualaba) {
                case 0:
                    url = "https://www.hautkatanga-gouv.net/teledeclaration/forget-password?id=";
                    break;
                case 1:
                    url = "http://www.sopel-rdc.com/forget-password?id=";
                    break;
                case 2:
                    url = "http://www.sopel-rdc.com/forget-password?id=";
                    break;
                case 3:
                    url = "https://www.hautkatanga-gouv.net/teledeclaration/forget-password?id=";
                    break;
            }

            Personne personne = DataAccess.getPersonne(mail);

            if (personne != null) {

                String code = Tools.generateRandomValue(7);

                if (DataAccess.setSendMailTrue(1, personne.getCode())) {
                    mailContent = String.format(mailContent, personne.toString(), code, url.concat(Tools.encoder(mail.concat(",").concat(code))));
                    sendNotification(mailContent, mailTitle, personne.getLoginWeb().getMail());
                    dataResult.put("code", 100);
                    dataResult.put("message", "Un nouveau mail d'initialisation de votre mot de passe a été envoyé à votre adresse.");
                    dataResult.put("alert", "alert-success");
                }

            } else {
                dataResult.put("code", 200);
                dataResult.put("message", "Votre adresse mail n'est pas reconnue dans le système. Veuillez initier une demande d'<a href=\"register\">inscription</a>.");
                dataResult.put("alert", "alert-danger");
            }

            response = dataResult.toString();

        } catch (JSONException | NumberFormatException e) {
            response = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return response;
    }

    public static String initPassword(String object) throws Exception {

        String response;
        propertiesConfig = new PropertiesConfig();
        String mailTitle = propertiesConfig.getContent("MAIL_CODE_INIT_PW_TITLE");
        String mailContent = propertiesConfig.getContent("MAIL_CODE_INIT_PW_CONTENT");

        try {
            JSONObject dataResult = new JSONObject();
            JSONObject obj = new JSONObject(object);

            String code = obj.getString("code");
            String codeInit = obj.getString("codeInit");
            String passWord = obj.getString("passWord");

            String codeDecode = Tools.decoder(code);

            Personne personne = DataAccess.getPersonne(codeDecode.split(",")[0]);

            if (personne != null) {

                if (personne.getLoginWeb().getSendMail() == 0) {
                    dataResult.put("code", 500);
                    dataResult.put("message", "Vous ne pouvez pas réinitialiser votre mot de passe en utilisant l'ancienne url."
                            + " Veuillez faire une nouvelle <a href=\"forget-password\">demande</a>.");
                    dataResult.put("alert", "alert-danger");
                    return dataResult.toString();
                }

                if (!codeDecode.split(",")[1].equals(codeInit)) {
                    dataResult.put("code", 400);
                    dataResult.put("message", "Le code d'initialisation saisi est incorect. Veuillez réessayer.");
                    return dataResult.toString();
                }

                if (DataAccess.setSendMailTrue(0, personne.getCode())) {
                    if (DataAccess.updatePassword(personne.getCode(), passWord)) {
//                        mailContent = String.format(mailContent, personne.toString(), "http://localhost:8080/teledeclaration/forget-password?id=".concat(Tools.encoder(code)));
//                        sendNotification(mailContent, mailTitle, personne.getLoginWeb().getMail());
                        dataResult.put("code", 100);
                        dataResult.put("message", "Votre mot de passe a été réinitilisé avec succès. <a href=\"login\">Connectez-vous</a>");
                        dataResult.put("alert", "alert-success");
                    } else {
                        dataResult.put("code", 300);
                        dataResult.put("message", "Votre mot de passe n'a pas pu être réinitilisé. Veuillez réessayer.");
                    }
                } else {
                    dataResult.put("code", 300);
                    dataResult.put("message", "Votre mot de passe n'a pas pu être réinitilisé. Veuillez réessayer.");
                }

            } else {
                dataResult.put("code", 200);
                dataResult.put("message", "Adresse mail non valide. Veuillez la resaisir.");
            }

            response = dataResult.toString();

        } catch (JSONException | NumberFormatException e) {
            response = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return response;
    }

    public static String saveAssujetti(String file) throws Exception {

        String response = GeneralConst.EMPTY_STRING;
        dataAccess = new DataAccess();
        propertiesConfig = new PropertiesConfig();

        try {

            Personne personne = new Personne();

            JSONObject data = new JSONObject(file);

            String agentcreate = propertiesConfig.getContent(PropertiesConst.Config.CODE_AGENT_SERVICE_WEB);
            String adresse;

            personne.setNif(data.getString("nif"));
            personne.setNom(data.getString("nom"));
            personne.setPrenoms(data.getString("prenom"));
            personne.setPostnom(data.getString("postnom"));
            personne.setFormeJuridique(new FormeJuridique(data.getString("forme")));
            personne.setAgentCreat(agentcreate);

            LoginWeb loginWeb = new LoginWeb();

            String password = Tools.generateRandomValue(8);

            loginWeb.setUsername(data.getString("mail"));
            loginWeb.setPassword(password);
            loginWeb.setMail(data.getString("mail"));
            loginWeb.setTelephone(data.getString("telephone"));
            loginWeb.setAgentCreat(Integer.valueOf(agentcreate));

            adresse = data.getString("adresse");
            JSONArray jsonAdresses = new JSONArray(adresse);
            List<AdressePersonne> adressePersonnes = getAdressePersonnes(jsonAdresses);

            personne.setLoginWeb(loginWeb);

            LoginWeb login = DataAccess.getLoginWebByTelephoneOrEmail(personne.getLoginWeb().getMail().trim(), personne.getLoginWeb().getTelephone().trim());

            List<Complement> complements = new ArrayList<>();

            if (personne.getFormeJuridique().getCode().equals("03")) {

                Complement cp1 = new Complement();
                Complement cp2 = new Complement();

                cp1.setEtat(true);
                cp1.setFkComplementForme(new ComplementForme("CF000000000000212015"));
                cp1.setValeur(data.getString("idnat"));

                cp2.setEtat(true);
                cp2.setFkComplementForme(new ComplementForme("CF000000000000022015"));
                cp2.setValeur(data.getString("rccm"));

                complements.add(cp1);
                complements.add(cp2);
            }

            personne.setComplementList(complements);

            JSONObject dataResult = new JSONObject();

            if (login != null) {

                if (personne.getLoginWeb().getMail().equals(login.getMail())) {

                    dataResult.put("code", "200");
                    dataResult.put("message", "L'adresse mail renseignée existe déjà dans le système.");

                }

                if (personne.getLoginWeb().getTelephone().equals(login.getTelephone())) {

                    dataResult.put("code", "200");
                    dataResult.put("message", "Le numéro de téléphone renseigné existe déjà dans le système.");

                }
                if (personne.getLoginWeb().getMail().equals(login.getMail())
                        && personne.getLoginWeb().getTelephone().equals(login.getTelephone())) {
                    dataResult.put("code", "200");
                    dataResult.put("message", "Le numéro de téléphone et l'adresse mail renseignés existent déjà dans le système.");
                }

            } else {

                if (DataAccess.savePersonne(personne, complements, adressePersonnes, loginWeb)) {
                    dataResult.put("code", "100");
                    dataResult.put("message", "Success");

                    String url = "http://localhost:8080/teleprocedure_egopass/register?id=";

                    try {

                        String mailContent = propertiesConfig.getContent("MAIL_SUBSCRIBE_CLIENT_CONTENT");
                        String mailSubjet = propertiesConfig.getContent("MAIL_SUBSCRIBE_CLIENT_SUBJECT");

                        mailContent = String.format(mailContent, personne.toString(),
                                url.concat(Tools.encoder(loginWeb.getMail().concat(",").concat(password))));
                        sendNotification(mailContent, mailSubjet, personne.getLoginWeb().getMail());
                    } catch (Exception e) {
                        dataResult.put("code", "200");
                        dataResult.put("message", "Votre inscription n'a pas abouti. Veuillez contacter l'administrateur");
                    }
                } else {

                    dataResult.put("code", "200");
                    dataResult.put("message", "Votre inscription n'a pas abouti. Veuillez réessayer ou contacter l'administrateur");
                }

            }

            response = dataResult.toString();

        } catch (JSONException | NumberFormatException e) {
            throw e;
        }

        return response;
    }

    public static int sendValidationCode(Personne personne, String code) throws IOException {

        propertiesConfig = new PropertiesConfig();
        propertiesMail = new PropertiesMail();

        String service_sms = propertiesConfig.getContent(PropertiesConst.Config.SMS_SERVICE);
        String mailjetUser = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_USERNAME);
        String mailjetPassword = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_PASSWORD);
        String mailjetURL = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_URL);
        String mailSmtp = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SMTP);

        String smsURL;

        if (service_sms.equals(GeneralConst.NumberString.ONE)) {
            smsURL = propertiesConfig.getContent(PropertiesConst.Config.SMS_URL);
        } else {
            smsURL = propertiesConfig.getContent(PropertiesConst.Config.SMS_URL2);
        }

        String smsSubscribe = propertiesConfig.getContent(PropertiesConst.Config.SMS_SUBSCRIBE);
        String mailSubscribeContent = propertiesMail.getContent(PropertiesConst.Mail.MAIL_CODE_VALIDATION_FR);
        String mailSubscribeSubjet = propertiesMail.getContent(PropertiesConst.Mail.MAIL_CODE_VALIDATION_FR_SUBJECT);

        String mailContent = String.format(mailSubscribeContent, personne.getNom(), code);

        MailSender mailSender = new MailSender();
        mailSender.setDate(new Date());
        mailSender.setHost(mailjetURL);
        mailSender.setSubject(mailSubscribeSubjet);
        mailSender.setMessage(mailContent);
        mailSender.setReciever(personne.getLoginWeb().getMail().trim());
        mailSender.setSender(mailSmtp);

        mailSender.sendMailWithAPI(mailjetUser, mailjetPassword);
//        String smsContent = String.format(smsSubscribe, personne.getNom(), personne.getLoginWeb().getMail().trim());
//
//        smsURL = String.format(smsURL, personne.getLoginWeb().getTelephone().trim(), smsContent);
//
//        SMSSender.sendSMS(smsURL);
        return 1;
    }

    private static String sendNotification(Personne personne) throws IOException {

        propertiesConfig = new PropertiesConfig();

        String mailjetUser = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_USERNAME);
        String mailjetPassword = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_PASSWORD);
        String mailjetURL = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_URL);
        String mailSmtp = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SMTP);

        String mailSubscribeContent = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SUBSCRIBE_CLIENT_CONTENT);
        String mailSubscribeSubjet = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SUBSCRIBE_CLIENT_SUBJECT);

        LoginWeb login = DataAccess.getLoginWebByTelephoneOrEmail(personne.getLoginWeb().getMail().trim(), personne.getLoginWeb().getTelephone().trim());

        String mailContent = String.format(mailSubscribeContent, personne.toString(), login.getUsername(), personne.getLoginWeb().getPassword().trim());

        MailSender mailSender = new MailSender();
        mailSender.setDate(new Date());
        mailSender.setHost(mailjetURL);
        mailSender.setSubject(mailSubscribeSubjet);
        mailSender.setMessage(mailContent);
        mailSender.setReciever(personne.getLoginWeb().getMail().trim());
        mailSender.setSender(mailSmtp);

        mailSender.sendMailWithAPI(mailjetUser, mailjetPassword);
        return "1";
    }

    private static String sendNotification(String... objet) throws IOException {

        propertiesConfig = new PropertiesConfig();

        String mailjetUser = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_USERNAME);
        String mailjetPassword = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_PASSWORD);
        String mailjetURL = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_URL);
        String mailSmtp = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SMTP);

        String mailContent = objet[0];
        String mailSubjet = objet[1];
        String emailReceiver = objet[2];

//        String emailCc = objet[3];
        MailSender mailSender = new MailSender();
        mailSender.setDate(new Date());
        mailSender.setHost(mailjetURL);
        mailSender.setSubject(mailSubjet);
        mailSender.setMessage(mailContent);
        mailSender.setReciever(emailReceiver);
        mailSender.setSender(mailSmtp);

        if (objet.length == 3) {
            try {
                mailSender.sendMailWithAPI(mailjetUser, mailjetPassword);
            } catch (Exception e) {
            }

        } else {
            String reference = objet[3];
            String dir = objet[4];
            mailSender.setReference(reference);
            mailSender.setPath(objet[5].split(",")[1]);

            try {
                mailSender.sendMailWithAPI_Vouchers(mailjetUser, mailjetPassword);
                DataAccess.livrerVoucher(reference);
                FileUtils.deleteDirectory(new File(dir));
            } catch (Exception e) {
                DataAccess.setCommandeNotDelivered(objet[5], reference);
                FileUtils.deleteDirectory(new File(dir));
            }

        }

        return "1";
    }

    public static int sendValidationEmail(Personne personne, String codevalidation) throws IOException {

        propertiesConfig = new PropertiesConfig();
        propertiesMail = new PropertiesMail();

        String service_sms = propertiesConfig.getContent(PropertiesConst.Config.SMS_SERVICE);
        String mailjetUser = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_USERNAME);
        String mailjetPassword = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_PASSWORD);
        String mailjetURL = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_URL);
        String mailSmtp = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SMTP);

        String smsURL;

        if (service_sms.equals(GeneralConst.NumberString.ONE)) {
            smsURL = propertiesConfig.getContent(PropertiesConst.Config.SMS_URL);
        } else {
            smsURL = propertiesConfig.getContent(PropertiesConst.Config.SMS_URL2);
        }

        String smsSubscribe = propertiesConfig.getContent(PropertiesConst.Config.SMS_SUBSCRIBE);
        String mailSubscribeContent = propertiesMail.getContent(PropertiesConst.Mail.MAIL_CODE_VALIDATION_FR);
        String mailSubscribeSubjet = propertiesMail.getContent(PropertiesConst.Mail.MAIL_CODE_VALIDATION_FR_SUBJECT);

        String mailContent = String.format(mailSubscribeContent, personne.toString(), codevalidation);

        MailSender mailSender = new MailSender();
        mailSender.setDate(new Date());
        mailSender.setHost(mailjetURL);
        mailSender.setSubject(mailSubscribeSubjet);
        mailSender.setMessage(mailContent);
        mailSender.setReciever(personne.getLoginWeb().getMail().trim());
        mailSender.setSender(mailSmtp);

        mailSender.sendMailWithAPI(mailjetUser, mailjetPassword);
        String smsContent = String.format(smsSubscribe, personne.getNom(), personne.getLoginWeb().getMail().trim());

        smsURL = String.format(smsURL, personne.getLoginWeb().getTelephone().trim(), smsContent);
//
        SMSSender.sendSMS(smsURL);
        return 1;
    }

    public static String getEntiteAdministrative() throws Exception {
        String response = GeneralConst.EMPTY_STRING;
        propertiesConfig = new PropertiesConfig();
        List<JSONObject> listVille = new ArrayList<>();
        dataAccess = new DataAccess();

        try {
            String codeProvinceHk = propertiesConfig.getContent(PropertiesConst.Config.CODE_PROVINCE_HK);
            List<EntiteAdministrative> eas = DataAccess.getEntiteAdmin(codeProvinceHk);
            if (!eas.isEmpty()) {
                for (EntiteAdministrative ea : eas) {
                    JSONObject jsonVille = new JSONObject();
                    List<JSONObject> listCommune = new ArrayList<>();
                    jsonVille.put("code", ea.getCode());
                    jsonVille.put("intitule", ea.getIntitule());

                    if (!ea.getEntiteAdministrativeList().isEmpty()) {
                        for (EntiteAdministrative ea1 : ea.getEntiteAdministrativeList()) {
                            JSONObject jsonCommune = new JSONObject();
                            List<JSONObject> listQuartier = new ArrayList<>();
                            jsonCommune.put("code", ea1.getCode());
                            jsonCommune.put("intitule", ea1.getIntitule());
                            jsonCommune.put("codeVille", ea1.getEntiteMere().getCode());

                            if (!ea1.getEntiteAdministrativeList().isEmpty()) {
                                for (EntiteAdministrative ea2 : ea1.getEntiteAdministrativeList()) {
                                    JSONObject jsonQuartier = new JSONObject();
                                    jsonQuartier.put("code", ea2.getCode());
                                    jsonQuartier.put("intitule", ea2.getIntitule());
                                    jsonQuartier.put("codeCommune", ea2.getEntiteMere().getCode());
                                    listQuartier.add(jsonQuartier);
                                }
                            }
                            jsonCommune.put("entiteQuartier", listQuartier);

                            listCommune.add(jsonCommune);
                        }
                    }
                    jsonVille.put("entiteCommune", listCommune);

                    listVille.add(jsonVille);

                }
                response = listVille.toString();
            } else {
                JSONObject json = new JSONObject();
                json.put("code", 200);
                json.put("message", "not found");
                response = json.toString();
            }

        } catch (Exception e) {
            throw e;
        }
        return response;
    }

    public static String getDivisionList() throws Exception {

        List<JSONObject> listDivision = new ArrayList<>();
        String response = GeneralConst.EMPTY_STRING;
        propertiesConfig = new PropertiesConfig();

        String code_division = propertiesConfig.getContent(PropertiesConst.Config.CODE_DIVISION);

        try {
            List<Division> divisions = DataAccess.getDivisionList(code_division);
            if (!divisions.isEmpty()) {
                for (Division division : divisions) {
                    List<JSONObject> listSite = new ArrayList<>();
                    JSONObject object = new JSONObject();
                    object.put("code", division.getCode());
                    object.put("intitule", division.getIntitule());
                    List<Site> sites = DataAccess.getSiteByDivision(division.getCode());
                    if (!sites.isEmpty()) {
                        for (Site site : sites) {
                            JSONObject object_site = new JSONObject();
                            object_site.put("code", site.getCode());
                            object_site.put("intitule", site.getIntitule());
                            object_site.put("codeDivision", site.getFkDivision().getCode());
                            listSite.add(object_site);
                        }
                    }
                    object.put("listSite", listSite);
                    listDivision.add(object);

                }
                response = listDivision.toString();
            } else {
                JSONObject json = new JSONObject();
                json.put("code", 200);
                response = json.toString();
            }
        } catch (Exception e) {
            JSONObject json = new JSONObject();
            json.put("code", 300);
            response = json.toString();
        }

        return response;
    }

    public static String getPalier(String codePersonne) throws Exception {

        JSONObject allDataAssujettisement = new JSONObject();
        List<JSONObject> dataAssujettisement = new ArrayList<>();

        try {

            List<Assujeti> assujettissements = DataAccess.getAssujettissementByPersonne(codePersonne);
            List<String> codeAbList = new ArrayList<>();

            for (Assujeti assujeti : assujettissements) {
                JSONObject jsonAssujetti = new JSONObject();

                if (codeAbList.contains(assujeti.getArticleBudgetaire().getCode())) {
                    continue;
                }

                codeAbList.add(assujeti.getArticleBudgetaire().getCode());

                boolean isPalier = assujeti.getArticleBudgetaire().getPalier();

                Palier palier = DataAccess.getPalierForBienImmobilier(
                        assujeti.getArticleBudgetaire().getCode().trim(),
                        assujeti.getBien().getFkTarif(),
                        assujeti.getPersonne().getFormeJuridique().getCode().trim(),
                        assujeti.getBien().getFkQuartier().trim(),
                        assujeti.getValeur().floatValue(), isPalier,
                        assujeti.getBien().getTypeBien().getCode());

                jsonAssujetti.put("idBien", assujeti.getBien().getId());

                BigDecimal taux = new BigDecimal(BigInteger.ZERO);

                BigDecimal valeurBase = new BigDecimal(BigInteger.ONE);

                if (palier != null) {

                    if (palier.getTypeTaux().equals("F")) {

                        if (assujeti.getValeur().floatValue() == 0) {

                            taux = palier.getMultiplierValeurBase() == 0 ? palier.getTaux()
                                    : palier.getTaux().multiply(valeurBase);

                        } else {

                            taux = palier.getMultiplierValeurBase() == 0 ? palier.getTaux()
                                    : palier.getTaux().multiply(assujeti.getValeur());
                        }

                    } else {

                        taux = assujeti.getValeur().multiply(palier.getTaux()).divide(BigDecimal.valueOf(GeneralConst.Numeric.CENT));
                    }

                    jsonAssujetti.put("taux", palier.getTaux());

                    if (palier.getTypeTaux().equals("%")) {

                        if (assujeti.getValeur().floatValue() > 0) {
                            jsonAssujetti.put("valeurBase", assujeti.getValeur());
                        } else {
                            jsonAssujetti.put("valeurBase", 0);
                        }

                    } else {
                        jsonAssujetti.put("valeurBase", 0);
                    }

                    jsonAssujetti.put("devise", palier.getDevise());
                    jsonAssujetti.put("codeAb", assujeti.getArticleBudgetaire().getCode());

                    Unite unite = DataAccess.getUnite(palier.getUnite());
                    TypeComplement complement = DataAccess.getTypeComplementbyComplementBien(assujeti.getComplementBien());
                    if (unite != null && complement != null) {

                        String valBase = !unite.getIntitule().equalsIgnoreCase("Montant") ? unite.getIntitule() : palier.getDevise();
                        jsonAssujetti.put("descriptionBien", complement.getIntitule() + ":" + assujeti.getValeur() + GeneralConst.SPACE + valBase);

                    } else {

                        jsonAssujetti.put("descriptionBien", GeneralConst.EMPTY_STRING);
                    }

                }
                jsonAssujetti.put("tarif", assujeti.getTarif().getIntitule());
                jsonAssujetti.put("tauxCumule", taux);
                dataAssujettisement.add(jsonAssujetti);

            }

            allDataAssujettisement.put("dataAssujettisement", dataAssujettisement.toString());
        } catch (Exception e) {
            throw e;
        }
        return dataAssujettisement.toString();
    }

    public static String saveRetraitDeclaration(String objet) throws IOException {

        propertiesConfig = new PropertiesConfig();

        String codeAgent = propertiesConfig.getContent(PropertiesConst.Config.CODE_AGENT_SERVICE_WEB);
        String codeVignette = propertiesConfig.getContent(PropertiesConst.Config.CODE_IMPOT_VIGNETTE);
        List<DeclarationMock> declarationListMock = new ArrayList<>();
        String returnValue = "";
        int n = 0;

        Date dateCreateDeclaration = new Date();

        try {

            JSONArray jsonRetrait = new JSONArray(objet);
            List<JSONObject> rdecl = new ArrayList<>();

            JSONArray jsonPenalites = null;

            BigDecimal amountPrincipGlobal = new BigDecimal("0");

            String numDeclaration = DataAccess.getNewid();

            DeclarationPenaliteMock declarationPenaliteMock = null;
            BigDecimal amountPean = new BigDecimal("0");

            if (jsonPenalites != null) {

                for (int j = 0; j < jsonPenalites.length(); j++) {

                    declarationPenaliteMock = new DeclarationPenaliteMock();

                    JSONObject jsonobject = jsonPenalites.getJSONObject(j);

                    amountPean = amountPean.add(BigDecimal.valueOf(Double.valueOf(jsonobject.getString("amountPenalite"))));

                    declarationPenaliteMock.setAmountPenalite(amountPean);
                    declarationPenaliteMock.setObservationRemise(jsonobject.getString("observationRemise"));
                    declarationPenaliteMock.setTauxRemise(jsonobject.getInt("tauxRemise"));
                }
            }

            for (int i = 0; i < jsonRetrait.length(); i++) {

                n++;

                //récuper la valeur de base dans assuj de la periode
                DeclarationMock declarationMock = new DeclarationMock();
                JSONObject jobject = new JSONObject();

                JSONObject jsonobject = jsonRetrait.getJSONObject(i);

                String nomRequerant = jsonobject.getString("nomRequerant");
                String personneId = jsonobject.getString("personneId");
                String codeAB = jsonobject.getString("codeAb");
                String codeBanque = jsonobject.getString("codeBanque");
                String codeCompteBancaire = jsonobject.getString("codeCompteBancaire");
                String codeSite = jsonobject.getString("codeSite");
                double penaliteAmount = Double.valueOf(jsonobject.getString("montantPenalites"));

                BigDecimal amountPrincip = new BigDecimal("0");
                amountPrincip = amountPrincip.add(BigDecimal.valueOf(Double.valueOf(jsonobject.getString("montant"))));

                amountPrincipGlobal = amountPrincipGlobal.add(amountPrincip);

                String periodes = Tools.getListToStringV2(jsonobject.getString("periode"));

                String periodeNames = Tools.getListToStringV2(jsonobject.getString("periodeName"));

                String[] dateArrayPD = periodes.split(",");

                List<PeriodeDeclarationSelection> listPD = new ArrayList<>();

                for (String pd : dateArrayPD) {

                    PeriodeDeclarationSelection periodeDeclarationSelection = new PeriodeDeclarationSelection();

                    periodeDeclarationSelection.setPeriode(Integer.valueOf(pd));
                    periodeDeclarationSelection.setPenalise(jsonPenalites == null ? 0 : 1);
                    periodeDeclarationSelection.setPrincipal(amountPrincip.floatValue() / dateArrayPD.length);

                    listPD.add(periodeDeclarationSelection);
                }

                declarationMock.setListPeriode(listPD);
                declarationMock.setPeriode(periodeNames);

                declarationMock.setIcmParamId(jsonobject.getString("icmParamId"));
                declarationMock.setCodeAssujettissement(jsonobject.getString("assujId"));
                declarationMock.setAmountPrincipal(amountPrincip);
                declarationMock.setAmountPenalite(new BigDecimal(penaliteAmount));
                declarationMock.setDevise(jsonobject.getString("devise"));
                declarationMock.setRequerant(nomRequerant);
                declarationMock.setAssujetti(personneId);
                declarationMock.setCodeDeclaration("");
                declarationMock.setCodeArticleBudgetaire(codeAB);
                declarationMock.setCodeAgentCreate(codeAgent);
                declarationMock.setDateCreate(dateCreateDeclaration);
                declarationMock.setEstPenalise(jsonobject.getInt("isPenalite"));

                declarationMock.setBanque(codeBanque);
                declarationMock.setCompteBancaire(codeCompteBancaire);
                declarationMock.setCodeSite(codeSite);

                declarationMock.setTauxRemise(jsonobject.getInt("tauxRemise"));
                declarationMock.setObservationRemise(jsonobject.getString("observationRemise"));
                declarationMock.setAmountRemisePenalite(new BigDecimal(Double.valueOf(jsonobject.getString("remise"))));
                declarationMock.setNumeroDeclaration(numDeclaration);

                //-----------
                jobject.put("code", numDeclaration);
//                jobject.put("code", "5E20D4CA-0BB9-4666-9545-AEDECD188187");
                jobject.put("title", periodeNames);
                jobject.put("amount", amountPrincip);
                jobject.put("devise", jsonobject.getString("devise"));
                rdecl.add(jobject);
                //-----------

                declarationListMock.add(declarationMock);
            }

            boolean result = DataAccess.saveRetraitDeclarationV2(declarationListMock, numDeclaration, declarationPenaliteMock);
//            boolean result = true;
            if (result) {
                List<RetraitDeclaration> rds = DataAccess.getListRetraitDeclarationByNewID(numDeclaration);
                String numero = GeneralConst.EMPTY_STRING;
                if (!rds.isEmpty()) {
                    for (RetraitDeclaration rd : rds) {
                        if (numero.equals(GeneralConst.EMPTY_STRING)) {
                            numero = rd.getCodeDeclaration();
                        }
                    }
                }
                JSONObject json = new JSONObject();
                json.put("code", 100);
                json.put("numero", numero);
//                json.put("numero", "25401");
                json.put("nbreBien", n);
                json.put("data", rdecl);
                returnValue = json.toString();
            } else {
                returnValue = GeneralConst.ResultCode.EXCEPTION_OPERATION;
            }
        } catch (JSONException | NumberFormatException e) {
            returnValue = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return returnValue;
    }

    public static String printNoteTaxation(String newId) {

        String dataReturn;
        String numero = GeneralConst.EMPTY_STRING;
        String periodeTitle = GeneralConst.EMPTY_STRING;
        String periode = GeneralConst.EMPTY_STRING;
        BigDecimal amountPD = new BigDecimal(0);

        try {

            List<RetraitDeclaration> rds = DataAccess.getListRetraitDeclarationByNewID(newId);
            if (!rds.isEmpty()) {
                for (RetraitDeclaration rd : rds) {
                    if (numero.equals(GeneralConst.EMPTY_STRING)) {
                        numero = rd.getCodeDeclaration();
                    }
                    List<Assujeti> assujetis = DataAccess.getAssujettissement(rd.getFkAssujetti(), rd.getFkAb());
                    if (!assujetis.isEmpty()) {
                        for (Assujeti assujeti : assujetis) {
                            List<PeriodeDeclaration> pds = DataAccess.getListPeriodeDeclarationByAssujV2(assujeti.getId());
                            if (!pds.isEmpty()) {
                                for (PeriodeDeclaration pd : pds) {
                                    if (Integer.valueOf(rd.getFkPeriode()).equals(pd.getId())) {
                                        if (periodeTitle.equals(GeneralConst.EMPTY_STRING)) {
                                            periode = "'" + rd.getFkPeriode() + "'";
                                            periodeTitle = Tools.getPeriodeIntitule(pd.getDebut(),
                                                    pd.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode());
                                        } else {
                                            periodeTitle += ", ".concat(Tools.getPeriodeIntitule(pd.getDebut(),
                                                    pd.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode()));
                                            periode += ",'" + rd.getFkPeriode() + "'";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    amountPD = amountPD.add(rd.getMontant());
                }
            }
            periode = "IN(".concat(periode).concat(")");
            int nbreAssuj = 0;
            List<Assujeti> assujetis = DataAccess.getAssujettisByPeriodeDeclaration(periode);
            if (!assujetis.isEmpty()) {
                nbreAssuj = assujetis.stream().map((_item) -> 1).reduce(nbreAssuj, Integer::sum);
            }

            Archive archive = DataAccess.getArchiveByRefDocument(numero);

            if (archive != null) {

                dataReturn = archive.getDocumentString();

            } else {

                PrintDocument printDocument = new PrintDocument();
                RetraitDeclaration retaDeclaration;

                retaDeclaration = DataAccess.getRetraiteclarationByID(Integer.valueOf(numero));

                if (retaDeclaration != null) {
                    if (nbreAssuj > 1) {
                        dataReturn = printDocument.createNoteTaxationDeclaration(retaDeclaration, retaDeclaration.getFkAgentCreate(), amountPD, "");
                    } else {
                        dataReturn = printDocument.createNoteTaxationDeclaration(retaDeclaration, retaDeclaration.getFkAgentCreate(), amountPD, periodeTitle);
                    }
                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public static String printNotePerception(String numero) {

        try {

            Archive archive = DataAccess.getArchiveByRefDocument(numero);

            if (archive != null) {

                return archive.getDocumentString();

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String loadAdresses(String libelle) throws IOException {

        propertiesConfig = new PropertiesConfig();
        List<JSONObject> jsonAdresses = new ArrayList<>();
        try {

            String codeHautKatanga = propertiesConfig.getContent("CODE_HAUT_KATANGA");
            List<EntiteAdministrative> avenues = dataAccess.getEntiteAdministratives(libelle == null
                    ? GeneralConst.EMPTY_STRING
                    : libelle);

            for (EntiteAdministrative avenue : avenues) {
                String codeProvince = avenue.getEntiteMere().getEntiteMere().getEntiteMere().getEntiteMere().getEntiteMere().getCode();
//                if (codeProvince.equals(codeHautKatanga)) {

//                System.out.println("avenue : " + avenue.getIntitule());
                JSONObject jsonAdresse = new JSONObject();
                jsonAdresse.put("codeAvenue", avenue == null
                        ? GeneralConst.EMPTY_STRING : avenue.getCode());

                jsonAdresse.put("avenue", avenue == null
                        ? GeneralConst.EMPTY_STRING : avenue.getIntitule());

                if (avenue != null) {
                    EntiteAdministrative quartier = avenue.getEntiteMere();
                    jsonAdresse.put("codeQuartier", quartier == null
                            ? GeneralConst.EMPTY_STRING : quartier.getCode());

                    jsonAdresse.put("quartier", quartier == null
                            ? GeneralConst.EMPTY_STRING : quartier.getIntitule());

                    if (quartier != null) {
                        EntiteAdministrative commune = quartier.getEntiteMere();
                        jsonAdresse.put("codeCommune", commune == null
                                ? GeneralConst.EMPTY_STRING : commune.getCode());

                        jsonAdresse.put("commune", commune == null
                                ? GeneralConst.EMPTY_STRING : commune.getIntitule());

                        if (commune != null) {
                            //EntiteAdministrative district = commune.getEntiteMere();
                            EntiteAdministrative ville = commune.getEntiteMere();
                            jsonAdresse.put("codeVille", ville == null
                                    ? GeneralConst.EMPTY_STRING : ville.getCode());

                            jsonAdresse.put("ville", ville == null
                                    ? GeneralConst.EMPTY_STRING : ville.getIntitule());

                            if (ville != null) {
                                EntiteAdministrative province = ville.getEntiteMere();
                                jsonAdresse.put("codeProvince", province == null
                                        ? GeneralConst.EMPTY_STRING : province.getCode());

                                jsonAdresse.put("province", province == null
                                        ? GeneralConst.EMPTY_STRING : province.getIntitule());
                            }
                        }
                    }
                }
                jsonAdresses.add(jsonAdresse);
//                }
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonAdresses.toString();
    }

    public static List<AdressePersonne> getAdressePersonnes(JSONArray jsonAdresses) {

        List<AdressePersonne> adressePersonnes = new ArrayList<>();
        try {
            for (int i = 0; i < jsonAdresses.length(); i++) {
                JSONObject jsonobject = jsonAdresses.getJSONObject(i);
                String codeAP = jsonobject.getString("codeAP");
                String code = jsonobject.getString("code");
                String numero = jsonobject.getString("numero");
                String defaut = jsonobject.getString("default");
                String etat = jsonobject.getString("etat");
                String chaine = jsonobject.getString("chaine");

                EntiteAdministrative avenue = DataAccess.getEntiteAdministrativeByCode(code);
                EntiteAdministrative quartier = avenue.getEntiteMere();
                EntiteAdministrative commune = quartier.getEntiteMere();
                EntiteAdministrative district = commune.getEntiteMere();
                EntiteAdministrative ville = district.getEntiteMere();
                EntiteAdministrative province = ville.getEntiteMere();

                Adresse adresse = new Adresse();
                adresse.setNumero(numero);
                adresse.setAvenue(avenue.getCode());
                adresse.setQuartier(quartier.getCode());
                adresse.setCommune(commune.getCode());
                adresse.setDistrict(district.getCode());
                adresse.setVille(ville.getCode());
                adresse.setProvince(province.getCode());
                adresse.setChaine(chaine.concat(" n° ").concat(numero));

                AdressePersonne adressePersonne = new AdressePersonne();
                adressePersonne.setEtat(etat.equals(GeneralConst.NumberString.ONE));
                adressePersonne.setCode(codeAP);
                adressePersonne.setParDefaut(defaut.equals("Oui"));
                adressePersonne.setAdresse(adresse);
                adressePersonnes.add(adressePersonne);
            }
        } catch (JSONException e) {
        }
        return adressePersonnes;
    }

    public static String loadTypeBienByService(int type) throws IOException {

        List<JSONObject> jsonTypeBiens = new ArrayList<>();
        List<JSONObject> jsonCategorieList = new ArrayList<>();
        List<JSONObject> jsonTarifsList = new ArrayList<>();
        List<JSONObject> jsonUsageBienList = new ArrayList<>();
        List<JSONObject> jsonQuartierList = new ArrayList<>();
        List<JSONObject> jsonEaList = new ArrayList<>();
        List<JSONObject> jsonData = new ArrayList<>();
        JSONObject initDataJson = new JSONObject();
        propertiesConfig = new PropertiesConfig();

        String result;

        try {

            List<TypeBien> typeBiens = DataAccess.getTypeBienByType(type);

            if (typeBiens.size() > 0) {

                for (TypeBien typeBien : typeBiens) {

                    JSONObject jsonTypeBien = new JSONObject();

                    jsonTypeBien.put("codeTypeBien",
                            typeBien == null ? GeneralConst.EMPTY_STRING : typeBien.getCode());

                    jsonTypeBien.put("libelleTypeBien", typeBien == null ? GeneralConst.EMPTY_STRING
                            : typeBien.getIntitule());

                    jsonTypeBien.put("estContractuel",
                            typeBien == null ? GeneralConst.EMPTY_STRING : (typeBien.getContractuel())
                                            ? GeneralConst.NumberNumeric.ONE
                                            : GeneralConst.NumberNumeric.ZERO);

                    jsonTypeBiens.add(jsonTypeBien);
                }

                List<Tarif> tarifList = new ArrayList<>();

                switch (type) {
                    case 1:
                        tarifList = DataAccess.getListTarifVignetteOrConcessionMine(propertiesConfig.getContent("CODE_TARIF_VIGNETTE"));
                        break;
                    case 2:
                        tarifList = DataAccess.getListTarifVignetteOrConcessionMine(propertiesConfig.getContent("CODE_TARIF_IMMOBILIER"));
                        break;
                    case 3:

                        if (propertiesConfig.getContent("LOAD_TARIF_ICM_ON_LIGNE").equals("1")) {
                            tarifList = DataAccess.getListTarifVignetteOrConcessionMine(propertiesConfig.getContent("CODE_TARIF_ICM_ON_LIGNE"));
                        } else {
                            tarifList = DataAccess.getListTarifVignetteOrConcessionMine(propertiesConfig.getContent("CODE_TARIF_ICM"));
                        }

                        break;
                    case 4:
                        tarifList = DataAccess.getListTarifPublicite(propertiesConfig.getContent("CODE_ARTICLE_BUDGETAIRE_PUBLICITE"));
                        break;
                }
                if (tarifList.size() > 0) {

                    for (Tarif tarif : tarifList) {

                        JSONObject jsonTarifBien = new JSONObject();

                        jsonTarifBien.put("tarifCode", tarif.getCode());
                        jsonTarifBien.put("tarifName", tarif.getIntitule().toUpperCase());

                        jsonTarifsList.add(jsonTarifBien);

                    }
                }

                List<UsageBien> listUsageBien = DataAccess.getListUsageBiens();

                if (listUsageBien.size() > 0) {

                    for (UsageBien usageBien : listUsageBien) {

                        JSONObject jsonUsageBien = new JSONObject();

                        jsonUsageBien.put("id", usageBien.getId());
                        jsonUsageBien.put("intitule", usageBien.getIntitule());

                        jsonUsageBienList.add(jsonUsageBien);
                    }
                }

                List<EntiteAdministrative> listCommune = DataAccess.getListCommuneHautKatanga(
                        propertiesConfig.getContent("CODE_DISTRICT_HAUT_KATANGA"));

                if (listCommune.size() > 0) {

                    for (EntiteAdministrative ea : listCommune) {

                        JSONObject jsonEa = new JSONObject();

                        String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");
                        jsonEa.put("code", ea.getCode());
                        jsonEa.put("communeComposite", ea.getIntitule().toUpperCase().concat(ville));
                        jsonEa.put("commune", ea.getIntitule().toUpperCase());
                        jsonEa.put("districtName", ea.getEntiteMere().getIntitule().toUpperCase());
                        jsonEa.put("districtCode", ea.getEntiteMere().getCode());

                        List<EntiteAdministrative> listQuartiers = DataAccess.getListQuartierByCommune(ea.getCode());

                        for (EntiteAdministrative eaQuaertier : listQuartiers) {

                            JSONObject jsonEaQuartier = new JSONObject();

                            jsonEaQuartier.put("communeCode", ea.getCode());
                            jsonEaQuartier.put("quartierCode", eaQuaertier.getCode());
                            jsonEaQuartier.put("quartierName", eaQuaertier.getIntitule().toUpperCase());

                            List<Tarif> listTarifs = DataAccess.getListCategorieImmobilierByQuartier(eaQuaertier.getCode());

                            if (listTarifs.size() > 0) {

                                for (Tarif tarif : listTarifs) {

                                    JSONObject jsonTarif = new JSONObject();

                                    jsonTarif.put("code", tarif.getCode());
                                    jsonTarif.put("intitule", tarif.getIntitule().toUpperCase());
                                    jsonTarif.put("codeQuartier", eaQuaertier.getCode());

                                    jsonCategorieList.add(jsonTarif);
                                }
                            }

                            jsonEaQuartier.put("categorieBienList", jsonCategorieList);

                            jsonQuartierList.add(jsonEaQuartier);

                        }

                        jsonEa.put("quartierList", jsonQuartierList);

                        jsonEaList.add(jsonEa);
                    }

                }

                initDataJson.put("typeBienList", jsonTypeBiens);
                initDataJson.put("tarifBienList", jsonTarifsList);
                initDataJson.put("usageBienList", jsonUsageBienList);
                initDataJson.put("eaList", jsonEaList);
                jsonData.add(initDataJson);

                result = jsonData.toString();

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public static String loadTypeComplementBiens(String codeTypeBien, String idBien) {

        JSONObject data = new JSONObject();

        try {

            infoComplementaires = null;

            if (idBien.equals(GeneralConst.EMPTY_NULL)) {
                data.put("bien", GeneralConst.EMPTY_STRING);
            } else {
                List<ComplementBien> complementBienList = DataAccess.getListComplementBien(idBien);
                if (complementBienList == null || complementBienList.isEmpty()) {
                    data.put("bien", GeneralConst.EMPTY_STRING);
                } else {
                    data.put("bien", getValeurComplementBiens(complementBienList));
                }
            }

            List<JSONObject> jsonTypeComplementBiens = getTypeComplementBiens(codeTypeBien);

            data.put("typeComplementBienList", jsonTypeComplementBiens);

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return data.toString();
    }

    public static JSONObject getValeurComplementBiens(List<ComplementBien> complementBiens) {

        JSONObject data = new JSONObject();

        try {

            Bien bien = complementBiens.get(0).getBien();
            data.put("idBien", bien.getId());
            data.put("intituleBien", bien.getIntitule());

            data.put("codeUsage", bien.getFkUsageBien() != null ? bien.getFkUsageBien() : GeneralConst.EMPTY_STRING);
            data.put("codeCommune", bien.getFkCommune() != null ? bien.getFkCommune() : GeneralConst.EMPTY_STRING);
            data.put("codeCategorie", bien.getFkTarif() != null ? bien.getFkTarif() : GeneralConst.EMPTY_STRING);
            data.put("codeQuartier", bien.getFkQuartier() != null ? bien.getFkQuartier() : GeneralConst.EMPTY_STRING);

            data.put("descriptionBien", bien.getDescription() != null ? bien.getDescription() : GeneralConst.EMPTY_STRING);
            data.put("dateAcquisition", bien.getAcquisitionList() == null ? GeneralConst.EMPTY_STRING : bien.getAcquisitionList().get(0).getDateAcquisition());
            data.put("codeTypeBien", bien.getTypeBien() == null ? GeneralConst.EMPTY_STRING : bien.getTypeBien().getCode());
            data.put("codeAdressePersonne", bien.getFkAdressePersonne() == null ? GeneralConst.EMPTY_STRING : bien.getFkAdressePersonne().getCode());
            data.put("chaineAdressePersonne", bien.getFkAdressePersonne() == null ? GeneralConst.EMPTY_STRING : bien.getFkAdressePersonne().getAdresse() == null ? GeneralConst.EMPTY_STRING : bien.getFkAdressePersonne().getAdresse().toString());

            List<JSONObject> dataList = new ArrayList<>();

            for (ComplementBien cb : complementBiens) {

                JSONObject dataCB = new JSONObject();
                dataCB.put("id", cb.getId());
                dataCB.put("codeTypeComplementBien", cb.getTypeComplement() == null ? GeneralConst.EMPTY_STRING : cb.getTypeComplement().getCode());
                dataCB.put("valeur", cb.getValeur());
                dataCB.put("uniteDevise", cb.getDevise() == null ? GeneralConst.EMPTY_STRING : cb.getDevise());
                dataList.add(dataCB);

            }

            data.put("complementList", dataList);

        } catch (Exception e) {

        }
        return data;
    }

    public static List<JSONObject> getTypeComplementBiens(String codeTypeBien) {

        List<JSONObject> jsonTypeComplementBiens = new ArrayList<>();

        try {
            List<TypeComplementBien> typeComplementBiens = DataAccess.getTypeComplementBienByTypeBien(codeTypeBien);

            if (typeComplementBiens != null) {
                int counter = 0;
                for (TypeComplementBien typeComplementBien : typeComplementBiens) {

                    JSONObject jsonTypeComplementBien = new JSONObject();
                    TypeComplement typeComplement = typeComplementBien.getComplement();

                    jsonTypeComplementBien.put("codeTypeComplement",
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getCode());

                    jsonTypeComplementBien.put("libelleTypeComplement",
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getIntitule());

                    jsonTypeComplementBien.put("objetInteraction",
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getObjetInteraction());

                    jsonTypeComplementBien.put("valeurPredefinie",
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getValeurPredefinie());

                    jsonTypeComplementBien.put("inputValue",
                            createControl(typeComplementBien, counter));

                    jsonTypeComplementBiens.add(jsonTypeComplementBien);
                    counter++;
                }
            }

        } catch (Exception e) {
        }
        return jsonTypeComplementBiens;
    }

    public static String createControl(final TypeComplementBien typeComplementBien, int counter) {

        String CONTROL_ID = GeneralConst.PREFIX_IDENTIFIANT_KEY.concat(String.valueOf(counter));
        String value = GeneralConst.EMPTY_STRING;

        TypeComplement typeComplement = typeComplementBien.getComplement();

        final String OBJET_INTERACTION = (typeComplement == null)
                ? GeneralConst.EMPTY_STRING : typeComplement.getObjetInteraction();

        final String CONTROL_VALUE = (typeComplement == null)
                ? GeneralConst.EMPTY_STRING : typeComplement.getIntitule();

        final boolean HAS_VALUES_LIST = (typeComplement == null)
                ? false : typeComplement.getValeurPredefinie();

        if (HAS_VALUES_LIST) {

            value = getControlList(
                    CONTROL_ID,
                    CONTROL_VALUE,
                    typeComplementBien,
                    GeneralConst.EMPTY_STRING);

        } else {

            switch (OBJET_INTERACTION) {
                case "":
                    value = getInputControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            "text",
                            typeComplementBien);
                    break;
                case "DATE":
                    value = getInputControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            "date",
                            typeComplementBien);
                    break;
                case "MONTANT":
                    value = getBoxControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            "number",
                            typeComplementBien, true);
                    break;
                case "NOMBRE":
                    value = getBoxControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            "number",
                            typeComplementBien, false);
                    break;
            }
        }

        return value;
    }

    public static String getBoxControl(String controlId, String controlValue, String type, TypeComplementBien typeComplementBien, boolean hasDevise) {

        String selectId, options = GeneralConst.EMPTY_STRING;
        List<Devise> devises = null;
        List<Unite> unites = null;

        if (hasDevise) {

            selectId = controlId + GeneralConst.DASH_SEPARATOR + "d";

            if (devises == null) {
                devises = DataAccess.getListAllDevises();
            }

            if (devises != null) {
                options = "<option value=\"0\" selected disabled>--- Devise ---</option>";
                for (Devise devise : devises) {
                    options += String.format("<option value =\"%s\" %s>%s</option>",
                            devise.getCode(),
                            GeneralConst.EMPTY_STRING,
                            devise.getIntitule());
                }
            }

        } else {

            selectId = controlId + GeneralConst.DASH_SEPARATOR + "u";

            if (unites == null) {
                unites = DataAccess.getListAllUnites();
            }

            if (unites != null) {
                options = "<option value=\"0\" selected disabled>--- Unité ---</option>";
                for (Unite unite : unites) {
                    options += String.format("<option value =\"%s\" %s>%s</option>",
                            unite.getCode(),
                            GeneralConst.EMPTY_STRING,
                            unite.getIntitule());
                }
            }

        }

        ComplementData complementData = getKeyNameComplement(typeComplementBien);

        String requiredValue = construstRequiredValue(complementData == null
                ? GeneralConst.NumberString.ZERO
                : complementData.keyName);

        String value = String.format(GeneralConst.BOX_CONTROL,
                controlId,
                controlValue,
                requiredValue,
                type,
                controlId,
                controlValue,
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.keyName == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.keyName),
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.complement == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.complement.getValeur()),
                selectId,
                options);

        return value;
    }

    public static String getInputControl(String controlId, String controlValue, String type, TypeComplementBien typeComplementBien) {

        ComplementData complementData = getKeyNameComplement(typeComplementBien);

        String requiredValue = construstRequiredValue(complementData == null
                ? GeneralConst.NumberString.ZERO
                : complementData.keyName);

        String value = String.format(GeneralConst.INPUT_CONTROL,
                controlId,
                controlValue,
                requiredValue,
                type,
                controlId,
                controlValue,
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.keyName == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.keyName),
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.complement == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.complement.getValeur()));
        return value;
    }

    public static class ComplementData {

        public String keyName;
        public ComplementBien complement;
    }

    public static ComplementData getKeyNameComplement(TypeComplementBien typeComplementBien) {

        ComplementData complementData = new ComplementData();

        complementData.complement = getCorrespondingComplement(typeComplementBien);

        String idComplement = (complementData.complement == null)
                ? GeneralConst.NumberString.ZERO
                : complementData.complement.getId();

        TypeComplement typeComplement = typeComplementBien.getComplement();

        complementData.keyName = idComplement
                .concat(GeneralConst.DASH_SEPARATOR)
                .concat(typeComplementBien.getCode())
                .concat(GeneralConst.DASH_SEPARATOR)
                .concat(typeComplement == null
                                ? GeneralConst.NumberString.ZERO
                                : (typeComplementBien.getComplement().getObligatoire()
                                        ? GeneralConst.NumberString.ONE
                                        : GeneralConst.NumberString.ZERO));
        return complementData;
    }

    public static ComplementBien getCorrespondingComplement(TypeComplementBien typeComplementBien) {

        if (infoComplementaires != null) {
            for (ComplementBien complementBien : infoComplementaires) {
                if (complementBien.getTypeComplement().equals(typeComplementBien)) {
                    return complementBien;
                }
            }
        }

        return null;
    }

    public static String selectedOption(ComplementBien complementBien, String item) {

        String result = GeneralConst.EMPTY_STRING;
        if (complementBien != null) {
            if (complementBien.getValeur().trim().equals(item.trim())) {
                return "selected";
            }
        }
        return result;
    }

    public static String construstRequiredValue(String codeTCB) {

        String requiredValue;
        String[] array = codeTCB.split(GeneralConst.DASH_SEPARATOR);

        if (array.length > 0) {
            requiredValue = array[2].equals(GeneralConst.NumberString.ZERO)
                    ? GeneralConst.EMPTY_STRING
                    : "<span style=\"color:red\"> *</span>";
        } else {
            requiredValue = GeneralConst.EMPTY_STRING;
        }
        return requiredValue;
    }

    public static String getControlList(String controlId, String controlValue, TypeComplementBien typeComplementBien, final String objetInteraction) {

        ComplementData complementData = getKeyNameComplement(typeComplementBien);

        String options = String.format("<option value =\"%s\" %s>%s</option>",
                GeneralConst.NumberNumeric.ZERO,
                GeneralConst.EMPTY_STRING,
                GeneralConst.TEXT_SELECTED_VALUE);

        TypeComplement typeComplement = (typeComplementBien == null) ? null : typeComplementBien.getComplement();
        if (typeComplement != null) {
            List<ValeurPredefinie> valeurPredefinies = typeComplement.getValeurPredefinieList();
            if (valeurPredefinies != null) {
                for (ValeurPredefinie valeurPredefinie : valeurPredefinies) {
                    options += String.format("<option value =\"%s\" %s>%s</option>",
                            valeurPredefinie.getCode(),
                            selectedOption(complementData == null
                                            ? null
                                            : complementData.complement, valeurPredefinie.getCode()),
                            valeurPredefinie.getValeur().toUpperCase());
                }
            }
        }

        String requiredValue = construstRequiredValue(complementData == null
                ? GeneralConst.NumberString.ZERO
                : complementData.keyName);

        String value = String.format(GeneralConst.COMPLEMENT_LIST,
                controlId,
                controlValue,
                requiredValue,
                controlId,
                controlValue,
                complementData == null ? GeneralConst.EMPTY_STRING : complementData.keyName,
                options);

        return value;
    }

    public static String getAdressePersonneByPersonne(String codePersonne) {

        List<JSONObject> adressePersonneList = new ArrayList<>();

        try {
            List<AdressePersonne> adressePersonnes = DataAccess.getAdressePersonneByPersonne(codePersonne);
            if (adressePersonnes.size() > 0) {
                for (AdressePersonne ap : adressePersonnes) {
                    JSONObject objet = new JSONObject();
                    objet.put("code", ap.getCode());
                    objet.put("codeAvenue", ap.getAdresse().getAvenue());
                    objet.put("chaine", ap.getAdresse().getChaine());
                    adressePersonneList.add(objet);
                }
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return adressePersonneList.toString();
    }

    public static String loadPropriétaires(String valueSearch) {

        try {

            List<JSONObject> listJsonObjects = new ArrayList<>();

            List<Personne> listPersonneAssujetti = DataAccess.getListAssujettisByCriterion(valueSearch.trim());

            if (listPersonneAssujetti != null && !listPersonneAssujetti.isEmpty()) {

                for (Personne personne : listPersonneAssujetti) {

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("code", personne.getCode().toUpperCase().trim());
                    jsonObject.put("nif", personne.getNif() == null ? GeneralConst.EMPTY_STRING : personne.getNif().toUpperCase().trim());
                    jsonObject.put("nomCompletAssujetti", personne.toString().trim());
                    jsonObject.put("libelleFormeJuridique", personne.getFormeJuridique().getIntitule().toUpperCase().trim());
                    jsonObject.put("codeFormeJuridique",
                            personne.getFormeJuridique().getCode().trim());

                    if (personne.getLoginWeb() != null) {

                        jsonObject.put("codeNTD", personne.getLoginWeb().getUsername() == null ? "" : personne.getLoginWeb().getUsername());

                    } else {
                        jsonObject.put("codeNTD", GeneralConst.EMPTY_STRING);
                    }

                    Adresse adresse = DataAccess.getAdresseDefaultByAssujetti(personne.getCode().trim());

                    if (adresse != null) {
                        jsonObject.put("adresseId", adresse.getId().trim());
                        jsonObject.put("adresseAssujetti", adresse.toString().toUpperCase().trim());
                    } else {
                        jsonObject.put("adresseId", GeneralConst.EMPTY_STRING);
                        jsonObject.put("adresseAssujetti", "Aucune adresse");
                    }

                    listJsonObjects.add(jsonObject);
                }

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

            return listJsonObjects.toString();

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String createBien(String object) {

        String result;

        try {
            propertiesConfig = new PropertiesConfig();
            JSONObject data = new JSONObject(object);

            String type = data.getString("type");
            String codeTypeBien = data.getString("codeTypeBien");
            String idBien = data.getString("idBien");
            String codePersonne = data.getString("codePersonne");
            String intituleBien = data.getString("intituleBien");
            String descriptionBien = data.getString("descriptionBien");
            String dateAcquisition = data.getString("dateAcquisition");
            String codeAP = data.getString("codeAP");
            String codeCategorie = data.getString("codeCategorie");

            String codeUsage = data.getString("codeUsage");
            String codeCommune = data.getString("codeCommune");
            String codeQuartier = data.getString("codeQuartier");

            String idUser = propertiesConfig.getContent("CODE_AGENT_SERVICE_WEB");

            JSONArray jsonComplementBiens = new JSONArray(data.getString("complementBiens"));

            Bien bien = new Bien();
            bien.setDescription(descriptionBien);
            bien.setIntitule(intituleBien);
            bien.setTypeBien(new TypeBien(codeTypeBien));
            bien.setFkAdressePersonne(codeAP.equals("") ? null : new AdressePersonne(codeAP));

            bien.setFkCommune(codeCommune);
            bien.setFkTarif(codeCategorie);
            bien.setFkUsageBien(Integer.valueOf(codeUsage));
            bien.setFkQuartier(codeQuartier);

            Acquisition acquisition = new Acquisition();
            acquisition.setDateAcquisition(dateAcquisition);
            acquisition.setPersonne(new Personne(codePersonne));

            List<ComplementBien> complementBiens = getComplementBiens(jsonComplementBiens);

            if (idBien != null && !idBien.equals(GeneralConst.EMPTY_STRING)) {
                bien.setId(idBien);
                if (DataAccess.updateBien(bien, acquisition, complementBiens, Integer.valueOf(idUser), type)) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            } else {
                if (DataAccess.saveBien(bien, acquisition, complementBiens, Integer.valueOf(idUser), type)) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public static List<ComplementBien> getComplementBiens(JSONArray complementsArray) {

        List<ComplementBien> complementBiens = new ArrayList<>();
        try {
            for (int i = 0; i < complementsArray.length(); i++) {
                JSONObject jsonobject = complementsArray.getJSONObject(i);
                String id = jsonobject.getString("id");
                String code = jsonobject.getString("code");
                String valeur = jsonobject.getString("valeur");
                String unite = jsonobject.getString("uniteDevise");

                ComplementBien complementBien = new ComplementBien();
                complementBien.setId(id.equals(GeneralConst.NumberString.ZERO) ? GeneralConst.EMPTY_STRING : id);
                complementBien.setTypeComplement(new TypeComplementBien(code));
                complementBien.setValeur(valeur);
                complementBien.setDevise(unite.equals(GeneralConst.EMPTY_STRING) ? null : unite);
                complementBiens.add(complementBien);
            }
        } catch (JSONException e) {
        }
        return complementBiens;
    }

    public static String getBiensPersonne(String codePersonne) throws Exception {

        String response;

        try {

            List<JSONObject> jsonBienList = new ArrayList<>();

            List<Acquisition> acquisitions = DataAccess.getBiensPersonne(codePersonne);

            if (acquisitions.size() > 0) {

                for (Acquisition acquisition : acquisitions) {

                    Bien bien = acquisition.getBien();

                    JSONObject jsonBien = new JSONObject();

                    jsonBien.put("id", bien.getId());
                    jsonBien.put("Libelle", bien.getIntitule());
                    jsonBien.put("LibelleAdresse", bien.getFkAdressePersonne().getAdresse().toString());
                    jsonBien.put("codeTypeBien", bien.getTypeBien().getCode());
                    jsonBien.put("libelleTypeBien", bien.getTypeBien().getIntitule());
                    jsonBien.put("isProprietaire", acquisition.getProprietaire());
                    jsonBienList.add(jsonBien);

                }
                response = jsonBienList.toString();
            } else {
                response = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            response = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return response;
    }

    public static String loadBiensPersonne(String codePersonne, String forLocation) throws IOException {

        List<JSONObject> listAssujettissement = new ArrayList<>();
        List<JSONObject> jsonBiens = new ArrayList<>();
        List<JSONObject> jsonAssujettis = new ArrayList<>();
        JSONObject jsonAssujettissement = new JSONObject();
        ArticleBudgetaire articleBudgetaire;

        String codeFormeJurique = GeneralConst.EMPTY_STRING;
        String infosComplementaire;

        propertiesConfig = new PropertiesConfig();

        try {

            String codeService = propertiesConfig.getContent("CODE_SERVICE");

            List<Acquisition> acquisitions = DataAccess.getBiensOfPersonneV3(codePersonne.trim());

            if (acquisitions.size() > 0) {
                int _type = 0;

                for (Acquisition acquisition : acquisitions) {

                    JSONObject jsonBien = new JSONObject();

                    TypeBienService typeBienService = DataAccess.getTypeBienServiceByTypeBien(acquisition.getBien().getTypeBien().getCode());

                    _type = typeBienService.getType();

                    codeFormeJurique = acquisition.getPersonne().getFormeJuridique().getCode();

                    jsonBien.put("idAcquisition",
                            acquisition.getBien() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : acquisition.getId());

                    jsonBien.put("type", _type);

                    jsonBien.put("dateAcquisition",
                            acquisition.getBien() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : acquisition.getDateAcquisition());

                    jsonBien.put("codeTypeBien",
                            acquisition.getBien() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : acquisition.getBien().getTypeBien() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : acquisition.getBien().getTypeBien().getCode());

                    jsonBien.put("libelleTypeBien",
                            acquisition.getBien() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : acquisition.getBien().getTypeBien() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : acquisition.getBien().getTypeBien().getIntitule().toUpperCase());

                    jsonBien.put("idBien",
                            acquisition.getBien() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : acquisition.getBien().getId());

                    if (acquisition.getBien().getIntitule() != null) {

                        jsonBien.put("intituleBien", acquisition.getBien().getIntitule().toUpperCase());

                    } else {
                        jsonBien.put("intituleBien", "");
                    }

                    jsonBien.put("descriptionBien",
                            acquisition.getBien().getDescription() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : acquisition.getBien().getDescription());

                    if (acquisition.getBien().getFkCommune() != null) {

                        EntiteAdministrative ea = DataAccess.getEntiteAdministrativeByCode(
                                acquisition.getBien().getFkCommune());

                        String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                        jsonBien.put("communeCode", ea.getCode());
                        jsonBien.put("communeName", ea.getIntitule().toUpperCase().concat(ville));

                    } else {
                        jsonBien.put("communeCode", GeneralConst.EMPTY_STRING);
                        jsonBien.put("communeName", GeneralConst.EMPTY_STRING);
                    }

                    if (acquisition.getBien().getFkQuartier() != null) {

                        EntiteAdministrative eaQuartier = DataAccess.getEntiteAdministrativeByCode(
                                acquisition.getBien().getFkQuartier());

                        jsonBien.put("quartierCode", eaQuartier.getCode());
                        jsonBien.put("quartierName", eaQuartier.getIntitule().toUpperCase());

                    } else {
                        jsonBien.put("quartierCode", GeneralConst.EMPTY_STRING);
                        jsonBien.put("quartierName", GeneralConst.EMPTY_STRING);
                    }

                    if (acquisition.getBien().getFkTarif() != null) {

                        Tarif tarif = DataAccess.getTarifByCode(acquisition.getBien().getFkTarif());

                        jsonBien.put("tarifCode", tarif.getCode());
                        jsonBien.put("tarifName", tarif.getIntitule().toUpperCase());

                    } else {
                        jsonBien.put("tarifCode", GeneralConst.EMPTY_STRING);
                        jsonBien.put("tarifName", GeneralConst.EMPTY_STRING);
                    }

                    infosComplementaire = GeneralConst.EMPTY_STRING;

                    if (acquisition.getBien().getFkUsageBien() != null) {

                        UsageBien usageBien = DataAccess.getUsageBienByCode(acquisition.getBien().getFkUsageBien());

                        jsonBien.put("usageCode", usageBien.getId());
                        jsonBien.put("usageName", usageBien.getIntitule().toUpperCase());
                        jsonBien.put("isImmobilier", GeneralConst.NumberString.ONE);

                    } else {
                        jsonBien.put("usageCode", GeneralConst.EMPTY_STRING);
                        jsonBien.put("usageName", GeneralConst.EMPTY_STRING);
                        jsonBien.put("isImmobilier", GeneralConst.NumberString.ZERO);
                    }

                    String valUnity = GeneralConst.EMPTY_STRING;

                    for (ComplementBien cb : acquisition.getBien().getComplementBienList()) {

                        if (cb.getDevise() != null && !cb.getDevise().isEmpty()) {

                            switch (cb.getDevise()) {
                                case GeneralConst.Devise.DEVISE_CDF:
                                case GeneralConst.Devise.DEVISE_USD:

                                    valUnity = cb.getDevise();

                                    break;
                                default:
                                    Unite unite = DataAccess.getUnitebyCode(cb.getDevise());
                                    valUnity = unite != null ? unite.getIntitule() : "";

                                    break;
                            }

                        } else {
                            valUnity = GeneralConst.EMPTY_STRING;
                        }

                        ValeurPredefinie valeurPredefinie = new ValeurPredefinie();
                        String txtValeurPredefinie = GeneralConst.EMPTY_STRING;

                        if (infosComplementaire.isEmpty()) {

                            if (cb.getTypeComplement().getComplement().getValeurPredefinie()) {

                                valeurPredefinie = DataAccess.getValeurPredefinieByCode(cb.getValeur(), cb.getTypeComplement().getComplement().getCode());
                                txtValeurPredefinie = valeurPredefinie == null ? "" : valeurPredefinie.getValeur().toUpperCase();

                                infosComplementaire = cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(txtValeurPredefinie).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");

                            } else {

                                infosComplementaire = cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(cb.getValeur()).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");
                            }

                        } else {

                            if (cb.getTypeComplement().getComplement().getValeurPredefinie()) {

                                valeurPredefinie = DataAccess.getValeurPredefinieByCode(cb.getValeur(), cb.getTypeComplement().getComplement().getCode());
                                txtValeurPredefinie = valeurPredefinie == null ? "" : valeurPredefinie.getValeur().toUpperCase();

                                infosComplementaire = infosComplementaire += cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(txtValeurPredefinie).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");

                            } else {
                                infosComplementaire = infosComplementaire += cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(cb.getValeur()).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");
                            }
                        }
                    }

                    jsonBien.put("complement", infosComplementaire);

                    jsonBien.put("chaineAdresse",
                            acquisition.getBien() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : acquisition.getBien().getFkAdressePersonne() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : acquisition.getBien().getFkAdressePersonne().getAdresse().toString());

                    jsonBien.put("codeAP",
                            acquisition.getBien() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : acquisition.getBien().getFkAdressePersonne() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : acquisition.getBien().getFkAdressePersonne().getCode());

                    jsonBien.put("proprietaire", acquisition.getProprietaire() == null
                            ? false : acquisition.getProprietaire());

                    String responsable = "Moi";
                    String intituleBien = acquisition.getBien() == null
                            ? GeneralConst.EMPTY_STRING
                            : acquisition.getBien().getIntitule().toUpperCase();
                    String intituleBienHtml = String.format(
                            "<p style=\"font-weight:bold\">%s</p>",
                            acquisition.getBien() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : acquisition.getBien().getIntitule().toUpperCase());

                    boolean isMaster = true;

                    if (!acquisition.getProprietaire()) {

                        if (forLocation != null) {
                            if (forLocation.equals(GeneralConst.NumberString.ONE)) {
                                continue;
                            }
                        }

                        isMaster = false;

                        Bien bien = acquisition.getBien();
                        if (bien != null) {
                            Personne personne = DataAccess.getResponsableBien(bien.getId());
                            if (personne != null) {
                                responsable = personne.toString();
                            }
                        }
                        intituleBienHtml += " <p style=\"color:red\">(Acquis par location)</p>";

                    }

                    jsonBien.put("isMaster", isMaster);
                    jsonBien.put("responsable", responsable);
                    jsonBien.put("intituleBien", intituleBien);
                    jsonBien.put("intituleBienHtml", intituleBienHtml);
                    jsonBiens.add(jsonBien);
                }

                List<Assujeti> assujettissements = new ArrayList<>();

                if (codeService.equals(propertiesConfig.getContent("CODE_SERVICE_TRANSPORT"))) {
                    assujettissements = DataAccess.getAssujettissementByPersonne(codePersonne, codeService);
                } else {
                    assujettissements = DataAccess.getAssujettissementByPersonneV2(codePersonne);
                }

                for (Assujeti assujeti : assujettissements) {

                    JSONObject jsonAssujetti = new JSONObject();

                    jsonAssujetti.put("idAssujettissement", assujeti.getId());
                    Bien bien;

                    if (assujeti.getBien() != null) {
                        bien = assujeti.getBien();
                        jsonAssujetti.put("idBien", bien.getId());
                        jsonAssujetti.put("intituleBien", bien.getIntitule());

                        if (bien.getFkAdressePersonne() != null) {
                            jsonAssujetti.put("adresseBien",
                                    bien.getFkAdressePersonne().getAdresse().toString());
                        } else {
                            jsonAssujetti.put("adresseBien",
                                    GeneralConst.EMPTY_STRING);
                        }

                        jsonAssujetti.put("valeurBase", assujeti.getValeur() == null ? 0 : assujeti.getValeur());
                        jsonAssujetti.put("uniteValeurBase", assujeti.getUniteValeur() == null ? assujeti.getArticleBudgetaire().getUnite().getIntitule().toUpperCase() : assujeti.getUniteValeur());

                        String description = GeneralConst.EMPTY_STRING;
                        List<ComplementBien> complementBiens = bien.getComplementBienList();
                        if (!complementBiens.isEmpty()) {

                            for (ComplementBien cb : complementBiens) {

                                TypeComplement typeComplement;

                                typeComplement = cb.getTypeComplement().getComplement();

                                if (typeComplement.getTaxable()) {

                                    String intitule = typeComplement.getIntitule();
                                    String valeur = GeneralConst.EMPTY_STRING;

                                    switch (typeComplement.getObjetInteraction()) {
                                        case "NOMBRE":
                                            if (cb.getDevise() != null && !cb.getDevise().isEmpty()) {
                                                String unite = DataAccess.getUniteComplement(cb.getDevise());
                                                valeur = cb.getValeur() + " " + unite;
                                            } else {
                                                valeur = cb.getValeur();
                                            }

                                            break;
                                        case "MONTANT":

                                            String montant = "";

                                            if (cb.getValeur() != null && !cb.getValeur().isEmpty()) {
                                                montant = Tools.formatNombreToString("###,###.###", BigDecimal.valueOf(Double.valueOf(cb.getValeur())));
                                            }

                                            String devise = GeneralConst.EMPTY_STRING;

                                            if (cb.getDevise() != null && !cb.getDevise().isEmpty()) {
                                                devise = " " + cb.getDevise();
                                            }

                                            valeur = montant + " " + devise;

                                            break;
                                        default:

                                    }

                                    if (description.isEmpty()) {
                                        description = " - " + intitule + " : <b>" + valeur + "</b>";
                                    } else {
                                        description += "<br/> - " + intitule + " : <b>" + valeur + "</b>";
                                    }

                                }
                            }

                            jsonAssujetti.put("infoComplementBien", description);

                        } else {

                            jsonAssujetti.put("infoComplementBien", GeneralConst.EMPTY_STRING);
                        }

                    } else {

                        jsonAssujetti.put("idBien", GeneralConst.EMPTY_STRING);
                        jsonAssujetti.put("intituleBien", GeneralConst.EMPTY_STRING);
                        jsonAssujetti.put("adresseBien", GeneralConst.EMPTY_STRING);
                        jsonAssujetti.put("infoComplementBien", GeneralConst.EMPTY_STRING);
                    }

                    jsonAssujetti.put("isManyAB", false);

                    jsonAssujetti.put("codeAB",
                            assujeti.getArticleBudgetaire() == null
                                    ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCode());

                    jsonAssujetti.put("intituleAB",
                            assujeti.getArticleBudgetaire() == null
                                    ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getIntitule());

                    jsonAssujetti.put("secteurActivite",
                            assujeti.getArticleBudgetaire() == null || assujeti.getArticleBudgetaire().getArticleGenerique() == null
                            || assujeti.getArticleBudgetaire().getArticleGenerique().getServiceAssiette() == null
                                    ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getArticleGenerique().getServiceAssiette().getIntitule());

                    jsonAssujetti.put("codeOfficiel",
                            assujeti.getArticleBudgetaire() == null
                                    ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCodeOfficiel());

                    jsonAssujetti.put("valeurBase",
                            assujeti.getValeur() == null ? GeneralConst.EMPTY_STRING : assujeti.getValeur());

                    jsonAssujetti.put("etat",
                            assujeti.getEtat() ? GeneralConst.NumberString.ONE : GeneralConst.NumberString.ZERO);

                    if (assujeti.getArticleBudgetaire() != null) {

                        articleBudgetaire = assujeti.getArticleBudgetaire();

                        AbComplementBien abComplementBien = DataAccess.getAbComplementBienByArticleBudgetaire(articleBudgetaire.getCode());

                        if (abComplementBien != null) {

                            if (abComplementBien.getTypeComplementBien().getComplement().getObjetInteraction().equals("MONTANT")) {

                                Palier palier = DataAccess.getFistPalierByArticleBudgetaireV2(articleBudgetaire.getCode(), codeFormeJurique);

                                jsonAssujetti.put("unite", assujeti.getUniteValeur() == null ? GeneralConst.EMPTY_ZERO : assujeti.getUniteValeur());

                            } else {
                                jsonAssujetti.put("unite", assujeti.getUniteValeur() == null ? GeneralConst.EMPTY_ZERO : assujeti.getUniteValeur());
                            }
                        } else {
                            jsonAssujetti.put("unite", assujeti.getUniteValeur() == null ? GeneralConst.EMPTY_ZERO : assujeti.getUniteValeur());
                        }

                    } else {
                        jsonAssujetti.put("unite", GeneralConst.EMPTY_STRING);
                    }

                    jsonAssujetti.put("duree",
                            assujeti.getDuree() == null ? GeneralConst.EMPTY_STRING : assujeti.getDuree());

                    Date dateDebut = ConvertDate.formatDate(ConvertDate.getValidFormatDatePrint(assujeti.getDateDebut()));
                    Date dateFin = ConvertDate.formatDate(ConvertDate.getValidFormatDatePrint(assujeti.getDateFin()));

                    jsonAssujetti.put("dateDebut",
                            assujeti.getPeriodicite() == null ? GeneralConst.EMPTY_STRING : Tools.getPeriodeIntitule(dateDebut, assujeti.getArticleBudgetaire() == null
                                                    ? GeneralConst.EMPTY_STRING
                                                    : assujeti.getArticleBudgetaire().getPeriodicite() == null
                                                            ? GeneralConst.EMPTY_STRING
                                                            : assujeti.getArticleBudgetaire().getPeriodicite().getCode()));

                    jsonAssujetti.put("dateFin",
                            assujeti.getPeriodicite() == null ? GeneralConst.EMPTY_STRING : Tools.getPeriodeIntitule(dateFin, assujeti.getArticleBudgetaire() == null
                                                    ? GeneralConst.EMPTY_STRING
                                                    : assujeti.getArticleBudgetaire().getPeriodicite() == null
                                                            ? GeneralConst.EMPTY_STRING
                                                            : assujeti.getArticleBudgetaire().getPeriodicite().getCode()));

                    jsonAssujetti.put("dateFin2", assujeti.getDateFin() == null
                            ? GeneralConst.EMPTY_STRING
                            : ConvertDate.getValidFormatDatePrint(assujeti.getDateFin()));

                    jsonAssujetti.put("reconduction",
                            assujeti.getRenouvellement() == null ? GeneralConst.EMPTY_STRING : assujeti.getRenouvellement());

                    jsonAssujetti.put("nombreJour",
                            assujeti.getArticleBudgetaire() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : assujeti.getArticleBudgetaire().getPeriodicite().getNbrJour());

                    jsonAssujetti.put("nombreJourLimite",
                            assujeti.getArticleBudgetaire().getNbrJourLimite() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : assujeti.getArticleBudgetaire().getNbrJourLimite());

                    String nouvellesEcheances = assujeti.getNouvellesEcheances();
                    String echeanceLegale, nbreJourLegalePaiement;

                    if (nouvellesEcheances == null || nouvellesEcheances.isEmpty()) {

                        echeanceLegale = assujeti.getArticleBudgetaire().getEcheanceLegale() == null
                                ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getEcheanceLegale();

                        nbreJourLegalePaiement = assujeti.getArticleBudgetaire().getDateLimitePaiement() == null
                                ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getDateLimitePaiement();

                    } else {

                        JSONObject nvlEcheanceJson = new JSONObject(nouvellesEcheances);
                        echeanceLegale = nvlEcheanceJson.getString("echeanceLegale");
                        nbreJourLegalePaiement = nvlEcheanceJson.getString("dateLimitePaiement");
                    }

                    jsonAssujetti.put("echeanceLegale", echeanceLegale);
                    jsonAssujetti.put("nbreJourLegalePaiement", nbreJourLegalePaiement);

                    if (assujeti.getArticleBudgetaire() == null) {
                        jsonAssujetti.put("periodeEcheance", GeneralConst.EMPTY_STRING);
                    } else {
                        if (assujeti.getArticleBudgetaire().getPeriodeEcheance() != null) {
                            jsonAssujetti.put("periodeEcheance",
                                    assujeti.getArticleBudgetaire().getPeriodeEcheance() == true
                                            ? GeneralConst.NumberString.ONE
                                            : GeneralConst.NumberString.ZERO);
                        } else {
                            jsonAssujetti.put("periodeEcheance", GeneralConst.EMPTY_STRING);
                        }
                    }

                    jsonAssujetti.put("codePeriodiciteAB",
                            assujeti.getArticleBudgetaire().getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : assujeti.getArticleBudgetaire().getPeriodicite().getCode());

                    jsonAssujetti.put("libellePeriodiciteAB",
                            assujeti.getArticleBudgetaire().getPeriodicite() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : assujeti.getArticleBudgetaire().getPeriodicite().getIntitule());

                    jsonAssujetti.put("montant", GeneralConst.Numeric.ZERO);
                    jsonAssujetti.put("devise", GeneralConst.Devise.DEVISE_USD);

                    jsonAssujetti.put("intituleTarif", assujeti.getTarif() == null
                            ? GeneralConst.EMPTY_STRING
                            : assujeti.getTarif().getIntitule());

                    List<JSONObject> jsonPeriodes = new ArrayList<>();

                    for (PeriodeDeclaration periode : assujeti.getPeriodeDeclarationList()) {

                        if (!assujeti.getEtat() && periode.getNoteCalcul() == null) {
                            continue;
                        }

                        JSONObject jsonPeriode = new JSONObject();
                        jsonPeriode.put("periodeID", periode.getId());
                        jsonPeriode.put("periode", Tools.getPeriodeIntitule(periode.getDebut(),
                                periode.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode()));

                        AdressePersonne adressePersonne = periode.getAssujetissement().getFkAdressePersonne();

                        jsonPeriode.put("libelleAdresse", (adressePersonne == null) ? GeneralConst.EMPTY_STRING : adressePersonne.getAdresse().toString());
                        jsonPeriode.put("dateLimite", periode.getDateLimite() != null ? Tools.formatDateToString(periode.getDateLimite()) : GeneralConst.EMPTY_STRING);
                        jsonPeriode.put("dateLimitePaiement", periode.getDateLimitePaiement() != null ? Tools.formatDateToString(periode.getDateLimitePaiement()) : GeneralConst.EMPTY_STRING);

                        jsonPeriode.put("statePeriodeDeclaration", periode.getEtat());

                        jsonPeriode.put("observation", periode.getObservation() != null
                                ? periode.getObservation() : GeneralConst.EMPTY_STRING);

                        if (periode.getEtat() == GeneralConst.Numeric.ZERO) {

                            Agent agent = DataAccess.getAgentByCode(String.valueOf(periode.getAgentMaj()));

                            if (agent != null) {
                                jsonPeriode.put("agentMaj", agent.toString().toUpperCase());
                            } else {
                                jsonPeriode.put("agentMaj", GeneralConst.EMPTY_STRING);
                            }

                            String dateMaj = ConvertDate.formatDateHeureToStringV2(periode.getDateMaj());

                            jsonPeriode.put("dateMaj", dateMaj);

                        } else {
                            jsonPeriode.put("agentMaj", GeneralConst.EMPTY_STRING);
                            jsonPeriode.put("dateMaj", GeneralConst.EMPTY_STRING);
                        }
                        //here
                        jsonPeriode.put("dateDeclaration", GeneralConst.EMPTY_STRING);
                        jsonPeriode.put("noteTaxation", GeneralConst.EMPTY_STRING);
                        jsonPeriode.put("notePerception", GeneralConst.EMPTY_STRING);
                        jsonPeriode.put("notePerceptionManuel", GeneralConst.EMPTY_STRING);
                        jsonPeriode.put("estPenalise",
                                GeneralConst.NumberString.ZERO);
                        jsonPeriode.put("estPenalisePaiement",
                                GeneralConst.NumberString.ZERO);

                        if (periode.getDateLimite() != null) {
                            if (Compare.before(periode.getDateLimite(), new Date())) {
                                jsonPeriode.put("estPenalise",
                                        GeneralConst.NumberString.ONE);
                            }
                        }

                        if (periode.getDateLimitePaiement() != null) {

                            if (Compare.before(periode.getDateLimitePaiement(), new Date())) {
                                jsonPeriode.put("estPenalisePaiement",
                                        GeneralConst.NumberString.ONE);
                            }
                        }

                        if (periode.getNoteCalcul() != null) {

                            NoteCalcul nc = DataAccess.getNoteCalculByNumero(periode.getNoteCalcul());

                            BigDecimal montantDu = DataAccess.getMontantTotalDuDetailNoteCalculv2(periode.getNoteCalcul());
                            BigDecimal totalDu = montantDu;

                            String devise = Tools.getDeviseByNC(nc);

                            if (nc.getDateCloture() != null) {
                                jsonPeriode.put("waittingClosing", "0");
                            } else {
                                jsonPeriode.put("waittingClosing", "1");
                            }

                            jsonPeriode.put("devise", devise);
                            jsonPeriode.put("noteTaxation", periode.getNoteCalcul());
                            jsonPeriode.put("dateDeclaration", Tools.formatDateToString(nc.getDateCreat()));
                            jsonPeriode.put("montantDu", montantDu);

                            jsonAssujetti.put("montant", montantDu);
                            jsonAssujetti.put("devise", devise);

                            jsonPeriode.put("codeOfficiel",
                                    periode.getAssujetissement().getArticleBudgetaire().getCodeOfficiel().toUpperCase());

                            jsonPeriode.put("totalDu", totalDu);
                            jsonPeriode.put("montantPayer", GeneralConst.Numeric.ZERO);
                            jsonPeriode.put("restePayer", totalDu);

                            NotePerception np = nc.getDetailsNcList().get(GeneralConst.Numeric.ZERO).getNotePerception();

                            if (np != null) {

                                jsonPeriode.put("notePerception", np.getNumero());
                                jsonPeriode.put("notePerceptionManuel", np.getNumero());
                                jsonPeriode.put("montantDu", np.getNetAPayer());

                                if (np.getNbrImpression() > GeneralConst.Numeric.ZERO) {
                                    jsonPeriode.put("devise", np.getDevise());
                                }

                            } else {
                                jsonPeriode.put("notePerceptionManuel",
                                        GeneralConst.EMPTY_STRING);
                            }

                        }

                        jsonPeriodes.add(jsonPeriode);
                    }

                    jsonAssujetti.put("listPeriodesDeclarations", jsonPeriodes);

                    jsonAssujettis.add(jsonAssujetti);
                }

                jsonAssujettissement.put("listBiens", jsonBiens);
                jsonAssujettissement.put("listArticleBudgetaires", jsonAssujettis);
                listAssujettissement.add(jsonAssujettissement);
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return listAssujettissement.toString();
    }

    public static String setNewPassword(String objet) {

        try {

            JSONObject jobjet = new JSONObject(objet);
            String userName = jobjet.getString("ntd");
            String password = jobjet.getString("password");

            if (DataAccess.updatePasswordV2(userName, password)) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public static String setLocation(String file) {

        String result;

        try {

            JSONObject jsono = new JSONObject(file);
            String codePersonne = jsono.getString("codePersonne");
            String idBien = jsono.getString("idBien");
            String dateAcquisition = jsono.getString("dateAcquisition");
            String referenceContrat = jsono.getString("referenceContrat");
            String numActeNotarie = jsono.getString("numActeNotarie");
            String dateActeNotarie = jsono.getString("dateActeNotarie");

            Acquisition acquisition = new Acquisition();
            acquisition.setPersonne(new Personne(codePersonne));
            acquisition.setBien(new Bien(idBien));
            acquisition.setDateAcquisition(dateAcquisition);
            acquisition.setProprietaire(false);
            acquisition.setReferenceContrat(referenceContrat);
            acquisition.setNumActeNotarie(numActeNotarie);
            acquisition.setDateActeNotarie(dateActeNotarie);

            if (DataAccess.createAcquisition(acquisition)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public static String setPermutation(String file) {

        String result = null;
        Acquisition acquis;

        try {
            JSONObject jsono = new JSONObject(file);
            String codePersonne = jsono.getString("codePersonne");
            String idBien = jsono.getString("idBien");
            String dateAcquisition = jsono.getString("dateAcquisition");
            String referenceContrat = jsono.getString("referenceContrat");
            String numActeNotarie = jsono.getString("numActeNotarie");
            String dateActeNotarie = jsono.getString("dateActeNotarie");
            String codeProprieteBien = jsono.getString("codeProprietaire");

            acquis = DataAccess.getAcquisistionByProprietaire(codeProprieteBien, idBien);

            if (acquis != null) {

                Acquisition acquisition = new Acquisition();
                acquisition.setPersonne(new Personne(codePersonne));
                acquisition.setBien(new Bien(idBien));
                acquisition.setDateAcquisition(dateAcquisition);
                acquisition.setProprietaire(true);
                acquisition.setReferenceContrat(referenceContrat);
                acquisition.setNumActeNotarie(numActeNotarie);
                acquisition.setDateActeNotarie(dateActeNotarie);

                if (DataAccess.saveAcquisition(acquisition, acquis.getId())) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public static String deleteBienByCode(String code) {
        try {

            if (DataAccess.deleteBien(code)) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String getArticleBudgetaireAssujettissable() throws IOException {

        List<JSONObject> objects = new ArrayList<>();
        propertiesConfig = new PropertiesConfig();

        try {
            String codeImpot = propertiesConfig.getContent(PropertiesConst.Config.IMPOT_ASSUJETTISSEMENT);
            List<ArticleBudgetaire> abs = DataAccess.getArticleBudgetaireAssujettissable(codeImpot);

            if (abs.size() > 0) {

                for (ArticleBudgetaire ab : abs) {
                    JSONObject object = new JSONObject();
                    object.put("codeAb", ab.getCode());
                    object.put("libelleAb", ab.getIntitule());
                    objects.add(object);
                }

                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String loadArticlesBudgetaires(String idBien, String ab, String type) throws IOException {

        List<JSONObject> jsonArticlesBudgetaires = new ArrayList<>();
        propertiesConfig = new PropertiesConfig();

        try {

            ArticleBudgetaire articleBudgetaire;
            Bien bien = null;
            ComplementBien cBien = null;
            List<AbComplementBien> abComplementBiens = new ArrayList<>();

            if (!ab.isEmpty()) {

                String impotFoncier = propertiesConfig.getContent("CODE_IMPOT_FONCIER");
                String impotIcm = propertiesConfig.getContent("CODE_IMPOT_ICM");
                String taxePublicite = propertiesConfig.getContent("CODE_TAXE_PUBLICITE");
                String paramComplementBien = propertiesConfig.getContent("PARAM_TYPE_BIEN_COMPLEMENT_TAXABLE");

                articleBudgetaire = DataAccess.getArticleBudgetaireByCode(ab);
                bien = DataAccess.getBienById(idBien);

                if (ab.equals(impotFoncier) || ab.equals(impotIcm) || ab.equals(taxePublicite)) {

                    Gson gson = new Gson();

                    Type listType = new TypeToken<List<ComplementMockIF>>() {
                    }.getType();

                    List<ComplementMockIF> complementMockIFs = gson.fromJson(paramComplementBien, listType);

                    String coComplementTyBien;

                    for (ComplementMockIF complementMockIF : complementMockIFs) {

                        if (complementMockIF.getTypeBien().equals(bien.getTypeBien().getCode())) {

                            coComplementTyBien = complementMockIF.getTypeComplementBien();
                            cBien = DataAccess.getComplementBien(coComplementTyBien, idBien);
                        }

                    }

                }

                if (articleBudgetaire != null) {

                    AbComplementBien abCompl = new AbComplementBien();
                    abCompl.setArticleBudgetaire(articleBudgetaire);
                    abComplementBiens.add(abCompl);
                }

            } else {

                abComplementBiens = DataAccess.getAssujettissableAbByBien(idBien);

            }

            for (AbComplementBien abComplementBien : abComplementBiens) {

                articleBudgetaire = abComplementBien.getArticleBudgetaire();
                TypeComplementBien typeComplementBien = abComplementBien.getTypeComplementBien();

                String codeTypeComplement = typeComplementBien == null
                        ? GeneralConst.EMPTY_STRING
                        : typeComplementBien.getComplement().getCode();

                String codeTypeComplementBien = typeComplementBien == null
                        ? GeneralConst.EMPTY_STRING
                        : typeComplementBien.getCode();

                JSONObject jsonArticleBudgetaire = new JSONObject();

                jsonArticleBudgetaire.put("intituleComplementIF",
                        cBien == null
                                ? GeneralConst.EMPTY_STRING
                                : cBien.getTypeComplement().getComplement().getIntitule());

                jsonArticleBudgetaire.put("ValeurComplementIF",
                        cBien == null
                                ? GeneralConst.EMPTY_STRING
                                : cBien.getValeur());

                switch (ab) {

                    case "00000000000002312021":
                    case "00000000000002272020":

                        if (cBien.getDevise() != null) {

                            Unite unite = DataAccess.getUnitebyCode(cBien.getDevise());

                            jsonArticleBudgetaire.put("codeUniteComplement",
                                    unite == null
                                            ? GeneralConst.EMPTY_STRING
                                            : unite.getIntitule());
                        } else {
                            jsonArticleBudgetaire.put("codeUniteComplement", GeneralConst.EMPTY_STRING);
                        }

                        break;

                    default:
                        jsonArticleBudgetaire.put("codeUniteComplement",
                                cBien == null
                                        ? GeneralConst.EMPTY_STRING
                                        : cBien.getDevise());
                        break;
                }

                jsonArticleBudgetaire.put("codeAB",
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getCode());

                jsonArticleBudgetaire.put("intituleAB",
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getIntitule());

                jsonArticleBudgetaire.put("codeOfficiel",
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getCodeOfficiel());

                jsonArticlesBudgetaires.add(jsonArticleBudgetaire);

                jsonArticleBudgetaire.put("codePeriodiciteAB",
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getPeriodicite().getCode());

                jsonArticleBudgetaire.put("libellePeriodiciteAB",
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getPeriodicite().getIntitule());

                jsonArticleBudgetaire.put("codeTarifAB",
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getTarif().getCode());

                String tarif = bien != null ? bien.getFkTarif() : GeneralConst.EMPTY_STRING;

                jsonArticleBudgetaire.put("codeTarifBien", tarif);

                List<JSONObject> jsonPaliers = getJsonPaliersByAB(articleBudgetaire, tarif, type);

                jsonArticleBudgetaire.put("paliers", jsonPaliers);

                jsonArticleBudgetaire.put("codeTypeComplement",
                        typeComplementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : codeTypeComplement);

                jsonArticleBudgetaire.put("libelleTypeComplement",
                        typeComplementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : typeComplementBien.getComplement().getIntitule());

                jsonArticleBudgetaire.put("baseVariable",
                        typeComplementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : (typeComplementBien.getComplement().getBaseVariable())
                                        ? GeneralConst.NumberString.ONE
                                        : GeneralConst.NumberString.ZERO);

                jsonArticleBudgetaire.put("periodiciteVariable",
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getPeriodiciteVariable());

                jsonArticleBudgetaire.put("tarifVariable",
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getTarifVariable());

                ComplementBien complementBien = DataAccess.getComplementBien(codeTypeComplementBien, idBien);

                jsonArticleBudgetaire.put("code",
                        typeComplementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : typeComplementBien.getCode());

                jsonArticleBudgetaire.put("id",
                        complementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : complementBien.getId());

                jsonArticleBudgetaire.put("valeur",
                        complementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : complementBien.getValeur());

                if (complementBien != null) {

                    if (complementBien.getTypeComplement().getComplement().getObjetInteraction().equals("MONTANT")) {
                        Palier palier = DataAccess.getFistPalierByArticleBudgetaire(articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getCode());
                        jsonArticleBudgetaire.put("unite", palier.getDevise());
                    } else {
                        jsonArticleBudgetaire.put("unite", articleBudgetaire.getUnite().getIntitule());
                    }
                } else {
                    jsonArticleBudgetaire.put("unite", GeneralConst.EMPTY_STRING);
                }

                jsonArticleBudgetaire.put("nombreJourLimite",
                        articleBudgetaire == null ? GeneralConst.NumberString.ZERO
                                : articleBudgetaire.getNbrJourLimite());

                String echeanceLegale = articleBudgetaire == null ? GeneralConst.EMPTY_STRING
                        : articleBudgetaire.getEcheanceLegale() == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getEcheanceLegale();

                jsonArticleBudgetaire.put("echeanceLegale", echeanceLegale);

                jsonArticleBudgetaire.put("nbreJourLegalePaiement",
                        articleBudgetaire == null ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getDateLimitePaiement() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getDateLimitePaiement());

                if (articleBudgetaire == null) {
                    jsonArticleBudgetaire.put("periodeEcheance", GeneralConst.EMPTY_STRING);
                } else {
                    if (articleBudgetaire.getPeriodeEcheance() != null) {
                        jsonArticleBudgetaire.put("periodeEcheance",
                                articleBudgetaire.getPeriodeEcheance() == true
                                        ? GeneralConst.NumberString.ONE
                                        : GeneralConst.NumberString.ZERO);
                    } else {
                        jsonArticleBudgetaire.put("periodeEcheance", GeneralConst.EMPTY_STRING);
                    }
                }

            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonArticlesBudgetaires.toString();
    }

    public static List<JSONObject> getJsonPaliersByAB(ArticleBudgetaire articleBudgetaire, String tarif, String type) {

        List<JSONObject> jsonPaliers = new ArrayList<>();

        try {

            List<Palier> paliers = DataAccess.getListPaliersByAB(articleBudgetaire.getCode(), type, tarif);

            if (paliers != null) {

                for (Palier palier : paliers) {

                    JSONObject jsonPalier = new JSONObject();

                    jsonPalier.put("codeTarif", palier.getTarif() == null
                            ? GeneralConst.EMPTY_STRING : palier.getTarif().getCode());

                    jsonPalier.put("palierTypeTaux", palier.getTypeTaux() == null
                            ? GeneralConst.EMPTY_STRING : palier.getTypeTaux());

                    jsonPalier.put("unite", palier.getUnite() == null
                            ? GeneralConst.EMPTY_STRING : palier.getUnite());

                    jsonPalier.put("tauxAB", palier.getTaux() == null
                            ? GeneralConst.NumberString.ZERO : palier.getTaux());

                    jsonPalier.put("codeTypePersonne",
                            palier.getTypePersonne() == null ? GeneralConst.ALL : palier.getTypePersonne().getCode());

                    jsonPalier.put("devise", palier.getDevise() == null
                            ? GeneralConst.NumberString.ZERO : palier.getDevise());

                    jsonPalier.put("borneInferieur", palier.getBorneInferieure());

                    jsonPalier.put("borneSuperieur", palier.getBorneSuperieure());

                    jsonPalier.put("multiplierValeurBase",
                            palier.getMultiplierValeurBase().equals(Short.valueOf(GeneralConst.NumberString.ONE))
                                    ? GeneralConst.NumberString.ONE : palier.getTypeTaux().equals("%")
                                            ? GeneralConst.NumberString.ONE : GeneralConst.NumberString.ZERO);

                    jsonPaliers.add(jsonPalier);
                }

            } else {
                JSONObject jsonPalier = new JSONObject();

                jsonPalier.put("codeTarif", articleBudgetaire.getTarif() == null
                        ? GeneralConst.EMPTY_STRING : articleBudgetaire.getTarif().getCode());

                jsonPalier.put("tauxAB", articleBudgetaire.getTarif() == null
                        ? GeneralConst.EMPTY_STRING
                        : articleBudgetaire.getTarif().getValeur() + articleBudgetaire.getTarif().getTypeValeur());

                jsonPalier.put("multiplierValeurBase", GeneralConst.NumberString.ONE);

                jsonPaliers.add(jsonPalier);
            }

        } catch (Exception e) {
        }

        return jsonPaliers;
    }

    public static String loadPeriodiciteAndTarifs(String codeAB, String formeJuridique, String tarifBien, String codeBien) {

        JSONObject jsonDataAssujettissement = new JSONObject();
        List<JSONObject> jsonPeriodicites = new ArrayList<>();
        List<JSONObject> jsonTarifs = new ArrayList<>();
        List<JSONObject> jsonList = new ArrayList<>();

        try {

            List<Periodicite> periodicites = DataAccess.getListPeriodicites();

            for (Periodicite periodicite : periodicites) {

                JSONObject jsonPeriodicite = new JSONObject();

                jsonPeriodicite.put("codePeriodicite",
                        periodicite == null
                                ? GeneralConst.EMPTY_STRING
                                : periodicite.getCode());

                jsonPeriodicite.put("intitulePeriodicite",
                        periodicite == null
                                ? GeneralConst.EMPTY_STRING
                                : periodicite.getIntitule());

                jsonPeriodicite.put("nombreJour",
                        periodicite == null
                                ? GeneralConst.NumberString.ZERO
                                : periodicite.getNbrJour());

                jsonPeriodicites.add(jsonPeriodicite);
            }

            Bien bien = DataAccess.getBienByCode(codeBien);

            String quartier = bien == null ? "" : bien.getFkQuartier();

            if (formeJuridique.isEmpty()) {
                formeJuridique = bien.getAcquisitionList().get(0).getPersonne().getFormeJuridique().getCode();
            }

            List<Palier> paliers = DataAccess.getListPaliersByABV2(codeAB, formeJuridique, tarifBien, quartier, bien.getTypeBien().getCode());

            List palierCodes = new ArrayList();

            for (Palier palier : paliers) {

                Tarif tarif = palier.getTarif();

                if (!palierCodes.contains(tarif.getCode())) {

                    JSONObject jsonTarif = new JSONObject();

                    jsonTarif.put("codeTarif",
                            tarif == null
                                    ? GeneralConst.EMPTY_STRING
                                    : tarif.getCode());

                    jsonTarif.put("intituleTarif",
                            tarif == null
                                    ? GeneralConst.EMPTY_STRING
                                    : tarif.getIntitule().toUpperCase());

                    jsonTarifs.add(jsonTarif);
                    palierCodes.add(tarif.getCode());
                }
            }

            jsonDataAssujettissement.put("listPeriodicites", jsonPeriodicites);
            jsonDataAssujettissement.put("listTarifs", jsonTarifs);
            List<JSONObject> jsonPaliers = getJsonPaliersByAB(paliers);
            jsonDataAssujettissement.put("paliers", jsonPaliers);
            jsonList.add(jsonDataAssujettissement);

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonList.toString();
    }

    public static List<JSONObject> getJsonPaliersByAB(List<Palier> paliers) {

        List<JSONObject> jsonPaliers = new ArrayList<>();

        try {

            for (Palier palier : paliers) {

                JSONObject jsonPalier = new JSONObject();

                jsonPalier.put("codeTarif", palier.getTarif() == null
                        ? GeneralConst.EMPTY_STRING : palier.getTarif().getCode());

                jsonPalier.put("intituleTarif", palier.getTarif() == null
                        ? GeneralConst.EMPTY_STRING : palier.getTarif().getIntitule());

                jsonPalier.put("palierTypeTaux", palier.getTypeTaux() == null
                        ? GeneralConst.EMPTY_STRING : palier.getTypeTaux());

                jsonPalier.put("unite", palier.getUnite() == null
                        ? GeneralConst.EMPTY_STRING : palier.getUnite());

                jsonPalier.put("tauxAB", palier.getTaux() == null
                        ? GeneralConst.NumberString.ZERO : palier.getTaux());

                jsonPalier.put("codeTypePersonne",
                        palier.getTypePersonne() == null ? GeneralConst.ALL : palier.getTypePersonne().getCode());

                jsonPalier.put("devise", palier.getDevise() == null
                        ? GeneralConst.NumberString.ZERO : palier.getDevise());

                jsonPalier.put("borneInferieur", palier.getBorneInferieure());

                jsonPalier.put("borneSuperieur", palier.getBorneSuperieure());

                jsonPalier.put("multiplierValeurBase",
                        palier.getMultiplierValeurBase().equals(Short.valueOf(GeneralConst.NumberString.ONE))
                                ? GeneralConst.NumberString.ONE : palier.getTypeTaux().equals("%")
                                        ? GeneralConst.NumberString.ONE : GeneralConst.NumberString.ZERO);

                jsonPaliers.add(jsonPalier);

            }

        } catch (Exception e) {
        }

        return jsonPaliers;
    }

    public static String getPeriodesDeclarations(String objet) {

        try {

            JSONObject jObjet = new JSONObject(objet);

            String codePeriodicite = jObjet.getString("codePeriodicite");
            String mois = jObjet.getString("mois");
            String annee = jObjet.getString("annee");
            String nombreJour = jObjet.getString("nombreJour");
            String nombreJourLimite = jObjet.getString("nombreJourLimite");
            String echeanceLegale = jObjet.getString("echeanceLegale");
            String periodeEcheance = jObjet.getString("periodeEcheance");
            String nombreJourLimitePaiement = jObjet.getString("nombreJourLimitePaiement");

            int nombreDeJours = Integer.parseInt(nombreJour.equals(GeneralConst.EMPTY_STRING)
                    ? GeneralConst.NumberString.ZERO
                    : nombreJour);

            List<JSONObject> jsonPeriodes = generatePeriodesDeclarations(
                    codePeriodicite,
                    GeneralConst.NumberString.ZERO,
                    mois,
                    annee,
                    nombreDeJours,
                    nombreJourLimite,
                    echeanceLegale,
                    nombreJourLimitePaiement,
                    false,
                    periodeEcheance
            );
            if (jsonPeriodes != null) {
                return jsonPeriodes.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public static List<JSONObject> generatePeriodesDeclarations(
            String codePeriodicite, String jour, String mois, String annee, int nombreJour,
            String nombreJourLimite, String echeanceLegale, String nombreJourLimitePaiement, boolean forTaxation, String periodeEcheance) {

        final String PONC = "PR0012015",
                JOUR = "PR0022015",
                MENS = "PR0032015",
                TRIME = "PR0052015",
                SEMS = "PR0072015",
                ANNEE = "PR0042015";

        List<JSONObject> jsonPeriodes = new ArrayList();

        Date dateDepart = ConvertDate.formatDate("01/" + mois + GeneralConst.SEPARATOR_SLASH_NO_SPACE + annee);
        int anneeDepart = Integer.parseInt(annee);
        int moisDepart = Integer.parseInt(mois);

        String DATE_DAY_ECHANCE = GeneralConst.EMPTY_STRING, DATE_MONTH_ECHEANCE = GeneralConst.EMPTY_STRING;
        String DATE_DAY_ECHANCE_PAIEMENT = GeneralConst.EMPTY_STRING, DATE_MONTH_ECHEANCE_PAIEMENT = GeneralConst.EMPTY_STRING;

        if (!echeanceLegale.equals(GeneralConst.NumberString.ZERO) && !echeanceLegale.equals(GeneralConst.EMPTY_STRING)) {
            String[] dateArray = echeanceLegale.split(GeneralConst.DASH_SEPARATOR);
            DATE_DAY_ECHANCE = dateArray[2];
            DATE_MONTH_ECHEANCE = dateArray[1];
        }

        if (!nombreJourLimitePaiement.equals(GeneralConst.NumberString.ZERO) && !nombreJourLimitePaiement.equals(GeneralConst.EMPTY_STRING)) {
            String[] dateArray = nombreJourLimitePaiement.split(GeneralConst.DASH_SEPARATOR);
            DATE_DAY_ECHANCE_PAIEMENT = dateArray[2];
            DATE_MONTH_ECHEANCE_PAIEMENT = dateArray[1];
        }

        switch (codePeriodicite) {
            case PONC:
            case JOUR:

                int length = 1;
                Date dateJour = new Date();
                if (forTaxation) {
                    length = 10;
                    dateJour = ConvertDate.formatDate(jour + "/" + mois + "/" + annee);
                    dateJour = ConvertDate.addDayOfDate(dateJour, 1);
                }

                jsonPeriodes = PeriodiciteData.getPeriodeJournaliere(dateJour, length, codePeriodicite, nombreJourLimitePaiement);
                break;

            case MENS:
            case "BIMEN":
            case TRIME:
            case SEMS:

                if (forTaxation) {
                    moisDepart += 1;
                    if (moisDepart > 12) {
                        moisDepart = 1;
                        anneeDepart += 1;
                    }
                }

                jsonPeriodes = PeriodiciteData.getPeriodesMensuelles(
                        nombreJour,
                        forTaxation,
                        dateDepart,
                        moisDepart,
                        anneeDepart,
                        DATE_DAY_ECHANCE,
                        DATE_DAY_ECHANCE_PAIEMENT,
                        codePeriodicite,
                        periodeEcheance);
                break;

            case ANNEE:
            case "2ANS":
            case "5ANS":

                anneeDepart = (forTaxation) ? (anneeDepart + 1) : anneeDepart;

                jsonPeriodes = PeriodiciteData.getPeriodesAnnuelles(
                        nombreJour,
                        forTaxation,
                        dateDepart,
                        anneeDepart,
                        DATE_DAY_ECHANCE,
                        DATE_MONTH_ECHEANCE,
                        DATE_DAY_ECHANCE_PAIEMENT,
                        DATE_MONTH_ECHEANCE_PAIEMENT,
                        codePeriodicite,
                        periodeEcheance);
        }

        return jsonPeriodes;
    }

    public static String createAssujettissement(String objet) {

        String result;

        try {

            JSONObject jObjet = new JSONObject(objet);

            String idBien = jObjet.getString("idBien");
            String codeArticleBudgetaire = jObjet.getString("codeAB");
            String dateDebut = jObjet.getString("dateDebut");
            String dateFin = jObjet.getString("dateFin");
            String valeurBase = jObjet.getString("valeurBase");
            String codePersonne = jObjet.getString("codePersonne");
            String nombreJourLimite = jObjet.getString("nombreJourLimite");
            String codePeriodicite = jObjet.getString("codePeriodicite");
            String codeTarif = jObjet.getString("codeTarif");
            String codeGestionnaire = jObjet.getString("codeGestionnaire");
            String codeAP = jObjet.getString("codeAP");
            String idComplementBien = jObjet.getString("idComplementBien");
//            String nvlEcheance = jObjet.getString("nvlEcheance");
            String uniteValeur = jObjet.getString("uniteValeur");
            String periodesDeclarations = jObjet.getString("periodesDeclarations");

            JSONArray jsonPeriodesDeclarations = new JSONArray(periodesDeclarations);

            JSONArray selectedABArray = new JSONArray();

            Assujeti assujettissement = DataAccess.getExistAssujettissement(
                    idBien,
                    codeArticleBudgetaire,
                    idComplementBien,
                    codePersonne, codeTarif);

            Date dateFinAssuj, dateDebutNewAssuj;

            if (assujettissement != null) {

                dateDebutNewAssuj = ConvertDate.formatDate(dateDebut);
                dateFinAssuj = ConvertDate.formatDate(ConvertDate.getValidFormatDatePrint(assujettissement.getDateFin()));

                if (dateDebutNewAssuj.before(dateFinAssuj)) {
                    return "2";
                }
            }

            Assujeti assujeti = new Assujeti();
            assujeti.setBien(new Bien(idBien));
            assujeti.setArticleBudgetaire(new ArticleBudgetaire(codeArticleBudgetaire));
            assujeti.setRenouvellement(Short.valueOf(GeneralConst.NumberString.ZERO));
            assujeti.setDateDebut(dateDebut);
            assujeti.setDateFin(dateFin);
            long base = Long.valueOf(valeurBase.equals(GeneralConst.EMPTY_STRING) ? GeneralConst.NumberString.ZERO : valeurBase);
            assujeti.setValeur(BigDecimal.valueOf(base));
            assujeti.setPersonne(new Personne(codePersonne));
            assujeti.setNbrJourLimite(Integer.valueOf(nombreJourLimite));
            assujeti.setPeriodicite(codePeriodicite);
            assujeti.setTarif(new Tarif(codeTarif));
            assujeti.setGestionnaire(codeGestionnaire);
            assujeti.setFkAdressePersonne(new AdressePersonne(codeAP));
            assujeti.setComplementBien(idComplementBien);
            assujeti.setUniteValeur(uniteValeur);

            List<PeriodeDeclaration> periodeDeclarations = getPeriodesDeclarations(jsonPeriodesDeclarations);

            LogUser logUser = new LogUser();

            if (DataAccess.saveAssujettissement(assujeti, periodeDeclarations, selectedABArray, logUser)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public static List<PeriodeDeclaration> getPeriodesDeclarations(JSONArray jsonPeriodesDeclarations) {

        List<PeriodeDeclaration> periodeDeclarations = new ArrayList<>();
        try {
            for (int i = 0; i < jsonPeriodesDeclarations.length(); i++) {

                JSONObject jsonobject = jsonPeriodesDeclarations.getJSONObject(i);
                String dateDebut = jsonobject.getString("dateDebut");
                String dateFin = jsonobject.getString("dateFin");
                String dateLimite = jsonobject.getString("dateLimite");
                String dateLimitePaiement = jsonobject.getString("dateLimitePaiement");

                PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();
                periodeDeclaration.setDebut(ConvertDate.formatDate(dateDebut));
                periodeDeclaration.setFin(ConvertDate.formatDate(dateFin));
                periodeDeclaration.setDateLimite(ConvertDate.formatDate(dateLimite));
                periodeDeclaration.setDateLimitePaiement(ConvertDate.formatDate(dateLimitePaiement));
                periodeDeclarations.add(periodeDeclaration);

            }
        } catch (JSONException e) {
        }
        return periodeDeclarations;
    }

    public static String getListSitesPeage() {

        List<JSONObject> objects = new ArrayList<>();

        try {
            List<Site> sites = DataAccess.getSitesPeage();
            if (sites.size() > 0) {
                for (Site site : sites) {
                    JSONObject object = new JSONObject();
                    object.put("idSite", site.getCode());
                    object.put("intituleSite", site.getIntitule());
                    objects.add(object);
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public static String getListTarifsGoPass() {

        List<JSONObject> objects = new ArrayList<>();

        try {

            List<Tarif> tarifs = DataAccess.getListTarifsPeage();
            if (tarifs.size() > 0) {
                for (Tarif tarif : tarifs) {
                    JSONObject object = new JSONObject();
                    object.put("idTarif", tarif.getCode());
                    object.put("intituleTarif", tarif.getIntitule());

                    List<JSONObject> objTarif = new ArrayList<>();
                    TarifSite ts = DataAccess.getTarifSite(tarif.getCode());
                    if (ts != null) {
                        JSONObject obj = new JSONObject();
                        obj.put("ID", ts.getId());
                        obj.put("taux", ts.getTaux());
                        obj.put("devise", ts.getFkDevise());
                        obj.put("fkTarif", tarif.getCode());
                        objTarif.add(obj);
                    }
                    object.put("listTarif", objTarif);

                    objects.add(object);
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public static String saveCommandeGoPass(String objet) throws IOException {

        try {

            propertiesConfig = new PropertiesConfig();
            int qtePlage = Integer.valueOf(propertiesConfig.getContent("QTE_PLAGE"));

            JSONObject jObject = new JSONObject(objet);
            JSONArray commande = new JSONArray(jObject.getString("listCommande"));

            String reference = Tools.generateRandomValue(10);
            reference = reference.toUpperCase();
            List<VoucherMock> vms = setCommandeGoPass(commande, reference);
            boolean saveCommande;

            saveCommande = DataAccess.saveCommandeGoPass(vms, qtePlage);
            if (saveCommande) {
                JSONObject obj = new JSONObject();
                obj.put("reference", reference);
                return obj.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String saveCommandeGoPassSpontanee(String objet) throws IOException {

        JSONObject objs = new JSONObject();
        JsonObject obj = new JsonObject();

        try {

            propertiesConfig = new PropertiesConfig();
            String abGoPass = propertiesConfig.getContent("AB_GOPASS");
            String compteBancaire = propertiesConfig.getContent("COMPTE_BANCAIRE_GOPASS");

            JSONObject jObject = new JSONObject(objet);

            CommandeGoPassSpontanneeMock cgpsm = new CommandeGoPassSpontanneeMock();
            cgpsm.setCodeTicket("codeTicket");
            cgpsm.setAgentCreat(jObject.getString("agentCreat"));
            cgpsm.setAgentUtilisation(jObject.getString("agentUtilisation"));
            cgpsm.setDevise(jObject.getString("devise"));
            cgpsm.setCategorieCode(jObject.getString("categorieCode"));
            cgpsm.setMontant(jObject.getDouble("montant"));
            cgpsm.setQuantite(1);
            cgpsm.setTypeGopassCode(jObject.getString("typeGopass"));
            cgpsm.setAeroportDestination(jObject.getString("aeroport_destination"));
            cgpsm.setAeroportProvenance(jObject.getString("aeroportProvenance"));
            cgpsm.setSexePassager(jObject.getString("sexe"));
            cgpsm.setCodeCompagnie(jObject.getInt("codeCompagnie"));
            cgpsm.setPassager(jObject.getString("passager"));
            cgpsm.setPieceIdentite(jObject.getString("pieceIdentite"));
            cgpsm.setReference(Tools.generateRandomValue(10).toUpperCase());
            cgpsm.setCompteBancaire(compteBancaire);
            cgpsm.setArticleBudgetaire(abGoPass);
            cgpsm.setToken(Tools.encoder(Tools.generateRandomValue(10).concat("@").concat(cgpsm.getReference()).
                    concat("@").concat(Tools.generateRandomValue(50))));

            boolean saveCommande;
            if (cgpsm.getCodeTicket().equals("") || cgpsm.getCodeTicket() == null) {
                saveCommande = DataAccess.saveCommandeGoPassSpontanee(cgpsm);
                if (saveCommande) {

                    if (generateGoPassSpontannee(cgpsm)) {
                        obj.addProperty("status", "100");
                        obj.addProperty("message", "Commande gopass et synchronisation effectuées avec succès!");
                    } else {
                        obj.addProperty("status", "200");
                        obj.addProperty("message", "Commande gopass effectuée avec succès mais échec synchronisation!");
                    }

                } else {
                    obj.addProperty("status", "300");
                    obj.addProperty("message", "Commande gopass echouée!");
                }
            } else {
                saveCommande = DataAccess.synchronisationGoPassSpontanee(cgpsm);
                if (saveCommande) {
                    obj.addProperty("status", "100");
                    obj.addProperty("message", "Synchronisation effectuée avec succès!");
                } else {
                    obj.addProperty("status", "200");
                    obj.addProperty("message", "Synchronisation echouée!");
                }
            }

        } catch (Exception e) {
            obj.addProperty("status", "200");
            obj.addProperty("message", "Une erreur est survenue.");
        }
        return obj.toString();
    }

    public static List<VoucherMock> setCommandeGoPass(JSONArray jsonCommande, String reference) throws IOException {

        propertiesConfig = new PropertiesConfig();
        List<VoucherMock> vms = new ArrayList<>();
        String compte_bancaire = null;

        try {

            int isPeageLualaba = Integer.valueOf(propertiesConfig.getContent("IS_PEAGE_LUALABA"));
            int agentCreate = Integer.valueOf(propertiesConfig.getContent("CODE_AGENT_SERVICE_WEB"));
            int etat = 3;

            switch (isPeageLualaba) {
                case 1:
                    etat = 2;
                    compte_bancaire = propertiesConfig.getContent("COMPTE_BANCAIRE_LUALABA");
                    break;
                case 2:
                    compte_bancaire = propertiesConfig.getContent("COMPTE_BANCAIRE_ACGT");
                    break;
            }

            for (int i = 0; i < jsonCommande.length(); i++) {

                JSONObject jsonobject = jsonCommande.getJSONObject(i);

                VoucherMock vm = new VoucherMock();
                vm.setFk_personne(jsonobject.getString("nif"));
                vm.setType(jsonobject.getString("typeName"));
                vm.setFkTarif(jsonobject.getString("tarifCode"));
                vm.setFkAb(jsonobject.getString("abCode"));
                vm.setQuantite(Integer.valueOf(jsonobject.getString("quantite")));
                vm.setMontant(Double.valueOf(jsonobject.getString("total")));
                vm.setPrix_unitaire(Double.valueOf(jsonobject.getString("montant")));
                vm.setDevise(jsonobject.getString("devise"));
                vm.setEtat(etat);
                vm.setReference(reference);
                vm.setCompteBancaire(compte_bancaire);
                vm.setAgentCreate(agentCreate);
                vms.add(vm);

            }
        } catch (JSONException e) {
            return null;
        }
        return vms;
    }

    public static String getCommandesGoPass(String nif) {

        List<JSONObject> objects = new ArrayList<>();

        try {

            List<Commande> commandes = DataAccess.getCommandesGoPass(nif);
            if (commandes.size() > 0) {
                for (Commande commande : commandes) {
                    JSONObject object = new JSONObject();
                    object.put("id", commande.getId());
                    object.put("total", commande.getMontant());
                    object.put("devise", commande.getDevise().getCode());
                    object.put("etat", commande.getEtat());
                    object.put("reference", commande.getReference());
                    object.put("date", Tools.formatDateToString(commande.getDateCreat()));
                    object.put("compteBancaire", commande.getFkCompteBancaire() == null
                            ? GeneralConst.EMPTY_STRING
                            : commande.getFkCompteBancaire().getCode());
                    object.put("notePerception", commande.getNotePaiement() == null
                            ? GeneralConst.NumberString.ZERO
                            : GeneralConst.NumberString.ONE);
                    object.put("preuvePaiement", commande.getPreuvePaiement() == null
                            ? GeneralConst.EMPTY_STRING
                            : commande.getPreuvePaiement());
                    object.put("token", commande.getToken() == null
                            ? GeneralConst.EMPTY_STRING
                            : commande.getToken());
                    List<DetailCommande> dcs = DataAccess.getDetailGoPass(commande.getId());
                    if (!dcs.isEmpty()) {

                        String categorie = "", type = "";
                        int nombreVoucher = 0;
                        if (dcs.size() == 1) {
                            categorie = dcs.get(0).getTarif();
                            type = dcs.get(0).getType();
                            nombreVoucher = dcs.get(0).getQuantite();
                        } else {
                            for (DetailCommande dc : dcs) {
//                                categorie += categorie.equals("") ? dc.getTarif().concat(" (")
//                                        .concat(dc.getType()).concat(")").concat(" : ")
//                                        .concat(String.valueOf(dc.getQuantite())) : "<hr>"
//                                        .concat(dc.getTarif()).concat(" (").concat(dc.getType()).concat(")")
//                                        .concat(" : ").concat(String.valueOf(dc.getQuantite()));
                                categorie += categorie.equals("") ? dc.getTarif().concat(" : ")
                                        .concat(String.valueOf(dc.getQuantite())) : "<hr>"
                                        .concat(dc.getTarif()).concat(" : ").concat(String.valueOf(dc.getQuantite()));
                                type += type.equals("") ? dc.getType() : "<hr>".concat(dc.getType());
                                nombreVoucher += dc.getQuantite();
                            }
                        }
                        object.put("nombre", nombreVoucher);
                        object.put("categorie", categorie);
                        object.put("type", type);
                        objects.add(object);
//
                    }
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }
    
    public static String getCommandesGoPassAll(String DateDebut,String DateFin) {

        List<JSONObject> objects = new ArrayList<>();

        try {

            List<Commande> commandes = DataAccess.getCommandesGoPassAll(DateDebut,DateFin);
            if (commandes.size() > 0) {
                for (Commande commande : commandes) {
                    JSONObject object = new JSONObject();
                    object.put("id", commande.getId());
                    object.put("total", commande.getMontant());
                    object.put("devise", commande.getDevise().getCode());
                    object.put("etat", commande.getEtat());
                    object.put("reference", commande.getReference());
                    object.put("date", Tools.formatDateToString(commande.getDateCreat()));
                    object.put("compteBancaire", commande.getFkCompteBancaire() == null
                            ? GeneralConst.EMPTY_STRING
                            : commande.getFkCompteBancaire().getCode());
                    object.put("notePerception", commande.getNotePaiement() == null
                            ? GeneralConst.NumberString.ZERO
                            : GeneralConst.NumberString.ONE);
                    object.put("preuvePaiement", commande.getPreuvePaiement() == null
                            ? GeneralConst.EMPTY_STRING
                            : commande.getPreuvePaiement());
                    object.put("token", commande.getToken() == null
                            ? GeneralConst.EMPTY_STRING
                            : commande.getToken());
                    object.put("Personne", commande.getFkPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : commande.getFkPersonne().getPrenoms()+" "+commande.getFkPersonne().getNom() + " "+commande.getFkPersonne().getPostnom());
                    List<DetailCommande> dcs = DataAccess.getDetailGoPass(commande.getId());
                    if (!dcs.isEmpty()) {

                        String categorie = "", type = "";
                        int nombreVoucher = 0;
                        if (dcs.size() == 1) {
                            categorie = dcs.get(0).getTarif();
                            type = dcs.get(0).getType();
                            nombreVoucher = dcs.get(0).getQuantite();
                        } else {
                            for (DetailCommande dc : dcs) {
//                                categorie += categorie.equals("") ? dc.getTarif().concat(" (")
//                                        .concat(dc.getType()).concat(")").concat(" : ")
//                                        .concat(String.valueOf(dc.getQuantite())) : "<hr>"
//                                        .concat(dc.getTarif()).concat(" (").concat(dc.getType()).concat(")")
//                                        .concat(" : ").concat(String.valueOf(dc.getQuantite()));
                                categorie += categorie.equals("") ? " - "+dc.getTarif().concat(" : ")
                                        .concat(String.valueOf(dc.getQuantite())) : " <br> - "
                                        .concat(dc.getTarif()).concat(" : ").concat(String.valueOf(dc.getQuantite()));
                                type += type.equals("") ? dc.getType() : "<hr>".concat(dc.getType());
                                nombreVoucher += dc.getQuantite();
                            }
                        }
                        object.put("nombre", nombreVoucher);
                        object.put("categorie", categorie);
                        object.put("type", type);
                        objects.add(object);
//
                    }
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String getCommandesGoPassByReference(String reference) {

        List<JSONObject> objects = new ArrayList<>();

        try {

            Commande commande = DataAccess.getListComandesGoPassByReference(reference);
            if (commande != null) {
                JSONObject object = new JSONObject();
                object.put("id", commande.getId());
                object.put("total", commande.getMontant());
                object.put("devise", commande.getDevise().getCode());
                object.put("etat", commande.getEtat());
                object.put("reference", commande.getReference());
                object.put("date", Tools.formatDateToString(commande.getDateCreat()));
                object.put("compteBancaire", commande.getFkCompteBancaire() == null
                        ? GeneralConst.EMPTY_STRING
                        : commande.getFkCompteBancaire().getCode());
                object.put("notePerception", commande.getNotePaiement() == null
                        ? GeneralConst.NumberString.ZERO
                        : GeneralConst.NumberString.ONE);
                object.put("preuvePaiement", commande.getPreuvePaiement() == null
                        ? GeneralConst.EMPTY_STRING
                        : commande.getPreuvePaiement());
                object.put("token", commande.getToken() == null
                        ? GeneralConst.EMPTY_STRING
                        : commande.getToken());
                List<DetailCommande> dc = DataAccess.getDetailGoPass(commande.getId());
                object.put("nombre", dc.get(0).getQuantite());

                object.put("categorie", dc.get(0).getTarif());
                objects.add(object);
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String getDetailCommandeVoucher(int idCmd) {

        List<JSONObject> listeGopass = new ArrayList<>();

        try {

            List<DetailCommande> dvs = DataAccess.getDetailGoPass(idCmd);
            if (dvs.size() > 0) {
                for (DetailCommande dv : dvs) {

                    JSONObject obj = new JSONObject();

                    obj.put("id", dv.getId());
                    obj.put("libTarif", dv.getTarif());
                    obj.put("prix_unit", dv.getPrixUnitaire());
                    obj.put("quantite", dv.getQuantite());
                    obj.put("montant", dv.getMontant());
                    obj.put("devise", dv.getDevise());

                    listeGopass.add(obj);
                }
                return listeGopass.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public static String getDetailsGoPassByCommandeId(int idCmd) {

        List<JSONObject> listeGopass = new ArrayList<>();

        try {

            List<DetailsGoPass> dgps = DataAccess.getDetailsGoPassByCommandeId(idCmd);
            if (dgps.size() > 0) {
                for (DetailsGoPass dgp : dgps) {

                    JSONObject obj = new JSONObject();

                    obj.put("id", dgp.getIdDetails());
                    obj.put("codeGopass", dgp.getCodeGoPass());
                    obj.put("dateGeneration", Tools.formatDateToString(dgp.getDateGeneration()));
                    obj.put("dateUtilisation", dgp.getDateUtilisation() == null
                            ? GeneralConst.DASH_SEPARATOR
                            : Tools.formatDateToStringV1(dgp.getDateUtilisation()));
                    obj.put("dateActivation", dgp.getDateActivation() == null
                            ? GeneralConst.DASH_SEPARATOR
                            : Tools.formatDateToStringV1(dgp.getDateActivation()));
                    obj.put("dateVol", dgp.getDateVol() == null
                            ? GeneralConst.DASH_SEPARATOR
                            : dgp.getDateVol());
                    obj.put("etat", dgp.getEtat());
                    obj.put("aeroportProvenance", dgp.getFkAeroportProvenance() == null
                            ? GeneralConst.DASH_SEPARATOR
                            : dgp.getFkAeroportProvenance().getIntitule());
                    obj.put("aeroportDestination", dgp.getFkAeroportDestination() == null
                            ? GeneralConst.DASH_SEPARATOR
                            : dgp.getFkAeroportDestination().getIntitule());
                    obj.put("codeTicket", dgp.getCodeTicketGenerate() == null
                            ? GeneralConst.DASH_SEPARATOR
                            : dgp.getCodeTicketGenerate());
                    obj.put("numeroVol", dgp.getNumeroVol() == null
                            ? GeneralConst.DASH_SEPARATOR
                            : dgp.getNumeroVol());

                    listeGopass.add(obj);
                }
                return listeGopass.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public static String setEtatPayCommande(String token) {
        String result = "";

        try {

            token = Tools.decoder(token);
            String ref_split[] = token.split("@");
            String reference = ref_split[1];

            String ref = reference.substring(10, reference.length());

            if (!ref.equals("") && ref.equals("CRT")) {
                result = payCommandeCarte(reference, ref);
            } else {
                result = payCommandeGoPass(reference, ref);
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;

    }

    private static String payCommandeCarte(String reference, String ref) {
        String result;
        CommandeCarte cmd = DataAccess.getCmdCarteAvailableToPay(reference);;
        if (cmd != null) {
            switch (cmd.getEtat()) {
                case 1:
                    result = GeneralConst.ResultCode.CMD_PAYED.concat(",").concat(reference);
                    break;
                case 2:
                    if (DataAccess.setEtatPayCommande(reference, ref)) {
                        if (generateGoPassCode(reference)) {
                            result = GeneralConst.ResultCode.SUCCES_OPERATION.concat(",").concat(reference);
                        } else {
                            result = GeneralConst.ResultCode.SUCCES_OPERATION.concat(",").concat(reference);
                        }
                    } else {
                        result = GeneralConst.ResultCode.FAILED_OPERATION.concat(",").concat(reference);
                    }
                    break;
                default:
                    result = GeneralConst.ResultCode.FAILED_OPERATION.concat(",").concat(reference);
                    break;
            }

        } else {
            result = GeneralConst.ResultCode.FAILED_OPERATION.concat(",").concat(reference);
        }
        return result;
    }

    private static String payCommandeGoPass(String reference, String ref) {
        String result;
        Commande cmd = DataAccess.getCmdAvailableToPay(reference);;
        if (cmd != null) {
            switch (cmd.getEtat()) {
                case 1:
                    result = GeneralConst.ResultCode.CMD_PAYED.concat(",").concat(reference);
                    break;
                case 2:
                    if (DataAccess.setEtatPayCommande(reference, ref)) {
                        if (generateGoPassCode(reference)) {
                            result = GeneralConst.ResultCode.SUCCES_OPERATION.concat(",").concat(reference);
                        } else {
                            result = GeneralConst.ResultCode.SUCCES_OPERATION.concat(",").concat(reference);
                        }
                    } else {
                        result = GeneralConst.ResultCode.FAILED_OPERATION.concat(",").concat(reference);
                    }
                    break;
                default:
                    result = GeneralConst.ResultCode.FAILED_OPERATION.concat(",").concat(reference);
                    break;
            }

        } else {
            result = GeneralConst.ResultCode.FAILED_OPERATION.concat(",").concat(reference);
        }
        return result;
    }

    public static String getListDetailVoucher(String nif) {

        List<JSONObject> objects = new ArrayList<>();
        try {
            List<DetailsGoPass> dgps = DataAccess.getListDetailVoucher(nif);
            if (dgps.size() > 0) {
                for (DetailsGoPass dgp : dgps) {
                    JSONObject object = new JSONObject();
                    object.put("id", dgp.getIdDetails());
                    object.put("fkGoPass", dgp.getFkGopasse().getIdGopasse());
                    object.put("codeGoPass", dgp.getCodeGoPass());
                    object.put("dateGeneration", Tools.formatDateToString(dgp.getDateGeneration()));
                    object.put("dateUtilisation", dgp.getDateUtilisation() == null
                            ? GeneralConst.DASH_SEPARATOR
                            : Tools.formatDateToStringV1(dgp.getDateUtilisation()));
                    object.put("etat", dgp.getEtat());
                    object.put("aeroportProvenance", dgp.getFkAeroportProvenance() == null
                            ? GeneralConst.EMPTY_STRING
                            : dgp.getFkAeroportProvenance().getIntitule());
                    object.put("aeroportDestination", dgp.getFkAeroportDestination() == null
                            ? GeneralConst.EMPTY_STRING
                            : dgp.getFkAeroportDestination().getIntitule());
                    objects.add(object);
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String printNotePaiement(String reference, String nif) {

        String dataReturn;
        String document = GeneralConst.EMPTY_STRING;

        try {

            Commande commande = DataAccess.getCommandesVoucher(nif, reference);
            if (commande != null) {
                document = commande.getNotePaiement();
            }

            if (document != null) {
                dataReturn = commande.getNotePaiement();

            } else {

                PrintDocument printDocument = new PrintDocument();

                if (commande != null) {
                    dataReturn = printDocument.createNotePaiementVoucher(commande);
                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public static String setPreuvePaiementCommande(String object) {

        try {
            JSONObject objet = new JSONObject(object);
            int id = objet.getInt("id");
            String base64 = objet.getString("base64");
            if (DataAccess.savePreuvePaiementCommande(id, base64)) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public static String saveCommandeCarte(String objet) throws IOException {

        try {
            JSONObject jObject = new JSONObject(objet);
            BigDecimal prix = new BigDecimal(jObject.getDouble("prix"));
            BigDecimal total = new BigDecimal(jObject.getDouble("total"));
            int quantite = jObject.getInt("quantite");
            String nif = jObject.getString("nif");
            String devise = jObject.getString("devise");
            String reference = Tools.generateRandomValue(10);
            reference = reference.toUpperCase().concat("CRT");

            CommandeCarte commandeCarte = new CommandeCarte();
            commandeCarte.setFkPersonne(new Personne(nif));
            commandeCarte.setQte(quantite);
            commandeCarte.setPrixUnitaire(prix);
            commandeCarte.setPrixTotal(total);
            commandeCarte.setEtat(2);
            commandeCarte.setReference(reference);
            commandeCarte.setDevise(devise);

            if (DataAccess.saveCommandeCartes(commandeCarte)) {
                JSONObject obj = new JSONObject();
                obj.put("reference", reference);
                return obj.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String getListComandesCartes(String nif) {

        List<JSONObject> objects = new ArrayList<>();

        try {

            List<CommandeCarte> cartes = DataAccess.getListComandesCartes(nif);
            if (cartes.size() > 0) {
                for (CommandeCarte carte : cartes) {
                    JSONObject objet = new JSONObject();
                    objet.put("id", carte.getId());
                    objet.put("prix_unit", carte.getPrixUnitaire());
                    objet.put("quantite", carte.getQte());
                    objet.put("total", carte.getPrixTotal());
                    objet.put("reference", carte.getReference());
                    objet.put("date_cmd", Tools.formatDateToString(carte.getDateCreate()));
                    objet.put("etat", carte.getEtat());
                    objet.put("devise", carte.getDevise());
                    objet.put("token", carte.getToken());
                    objects.add(objet);
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String attributeCarteGopass(String objet) {

        try {

            JSONObject obj = new JSONObject(objet);
            int idCarte = obj.getInt("idCarte");
            String voucherData = obj.getString("voucherData");
            int type = obj.getInt("type");

            String stringVoucher = Tools.getListToString(voucherData);

            List<String> vouchertList = new ArrayList<>(
                    Arrays.asList(stringVoucher.split(","))
            );

            if (DataAccess.attributeCarteGopass(idCarte, vouchertList, type)) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String getListCartesGopass(String nif) {

        List<JSONObject> objects = new ArrayList<>();

        try {

            List<CarteGoPass> cgps = DataAccess.getListCartesGoPass(nif);
            if (cgps.size() > 0) {
                for (CarteGoPass cgp : cgps) {
                    JSONObject object = new JSONObject();
                    object.put("id", cgp.getId());
                    object.put("numero", cgp.getNumeroCarte());
                    object.put("cmdId", cgp.getFkCommandeCarte().getId());
                    object.put("dateGenerate", Tools.formatDateToString(cgp.getDateGenerate()));
                    object.put("dateUpdate", Tools.formatDateToString(cgp.getFkCommandeCarte().getDateUpdate()));
                    object.put("fkPersonne", cgp.getFkPersonne().getCode());
                    object.put("etat", cgp.getEtat());
                    object.put("observation", cgp.getObservation());
                    objects.add(object);
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public static String getGoPassCarte(String nif, int idCarte, int type) {

        List<JSONObject> objects = new ArrayList<>();

        try {
            List<DetailsGoPass> dgps = DataAccess.getVouchersCarte(nif, idCarte, type);
            if (dgps.size() > 0) {
                for (DetailsGoPass dgp : dgps) {
                    JSONObject object = new JSONObject();
                    object.put("id", dgp.getIdDetails());
                    object.put("fkGoPass", dgp.getFkGopasse().getIdGopasse());
                    object.put("codeGoPass", dgp.getCodeGoPass());
                    object.put("dateGeneration", Tools.formatDateToString(dgp.getDateGeneration()));
                    object.put("dateUtilisation", dgp.getDateUtilisation() == null
                            ? GeneralConst.DASH_SEPARATOR
                            : Tools.formatDateToStringV1(dgp.getDateUtilisation()));
                    object.put("etat", dgp.getEtat());
                    object.put("aeroportProvenance", dgp.getFkAeroportProvenance() == null
                            ? GeneralConst.EMPTY_STRING
                            : dgp.getFkAeroportProvenance().getIntitule());
                    object.put("aeroportDestination", dgp.getFkAeroportDestination() == null
                            ? GeneralConst.EMPTY_STRING
                            : dgp.getFkAeroportDestination().getIntitule());
                    objects.add(object);
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String getDataDashBoard(String datedebut, String datefin, String nif) throws Exception {

        String response = GeneralConst.EMPTY_STRING;

        List<JSONObject> jsonDashboardDataList = new ArrayList<>();
        JSONObject dataResult = new JSONObject();

        try {

            List<Object> dashboards = DataAccess.getDataDashBoard(datedebut, datefin, nif);

            if (dashboards.size() > 0) {

                Iterator itr = dashboards.iterator();

                while (itr.hasNext()) {
                    JSONObject jsonDashboardData = new JSONObject();
                    Object[] obj = (Object[]) itr.next();

                    jsonDashboardData.put("totalBiens", String.valueOf(obj[0]));
                    jsonDashboardData.put("totalDeclarations", String.valueOf(obj[1]));

                    jsonDashboardDataList.add(jsonDashboardData);
                }
                response = jsonDashboardDataList.toString();

            } else {
                dataResult.put("code", "200");
                dataResult.put("message", "echec");
                response = dataResult.toString();
            }

        } catch (Exception e) {
            throw e;
        }

        return response;
    }

    public static String getDataDashBoardGopass(String datedebut, String datefin, String nif, int isDateFiltre) throws Exception {

        String response = GeneralConst.EMPTY_STRING;

        List<JSONObject> jsonDashboardDataList = new ArrayList<>();
        JSONObject dataResult = new JSONObject();

        try {

            List<Object> dashboards = DataAccess.getDataDashBoardPeage(datedebut, datefin, nif, isDateFiltre);

            if (dashboards.size() > 0) {

                Iterator itr = dashboards.iterator();

                while (itr.hasNext()) {
                    JSONObject jsonDashboardData = new JSONObject();
                    Object[] obj = (Object[]) itr.next();

                    jsonDashboardData.put("totalGopass", String.valueOf(obj[0]));
                    jsonDashboardData.put("totalGopassBlancs", String.valueOf(obj[1]));
                    jsonDashboardData.put("totalGopassActives", String.valueOf(obj[2]));
                    jsonDashboardData.put("totalGopassUtilises", String.valueOf(obj[3]));
//                    jsonDashboardData.put("nombreVoucherNotAffected", String.valueOf(obj[4]));
//                    jsonDashboardData.put("nombreCmdCartes", String.valueOf(obj[5]));
//                    jsonDashboardData.put("nombreCardAffected", String.valueOf(obj[6]));
//                    jsonDashboardData.put("nombreCardNotAffected", String.valueOf(obj[7]));

                    jsonDashboardDataList.add(jsonDashboardData);
                }
                response = jsonDashboardDataList.toString();

            } else {
                dataResult.put("code", "200");
                dataResult.put("message", "echec");
                response = dataResult.toString();
            }

        } catch (Exception e) {
            throw e;
        }

        return response;
    }

    public static String getListSiteAxePeage(int fkAxe) {

        List<JSONObject> objects = new ArrayList<>();
        try {
            List<AxePeage> aps = DataAccess.getListSiteAxePeage(fkAxe);
            if (aps.size() > 0) {
                for (AxePeage ap : aps) {
                    JSONObject object = new JSONObject();
                    object.put("id", ap.getId());
                    object.put("fkAxe", ap.getFkAxe().getId());
                    object.put("idSite", ap.getFkSite().getCode());
                    object.put("intitule", ap.getFkSite().getIntitule());
                    objects.add(object);
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String updateUserInfos(String objet) {

        try {

            JSONObject obj = new JSONObject(objet);
            String phone = obj.getString("phonenumber");
            String mail = obj.getString("email");
            String idUser = obj.getString("nif");

            LoginWeb lw = new LoginWeb();
            lw.setTelephone(phone);
            lw.setMail(mail);
            lw.setFkPersonne(idUser);

            if (DataAccess.updateUserInfos(lw)) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String verifyNoteTaxation(String referenceDocument) {

        try {

            List<RetraitDeclaration> declarationList = DataAccess.getListRetraitDeclarationByCodeDeclarationV2(
                    referenceDocument);

            if (!declarationList.isEmpty()) {

                JSONObject declarationJson = new JSONObject();

                ArticleBudgetaire articleBudgetaire = DataAccess.getArticleBudgetaireByCode(declarationList.get(0).getFkAb());

//                boolean nonAutoriser = false;
                if (declarationList.get(0).getEtat() == 0) {

                    return "-0"; // Note de taxation désactivée

                } else if (declarationList.get(0).getEtat() == 2) {
                    return "2"; // Note de taxation mère échelonnée
                }

                String tarifLibelle = GeneralConst.EMPTY_STRING;
                String articleLibelle = articleBudgetaire.getIntitule();

                String articleComposite = GeneralConst.EMPTY_STRING;

                if (!"TOUT".equalsIgnoreCase(articleBudgetaire.getTarif().getIntitule())) {
                    tarifLibelle = GeneralConst.TWO_POINTS.concat(articleBudgetaire.getTarif().getIntitule());
                }

                if (!articleBudgetaire.getTarif().getIntitule().equals("All")) {
                    articleComposite = articleLibelle.concat(GeneralConst.TWO_POINTS).concat(tarifLibelle);
                } else {
                    articleComposite = articleLibelle;
                }

                String codeDeclaration = declarationList.get(0).getCodeDeclaration().trim();

                Personne personne = DataAccess.getPersonneByCode_V2(declarationList.get(0).getFkAssujetti().trim());

                declarationJson.put("referenceDocument",
                        declarationList.get(0).getCodeDeclaration().trim());

                if (personne != null) {

                    declarationJson.put("typePersonne", personne.getFormeJuridique().getIntitule());
                    declarationJson.put("assujettiCode", personne.getCode());
                    declarationJson.put("assujettiName", personne.toString().toUpperCase());

                    declarationJson.put("userName",
                            personne.getLoginWeb() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getLoginWeb().getUsername());

                    AdressePersonne adressePersonne = DataAccess.getDefaultAdressByPersone(personne.getCode());

                    if (adressePersonne != null) {
                        declarationJson.put("adressePersonne", adressePersonne.getAdresse().toString().toUpperCase());
                    } else {
                        declarationJson.put("adressePersonne", GeneralConst.EMPTY_STRING);
                    }

                } else {
                    declarationJson.put("typePersonne", GeneralConst.EMPTY_STRING);
                    declarationJson.put("assujettiName", GeneralConst.EMPTY_STRING);
                    declarationJson.put("userName", GeneralConst.EMPTY_STRING);
                }

                declarationJson.put("typeDocumentLibelle", "NOTE DE TAXATION");
                declarationJson.put("typeDocumentCode", "DEC");
                declarationJson.put("CompteBancaireCode",
                        declarationList.get(0).getFkCompteBancaire() != null ? declarationList.get(0).getFkCompteBancaire() : "");
                declarationJson.put("banqueCode",
                        declarationList.get(0).getFkBanque() != null ? declarationList.get(0).getFkBanque() : "");
                declarationJson.put("articleName", articleComposite.toUpperCase());
                declarationJson.put("deviseCode", declarationList.get(0).getDevise());
                BigDecimal sumTotalDeclaration = new BigDecimal(0);

                List<JSONObject> detailDeclarationJsonList = new ArrayList<>();

                BigDecimal amountRD = new BigDecimal("0");
                String periodeNames = "";

                PeriodeDeclaration p = new PeriodeDeclaration();
                RetraitDeclaration rdPere = new RetraitDeclaration();
                ArticleBudgetaire ab = new ArticleBudgetaire();

                if (declarationList.get(0).getEtat() == 4 || declarationList.get(0).getEtat() == 3) {

                    amountRD = amountRD.add(declarationList.get(0).getMontant());

                    rdPere = DataAccess.getRetraitDeclarationByID(declarationList.get(0).getRetraitDeclarationMere());
                    p = DataAccess.getPeriodeDeclarationById(Integer.valueOf(rdPere.getFkPeriode()));
                    ab = DataAccess.getArticleBudgetaireByCode(rdPere.getFkAb());

                    periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode());

                } else {

                    if (declarationList.get(0).getNewId() != null) {

                        List<RetraitDeclaration> retraitDeclarations = DataAccess.getListRetraitDeclarationByNewID_V2(declarationList.get(0).getNewId());

                        if (!retraitDeclarations.isEmpty()) {

                            for (RetraitDeclaration rd : retraitDeclarations) {

                                amountRD = amountRD.add(rd.getMontant());

                                p = DataAccess.getPeriodeDeclarationByIdV2(Integer.valueOf(rd.getFkPeriode()));
                                ab = DataAccess.getArticleBudgetaireByCodeV2(rd.getFkAb());

                                if (periodeNames.isEmpty()) {

                                    periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                } else {
                                    periodeNames += periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                }

                                //JsonObject rdJson = new JsonObject();
                            }
                        }
                    } else {
                        amountRD = amountRD.add(declarationList.get(0).getMontant());

                        p = DataAccess.getPeriodeDeclarationById(Integer.valueOf(declarationList.get(0).getFkPeriode()));
                        ab = DataAccess.getArticleBudgetaireByCode(declarationList.get(0).getFkAb());

                        periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode());
                    }
                }

                for (RetraitDeclaration retraitDeclaration : declarationList) {

                    JSONObject detailDeclarationJson = new JSONObject();

                    PeriodeDeclaration periodeDeclaration = DataAccess.getPeriodeDeclarationByCode(
                            retraitDeclaration.getFkPeriode());

                    if (periodeDeclaration == null) {
                        periodeDeclaration = DataAccess.getPeriodeDeclarationByCode(
                                rdPere.getFkPeriode());
                    }

                    if (periodeDeclaration != null) {

                        detailDeclarationJson.put("periodeDeclaration", periodeNames);

                        if (periodeDeclaration.getAssujetissement().getBien() != null) {

                            Bien bien = periodeDeclaration.getAssujetissement().getBien();

                            String bienName = bien.getIntitule().toUpperCase();
                            String bienDesciption = bien.getDescription() != null ? bien.getDescription() : GeneralConst.EMPTY_STRING;
                            String bienType = bien.getTypeBien().getIntitule().toUpperCase();
                            String bienAddresse = bien.getFkAdressePersonne().getAdresse().toString().toUpperCase();

                            String bienComposite = "Type : ".concat(bienType).concat("<br/>").concat("Intitulé : ").concat(
                                    bienName).concat("<br/>").concat("Description : ").concat(
                                            bienDesciption).concat("<br/>").concat("Adresse : ").concat(
                                            bienAddresse);

                            detailDeclarationJson.put("bienDeclaration", bienComposite);

                        } else {
                            detailDeclarationJson.put("bienDeclaration", GeneralConst.EMPTY_STRING);
                            detailDeclarationJson.put("periodeDeclaration", GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        detailDeclarationJson.put("bienDeclaration", GeneralConst.EMPTY_STRING);
                        detailDeclarationJson.put("periodeDeclaration", GeneralConst.EMPTY_STRING);
                    }

                    detailDeclarationJson.put("netAPayer", amountRD);
                    sumTotalDeclaration = sumTotalDeclaration.add(amountRD);
                    detailDeclarationJsonList.add(detailDeclarationJson);

                }

                Bordereau bordereau = DataAccess.getBordereauByDeclaration(codeDeclaration);

                String mesageContent;

                if (bordereau != null) {

                    declarationJson.put("paiementExisting", GeneralConst.NumberString.ONE);
                    declarationJson.put("bordereau", bordereau.getNumeroBordereau());
                    declarationJson.put("idEtatPaiement", bordereau.getEtat().getCode());

                    switch (bordereau.getEtat().getCode()) {

                        case 3:
                            mesageContent = "Cette déclaration est déjà payée et reste en attente de traitement à la banque";
                            break;
                        case 2:
                            mesageContent = "Cette déclaration est déjà payée et confirmer à la banque";
                            break;
                        default:
                            mesageContent = "";
                            break;

                    }

                    declarationJson.put("messagePaiement", mesageContent);
                    declarationJson.put("compteBancaireBordereau",
                            declarationList.get(0).getFkCompteBancaire() == null
                                    ? bordereau.getCompteBancaire().getCode() : declarationList.get(0).getFkCompteBancaire());
                    declarationJson.put("dateBordereau",
                            ConvertDate.formatDateToStringOfFormat(bordereau.getDatePaiement(), "dd/MM/YYYY"));
                    declarationJson.put("montantPaye", sumTotalDeclaration);

                } else {
                    declarationJson.put("paiementExisting", GeneralConst.NumberString.ZERO);
                    declarationJson.put("bordereau", GeneralConst.EMPTY_STRING);
                    declarationJson.put("messagePaiement", GeneralConst.EMPTY_STRING);
                    declarationJson.put("idEtatPaiement", GeneralConst.EMPTY_STRING);
                    declarationJson.put("compteBancaireBordereau", GeneralConst.EMPTY_STRING);
                    declarationJson.put("dateBordereau", GeneralConst.EMPTY_STRING);
                    declarationJson.put("montantPaye", GeneralConst.EMPTY_STRING);
                }

                declarationJson.put("netAPayer", sumTotalDeclaration);
                declarationJson.put("payer", 0);
                declarationJson.put("solde", sumTotalDeclaration);
                declarationJson.put("detailDeclarationJsonList", detailDeclarationJsonList.toString());

                return declarationJson.toString();
//                }

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String verifyCommandeByReference(String reference) {

        try {

            Commande commande = DataAccess.verifyCommandeByReference(reference);

            if (commande != null) {

                List<JSONObject> listJsonObjects = new ArrayList<>();

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("datePaiement",
                        Tools.formatDateToString(commande.getDatePaiement()));

                String ab = commande.getFkAb().getCode();
                jsonObject.put("codeArticleBudgetaire", ab);
                jsonObject.put("intituleArticleBudgetaire", commande.getFkAb().getIntitule());
                jsonObject.put("dateOrdonnancement", Tools.formatDateToString(commande.getDateCreat()));

                jsonObject.put("document", "NOTE DE PAIEMENT DE COMMANDE DES VOUCHERS");
                jsonObject.put("typeDocument", "NOTE DE PAIEMENT DE COMMANDE DES VOUCHERS");

                Personne personne = commande.getFkPersonne();

                String requerant = String.format("%s %s %s", personne.getNom(), personne.getPostnom(), personne.getPrenoms());

                jsonObject.put("requerant", requerant);
                jsonObject.put("assujettiName", requerant);

                jsonObject.put("userName",
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : personne.getLoginWeb().getUsername());

                jsonObject.put("reference", commande.getReference());
                jsonObject.put("bordereau", commande.getReferencePaiement());
                jsonObject.put("documentApure", commande.getReference());

                String dateBd = Tools.formatDateToString(commande.getDatePaiement());

                jsonObject.put("dateBordereau", dateBd);
                jsonObject.put("banque", commande.getFkCompteBancaire().getBanque().getIntitule().trim());
                jsonObject.put("banqueName", commande.getFkCompteBancaire().getBanque().getIntitule().trim());
                jsonObject.put("compteBancaire", commande.getFkCompteBancaire().getCode().trim());
                jsonObject.put("devise", commande.getDevise().getCode());
                jsonObject.put("montantPaye", commande.getMontant());
                jsonObject.put("codeJournal", commande.getId());

                Agent agent = DataAccess.getAgentByCode(commande.getAgentPaiement().getCode());

                jsonObject.put("agentApurement", agent.toString().toUpperCase());
                jsonObject.put("montantApure", commande.getMontant());

                jsonObject.put("numeroReleveBancaire", "");
                jsonObject.put("dateReleveBancaire", "");

                jsonObject.put("etatPaiement", "VALIDER");

                jsonObject.put("agentPaiement", agent.toString().toUpperCase());

                jsonObject.put("observation", "PAIEMENT COMMANDE DES VOUCHERS");

                listJsonObjects.add(jsonObject);

                return listJsonObjects.toString();

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String savePaiementCommandeVoucher(String file) {

        try {

            propertiesConfig = new PropertiesConfig();

            JSONObject objet = new JSONObject(file);
            String referenceDocument = objet.getString("referenceDocument");
            String referencePaiement = objet.getString("referencePaiement");
            String userId = objet.getString("userId");

            Agent agent = DataAccess.getAgentByCode(Integer.valueOf(userId));
            if (agent != null) {

                if (DataAccess.savePaiementCommandeVoucher(referenceDocument, referencePaiement, Integer.valueOf(userId), agent.getSite().getCode())) {
                    return GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    return GeneralConst.ResultCode.FAILED_OPERATION;
                }
            } else {
                return GeneralConst.ResultCode.USER_NOT_AUTORISE;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String saveDeclaration(String file) {

        boolean result;

        try {

            JSONObject objet = new JSONObject(file);
            String referenceDocument = objet.getString("referenceDocument");
            String referencePaiement = objet.getString("referencePaiement");
            String datePaiement = objet.getString("datePaiement");
            String userId = objet.getString("userId");
            String accountBank = objet.getString("compteBancaireCode");
            String amountPaid = objet.getString("amountPaid");

            Bordereau bordereau = new Bordereau();
            Agent agent = DataAccess.getAgentByCode(Integer.valueOf(userId));
            List<RetraitDeclaration> declarationList = new ArrayList<>();

            String assujettiCode = GeneralConst.EMPTY_STRING;
            String centreCode = GeneralConst.EMPTY_STRING;

            declarationList = DataAccess.getListRetraitDeclarationByCodeDeclaration(
                    referenceDocument);

            BigDecimal sumTotalDeclaration = new BigDecimal(0);

            if (!declarationList.isEmpty()) {

                assujettiCode = declarationList.get(0).getFkAssujetti();
                Agent agentDeclaration = DataAccess.getAgentByCode(Integer.valueOf(declarationList.get(0).getFkAgentCreate()));

                if (agentDeclaration != null) {
                    centreCode = agentDeclaration.getSite().getCode();
                }

                BigDecimal amountRD = new BigDecimal("0");

                if (declarationList.get(0).getEtat() == 4 || declarationList.get(0).getEtat() == 3) {

                    amountRD = amountRD.add(BigDecimal.valueOf(Double.valueOf(amountPaid)));

                } else {
                    amountRD = amountRD.add(BigDecimal.valueOf(Double.valueOf(amountPaid)));
                }

                for (RetraitDeclaration retraitDeclaration : declarationList) {

                    accountBank = retraitDeclaration.getFkCompteBancaire();
                }

                sumTotalDeclaration = amountRD;
            }

            if (DataAccess.checkReferePaiementExists(referencePaiement.trim(), accountBank, "DEC")) {
                return "700";
            } else {

                bordereau.setNumeroBordereau(referencePaiement);
                bordereau.setNomComplet(assujettiCode);
                bordereau.setDatePaiement(ConvertDate.formatDate(
                        ConvertDate.getValidFormatDateString(datePaiement)));
                bordereau.setTotalMontantPercu(sumTotalDeclaration);
                bordereau.setCompteBancaire(new CompteBancaire(accountBank));
                bordereau.setAgent(agent);
                bordereau.setDateCreate(new Date());
                bordereau.setEtat(new Etat(GeneralConst.Numeric.THREE));
                bordereau.setNumeroDeclaration(referenceDocument);
                bordereau.setCentre(centreCode);

                CompteBancaire cb = DataAccess.getCompteBancaireByCode_V3(accountBank);

                result = DataAccess.createDeclaration(bordereau, declarationList, cb.getDevise().getCode());

                if (result) {
                    return GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    return GeneralConst.ResultCode.FAILED_OPERATION;
                }

            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String generateVoucher(String reference) {

        String listeCode = "<ul>", societe = "";

        try {
            //Create folder
            String dir = System.getProperty("user.dir").concat("/").concat(reference).concat("/").concat(reference);
            String dir2 = System.getProperty("user.dir").concat("/").concat(reference);
            File theDir = new File(dir);
            if (!theDir.exists()) {
                theDir.mkdirs();
            }

            String filename = dir.concat("/").concat(reference) + ".xls";
            FileOutputStream out = new FileOutputStream(filename);
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("Feuille 1");
            sheet.setColumnWidth((short) 0, (short) (100 * 75));
            sheet.setColumnWidth((short) 1, (short) (100 * 75));

            HSSFRow rowhead = sheet.createRow((short) 0);
            rowhead.createCell(0).setCellValue("CODES");
            rowhead.createCell(1).setCellValue("QR CODES");

            int j = 0;
            List<Voucher> vouchers = DataAccess.getVoucherByCommande(reference);
            if (!vouchers.isEmpty()) {
                for (Voucher voucher : vouchers) {
                    societe = voucher.getFkCommande().getFkPersonne().toString().concat(",")
                            .concat(voucher.getFkCommande().getFkPersonne().getLoginWeb().getMail());
                    for (int i = 0; i < voucher.getQte(); i++) {
                        if (i > Integer.valueOf(GeneralConst.Plage.PLAGE.split(",")[j].split(":")[0])) {
                            j++;
                        }

                        String code = voucher.getFkAxe().getSigle().concat(GeneralConst.DASH_SEPARATOR).
                                concat(voucher.getFkTarif().getSigle()).concat(GeneralConst.DASH_SEPARATOR).
                                concat(reference.toUpperCase().replaceAll("[^A-Za-z]", "").substring(0, 2)).
                                concat(GeneralConst.DASH_SEPARATOR).concat(new DecimalFormat("0").format(Math.random() * 1000000));

                        QRCodeGenerate.generateQRCodeImage(code, reference, dir);

                        listeCode += "<li style=\"width:33px\">".concat(code).concat("</li>");

                        //Ecriture dans excel
                        HSSFRow row = sheet.createRow((short) i + 1);
                        row.createCell(0).setCellValue(code);
                        row.createCell(1).setCellValue(code.concat(".png"));
//                        codeVoucher.add(voucher.getFkAxe().getSigle() + GeneralConst.Plage.PLAGE.split(",")[j].split(":")[1] + df.format(Math.random() * 1000000));
//                        System.out.println(code);
                        DataAccess.generateGoPass(voucher.getId(), code);
                    }
                }
                workbook.write(out);
                out.close();
                listeCode += "</ul>,".concat(societe).concat(",").concat(dir2).concat(",").concat(dir.concat(".zip"));
                ZipFolder.zipFolder(Paths.get(dir), Paths.get(dir.concat(".zip")));
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        return listeCode;
    }

    private static boolean generateGoPassCode(String reference) {

        List<GoPass> gps = DataAccess.getGoPassByCommande(reference);
        if (!gps.isEmpty()) {
            int j = 0;
            DecimalFormat df = new DecimalFormat("0");
            for (GoPass gp : gps) {
                for (int i = 0; i < gp.getQuantite(); i++) {
                    if (i > Integer.valueOf(GeneralConst.Plage.PLAGE.split(",")[j].split(":")[0])) {
                        j++;
                    }
                    String code = reference.substring(0, 2).
                            concat(GeneralConst.Plage.PLAGE.split(",")[j].split(":")[1]).
                            concat(df.format(Math.random() * 1000000));
                    DataAccess.generateGoPass(gp.getIdGopasse(), code);
                }
            }
            return true;
        } else {
            return false;
        }

    }

    private static boolean generateGoPassSpontannee(CommandeGoPassSpontanneeMock cgpsm) {

        List<GoPass> gps = DataAccess.getGoPassByCommande(cgpsm.getReference());
        if (!gps.isEmpty()) {
            int j = 0;
            DecimalFormat df = new DecimalFormat("0");
            for (GoPass gp : gps) {
                for (int i = 0; i < gp.getQuantite(); i++) {
                    if (i > Integer.valueOf(GeneralConst.Plage.PLAGE.split(",")[j].split(":")[0])) {
                        j++;
                    }
                    String code = cgpsm.getReference().substring(0, 2).
                            concat(GeneralConst.Plage.PLAGE.split(",")[j].split(":")[1]).
                            concat(df.format(Math.random() * 1000000));
                    DataAccess.generateGoPassSpontannee(gp.getIdGopasse(), cgpsm, code);
                }
            }
            return true;
        } else {
            return false;
        }

    }

    public static String generateVouchersWithDirectSwift(String reference, double amount) {

        try {

            Commande commande = DataAccess.getListComandesGoPassByReference(reference);
            if (commande != null) {
                if (commande.getMontant().doubleValue() > amount) {
                    if (DataAccess.generateVouchersWithDirectSwift(commande.getId(), commande.getReference(), commande.getMontant().doubleValue(), amount)) {
                        return GeneralConst.NumberString.TWO;//Conciliation faite avec succès
                    } else {
                        return GeneralConst.NumberString.ZERO;
                    }
                } else {
                    String response = setEtatPayCommande(commande.getToken());
                    return response.split(",")[1];//If 1: Vouchers générés avec succès
                }
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;//Opération non effectuée
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String autoSendMailCommandeVouchers(String reference) {

        try {

            propertiesConfig = new PropertiesConfig();

            String mailSendVouchersSubjet = propertiesConfig.getContent("MAIL_SEND_CMD_VOUCHERS_SUBJECT");
            String mailSendVouchersContent = propertiesConfig.getContent("MAIL_SEND_CMD_VOUCHERS_CONTENT");

            Commande cmd = DataAccess.getCommandesVoucherByReference(reference);
            if (cmd != null) {
                mailSendVouchersSubjet = String.format(mailSendVouchersSubjet, reference);
                mailSendVouchersContent = String.format(mailSendVouchersContent, cmd.getFkPersonne().toString(), cmd.getFichierZip().split(",")[0]);
                sendNotification(mailSendVouchersContent, mailSendVouchersSubjet, cmd.getFkPersonne().getLoginWeb().getMail(), reference, "", cmd.getFichierZip());
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public static String rechercheAvanceeBien(String nif, String typeBien, String dateDebut, String dateFin) {

        List<JSONObject> objects = new ArrayList<>();
        try {
            List<Acquisition> acquisitions;
            if (typeBien.equals("null")) {
                acquisitions = DataAccess.getBiensOfPersonneV3(nif);
            } else {
                acquisitions = DataAccess.rechercheAvanceeBien(nif, typeBien, dateDebut, dateFin);
            }
            if (acquisitions.size() > 0) {
                for (Acquisition acquisition : acquisitions) {
                    JSONObject object = new JSONObject();
                    Bien bien = acquisition.getBien();
                    object.put("idBien", bien.getId());
                    object.put("chaineAdresse", bien.getFkAdressePersonne().getAdresse().toString());
                    object.put("isImmobilier", bien.getFkUsageBien() != null ? GeneralConst.NumberString.ONE : GeneralConst.NumberString.ZERO);
                    String responsable = "Moi";
                    String intituleBienHtml = String.format(
                            "<p style=\"font-weight:bold\">%s</p>", bien.getIntitule().toUpperCase());
                    if (!acquisition.getProprietaire()) {

                        Personne personne = DataAccess.getResponsableBien(bien.getId());
                        if (personne != null) {
                            responsable = personne.toString();
                        }
                        intituleBienHtml += " <p style=\"color:red\">(Acquis par location)</p>";

                    }

                    object.put("idAcquisition", bien.getId());
                    object.put("responsable", responsable);
                    object.put("intituleBien", bien.getIntitule());
                    object.put("intituleBienHtml", intituleBienHtml);
                    object.put("codeTypeBien", bien.getTypeBien().getCode());
                    object.put("libelleTypeBien", bien.getTypeBien().getIntitule());

                    objects.add(object);
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String getTypeBien() {

        List<JSONObject> objects = new ArrayList<>();
        try {
            List<TypeBien> tbs = DataAccess.getTypeBien();
            if (tbs.size() > 0) {
                for (TypeBien tb : tbs) {
                    JSONObject object = new JSONObject();
                    object.put("code", tb.getCode());
                    object.put("intitule", tb.getIntitule());
                    objects.add(object);
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String activateAccount(String code) {

        try {

            String codeDecode = Tools.decoder(code);
            Personne personne = DataAccess.getPersonne(codeDecode.split(",")[0]);
            if (personne != null) {
                if (DataAccess.activateAccount(personne.getCode())) {
                    return "1";
                } else {
                    return "0";
                }
            } else {
                return "2";
            }

        } catch (Exception e) {
            return "-1";
        }

    }

    public static String getGoPassBlancs(String nif) {

        List<JSONObject> objects = new ArrayList<>();
        try {
            List<DetailsGoPass> dgps = DataAccess.getGoPassBlancs(nif);
            if (dgps.size() > 0) {
                for (DetailsGoPass dgp : dgps) {
                    JSONObject object = new JSONObject();
                    object.put("id", dgp.getIdDetails());
                    object.put("code", dgp.getCodeGoPass());
                    object.put("codeTarif", dgp.getFkGopasse().getFkTarif().getCode());
                    object.put("intituleTarif", dgp.getFkGopasse().getFkTarif().getIntitule());
                    object.put("type", dgp.getFkGopasse().getTypeGopasse());
                    object.put("montant", dgp.getFkGopasse().getFkTarif().getTarifSiteList().get(0).getTaux());
                    object.put("devise", dgp.getFkGopasse().getFkTarif().getTarifSiteList().get(0).getFkDevise());
                    object.put("dateCreate", Tools.formatDateToString(dgp.getDateGeneration()));
                    objects.add(object);
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }
    
    public static String getGoPassBlancsByCommande(String Commande) {

        List<JSONObject> objects = new ArrayList<>();
        try {
            List<DetailsGoPass> dgps = DataAccess.getGoPassBlancsByCommande(Commande);
            if (dgps.size() > 0) {
                for (DetailsGoPass dgp : dgps) {
                    JSONObject object = new JSONObject();
                    object.put("id", dgp.getIdDetails());
                    object.put("code", dgp.getCodeGoPass());
                    object.put("codeTarif", dgp.getFkGopasse().getFkTarif().getCode());
                    object.put("intituleTarif", dgp.getFkGopasse().getFkTarif().getIntitule());
                    object.put("etat", dgp.getEtat());
                    object.put("Passager", dgp.getEtat() == null ? "" : dgp.getPassager());
                    object.put("Sexe", dgp.getSexePassager() == null ? "" : dgp.getSexePassager());
                    object.put("DateVol", dgp.getDateVol()== null? "" : dgp.getDateVol());
                    object.put("NumeroVol", dgp.getNumeroVol()== null? "" : dgp.getNumeroVol());
                    object.put("Compagnie", dgp.getFkCompagnie()== null? "" : dgp.getFkCompagnie().getIntitule());
                    object.put("AeroportDepart", dgp.getFkAeroportProvenance()== null? "" : dgp.getFkAeroportProvenance().getIntitule()+" ("+dgp.getFkAeroportProvenance().getCodeAeroport()+")");
                    object.put("AeroportDestination", dgp.getFkAeroportDestination()== null? "" : dgp.getFkAeroportDestination().getIntitule()+" ("+dgp.getFkAeroportDestination().getCodeAeroport()+")");
                    object.put("type", dgp.getFkGopasse().getTypeGopasse());
                    object.put("montant", dgp.getFkGopasse().getFkTarif().getTarifSiteList().get(0).getTaux());
                    object.put("devise", dgp.getFkGopasse().getFkTarif().getTarifSiteList().get(0).getFkDevise());
                    object.put("dateCreate", Tools.formatDateToString(dgp.getDateGeneration()));
                    objects.add(object);
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String getGoPassByCode(String Code) {

        List<JSONObject> objects = new ArrayList<>();
        try {
            List<DetailsGoPass> dgps = DataAccess.getGoPassByCode(Code);
            if (dgps.size() > 0) {
                for (DetailsGoPass dgp : dgps) {
                    JSONObject object = new JSONObject();
                    object.put("id", dgp.getIdDetails());
                    object.put("code", dgp.getCodeGoPass());
                    object.put("codeTarif", dgp.getFkGopasse().getFkTarif().getCode());
                    object.put("intituleTarif", dgp.getFkGopasse().getFkTarif().getIntitule());
                    object.put("Etat", dgp.getEtat());
                    if (dgp.getFkAeroportProvenance() != null) {
                        object.put("AeroportDepart", dgp.getFkAeroportProvenance().getIntitule() +" ("+dgp.getFkAeroportProvenance().getCodeAeroport()+")");
                        object.put("AeroportDestination", dgp.getFkAeroportDestination().getIntitule()+" ("+dgp.getFkAeroportDestination().getCodeAeroport()+")");
                        object.put("DateVol", dgp.getDateVol());
                        object.put("Passager", dgp.getPassager());
                        object.put("Sexe", dgp.getSexePassager());
                        object.put("NumeroVol", dgp.getNumeroVol());
                        object.put("Compagnie", dgp.getFkCompagnie().getIntitule());
                        object.put("code_provenance", dgp.getFkAeroportProvenance().getCodeAeroport());
                        object.put("code_destination", dgp.getFkAeroportDestination().getCodeAeroport());
                    }
                    object.put("type", dgp.getFkGopasse().getTypeGopasse());
                    object.put("montant", dgp.getFkGopasse().getFkTarif().getTarifSiteList().get(0).getTaux());
                    object.put("devise", dgp.getFkGopasse().getFkTarif().getTarifSiteList().get(0).getFkDevise());
                    object.put("dateCreate", Tools.formatDateToString(dgp.getDateGeneration()));
                    objects.add(object);
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public static String autoSendMailCommandeVouchers() {

        try {

            propertiesConfig = new PropertiesConfig();

            String mailSendVouchersSubjet = propertiesConfig.getContent("MAIL_SEND_CMD_VOUCHERS_SUBJECT");
            String mailSendVouchersContent = propertiesConfig.getContent("MAIL_SEND_CMD_VOUCHERS_CONTENT");

            List<Commande> cmds = DataAccess.getCommandesVoucherNotDelivered();
            if (!cmds.isEmpty()) {
                for (Commande cmd : cmds) {
                    mailSendVouchersSubjet = String.format(mailSendVouchersSubjet, cmd.getReference());
                    mailSendVouchersContent = String.format(mailSendVouchersContent, cmd.getFkPersonne().toString(), Tools.decoder(cmd.getFichierZip().split(",")[0]));
                    sendNotification(mailSendVouchersContent, mailSendVouchersSubjet, cmd.getFkPersonne().getLoginWeb().getMail(), cmd.getReference(), "", cmd.getFichierZip());
                }
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        scheduler = Executors.newSingleThreadScheduledExecutor();
        Timer timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                autoSendMailCommandeVouchers();
            }
        }, 0, 600000);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        scheduler.shutdown();
    }

}
