/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.business;

import cd.hologram.apigopass.constants.GeneralConst;
import cd.hologram.apigopass.constants.PropertiesConst;
import cd.hologram.apigopass.entities.Agent;
import cd.hologram.apigopass.entities.Amr;
import cd.hologram.apigopass.entities.ArticleBudgetaire;
import cd.hologram.apigopass.entities.Assujeti;
import cd.hologram.apigopass.entities.BanqueAb;
import cd.hologram.apigopass.entities.BonAPayer;
import cd.hologram.apigopass.entities.Bordereau;
import cd.hologram.apigopass.entities.CompteBancaire;
import cd.hologram.apigopass.entities.DetailBordereau;
import cd.hologram.apigopass.entities.DetailsNc;
import cd.hologram.apigopass.entities.Etat;
import cd.hologram.apigopass.entities.Journal;
import cd.hologram.apigopass.entities.LoginWeb;
import cd.hologram.apigopass.entities.NotePerception;
import cd.hologram.apigopass.entities.Palier;
import cd.hologram.apigopass.entities.PeriodeDeclaration;
import cd.hologram.apigopass.entities.Personne;
import cd.hologram.apigopass.entities.RetraitDeclaration;
import cd.hologram.apigopass.entities.Service;
import cd.hologram.apigopass.entities.Site;
import cd.hologram.apigopass.entities.Taux;
import cd.hologram.apigopass.mock.AMR;
import cd.hologram.apigopass.mock.Declaration;
import cd.hologram.apigopass.mock.DetailDeclaration;
import cd.hologram.apigopass.mock.Noteperception;
import cd.hologram.apigopass.mock.Periode;
import cd.hologram.apigopass.pojo.RDeclaration;
import cd.hologram.apigopass.properties.PropertiesConfig;
import cd.hologram.apigopass.utils.DateUtil;
import cd.hologram.apigopass.utils.InsecureHostnameVerifier;
import cd.hologram.apigopass.utils.InsecureTrustManager;
import cd.hologram.apigopass.utils.ResponseMessage;
import cd.hologram.apigopass.utils.Tools;
import com.google.gson.Gson;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author moussa.toure
 */
public class DocumentBusiness {

    static DataAccess dataAccess;
    static ResponseMessage responseMessage;

    public static String getABService(String service) throws Exception {

        String response = GeneralConst.EMPTY_STRING;

        List<JSONObject> ABJsonList = new ArrayList<>();

        try {

            dataAccess = new DataAccess();

            List<ArticleBudgetaire> articleBudgetaires = dataAccess.articleBudgetaires(service);

            for (ArticleBudgetaire ab : articleBudgetaires) {

                JSONObject abNObject = new JSONObject();

                abNObject.put("id", ab.getCode());
                abNObject.put("libelle", ab.getIntitule());

                List<JSONObject> paliersJsonList = new ArrayList<>();
                List<Palier> paliers = ab.getPalierList();

                for (Palier palier : paliers) {

                    if (!Integer.valueOf(palier.getEtat()).equals(1)) {
                        continue;
                    }

                    JSONObject obPalier = new JSONObject();
                    obPalier.put("id", palier.getTarif().getCode());
                    obPalier.put("libelle", palier.getTarif().getIntitule());
                    obPalier.put("forme", palier.getTypePersonne().getCode());
                    obPalier.put("base", palier.getMultiplierValeurBase());
                    obPalier.put("unite", palier.getUnite());
                    obPalier.put("type", palier.getTypeTaux());
                    obPalier.put("taux", palier.getTaux());
                    obPalier.put("devise", palier.getDevise());

                    paliersJsonList.add(obPalier);
                }

                abNObject.put("tarifs", paliersJsonList.toString());
                ABJsonList.add(abNObject);

            }

            response = ABJsonList.toString();

        } catch (Exception e) {
            throw e;
        }

        return response;

    }

    public static String getService() throws Exception {

        String response = GeneralConst.EMPTY_STRING;

        List<JSONObject> serviceJsonList = new ArrayList<>();

        try {

            dataAccess = new DataAccess();

            List<Service> services = dataAccess.getServiceAssiete();

            for (Service service : services) {

                JSONObject serNObject = new JSONObject();

                serNObject.put("id", service.getCode());
                serNObject.put("libelle", service.getIntitule());

                serviceJsonList.add(serNObject);

            }

            response = serviceJsonList.toString();

        } catch (Exception e) {
            throw e;
        }

        return response;

    }

    public static String getPaliers(String ab) throws Exception {

        String response = GeneralConst.EMPTY_STRING;

        List<JSONObject> palierJsonList = new ArrayList<>();

        try {
            dataAccess = new DataAccess();

            List<Palier> paliers = dataAccess.getTarifByArticle(ab);

            if (!paliers.isEmpty()) {

                for (Palier palier : paliers) {

                    JSONObject objectPalier = new JSONObject();

                    objectPalier.put("id", palier.getTarif().getCode());
                    objectPalier.put("libelle", palier.getTarif().getIntitule());
                    objectPalier.put("type", palier.getTypePersonne().getCode());

                    palierJsonList.add(objectPalier);
                }
                response = palierJsonList.toString();
            } else {
                response = "-6";
            }

        } catch (Exception e) {
            throw e;
        }

        return response;
    }

    public static String getDeclaration(String numero, String lang, String format, int version) throws IOException {

        String response = GeneralConst.EMPTY_STRING;
        responseMessage = new ResponseMessage();
        Declaration dec = new Declaration();
        List<DetailDeclaration> decDetail = new ArrayList<>();

        try {

            dataAccess = new DataAccess();

            List<RetraitDeclaration> declarationList = dataAccess.getRetraitDeclarationByCode(numero);

            if (declarationList.isEmpty()) {

                response = responseMessage.getDocumentException(lang, numero, GeneralConst.TypeDocument.DEC, format);

            } else {

                RetraitDeclaration declaration = declarationList.get(0);

                Personne personne = dataAccess.getPersonneByCode(declaration.getFkAssujetti());
                ArticleBudgetaire ab = dataAccess.getArticleBudgetairebyCode(declaration.getFkAb());

                BanqueAb banqueAb = DataAccess.getBanqueAbByArticleB(ab);

                String requerant = personne != null ? personne.toString() : GeneralConst.EMPTY_STRING;
                LoginWeb lw = DataAccess.getLoginWeb_V2(personne.getCode());
                String adress = personne.getAdressePersonneList().get(0).getAdresse().toString();
                String article = ab != null ? ab.getCodeOfficiel() + GeneralConst.SEPARATOR_SLASH + ab.getIntitule() : GeneralConst.EMPTY_STRING;

                dec.setNumero(declaration.getCodeDeclaration());
                dec.setRequerant(requerant);
                dec.setTelephone(lw.getTelephone());
                dec.setAdresseRequerant(adress);
                dec.setDateRetrait(DateUtil.formatDateToString(declaration.getDateCreate()));
                dec.setArticleBudgetaire(article);
                dec.setArticleGenerique(ab != null ? ab.getIntitule() : GeneralConst.DASH_SEPARATOR);

                BigDecimal totalSum = BigDecimal.ZERO;

                for (RetraitDeclaration declarationList1 : declarationList) {

                    totalSum = totalSum.add(declarationList1.getMontant());

                    DetailDeclaration dd = new DetailDeclaration();

                    PeriodeDeclaration pd = dataAccess.getPeriodeDeclaration(Integer.valueOf(declarationList1.getFkPeriode()));

                    Assujeti assujeti = pd.getAssujetissement();

                    String detailsBien = assujeti.getBien() != null ? assujeti.getBien().getIntitule().concat(GeneralConst.SPACE).concat(GeneralConst.BRAKET_OPEN).concat(assujeti.getBien().getTypeBien().getIntitule()).concat(GeneralConst.BRAKET_CLOSE) : GeneralConst.EMPTY_STRING;

                    String periode = Tools.getPeriodeIntitule(pd.getDebut(), ab != null ? ab.getPeriodicite().getNbrJour().toString() : GeneralConst.EMPTY_STRING);
                    dd.setDateEcheance(DateUtil.formatDateToString(pd.getDateLimite()));
                    dd.setBien(detailsBien);
                    dd.setPeriode(periode);
                    dd.setMontant(declarationList1.getMontant());
                    dd.setDevise(declarationList1.getDevise());
                    dd.setCompte(banqueAb.getFkCompte().getCode());

                    decDetail.add(dd);

                }

                dec.setMontantDu(totalSum);
                dec.setDevise(declaration.getDevise());

                if (version == 2) {
                    dec.setDetails(decDetail);
                }

                if (!format.equals(GeneralConst.Format_Response.JSON)) {

                    response = Tools.convertToXML(Declaration.class, dec);

                } else {
                    Gson gson = new Gson();
                    response = gson.toJson(dec);
                }

            }

        } catch (NumberFormatException | IOException e) {

            throw e;
        }

        return response;

    }

    public static String getDeclarationByNif(String nif, String lang, String format, int version) throws IOException {

        String response = GeneralConst.EMPTY_STRING;
        responseMessage = new ResponseMessage();

        try {

            dataAccess = new DataAccess();

            List<RetraitDeclaration> declarationList = dataAccess.getRetraitDeclaration(nif);
            List<Declaration> listDeclaration = new ArrayList<>();

            if (declarationList.isEmpty()) {

                response = "-6";

            } else {

                for (RetraitDeclaration declaration : declarationList) {

                    List<DetailDeclaration> decDetail = new ArrayList<>();
                    Declaration dec = new Declaration();

                    Personne personne = dataAccess.getPersonneByCode(declaration.getFkAssujetti());
                    ArticleBudgetaire ab = dataAccess.getArticleBudgetairebyCode(declaration.getFkAb());

                    String requerant = personne != null ? personne.toString() : GeneralConst.EMPTY_STRING;
                    String article = ab != null ? ab.getCodeOfficiel() + GeneralConst.SEPARATOR_SLASH + ab.getIntitule() : GeneralConst.EMPTY_STRING;

                    dec.setNumero(declaration.getCodeDeclaration());
                    dec.setRequerant(requerant);
                    dec.setDateRetrait(DateUtil.formatDateToString(declaration.getDateCreate()));
                    dec.setArticleBudgetaire(article);

                    //declaration.getFkAssujetti();
                    BigDecimal totalSum = BigDecimal.ZERO;

                    totalSum = totalSum.add(declaration.getMontant());

//                    for (DetailDeclaration detailDeclaration : dec.getDetails()) {
//
//                        PeriodeDeclaration pd = dataAccess.getPeriodeDeclaration(Integer.valueOf(dec.getPeriode()));
//
//                        Assujeti assujeti = pd.getAssujetissement();
//
//                        DetailDeclaration dd = new DetailDeclaration();
//                        String detailsBien = assujeti.getBien() != null ? assujeti.getBien().getIntitule().concat(GeneralConst.SPACE)
//                                .concat(GeneralConst.BRAKET_OPEN).concat(assujeti.getBien().getTypeBien().getIntitule()).concat(GeneralConst.BRAKET_CLOSE) : GeneralConst.EMPTY_STRING;
//
//                        String periode = Tools.getPeriodeIntitule(pd.getDebut(), ab != null ? ab.getPeriodicite().getNbrJour().toString() : GeneralConst.EMPTY_STRING);
//                        dd.setDateEcheance(DateUtil.formatDateToString(pd.getDateLimite()));
//                        dd.setBien(detailsBien);
//                        dd.setPeriode(periode);
//                        dd.setMontant(detailDeclaration.getMontant());
//                        dd.setDevise(detailDeclaration.getDevise());
//
//                        decDetail.add(dd);
//
//                    }
//
//                    dec.setDetails(decDetail);
                    dec.setMontantDu(totalSum);
                    dec.setDevise(declaration.getDevise());

                    if (version == 2) {
                        dec.setDetails(decDetail);
                    }

                    listDeclaration.add(dec);

                }

                Gson gson = new Gson();
                response = gson.toJson(listDeclaration);

            }

        } catch (NumberFormatException e) {
            response = GeneralConst.ResultCode.FAILED_OPERATION;
        }

        return response;

    }

    public static List<Periode> getPeriodeInformation(String taxID, String tarifID, String assCode, int valeur) {

        dataAccess = new DataAccess();
        List<Periode> list = new ArrayList<>();

        try {

            ArticleBudgetaire ab = dataAccess.getArticleBudgetairebyCode(taxID);
            List<RDeclaration> declarations = dataAccess.getDeclarationByAssujettissement(assCode);
            //List<PeriodeDeclaration> declarations = dataAccess.getPeriodeByAssujettissement(assCode);
            Palier palier = dataAccess.getPalierByTarif(tarifID, taxID);

            BigDecimal amount;

            amount = palier.getTaux();

            if (palier.getTypeTaux().equals(GeneralConst.TypeTaux.F)) {

                Integer multiplier = Integer.valueOf(palier.getMultiplierValeurBase());

                if (multiplier.equals(GeneralConst.Numeric.ONE)) {
                    amount = amount.multiply(new BigDecimal(valeur));
                }

            } else {

                amount = new BigDecimal(valeur).multiply(amount).divide(new BigDecimal(GeneralConst.Numeric.CENT));
            }

            String devise = palier.getDevise();
            if (devise.equals(GeneralConst.Devise.DEVISE_USD)) {
                Taux taux = DataAccess.getTauxByDevise(devise);
                if (taux != null) {
                    amount = amount.multiply(taux.getTaux());
                    palier.setDevise(GeneralConst.Devise.DEVISE_CDF);
                }
            }

            for (RDeclaration declaration : declarations) {

                String pd = Tools.getPeriodeIntitule(declaration.getDebut(), ab != null ? ab.getPeriodicite().getNbrJour().toString() : GeneralConst.EMPTY_STRING);

                list.add(new Periode(declaration.getCode(), declaration.getId(), pd, amount, palier.getDevise()));
            }

        } catch (Exception e) {
            throw e;
        }
        return list;
    }

    public static String getJournalCode(String personne, String lang, String format, int version) throws IOException, JSONException {

        String response = GeneralConst.EMPTY_STRING;
        responseMessage = new ResponseMessage();
        List<JSONObject> objects = new ArrayList<>();

        try {

            dataAccess = new DataAccess();

            List<Journal> journalList = dataAccess.getJournalByCode(personne);

            if (journalList.isEmpty()) {
//                response = responseMessage.getDocumentException(lang, personne, GeneralConst.TypeDocument.DEC, format);
                response = "-6";
            } else {

                for (Journal journal : journalList) {
                    JSONObject object = new JSONObject();
                    object.put("codeJournal", journal.getCode());
                    object.put("montant", journal.getMontantPercu().doubleValue());
                    object.put("modepaiement", journal.getModePaiement());
                    object.put("devise", journal.getDevise().getCode());
                    object.put("etat", journal.getEtat());
                    object.put("date", Tools.formatDateToStringV3(journal.getDateCreat()));
                    objects.add(object);
                }
                response = objects.toString();

            }

        } catch (NumberFormatException e) {

            throw e;
        }

        return response;

    }

    public static Periode getPeriodeInformation(String taxID, String tarifID, int valeur, String period, Date debut, String amountUser, String devise) {

        dataAccess = new DataAccess();

        try {

            ArticleBudgetaire ab = dataAccess.getArticleBudgetairebyCode(taxID);
            Palier palier = dataAccess.getPalierByTarif(tarifID, taxID);

            BigDecimal amount;

            if (amountUser.isEmpty()) {

                amount = palier.getTaux();

                if (palier.getTypeTaux().equals(GeneralConst.TypeTaux.F)) {
                    Integer multiplier = Integer.valueOf(palier.getMultiplierValeurBase());
                    if (multiplier.equals(GeneralConst.Numeric.ONE)) {
                        amount = amount.multiply(new BigDecimal(valeur));
                    }
                } else {
                    amount = new BigDecimal(valeur).multiply(amount).divide(new BigDecimal(GeneralConst.Numeric.CENT));
                }

            } else {

                amount = new BigDecimal(Double.parseDouble(amountUser));
            }

            Taux taux;

            if (devise.isEmpty()) {
                taux = DataAccess.getTauxByDevise(GeneralConst.Devise.DEVISE_CDF);
                devise = palier.getDevise();
            } else {
                taux = DataAccess.getTauxByDevise(palier.getDevise());
            }

            String pd = Tools.getPeriodeIntitule(debut, ab != null ? ab.getPeriodicite().getNbrJour().toString() : GeneralConst.EMPTY_STRING);

            return new Periode(GeneralConst.EMPTY_STRING, Integer.valueOf(period), pd, amount, devise, taux.getTaux());

        } catch (Exception e) {
        }

        return null;
    }

    public static String getAmrByNumero(String numero, String lang, String format) throws IOException, JSONException {

        String response = GeneralConst.EMPTY_STRING;
        responseMessage = new ResponseMessage();
        AMR amrMock = new AMR();

        try {

            dataAccess = new DataAccess();

            Amr amr = dataAccess.getAMRByNumero(numero);

            if (amr != null) {

                Date dateCreate = DateUtil.formatStringFullToDate(amr.getDateCreat());
                String requerant = amr.getFichePriseCharge().getPersonne() != null ? amr.getFichePriseCharge().getPersonne().toString() : GeneralConst.EMPTY_STRING;

                amrMock.setNumero(amr.getNumero());
                amrMock.setType(amr.getTypeAmr());
                amrMock.setRequerant(requerant);
                amrMock.setDateEdition(DateUtil.formatDateToString(dateCreate));
                amrMock.setDateEcheance(DateUtil.formatDateToString(amr.getDateEcheance()));

                String devise = amr.getFichePriseCharge().getDevise().equals(GeneralConst.EMPTY_STRING)
                        ? GeneralConst.Devise.DEVISE_CDF : amr.getFichePriseCharge().getDevise();

                amrMock.setDevise(devise);
                amrMock.setMontantDu(amr.getNetAPayer());
                amrMock.setMontantPaye(amr.getPayer());
                amrMock.setMontantSolde(amr.getSolde());

                if (!format.equals(GeneralConst.Format_Response.JSON)) {

                    response = Tools.convertToXML(AMR.class, amrMock);

                } else {
                    Gson gson = new Gson();
                    response = gson.toJson(amrMock);
                }

            } else {

                response = responseMessage.getDocumentException(lang, numero, GeneralConst.TypeDocument.AMR, format);
            }

        } catch (IOException e) {
            throw e;
        }

        return response;

    }

    public static void logRequest(String client, String url, String response) {

        try {

            dataAccess = new DataAccess();

            dataAccess.logRequest(client, url, response);

        } catch (Exception e) {
        }
    }

    public static String getNotePerceptionById(String numero, String lang, String format) throws IOException, JSONException {

        String response = GeneralConst.EMPTY_STRING;
        responseMessage = new ResponseMessage();

        try {

            List<JSONObject> listArticle = new ArrayList<>();

            dataAccess = new DataAccess();

            NotePerception notePerception = dataAccess.getNotePerceptionByNumero(numero);

            if (notePerception != null) {

                if (notePerception.getNbrImpression().equals(0)) {
                    response = responseMessage.getDocumentException(lang, numero, GeneralConst.TypeDocument.NP, format);
                } else {

                    Date dateEcheance = notePerception.getDateEcheancePaiement() != null ? DateUtil.formatStringFullToDate(notePerception.getDateEcheancePaiement()) : null;
                    Date dateOrdonnancement = DateUtil.formatStringFullToDate(notePerception.getDateCreat());

                    for (DetailsNc dnc : notePerception.getNoteCalcul().getDetailsNcList()) {

                        JSONObject jsonDNC = new JSONObject();

                        jsonDNC.put("code", dnc.getArticleBudgetaire().getCodeOfficiel());
                        jsonDNC.put("libelle", dnc.getArticleBudgetaire().getIntitule());

                        listArticle.add(jsonDNC);
                    }

                    Personne personne = dataAccess.getPersonneByCode(notePerception.getNoteCalcul().getPersonne());
                    Service service = dataAccess.getServiceByCode(notePerception.getNoteCalcul().getService());
                    CompteBancaire compteBancaire = dataAccess.getCompteBancaireByCode(notePerception.getCompteBancaire());

                    Noteperception npMock = new Noteperception();

                    String banque = compteBancaire != null ? compteBancaire.getBanque() != null ? compteBancaire.getBanque().getIntitule() : GeneralConst.EMPTY_STRING : GeneralConst.EMPTY_STRING;
                    String banqueSwift = compteBancaire != null ? compteBancaire.getBanque() != null ? compteBancaire.getBanque().getCodeSuift() : GeneralConst.EMPTY_STRING : GeneralConst.EMPTY_STRING;
                    String compteBank = compteBancaire != null ? compteBancaire.getCode() : GeneralConst.EMPTY_STRING;
                    String requerant = personne != null ? personne.toString() : GeneralConst.EMPTY_STRING;
                    String secteur = service != null ? service.getIntitule() : GeneralConst.EMPTY_STRING;
                    String devise = notePerception.getDevise() != null ? notePerception.getDevise().getCode() : GeneralConst.Devise.DEVISE_CDF;

                    npMock.setBanque(banque);
                    npMock.setBanqueSwift(banqueSwift);
                    npMock.setNumero(numero);
                    npMock.setRequerant(requerant);
                    npMock.setDateOrdonnancement(DateUtil.formatDateToString(dateOrdonnancement));
                    if (dateEcheance != null) {
                        npMock.setDateEcheance(DateUtil.formatDateToString(dateEcheance));
                    }
                    npMock.setSecteur(secteur);
                    npMock.setArticleBudgetaire(listArticle.toString());
                    npMock.setCompteBancaire(compteBank);
                    npMock.setMontantDu(notePerception.getNetAPayer());
                    npMock.setMontantPaye(notePerception.getPayer());
                    npMock.setMontantSolde(notePerception.getSolde());
                    npMock.setDevise(devise);

                    if (!format.equals(GeneralConst.Format_Response.JSON)) {

                        response = Tools.convertToXML(Noteperception.class, npMock);

                    } else {
                        Gson gson = new Gson();
                        response = gson.toJson(npMock);
                    }
                }
            } else {

                response = responseMessage.getDocumentException(lang, numero, GeneralConst.TypeDocument.NP, format);

            }

        } catch (IOException e) {
            throw e;
        }

        return response;
    }

    public static String login(String login, String password) throws Exception {

        String response = GeneralConst.EMPTY_STRING;

        try {

            PropertiesConfig propertiesConfig = new PropertiesConfig();

            MultivaluedMap headers = new MultivaluedHashMap();

            SSLContext sc = SSLContext.getInstance("TLSv1");

            TrustManager[] trustAllCerts = {new InsecureTrustManager()};
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HostnameVerifier allHostsValid = new InsecureHostnameVerifier();

            Client client = ClientBuilder.newBuilder().sslContext(sc).hostnameVerifier(allHostsValid).build();

            String urlHiconnect = propertiesConfig.getContent(PropertiesConst.Config.URL_HICONNECT_LOGIN);
            WebTarget target = client.target(urlHiconnect);

            Form form = new Form();
            form.param("operation", "generateToken");
            form.param("username", login);
            form.param("password", password);

            Entity<Form> entity = Entity.entity(form,
                    MediaType.APPLICATION_FORM_URLENCODED_TYPE);

            response = target.request()
                    .headers(headers)
                    .post(entity, String.class);

        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            throw e;
        }

        return response;
    }

    public static String payment(String file, String lang, String format) throws IOException, Exception {

        String response = GeneralConst.EMPTY_STRING;
        responseMessage = new ResponseMessage();
        boolean resultPayment;

        List<String> listParamMissing = new ArrayList<>();

        JSONObject objPayment;

        try {
            objPayment = new JSONObject(file);
        } catch (Exception e) {
            return responseMessage.getBodyException(lang, format);
        }

        try {

            if (!objPayment.has(GeneralConst.paymentKEY.BORDEREAU)) {
                listParamMissing.add(GeneralConst.paymentKEY.BORDEREAU);
            }

            if (!objPayment.has(GeneralConst.paymentKEY.PAIEMENT_DATE)) {
                listParamMissing.add(GeneralConst.paymentKEY.PAIEMENT_DATE);
            }

            if (!objPayment.has(GeneralConst.paymentKEY.DOCUMENT_TYPE)) {
                listParamMissing.add(GeneralConst.paymentKEY.DOCUMENT_TYPE);
            }

            if (!objPayment.has(GeneralConst.paymentKEY.ACCOUNT)) {
                listParamMissing.add(GeneralConst.paymentKEY.ACCOUNT);
            }

            if (!objPayment.has(GeneralConst.paymentKEY.CURRENCY)) {
                listParamMissing.add(GeneralConst.paymentKEY.CURRENCY);
            }

            if (!objPayment.has(GeneralConst.paymentKEY.DOCUMENT_NUMBER)) {
                listParamMissing.add(GeneralConst.paymentKEY.DOCUMENT_NUMBER);
            }

            if (!objPayment.has(GeneralConst.paymentKEY.MONTANT)) {
                listParamMissing.add(GeneralConst.paymentKEY.MONTANT);
            }

            if (!listParamMissing.isEmpty()) {

                response = responseMessage.getBodyException(lang, listParamMissing.toString().replaceAll("\\[|\\]", ""), format);

            } else {

                String numeroBorderau = objPayment.getString(GeneralConst.paymentKEY.BORDEREAU);
                String datePaiement = objPayment.getString(GeneralConst.paymentKEY.PAIEMENT_DATE);
                String typeDocument = objPayment.getString(GeneralConst.paymentKEY.DOCUMENT_TYPE).toUpperCase();
                String compteBancaire = objPayment.getString(GeneralConst.paymentKEY.ACCOUNT);
                String numeroDocument = objPayment.getString(GeneralConst.paymentKEY.DOCUMENT_NUMBER);
                String devise = objPayment.getString(GeneralConst.paymentKEY.CURRENCY);
                Long montantDu = objPayment.getLong(GeneralConst.paymentKEY.MONTANT);

                PropertiesConfig propertiesConfig = new PropertiesConfig();

                String modePaiment = propertiesConfig.getContent(PropertiesConst.Config.MODE_PAIEMENT);
                String agentPaiement = propertiesConfig.getContent(PropertiesConst.Config.CODE_AGENT_SERVICE_WEB);

                String siteCode = GeneralConst.EMPTY_STRING;
                String personneCode = GeneralConst.EMPTY_STRING;

                dataAccess = new DataAccess();

                Bordereau bordereauObj = dataAccess.getBordereauByNumero(numeroBorderau, false);

                if (bordereauObj != null) {
                    return responseMessage.geBankRefNmberException(lang, format);
                }

                boolean canMakePayement = true;
                boolean isDeclaration = false;

                switch (typeDocument) {

                    case GeneralConst.TypeDocument.NP:

                        NotePerception notePerception = dataAccess.getNotePerceptionByNumero(numeroDocument);

                        if (notePerception == null) {

                            canMakePayement = false;
                            response = responseMessage.getDocumentException(lang, numeroDocument, typeDocument, format);

                        } else {

                            if (!notePerception.getCompteBancaire().equals(compteBancaire)) {

                                canMakePayement = false;
                                response = responseMessage.getDocumentException(lang, numeroDocument, typeDocument, format);

                            } else {
                                if (!notePerception.getCompteBancaire().equals(compteBancaire)) {

                                    canMakePayement = false;
                                    response = responseMessage.geBankAccountException(lang, format);

                                }
                                siteCode = notePerception.getSite();
                                personneCode = notePerception.getNoteCalcul().getPersonne();
                            }
                        }

                        break;
                    case GeneralConst.TypeDocument.BAP:

                        BonAPayer bonAPayer = dataAccess.getBonPayerByCode(numeroDocument);

                        if (bonAPayer != null) {

                            siteCode = bonAPayer.getFkNoteCalcul().getSite();
                            personneCode = bonAPayer.getFkPersonne().getCode();

                        } else {

                            canMakePayement = false;
                            response = responseMessage.getDocumentException(lang, numeroDocument, typeDocument, format);

                        }

                        break;

                    case GeneralConst.TypeDocument.AMR1:
                    case GeneralConst.TypeDocument.AMR2:

                        Amr amr = dataAccess.getAMRByNumero(numeroDocument);

                        if (amr != null) {

                            if (amr.getFichePriseCharge() != null) {

                                if (!amr.getFichePriseCharge().getDetailFichePriseChargeList().isEmpty()) {

                                    Site site = dataAccess.getSiteByNoteCalcul(amr.getFichePriseCharge().getDetailFichePriseChargeList().get(0).getNc());

                                    if (site != null) {
                                        siteCode = site.getCode().trim();
                                    } else {
                                        canMakePayement = false;
                                        response = responseMessage.getDocumentException(lang, numeroDocument, typeDocument, format);
                                    }
                                } else {
                                    canMakePayement = false;
                                    response = responseMessage.getDocumentException(lang, numeroDocument, typeDocument, format);
                                }

                                personneCode = amr.getFichePriseCharge().getPersonne().getCode();

                            } else {
                                canMakePayement = false;
                                response = responseMessage.getDocumentException(lang, numeroDocument, typeDocument, format);
                            }

                        } else {
                            canMakePayement = false;
                            response = responseMessage.getDocumentException(lang, numeroDocument, typeDocument, format);
                        }

                        break;
                    case GeneralConst.TypeDocument.DEC:

                        List<RetraitDeclaration> declarationList = dataAccess.getRetraitDeclarationByCodeV2(numeroDocument);

                        if (declarationList.isEmpty()) {
                            declarationList = dataAccess.getRetraitDeclarationByCode(numeroDocument);
                        }

                        RetraitDeclaration declaration = declarationList.isEmpty() ? null : new RetraitDeclaration(Integer.valueOf(numeroDocument));

                        isDeclaration = true;

                        if (declaration == null) {

                            canMakePayement = false;
                            response = responseMessage.getDocumentException(lang, numeroDocument, typeDocument, format);

                        } else {

                            //vérifier si déjà le paiement
                            //Bordereau bordereauObj = dataAccess.getBordereauByNumero(declaration.getCodeDeclaration(),true);
                            CompteBancaire cb = dataAccess.getCompteBancaireByCode(compteBancaire);
//                            BanqueAb banqueAb = dataAccess.getAbByCompteAb(declaration.getFkAb(), compteBancaire);
//
//                            if (banqueAb == null || !cb.getDevise().getCode().equals(devise)) {
//                                return responseMessage.geBankAccountException(lang, format);
//                            }

                            personneCode = declarationList.get(0).getFkAssujetti();

                            Bordereau bordereau = new Bordereau();

                            List<DetailBordereau> detailBordereaus = new ArrayList<>();

                            bordereau.setNumeroBordereau(numeroBorderau);
                            bordereau.setAgent(new Agent(Integer.valueOf(agentPaiement)));
                            bordereau.setDateCreate(new Date());
                            bordereau.setCentre(null);
                            bordereau.setNumeroAttestationPaiement(null);
                            bordereau.setCompteBancaire(new CompteBancaire(compteBancaire));
                            bordereau.setDatePaiement(DateUtil.formatStringFullToDate(datePaiement));
                            bordereau.setNomComplet(personneCode);
                            bordereau.setEtat(new Etat(GeneralConst.NumberNumeric.TWO));
                            bordereau.setNumeroDeclaration(declaration.getId() + "");
                            bordereau.setTotalMontantPercu(new BigDecimal(montantDu));

                            for (RetraitDeclaration rd : declarationList) {

                                DetailBordereau detailBordereau = new DetailBordereau();

                                detailBordereau.setArticleBudgetaire(new ArticleBudgetaire(rd.getFkAb()));
                                detailBordereau.setDevise(rd.getDevise());
                                detailBordereau.setMontantPercu(rd.getMontant());
                                detailBordereau.setPeriodicite(rd.getFkPeriode());

                                detailBordereaus.add(detailBordereau);

                            }

                            resultPayment = dataAccess.saveDeclaration(bordereau, detailBordereaus);

//                            resultPayment = dataAccess.saveDeclaration(
//                                    numeroBorderau,
//                                    personneCode,
//                                    datePaiement,
//                                    montantDu,
//                                    compteBancaire,
//                                    agentPaiement,
//                                    declaration.getCodeDeclaration(),
//                                    declaration.getFkAb(),
//                                    declaration.getFkPeriode(),
//                                    devise,
//                                    GeneralConst.NumberNumeric.TWO);
                            response = responseMessage.getSavePayment(lang, resultPayment, GeneralConst.Format_Response.JSON, declaration.getId() + "");
                        }

                        break;
                    default:
                        canMakePayement = false;
                        response = responseMessage.getDocumentException(lang, numeroDocument, typeDocument, format);

                }

                if (canMakePayement && !isDeclaration) {

                    //vérifier si déjà le paiement
                    //
                    resultPayment = dataAccess.savePayment(
                            siteCode,
                            numeroDocument,
                            typeDocument,
                            montantDu,
                            compteBancaire,
                            numeroBorderau,
                            datePaiement,
                            GeneralConst.NumberNumeric.ZERO,
                            GeneralConst.EMPTY_STRING,
                            null,
                            agentPaiement,
                            null,
                            personneCode,
                            GeneralConst.NumberNumeric.ONE,
                            modePaiment
                    );

                    response = responseMessage.getSavePayment(lang, resultPayment, format, numeroDocument);

                }
            }

        } catch (IOException | JSONException e) {
            throw e;
        }

        return response;
    }

    public static Periode getPeriodeInformation(String taxID, String tarifID, int valeur, String period, Date debut) {

        dataAccess = new DataAccess();

        try {

            ArticleBudgetaire ab = dataAccess.getArticleBudgetairebyCode(taxID);
            Palier palier = dataAccess.getPalierByTarif(tarifID, taxID);

            BigDecimal amount;
            amount = palier.getTaux();

            if (palier.getTypeTaux().equals(GeneralConst.TypeTaux.F)) {
                Integer multiplier = Integer.valueOf(palier.getMultiplierValeurBase());
                if (multiplier.equals(GeneralConst.Numeric.ONE)) {
                    amount = amount.multiply(new BigDecimal(valeur));
                }
            } else {
                amount = new BigDecimal(valeur).multiply(amount).divide(new BigDecimal(GeneralConst.Numeric.CENT));
            }

            String devise = palier.getDevise();
            if (devise.equals(GeneralConst.Devise.DEVISE_USD)) {
                Taux taux = DataAccess.getTauxByDevise(devise);
                if (taux != null) {
                    amount = amount.multiply(taux.getTaux());
                    palier.setDevise(GeneralConst.Devise.DEVISE_CDF);
                }
            }

            String pd = Tools.getPeriodeIntitule(debut, ab != null ? ab.getPeriodicite().getNbrJour().toString() : GeneralConst.EMPTY_STRING);

            return new Periode("", Integer.valueOf(period), pd, amount, palier.getDevise());

        } catch (Exception e) {
        }

        return null;
    }

    public static List<Periode> getPeriodeInformation(String taxID, String assCode, boolean getDetail) {

        dataAccess = new DataAccess();
        List<Periode> list = new ArrayList<>();

        try {

            ArticleBudgetaire ab = dataAccess.getArticleBudgetairebyCode(taxID);
            List<RDeclaration> declarations = dataAccess.getDeclarationByAssujettissement(assCode);

            for (RDeclaration declaration : declarations) {

                if (!getDetail) {

                    String pd = Tools.getPeriodeIntitule(declaration.getDebut(), ab != null ? ab.getPeriodicite().getNbrJour().toString() : GeneralConst.EMPTY_STRING);
                    list.add(new Periode(declaration.getCode(), declaration.getId(), pd, new BigDecimal(BigInteger.ZERO), GeneralConst.EMPTY_STRING, new BigDecimal(BigInteger.ZERO)));

                } else {

                    RetraitDeclaration rd = DataAccess.getRetraitByPeriode(declaration.getId());

                    String pd = Tools.getPeriodeIntitule(declaration.getDebut(), ab != null ? ab.getPeriodicite().getNbrJour().toString() : GeneralConst.EMPTY_STRING);

                    Taux taux = null;

                    if (rd.getDevise().equals(GeneralConst.Devise.DEVISE_CDF)) {

                        taux = DataAccess.getTauxByDevise(GeneralConst.Devise.DEVISE_USD);
                    }

                    list.add(new Periode(declaration.getCode(), declaration.getId(), pd, rd.getMontant(), rd.getDevise(), taux != null ? taux.getTaux() : new BigDecimal(BigInteger.ONE), Tools.formatDateToString(rd.getDateCreate())));
                }

            }

        } catch (Exception e) {
            throw e;
        }
        return list;
    }

    public static String authenticationMpataProcess(String objet) throws IOException {

        PropertiesConfig propertiesConfig = new PropertiesConfig();

        String response, result, token = GeneralConst.EMPTY_STRING;
        String application = propertiesConfig.getContent(PropertiesConst.Config.MPATA_APPLICATION);
        String password = propertiesConfig.getContent(PropertiesConst.Config.MPATA_PASSWORD);
        String username = propertiesConfig.getContent(PropertiesConst.Config.MPATA_USERNAME);
        String url = propertiesConfig.getContent(PropertiesConst.Config.E_MPATA_URL_AUTH);
        String url_check_pin = propertiesConfig.getContent(PropertiesConst.Config.E_MPATA_URL_CHECK_PIN);

        try {

            JSONObject jObject = new JSONObject(objet);
            String numero = jObject.getString("numero");
            String pin = jObject.getString("pin");

            MultivaluedMap headers = new MultivaluedHashMap();

            SSLContext sc = SSLContext.getInstance("TLSv1");

            TrustManager[] trustAllCerts = {new InsecureTrustManager()};
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HostnameVerifier allHostsValid = new InsecureHostnameVerifier();

            Client client = ClientBuilder.newBuilder().sslContext(sc).hostnameVerifier(allHostsValid).build();

            WebTarget target = client.target(url);

            String body = String.format(GeneralConst.ParamMpata.PARAM_JSON, application, password, username);

            response = target.request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(body), String.class);

            try {
                JSONObject responsejson = new JSONObject(response);
                token = responsejson.getString("jwttoken");
            } catch (Exception e) {
            }

            headers.add("Authorization", "Bearer " + token);
            headers.add("Accept", "application/json");

            WebTarget target_pin = client.target(url_check_pin.concat(pin).concat("/").concat(numero));
            response = target_pin.request().headers(headers).get(String.class);

            client.close();

            JSONObject responsejson = new JSONObject(response);
            result = responsejson.getString("status");
            if (result.equals("OK")) {
                JSONObject obj = new JSONObject();
                obj.put("token", token);
                return obj.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public static String transfertMpata(String objet) throws IOException {

        PropertiesConfig propertiesConfig = new PropertiesConfig();

        String response;
        String url = propertiesConfig.getContent(PropertiesConst.Config.E_MPATA_URL_TRANSACTION);

        try {

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            Date date = new Date();

            JSONObject jObject = new JSONObject(objet);
            String amount = jObject.getString("amount");
            String currency = jObject.getString("currency");
            String dateDuJour = format.format(date);
            String description = jObject.getString("description");
            String numero_sender = jObject.getString("sender");
            String token = jObject.getString("token");

            MultivaluedMap headers = new MultivaluedHashMap();

            SSLContext sc = SSLContext.getInstance("TLSv1");

            TrustManager[] trustAllCerts = {new InsecureTrustManager()};
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HostnameVerifier allHostsValid = new InsecureHostnameVerifier();

            Client client = ClientBuilder.newBuilder().sslContext(sc).hostnameVerifier(allHostsValid).build();

            WebTarget target = client.target(url);

            String body = String.format(
                    GeneralConst.ParamMpata.PARAM_TRANSFERT_JSON,
                    amount,
                    currency,
                    dateDuJour,
                    description,
                    numero_sender
            );

            headers.add("Authorization", "Bearer " + token);
            headers.add("Accept", "application/json");

            response = target.request(MediaType.APPLICATION_JSON).headers(headers)
                    .post(Entity.json(body), String.class);

            client.close();

            JSONArray dataKeys = new JSONArray(response);
            JSONObject obj = dataKeys.getJSONObject(0);
            System.out.println(response);

        } catch (Exception e) {
//            System.out.println(e.getMessage());
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return response;
    }
    
}