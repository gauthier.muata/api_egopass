/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.business;

import cd.hologram.apigopass.entities.Amr;
import cd.hologram.apigopass.entities.ArticleBudgetaire;
import cd.hologram.apigopass.entities.BanqueAb;
import cd.hologram.apigopass.entities.BonAPayer;
import cd.hologram.apigopass.entities.Bordereau;
import cd.hologram.apigopass.entities.CompteBancaire;
import cd.hologram.apigopass.entities.Journal;
import cd.hologram.apigopass.entities.NotePerception;
import cd.hologram.apigopass.entities.Palier;
import cd.hologram.apigopass.entities.PeriodeDeclaration;
import cd.hologram.apigopass.entities.Personne;
import cd.hologram.apigopass.entities.RetraitDeclaration;
import cd.hologram.apigopass.entities.Service;
import java.util.List;

/**
 *
 * @author moussa.toure
 */
public interface IDataAccess {
    
    public NotePerception getNotePerceptionByNumero(String numero);
    public Amr getAMRByNumero(String numero);
    public Personne getPersonneByCode(String code);
    public ArticleBudgetaire getArticleBudgetairebyCode(String code);
    public Service getServiceByCode(String code);
    public CompteBancaire getCompteBancaireByCode(String code);
    public BanqueAb getAbByCompteAb(String Ab,String compte);
    public List<RetraitDeclaration> getRetraitDeclarationByCode(String codeDeclaration);
    public PeriodeDeclaration getPeriodeDeclaration(int code);
    public List<PeriodeDeclaration> getPeriodeByAssujettissement(String assu);
    public Palier getPalierByTarif(String tarif, String articleBudgetaire);
    public BonAPayer getBonPayerByCode(String code);
    public Bordereau getBordereauByNumero(String numero,boolean isDeclaration);
    public List<Journal> getJournal(String reference);
    public List<Bordereau> getBordereau(String reference);
    public List<Palier> getTarifByArticle(String ab);
    public List<Service> getServiceAssiete();
    public List<ArticleBudgetaire> articleBudgetaires(String service);
}
