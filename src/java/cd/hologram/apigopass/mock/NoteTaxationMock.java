/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.io.Serializable;

/**
 *
 * @author PC
 */
public class NoteTaxationMock implements Serializable{
    
    String flag;
    String codeQR;
    String numeroNoteTaxation;
    String contribuable;
    String nif;
    String redevable;
    String adresseAssujetti;
    String typeNoteTaxation;
    String periodeDeclaration;
    String impot;
    String adresseBien;
    String montantChiffre;
    String montantLettre;
    String numeroDeclaration;
    String banque;
    String compteBancaire;
    String dateTaxation;
    String lieuTaxation;
    String nomFonctionTaxateur;
    String dateEcheance;
    String entiteEmetrice;
    

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCodeQR() {
        return codeQR;
    }

    public void setCodeQR(String codeQR) {
        this.codeQR = codeQR;
    }

    public String getNumeroNoteTaxation() {
        return numeroNoteTaxation;
    }

    public void setNumeroNoteTaxation(String numeroNoteTaxation) {
        this.numeroNoteTaxation = numeroNoteTaxation;
    }

    public String getContribuable() {
        return contribuable;
    }

    public void setContribuable(String contribuable) {
        this.contribuable = contribuable;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getRedevable() {
        return redevable;
    }

    public void setRedevable(String redevable) {
        this.redevable = redevable;
    }

    public String getAdresseAssujetti() {
        return adresseAssujetti;
    }

    public void setAdresseAssujetti(String adresseAssujetti) {
        this.adresseAssujetti = adresseAssujetti;
    }

    public String getTypeNoteTaxation() {
        return typeNoteTaxation;
    }

    public void setTypeNoteTaxation(String typeNoteTaxation) {
        this.typeNoteTaxation = typeNoteTaxation;
    }

    public String getPeriodeDeclaration() {
        return periodeDeclaration;
    }

    public void setPeriodeDeclaration(String periodeDeclaration) {
        this.periodeDeclaration = periodeDeclaration;
    }

    public String getImpot() {
        return impot;
    }

    public void setImpot(String impot) {
        this.impot = impot;
    }

    public String getAdresseBien() {
        return adresseBien;
    }

    public void setAdresseBien(String adresseBien) {
        this.adresseBien = adresseBien;
    }

    public String getMontantChiffre() {
        return montantChiffre;
    }

    public void setMontantChiffre(String montantChiffre) {
        this.montantChiffre = montantChiffre;
    }

    public String getMontantLettre() {
        return montantLettre;
    }

    public void setMontantLettre(String montantLettre) {
        this.montantLettre = montantLettre;
    }

    public String getNumeroDeclaration() {
        return numeroDeclaration;
    }

    public void setNumeroDeclaration(String numeroDeclaration) {
        this.numeroDeclaration = numeroDeclaration;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public String getDateTaxation() {
        return dateTaxation;
    }

    public void setDateTaxation(String dateTaxation) {
        this.dateTaxation = dateTaxation;
    }

    public String getLieuTaxation() {
        return lieuTaxation;
    }

    public void setLieuTaxation(String lieuTaxation) {
        this.lieuTaxation = lieuTaxation;
    }

    public String getNomFonctionTaxateur() {
        return nomFonctionTaxateur;
    }

    public void setNomFonctionTaxateur(String nomFonctionTaxateur) {
        this.nomFonctionTaxateur = nomFonctionTaxateur;
    }

    public String getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(String dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public String getEntiteEmetrice() {
        return entiteEmetrice;
    }

    public void setEntiteEmetrice(String entiteEmetrice) {
        this.entiteEmetrice = entiteEmetrice;
    }
    
    
    
}
