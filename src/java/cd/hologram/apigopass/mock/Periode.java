/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.math.BigDecimal;

/**
 *
 * @author moussa.toure
 */
public class Periode {

    private String code;
    private int id;
    private String title;
    private BigDecimal amount;
    private String devise;
    private BigDecimal taux;
    private String dateCreate;

    public Periode(String code, int id, String title, BigDecimal amount, String devise) {
        this.code = code;
        this.id = id;
        this.title = title;
        this.amount = amount;
        this.devise = devise;
    }
    
    public Periode(String code, int id, String title, BigDecimal amount, String devise, BigDecimal taux) {
        this.code = code;
        this.id = id;
        this.title = title;
        this.amount = amount;
        this.devise = devise;
        this.taux = taux;
    }
    
    public Periode(String code, int id, String title, BigDecimal amount, String devise, BigDecimal taux, String dateCreate) {
        this.code = code;
        this.id = id;
        this.title = title;
        this.amount = amount;
        this.devise = devise;
        this.taux = taux;
        this.dateCreate = dateCreate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String Devise) {
        this.devise = Devise;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }
}
