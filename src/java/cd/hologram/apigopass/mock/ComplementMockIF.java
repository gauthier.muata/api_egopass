/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

/**
 *
 * @author moussa.toure
 */
public class ComplementMockIF {
    
    String typeBien;
    String typeComplementBien;

    public ComplementMockIF() {
    }

    public ComplementMockIF(String typeBien, String typeComplementBien) {
        this.typeBien = typeBien;
        this.typeComplementBien = typeComplementBien;
    }

    public String getTypeBien() {
        return typeBien;
    }

    public void setTypeBien(String typeBien) {
        this.typeBien = typeBien;
    }

    public String getTypeComplementBien() {
        return typeComplementBien;
    }

    public void setTypeComplementBien(String typeComplementBien) {
        this.typeComplementBien = typeComplementBien;
    }
    
}
