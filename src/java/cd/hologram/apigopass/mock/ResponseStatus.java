/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class ResponseStatus {

    private String code;
    private String message;
    List<MockTypePersonne> personTypeList;
    List<MockTypeBien> propertyTypeList;
    List<MockPersonne> personList;
    List<MockPersonne> personObject;
    List<Transaction> transactionObject;
    List<MockTarif> rateObject;
    List<Periode> data;
    PersonAssuj personAssuj;
    List<MockEntiteAdministrative> administrativeEntityList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MockTypePersonne> getPersonTypeList() {
        return personTypeList;
    }

    public void setPersonTypeList(List<MockTypePersonne> personTypeList) {
        this.personTypeList = personTypeList;
    }

    public List<MockTypeBien> getPropertyTypeList() {
        return propertyTypeList;
    }

    public void setPropertyTypeList(List<MockTypeBien> propertyTypeList) {
        this.propertyTypeList = propertyTypeList;
    }

    public List<MockPersonne> getPersonList() {
        return personList;
    }

    public void setPersonList(List<MockPersonne> personList) {
        this.personList = personList;
    }

    public List<MockPersonne> getPersonObject() {
        return personObject;
    }

    public void setPersonObject(List<MockPersonne> personObject) {
        this.personObject = personObject;
    }

    public List<Periode> getData() {
        return data;
    }

    public void setData(List<Periode> data) {
        this.data = data;
    }

    public PersonAssuj getPersonAssuj() {
        return personAssuj;
    }

    public void setPersonAssuj(PersonAssuj personAssuj) {
        this.personAssuj = personAssuj;
    }

    public List<Transaction> getTransactionObject() {
        return transactionObject;
    }

    public void setTransactionObject(List<Transaction> transactionObject) {
        this.transactionObject = transactionObject;
    }

    public List<MockTarif> getRateObject() {
        return rateObject;
    }

    public void setRateObject(List<MockTarif> rateObject) {
        this.rateObject = rateObject;
    }

    public List<MockEntiteAdministrative> getAdministrativeEntityList() {
        return administrativeEntityList;
    }

    public void setAdministrativeEntityList(List<MockEntiteAdministrative> administrativeEntityList) {
        this.administrativeEntityList = administrativeEntityList;
    }
}
