/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.io.Serializable;

/**
 *
 * @author Juslin TSHIAMUA
 */
public class VoucherMock implements Serializable {

    String fk_personne;
    String fkAb;
    String fkTarif;
    String type;
    int quantite;
    double prix_unitaire;
    double montant;
    double montantInitial;
    String devise;
    int agent_create;
    int etat;
    String reference;
    String compteBancaire;
    String contribuable;
    String nif;
    String adresse;
    String montantLettre;
    String montantChiffre;
    String banque;
    String codeQR;
    String dateNote;
    String flag;
    double remise;
    int numeroNote;
    int agentCreate;

    public String getFk_personne() {
        return fk_personne;
    }

    public void setFk_personne(String fk_personne) {
        this.fk_personne = fk_personne;
    }

    public String getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(String fkTarif) {
        this.fkTarif = fkTarif;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAgent_create() {
        return agent_create;
    }

    public void setAgent_create(int agent_create) {
        this.agent_create = agent_create;
    }

    public String getFkAb() {
        return fkAb;
    }

    public void setFkAb(String fkAb) {
        this.fkAb = fkAb;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public double getPrix_unitaire() {
        return prix_unitaire;
    }

    public void setPrix_unitaire(double prix_unitaire) {
        this.prix_unitaire = prix_unitaire;
    }

    public double getMontant() {
        return montant;
    }

    public double getMontantInitial() {
        return montantInitial;
    }

    public void setMontantInitial(double montantInitial) {
        this.montantInitial = montantInitial;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public String getContribuable() {
        return contribuable;
    }

    public void setContribuable(String contribuable) {
        this.contribuable = contribuable;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getMontantLettre() {
        return montantLettre;
    }

    public void setMontantLettre(String montantLettre) {
        this.montantLettre = montantLettre;
    }

    public String getMontantChiffre() {
        return montantChiffre;
    }

    public void setMontantChiffre(String montantChiffre) {
        this.montantChiffre = montantChiffre;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }

    public String getCodeQR() {
        return codeQR;
    }

    public void setCodeQR(String codeQR) {
        this.codeQR = codeQR;
    }

    public String getDateNote() {
        return dateNote;
    }

    public void setDateNote(String dateNote) {
        this.dateNote = dateNote;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public int getNumeroNote() {
        return numeroNote;
    }

    public void setNumeroNote(int numeroNote) {
        this.numeroNote = numeroNote;
    }

    public int getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(int agentCreate) {
        this.agentCreate = agentCreate;
    }

}
