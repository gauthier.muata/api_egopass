/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class PersonCreate {

    private String code;
    private String name;
    private String lastname;
    private String firstname;
    private String nif;
    private String type;
    private MockAdresse address;
    private ComplementsPersonnePhysique naturalPersonComplements;
    private ComplementsPersonneMorale legalPersonComplements;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MockAdresse getAddress() {
        return address;
    }

    public void setAddress(MockAdresse address) {
        this.address = address;
    }

    public ComplementsPersonnePhysique getNaturalPersonComplements() {
        return naturalPersonComplements;
    }

    public void setNaturalPersonComplements(ComplementsPersonnePhysique naturalPersonComplements) {
        this.naturalPersonComplements = naturalPersonComplements;
    }

    public ComplementsPersonneMorale getLegalPersonComplements() {
        return legalPersonComplements;
    }

    public void setLegalPersonComplements(ComplementsPersonneMorale legalPersonComplements) {
        this.legalPersonComplements = legalPersonComplements;
    }
}
