/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author WILLY
 */
public class MockRetraitDeclaration {
    
    int id;
    String requerant;
    String fkAssujetti;
    String codeDeclaration;
    String fkArticleBudgetaire;
    String fkPeriode;
    String fkAgent;
    Date dateCreate;
    BigDecimal valeurBase = new BigDecimal("0");
    BigDecimal amount = new BigDecimal("0");
    String devise;
    int etat;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRequerant() {
        return requerant;
    }

    public void setRequerant(String requerant) {
        this.requerant = requerant;
    }

    public String getFkAssujetti() {
        return fkAssujetti;
    }

    public void setFkAssujetti(String fkAssujetti) {
        this.fkAssujetti = fkAssujetti;
    }

    public String getCodeDeclaration() {
        return codeDeclaration;
    }

    public void setCodeDeclaration(String codeDeclaration) {
        this.codeDeclaration = codeDeclaration;
    }

    public String getFkArticleBudgetaire() {
        return fkArticleBudgetaire;
    }

    public void setFkArticleBudgetaire(String fkArticleBudgetaire) {
        this.fkArticleBudgetaire = fkArticleBudgetaire;
    }

    public String getFkPeriode() {
        return fkPeriode;
    }

    public void setFkPeriode(String fkPeriode) {
        this.fkPeriode = fkPeriode;
    }

    public String getFkAgent() {
        return fkAgent;
    }

    public void setFkAgent(String fkAgent) {
        this.fkAgent = fkAgent;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public BigDecimal getValeurBase() {
        return valeurBase;
    }

    public void setValeurBase(BigDecimal valeurBase) {
        this.valeurBase = valeurBase;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
    
    
}
