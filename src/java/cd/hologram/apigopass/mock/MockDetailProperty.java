/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class MockDetailProperty {
    String locationId;
    List<MockPersonneLocation> properiesList;

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public List<MockPersonneLocation> getProperiesList() {
        return properiesList;
    }

    public void setProperiesList(List<MockPersonneLocation> properiesList) {
        this.properiesList = properiesList;
    }

    public MockDetailProperty() {
    }
    
    
    
}
