/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.util.List;

/**
 *
 * @author emmanuel.tsasa
 */
public class PersonAssuj {

    String personCode;
    MockComplement complement;
    List<Periode> periodeList;

    public String getPersonCode() {
        return personCode;
    }

    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    public MockComplement getComplement() {
        return complement;
    }

    public void setComplement(MockComplement complement) {
        this.complement = complement;
    }

    public List<Periode> getPeriodeList() {
        return periodeList;
    }

    public void setPeriodeList(List<Periode> periodeList) {
        this.periodeList = periodeList;
    }
}
