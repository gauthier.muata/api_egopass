/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Success {
    
    public String code;
    public String message;
    
    //
    private List<MockPersonne> personList;
    private List<MockArticleBudgetaire> taxList;
    private List<MockEntiteAdministrative> cityList;
    //

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MockPersonne> getPersonne() {
        return personList;
    }

    public void setPersonne(List<MockPersonne> personneList) {
        this.personList = personneList;
    }

    public List<MockArticleBudgetaire> getTaxList() {
        return taxList;
    }

    public void setTaxList(List<MockArticleBudgetaire> taxList) {
        this.taxList = taxList;
    }

    public List<MockEntiteAdministrative> getCityList() {
        return cityList;
    }

    public void setCityList(List<MockEntiteAdministrative> cityList) {
        this.cityList = cityList;
    }
    
}
