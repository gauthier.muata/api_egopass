/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author PC
 */
public class PeriodeDeclarationSelection implements Serializable{
    
    int periode;
    int penalise;
    float principal;
    BigDecimal penalite;

    public int getPeriode() {
        return periode;
    }

    public void setPeriode(int periode) {
        this.periode = periode;
    }

    public int getPenalise() {
        return penalise;
    }

    public void setPenalise(int penalise) {
        this.penalise = penalise;
    }

    public float getPrincipal() {
        return principal;
    }

    public void setPrincipal(float principal) {
        this.principal = principal;
    }

    public BigDecimal getPenalite() {
        return penalite;
    }

    public void setPenalite(BigDecimal penalite) {
        this.penalite = penalite;
    }
    
    
}
