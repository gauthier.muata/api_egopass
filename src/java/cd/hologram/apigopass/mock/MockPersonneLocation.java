/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class MockPersonneLocation {

    MockPersonne personObject;
    String address;
    String period;
    String builtArea;
    String openSpace;
    String numberOfAppartment;
    String numberOfStage;
    String amountToPay;
    String amountPayed;
    String rate;
    List<MockPersonne> person;

    public MockPersonne getPersonObject() {
        return personObject;
    }

    public void setPersonObject(MockPersonne personObject) {
        this.personObject = personObject;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getBuiltArea() {
        return builtArea;
    }

    public void setBuiltArea(String builtArea) {
        this.builtArea = builtArea;
    }

    public String getOpenSpace() {
        return openSpace;
    }

    public void setOpenSpace(String openSpace) {
        this.openSpace = openSpace;
    }

    public String getNumberOfAppartment() {
        return numberOfAppartment;
    }

    public void setNumberOfAppartment(String numberOfAppartment) {
        this.numberOfAppartment = numberOfAppartment;
    }

    public String getNumberOfStage() {
        return numberOfStage;
    }

    public void setNumberOfStage(String numberOfStage) {
        this.numberOfStage = numberOfStage;
    }

    public String getAmountToPay() {
        return amountToPay;
    }

    public void setAmountToPay(String amountToPay) {
        this.amountToPay = amountToPay;
    }

    public String getAmountPayed() {
        return amountPayed;
    }

    public void setAmountPayed(String amountPayed) {
        this.amountPayed = amountPayed;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public List<MockPersonne> getPerson() {
        return person;
    }

    public void setPerson(List<MockPersonne> person) {
        this.person = person;
    }

}
