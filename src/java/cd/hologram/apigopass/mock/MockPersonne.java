/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class MockPersonne {

    String code;
    String name;
    String lastname;
    String firstname;
    String nif;
    String address;
    String phoneNumber;
    String mailAddress;
    String personType;
    Boolean isOwner;

    private List<MockBien> propertiesList;
    private List<MockTypePersonne> personTypeList;
    private List<BienCreate> propertyList;
    private List<Success> status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return name;
    }

    public void setNom(String name) {
        this.name = name;
    }

    public String getPostnom() {
        return lastname;
    }

    public void setPostnom(String lastname) {
        this.lastname = lastname;
    }

    public String getPrenom() {
        return firstname;
    }

    public void setPrenom(String firstname) {
        this.firstname = firstname;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getAdresse() {
        return address;
    }

    public void setAdresse(String address) {
        this.address = address;
    }

    public List<MockBien> getBienList() {
        return propertiesList;
    }

    public void setBienList(List<MockBien> propertiesList) {
        this.propertiesList = propertiesList;
    }

    public Boolean getIsOwner() {
        return isOwner;
    }

    public void setIsOwner(Boolean isOwner) {
        this.isOwner = isOwner;
    }

    public List<Success> getStatusList() {
        return status;
    }

    public void setStatusList(List<Success> status) {
        this.status = status;
    }

    public List<MockTypePersonne> getPersonTypeList() {
        return personTypeList;
    }

    public void setPersonTypeList(List<MockTypePersonne> personTypeList) {
        this.personTypeList = personTypeList;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public List<BienCreate> getPropertyList() {
        return propertyList;
    }

    public void setPropertyList(List<BienCreate> propertyList) {
        this.propertyList = propertyList;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }
}
