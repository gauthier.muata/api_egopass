/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class MockBien {

    String code;
    String propertyName;
    String description;
    String longitude;
    String lattitude;
    String built_area;
    String open_space;
    String rented_amount;
    String annual_amount;
    String propertyAdress;
    String hasPayed;
    
    private List<Transaction> transactionStatus;
    
    private List<MockNotePerception> transactionsList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return propertyName;
    }

    public void setIntitule(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public String getSuperficie_batie() {
        return built_area;
    }

    public void setSuperficie_batie(String built_area) {
        this.built_area = built_area;
    }

    public String getSuperficie_non_batie() {
        return open_space;
    }

    public void setSuperficie_non_batie(String open_space) {
        this.open_space = open_space;
    }

    public String getMontant_loye() {
        return rented_amount;
    }

    public void setMontant_loye(String rented_amount) {
        this.rented_amount = rented_amount;
    }

    public String getMontant_annuel() {
        return annual_amount;
    }

    public void setMontant_annuel(String annual_amount) {
        this.annual_amount = annual_amount;
    }

    public String getAdresseBien() {
        return propertyAdress;
    }

    public void setAdresseBien(String propertyAdress) {
        this.propertyAdress = propertyAdress;
    }

    public List<MockNotePerception> getTransactionsList() {
        return transactionsList;
    }

    public void setTransactionsList(List<MockNotePerception> transactionsList) {
        this.transactionsList = transactionsList;
    }

    public List<Transaction> getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(List<Transaction> transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getStatus() {
        return hasPayed;
    }

    public void setStatus(String hasPayed) {
        this.hasPayed = hasPayed;
    }
    
    
}
