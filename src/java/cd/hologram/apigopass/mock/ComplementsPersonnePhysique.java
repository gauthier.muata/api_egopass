/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

/**
 *
 * @author juslin.tshiamua
 */
public class ComplementsPersonnePhysique {

    private String CF000000000000052015_birthDay;
    private String CF000000000000062015_sex;
    private String CF000000000000142015_postalAddress;
    private String CF000000000000152015_email;
    private String CF000000000000162015_taxNumber;
    private String CF000000000000172015_phoneNumber;

    public String getCF000000000000052015_birthDay() {
        return CF000000000000052015_birthDay;
    }

    public void setCF000000000000052015_birthDay(String CF000000000000052015_birthDay) {
        this.CF000000000000052015_birthDay = CF000000000000052015_birthDay;
    }

    public String getCF000000000000062015_sex() {
        return CF000000000000062015_sex;
    }

    public void setCF000000000000062015_sex(String CF000000000000062015_sex) {
        this.CF000000000000062015_sex = CF000000000000062015_sex;
    }

    public String getCF000000000000142015_postalAddress() {
        return CF000000000000142015_postalAddress;
    }

    public void setCF000000000000142015_postalAddress(String CF000000000000142015_postalAddress) {
        this.CF000000000000142015_postalAddress = CF000000000000142015_postalAddress;
    }

    public String getCF000000000000152015_email() {
        return CF000000000000152015_email;
    }

    public void setCF000000000000152015_email(String CF000000000000152015_email) {
        this.CF000000000000152015_email = CF000000000000152015_email;
    }

    public String getCF000000000000162015_taxNumber() {
        return CF000000000000162015_taxNumber;
    }

    public void setCF000000000000162015_taxNumber(String CF000000000000162015_taxNumber) {
        this.CF000000000000162015_taxNumber = CF000000000000162015_taxNumber;
    }

    public String getCF000000000000172015_phoneNumber() {
        return CF000000000000172015_phoneNumber;
    }

    public void setCF000000000000172015_phoneNumber(String CF000000000000172015_phoneNumber) {
        this.CF000000000000172015_phoneNumber = CF000000000000172015_phoneNumber;
    }
    
}
