/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import cd.hologram.apigopass.entities.NotePerception;
import java.io.Serializable;

/**
 *
 * @author moussa.toure
 */
public class NotePerceptionPrint implements Serializable {

    String numeroNotePerception;
    String nomContribuable;
    String numeroImpotContribuable;
    String idNationaleContribuable;
    String avenueRueContribuable;
    String communeContribuable;
    String boitePostale;
    String phoneContribuable;
    String serviceAssiete;
    String acteGenerateur;
    String numeroNoteCalcul;
    String referenceOfficiel;
    String montantEnChiffre;
    String montantEnLettre;
    String dateTaxation;
    String nomTaxateur;
    String articlesBudgetaires;
    String avisOrdonnateur;
    String compteBancaire;
    String banque;
    String dateOrdonnancement;
    String dateEcheance;
    String nomOrdonnanceur;
    String urlTrackingDoc;
    String codeCentreOrdonnancement;
    String nomReceptionniste;
    String dateReception;
    String codeQR;
    String nifContribuable;
    String adresseContribuable;
    String logo;
    String qte;
    String villePrint;

    ActeGenerateurPrint acteGenerateurPrint;

    NotePerception NpMere;

    public NotePerception getNpMere() {
        return NpMere;
    }

    public void setNpMere(NotePerception NpMere) {
        this.NpMere = NpMere;
    }

    public String getAdresseContribuable() {
        return adresseContribuable;
    }

    public void setAdresseContribuable(String adresseContribuable) {
        this.adresseContribuable = adresseContribuable;
    }

    public String getNifContribuable() {
        return nifContribuable;
    }

    public void setNifContribuable(String nifContribuable) {
        this.nifContribuable = nifContribuable;
    }

    public String getNumeroNotePerception() {
        return numeroNotePerception;
    }

    public void setNumeroNotePerception(String numeroNotePerception) {
        this.numeroNotePerception = numeroNotePerception;
    }

    public String getNomContribuable() {
        return nomContribuable;
    }

    public void setNomContribuable(String nomContribuable) {
        this.nomContribuable = nomContribuable;
    }

    public String getNumeroImpotContribuable() {
        return numeroImpotContribuable;
    }

    public void setNumeroImpotContribuable(String numeroImpotContribuable) {
        this.numeroImpotContribuable = numeroImpotContribuable;
    }

    public String getIdNationaleContribuable() {
        return idNationaleContribuable;
    }

    public void setIdNationaleContribuable(String idNationaleContribuable) {
        this.idNationaleContribuable = idNationaleContribuable;
    }

    public String getAvenueRueContribuable() {
        return avenueRueContribuable;
    }

    public void setAvenueRueContribuable(String avenueRueContribuable) {
        this.avenueRueContribuable = avenueRueContribuable;
    }

    public String getCommuneContribuable() {
        return communeContribuable;
    }

    public void setCommuneContribuable(String communeContribuable) {
        this.communeContribuable = communeContribuable;
    }

    public String getBoitePostale() {
        return boitePostale;
    }

    public void setBoitePostale(String boitePostale) {
        this.boitePostale = boitePostale;
    }

    public String getPhoneContribuable() {
        return phoneContribuable;
    }

    public void setPhoneContribuable(String phoneContribuable) {
        this.phoneContribuable = phoneContribuable;
    }

    public String getServiceAssiete() {
        return serviceAssiete;
    }

    public void setServiceAssiete(String serviceAssiete) {
        this.serviceAssiete = serviceAssiete;
    }

    public String getActeGenerateur() {
        return acteGenerateur;
    }

    public void setActeGenerateur(String acteGenerateur) {
        this.acteGenerateur = acteGenerateur;
    }

    public String getNumeroNoteCalcul() {
        return numeroNoteCalcul;
    }

    public void setNumeroNoteCalcul(String numeroNoteCalcul) {
        this.numeroNoteCalcul = numeroNoteCalcul;
    }

    public String getReferenceOfficiel() {
        return referenceOfficiel;
    }

    public void setReferenceOfficiel(String referenceOfficiel) {
        this.referenceOfficiel = referenceOfficiel;
    }

    public String getMontantEnChiffre() {
        return montantEnChiffre;
    }

    public void setMontantEnChiffre(String montantEnChiffre) {
        this.montantEnChiffre = montantEnChiffre;
    }

    public String getMontantEnLettre() {
        return montantEnLettre;
    }

    public void setMontantEnLettre(String montantEnLettre) {
        this.montantEnLettre = montantEnLettre;
    }

    public String getDateTaxation() {
        return dateTaxation;
    }

    public void setDateTaxation(String dateTaxation) {
        this.dateTaxation = dateTaxation;
    }

    public String getNomTaxateur() {
        return nomTaxateur;
    }

    public void setNomTaxateur(String nomTaxateur) {
        this.nomTaxateur = nomTaxateur;
    }

    public String getArticlesBudgetaires() {
        return articlesBudgetaires;
    }

    public void setArticlesBudgetaires(String articlesBudgetaires) {
        this.articlesBudgetaires = articlesBudgetaires;
    }

    public String getAvisOrdonnateur() {
        return avisOrdonnateur;
    }

    public void setAvisOrdonnateur(String avisOrdonnateur) {
        this.avisOrdonnateur = avisOrdonnateur;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }

    public String getDateOrdonnancement() {
        return dateOrdonnancement;
    }

    public void setDateOrdonnancement(String dateOrdonnancement) {
        this.dateOrdonnancement = dateOrdonnancement;
    }

    public String getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(String dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public String getNomOrdonnanceur() {
        return nomOrdonnanceur;
    }

    public void setNomOrdonnanceur(String nomOrdonnanceur) {
        this.nomOrdonnanceur = nomOrdonnanceur;
    }

    public String getUrlTrackingDoc() {
        return urlTrackingDoc;
    }

    public void setUrlTrackingDoc(String urlTrackingDoc) {
        this.urlTrackingDoc = urlTrackingDoc;
    }

    public String getCodeCentreOrdonnancement() {
        return codeCentreOrdonnancement;
    }

    public void setCodeCentreOrdonnancement(String codeCentreOrdonnancement) {
        this.codeCentreOrdonnancement = codeCentreOrdonnancement;
    }

    public String getNomReceptionniste() {
        return nomReceptionniste;
    }

    public void setNomReceptionniste(String nomReceptionniste) {
        this.nomReceptionniste = nomReceptionniste;
    }

    public String getDateReception() {
        return dateReception;
    }

    public void setDateReception(String dateReception) {
        this.dateReception = dateReception;
    }

    public String getCodeQR() {
        return codeQR;
    }

    public void setCodeQR(String codeQR) {
        this.codeQR = codeQR;
    }

    public ActeGenerateurPrint getActeGenerateurPrint() {
        return acteGenerateurPrint;
    }

    public void setActeGenerateurPrint(ActeGenerateurPrint acteGenerateurPrint) {
        this.acteGenerateurPrint = acteGenerateurPrint;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getQte() {
        return qte;
    }

    public void setQte(String qte) {
        this.qte = qte;
    }

    public String getVillePrint() {
        return villePrint;
    }

    public void setVillePrint(String villePrint) {
        this.villePrint = villePrint;
    }
    
    
}
