/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

/**
 *
 * @author juslin.tshiamua
 */
public class MockAdresse {
    private String avenueCode;
    private String number;

    public String getAvenueCode() {
        return avenueCode;
    }

    public void setAvenueCode(String avenueCode) {
        this.avenueCode = avenueCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    
}
