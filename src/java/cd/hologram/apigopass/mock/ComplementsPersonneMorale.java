/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

/**
 *
 * @author juslin.tshiamua
 */
public class ComplementsPersonneMorale {
    
    private String CF000000000000022015_rccm;
    private String CF000000000000032015_creationDate;
    private String CF000000000000072015_taxNumber;
    private String CF000000000000182015_postalAddress;
    private String CF000000000000192015_email;
    private String CF000000000000202015_phoneNumber;
    private String CF000000000000212015_nrc;
    private String CF000000000000542016_POBox;
    private String CF000000000000552016_fax;

    public String getCF000000000000022015_rccm() {
        return CF000000000000022015_rccm;
    }

    public void setCF000000000000022015_rccm(String CF000000000000022015_rccm) {
        this.CF000000000000022015_rccm = CF000000000000022015_rccm;
    }

    public String getCF000000000000032015_creationDate() {
        return CF000000000000032015_creationDate;
    }

    public void setCF000000000000032015_creationDate(String CF000000000000032015_creationDate) {
        this.CF000000000000032015_creationDate = CF000000000000032015_creationDate;
    }

    public String getCF000000000000072015_taxNumber() {
        return CF000000000000072015_taxNumber;
    }

    public void setCF000000000000072015_taxNumber(String CF000000000000072015_taxNumber) {
        this.CF000000000000072015_taxNumber = CF000000000000072015_taxNumber;
    }

    public String getCF000000000000182015_postalAddress() {
        return CF000000000000182015_postalAddress;
    }

    public void setCF000000000000182015_postalAddress(String CF000000000000182015_postalAddress) {
        this.CF000000000000182015_postalAddress = CF000000000000182015_postalAddress;
    }

    public String getCF000000000000192015_email() {
        return CF000000000000192015_email;
    }

    public void setCF000000000000192015_email(String CF000000000000192015_email) {
        this.CF000000000000192015_email = CF000000000000192015_email;
    }

    public String getCF000000000000202015_phoneNumber() {
        return CF000000000000202015_phoneNumber;
    }

    public void setCF000000000000202015_phoneNumber(String CF000000000000202015_phoneNumber) {
        this.CF000000000000202015_phoneNumber = CF000000000000202015_phoneNumber;
    }

    public String getCF000000000000212015_nrc() {
        return CF000000000000212015_nrc;
    }

    public void setCF000000000000212015_nrc(String CF000000000000212015_nrc) {
        this.CF000000000000212015_nrc = CF000000000000212015_nrc;
    }

    public String getCF000000000000542016_POBox() {
        return CF000000000000542016_POBox;
    }

    public void setCF000000000000542016_POBox(String CF000000000000542016_POBox) {
        this.CF000000000000542016_POBox = CF000000000000542016_POBox;
    }

    public String getCF000000000000552016_fax() {
        return CF000000000000552016_fax;
    }

    public void setCF000000000000552016_fax(String CF000000000000552016_fax) {
        this.CF000000000000552016_fax = CF000000000000552016_fax;
    }
    
}
