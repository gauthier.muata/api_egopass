/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class MockTypePersonne {

    private String code;
    private String title;
    private String complementCode;
    private String complementName;
    private List<MockTypePersonne> complementlist;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComplementCode() {
        return complementCode;
    }

    public void setComplementCode(String complementCode) {
        this.complementCode = complementCode;
    }

    public String getComplementName() {
        return complementName;
    }

    public void setComplementName(String complementName) {
        this.complementName = complementName;
    }

    public List<MockTypePersonne> getComplementlist() {
        return complementlist;
    }

    public void setComplementlist(List<MockTypePersonne> complementlist) {
        this.complementlist = complementlist;
    }

}
