/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class BienCreate {

    String id;
    String name;
    String description;
    String dateAcquisition;
    String personCode;
    private MockAdresse address;
    private ComplementsBienCreate propertyComplements;
    private List<ComplementsBienCreate> propertyComplement;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MockAdresse getAddress() {
        return address;
    }

    public void setAddress(MockAdresse address) {
        this.address = address;
    }

    public ComplementsBienCreate getPropertyComplements() {
        return propertyComplements;
    }

    public void setPropertyComplements(ComplementsBienCreate propertyComplements) {
        this.propertyComplements = propertyComplements;
    }

    public String getDateAcquisition() {
        return dateAcquisition;
    }

    public void setDateAcquisition(String dateAcquisition) {
        this.dateAcquisition = dateAcquisition;
    }

    public String getPersonCode() {
        return personCode;
    }

    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    public List<ComplementsBienCreate> getPropertyComplement() {
        return propertyComplement;
    }

    public void setPropertyComplement(List<ComplementsBienCreate> propertyComplement) {
        this.propertyComplement = propertyComplement;
    }
}
