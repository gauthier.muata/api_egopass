/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.math.BigDecimal;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */

@XmlRootElement
public class Declaration {

    private String numero;
    private String requerant;
    private String telephone;
    private String adresseRequerant;
    private String dateRetrait;
    private String dateEcheance;
    private String periode;
    private String articleBudgetaire;
    private String articleGenerique;
    private BigDecimal montantDu;
    private BigDecimal montantPaye;
    private BigDecimal solde;
    private String devise;
    private List<DetailDeclaration> details;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getRequerant() {
        return requerant;
    }

    public void setRequerant(String requerant) {
        this.requerant = requerant;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDateRetrait() {
        return dateRetrait;
    }

    public void setDateRetrait(String dateRetrait) {
        this.dateRetrait = dateRetrait;
    }

    public String getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(String dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(String articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public String getArticleGenerique() {
        return articleGenerique;
    }

    public void setArticleGenerique(String articleGenerique) {
        this.articleGenerique = articleGenerique;
    }

    public BigDecimal getMontantDu() {
        return montantDu;
    }

    public void setMontantDu(BigDecimal montantDu) {
        this.montantDu = montantDu;
    }

    public BigDecimal getMontantPaye() {
        return montantPaye;
    }

    public void setMontantPaye(BigDecimal montantPaye) {
        this.montantPaye = montantPaye;
    }

    public BigDecimal getMontantSolde() {
        return solde;
    }

    public void setMontantSolde(BigDecimal montantSolde) {
        this.solde = montantSolde;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public BigDecimal getSolde() {
        return solde;
    }

    public void setSolde(BigDecimal solde) {
        this.solde = solde;
    }

    public List<DetailDeclaration> getDetails() {
        return details;
    }

    public void setDetails(List<DetailDeclaration> details) {
        this.details = details;
    }

    public String getAdresseRequerant() {
        return adresseRequerant;
    }

    public void setAdresseRequerant(String adresseRequerant) {
        this.adresseRequerant = adresseRequerant;
    }
    
    
}
