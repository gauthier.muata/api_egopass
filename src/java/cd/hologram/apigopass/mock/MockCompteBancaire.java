/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

/**
 *
 * @author juslin.tshiamua
 */
public class MockCompteBancaire {

    private String banckAccount;
    private String banckAccountName;

    public String getBanckAccount() {
        return banckAccount;
    }

    public void setBanckAccount(String banckAccount) {
        this.banckAccount = banckAccount;
    }

    public String getBanckAccountName() {
        return banckAccountName;
    }

    public void setBanckAccountName(String banckAccountName) {
        this.banckAccountName = banckAccountName;
    }
}
