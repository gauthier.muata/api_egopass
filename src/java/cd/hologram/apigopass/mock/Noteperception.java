/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@XmlRootElement
public class Noteperception {

    private String numero;
    private String requerant;
    private String secteur;
    private String articleBudgetaire;
    private String dateOrdonnancement;
    private String dateEcheance;
    private String banqueSwift;
    private String banque;
    private String compteBancaire;
    private BigDecimal montantDu;
    private BigDecimal montantPaye;
    private BigDecimal solde;
    private String devise;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getRequerant() {
        return requerant;
    }

    public void setRequerant(String requerant) {
        this.requerant = requerant;
    }

    public String getSecteur() {
        return secteur;
    }

    public void setSecteur(String secteur) {
        this.secteur = secteur;
    }

    public String getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(String articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public String getDateOrdonnancement() {
        return dateOrdonnancement;
    }

    public void setDateOrdonnancement(String dateOrdonnancement) {
        this.dateOrdonnancement = dateOrdonnancement;
    }

    public String getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(String dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public BigDecimal getMontantDu() {
        return montantDu;
    }

    public void setMontantDu(BigDecimal montantDu) {
        this.montantDu = montantDu;
    }

    public BigDecimal getMontantPaye() {
        return montantPaye;
    }

    public void setMontantPaye(BigDecimal montantPaye) {
        this.montantPaye = montantPaye;
    }

    public BigDecimal getMontantSolde() {
        return solde;
    }

    public void setMontantSolde(BigDecimal montantSolde) {
        this.solde = montantSolde;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getBanqueSwift() {
        return banqueSwift;
    }

    public void setBanqueSwift(String banqueSwift) {
        this.banqueSwift = banqueSwift;
    }

    public BigDecimal getSolde() {
        return solde;
    }

    public void setSolde(BigDecimal solde) {
        this.solde = solde;
    }
    
    
}
