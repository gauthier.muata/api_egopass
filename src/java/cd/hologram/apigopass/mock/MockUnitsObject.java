/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class MockUnitsObject {
    private String code;
    private String message;
    List<MockUnit> unitOfMeasureList;
    List<MockDevise> currencyList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MockUnit> getUnitOfMeasureList() {
        return unitOfMeasureList;
    }

    public void setUnitOfMeasureList(List<MockUnit> unitOfMeasureList) {
        this.unitOfMeasureList = unitOfMeasureList;
    }

    public List<MockDevise> getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(List<MockDevise> currencyList) {
        this.currencyList = currencyList;
    }
    
}
