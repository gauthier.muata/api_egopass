/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

/**
 *
 * @author Juslin TSHIAMUA
 */
public class CommandeGoPassSpontanneeMock {

    String pieceIdentite,
            passager,
            sexePassager,
            typeGopassCode,
            categorieCode,
            agentCreat,
            agentUtilisation,
            devise,
            dateProd,
            aeroportProvenance,
            aeroportDestination,
            codeTicket,
            reference,
            compteBancaire,
            articleBudgetaire,
            typePaiement,
            motifAnnulation,
            token;
    Integer codeCompagnie,
            etat,
            quantite;
    double montant,
            prixUnitaire;

    public String getPieceIdentite() {
        return pieceIdentite;
    }

    public void setPieceIdentite(String pieceIdentite) {
        this.pieceIdentite = pieceIdentite;
    }

    public String getPassager() {
        return passager;
    }

    public void setPassager(String passager) {
        this.passager = passager;
    }

    public String getSexePassager() {
        return sexePassager;
    }

    public void setSexePassager(String sexePassager) {
        this.sexePassager = sexePassager;
    }

    public String getTypeGopassCode() {
        return typeGopassCode;
    }

    public void setTypeGopassCode(String typeGopassCode) {
        this.typeGopassCode = typeGopassCode;
    }

    public Integer getCodeCompagnie() {
        return codeCompagnie;
    }

    public void setCodeCompagnie(Integer codeCompagnie) {
        this.codeCompagnie = codeCompagnie;
    }

    public String getCategorieCode() {
        return categorieCode;
    }

    public void setCategorieCode(String categorieCode) {
        this.categorieCode = categorieCode;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getAgentUtilisation() {
        return agentUtilisation;
    }

    public void setAgentUtilisation(String agentUtilisation) {
        this.agentUtilisation = agentUtilisation;
    }

    public double getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public String getDateProd() {
        return dateProd;
    }

    public void setDateProd(String dateProd) {
        this.dateProd = dateProd;
    }

    public String getAeroportProvenance() {
        return aeroportProvenance;
    }

    public void setAeroportProvenance(String aeroportProvenance) {
        this.aeroportProvenance = aeroportProvenance;
    }

    public String getAeroportDestination() {
        return aeroportDestination;
    }

    public void setAeroportDestination(String aeroportDestination) {
        this.aeroportDestination = aeroportDestination;
    }

    public String getCodeTicket() {
        return codeTicket;
    }

    public void setCodeTicket(String codeTicket) {
        this.codeTicket = codeTicket;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public String getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(String articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public String getTypePaiement() {
        return typePaiement;
    }

    public void setTypePaiement(String typePaiement) {
        this.typePaiement = typePaiement;
    }

    public String getMotifAnnulation() {
        return motifAnnulation;
    }

    public void setMotifAnnulation(String motifAnnulation) {
        this.motifAnnulation = motifAnnulation;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
