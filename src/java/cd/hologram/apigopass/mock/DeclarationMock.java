/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author PC
 */
public class DeclarationMock implements Serializable {

    String periode;
    List<PeriodeDeclarationSelection> listPeriode;
    String icmParamId;
    BigDecimal amountPrincipal;
    BigDecimal amountPenalite;
    String devise;
    String requerant;
    String assujetti;
    String codeDeclaration;
    String codeArticleBudgetaire;
    String codeAgentCreate;
    Date dateCreate;
    int estPenalise;
    String banque;
    String compteBancaire;
    String codeAssujettissement;
    String codeSite;

    BigDecimal amountRemisePenalite;
    String observationRemise;
    int tauxRemise;
    String numeroDeclaration;

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public BigDecimal getAmountPrincipal() {
        return amountPrincipal;
    }

    public void setAmountPrincipal(BigDecimal amountPrincipal) {
        this.amountPrincipal = amountPrincipal;
    }

    public BigDecimal getAmountPenalite() {
        return amountPenalite;
    }

    public void setAmountPenalite(BigDecimal amountPenalite) {
        this.amountPenalite = amountPenalite;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getRequerant() {
        return requerant;
    }

    public void setRequerant(String requerant) {
        this.requerant = requerant;
    }

    public String getAssujetti() {
        return assujetti;
    }

    public void setAssujetti(String assujetti) {
        this.assujetti = assujetti;
    }

    public String getCodeDeclaration() {
        return codeDeclaration;
    }

    public void setCodeDeclaration(String codeDeclaration) {
        this.codeDeclaration = codeDeclaration;
    }

    public String getCodeArticleBudgetaire() {
        return codeArticleBudgetaire;
    }

    public void setCodeArticleBudgetaire(String codeArticleBudgetaire) {
        this.codeArticleBudgetaire = codeArticleBudgetaire;
    }

    public String getCodeAgentCreate() {
        return codeAgentCreate;
    }

    public void setCodeAgentCreate(String codeAgentCreate) {
        this.codeAgentCreate = codeAgentCreate;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public int getEstPenalise() {
        return estPenalise;
    }

    public void setEstPenalise(int estPenalise) {
        this.estPenalise = estPenalise;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public BigDecimal getAmountRemisePenalite() {
        return amountRemisePenalite;
    }

    public void setAmountRemisePenalite(BigDecimal amountRemisePenalite) {
        this.amountRemisePenalite = amountRemisePenalite;
    }

    public String getObservationRemise() {
        return observationRemise;
    }

    public void setObservationRemise(String observationRemise) {
        this.observationRemise = observationRemise;
    }

    public int getTauxRemise() {
        return tauxRemise;
    }

    public void setTauxRemise(int tauxRemise) {
        this.tauxRemise = tauxRemise;
    }

    public String getIcmParamId() {
        return icmParamId;
    }

    public void setIcmParamId(String icmParamId) {
        this.icmParamId = icmParamId;
    }

    public List<PeriodeDeclarationSelection> getListPeriode() {
        return listPeriode;
    }

    public void setListPeriode(List<PeriodeDeclarationSelection> listPeriode) {
        this.listPeriode = listPeriode;
    }

    public String getCodeAssujettissement() {
        return codeAssujettissement;
    }

    public void setCodeAssujettissement(String codeAssujettissement) {
        this.codeAssujettissement = codeAssujettissement;
    }

    public String getCodeSite() {
        return codeSite;
    }

    public void setCodeSite(String codeSite) {
        this.codeSite = codeSite;
    }

    public String getNumeroDeclaration() {
        return numeroDeclaration;
    }

    public void setNumeroDeclaration(String numeroDeclaration) {
        this.numeroDeclaration = numeroDeclaration;
    }
    
}
