/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import cd.hologram.apigopass.entities.CompteBancaire;
import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class MockArticleBudgetaire {

    String taxCode;
    String taxName;
    private List<MockCompteBancaire> banckAccountList;

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getTaxName() {
        return taxName;
    }

    public void setTaxName(String taxName) {
        this.taxName = taxName;
    }

    public List<MockCompteBancaire> getBanckAccountList() {
        return banckAccountList;
    }

    public void setBanckAccountList(List<MockCompteBancaire> banckAccountList) {
        this.banckAccountList = banckAccountList;
    }
}
