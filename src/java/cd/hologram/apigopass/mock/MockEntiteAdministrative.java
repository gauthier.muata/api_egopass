/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.mock;

import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class MockEntiteAdministrative {

    private String cityCode;
    private String cityName;
    private String districtCode;
    private String districtName;
    private String communeCode;
    private String communeName;
    private String quarterCode;
    private String quarterName;
    private String avenueCode;
    private String avenueName;
    private String numero;
    private String chaine;
    private String adresse;
    private List<MockEntiteAdministrative> districtList;
    private List<MockEntiteAdministrative> communeList;
    private List<MockEntiteAdministrative> quarterList;
    private List<MockEntiteAdministrative> avenueList;

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getCommuneCode() {
        return communeCode;
    }

    public void setCommuneCode(String communeCode) {
        this.communeCode = communeCode;
    }

    public String getCommuneName() {
        return communeName;
    }

    public void setCommuneName(String communeName) {
        this.communeName = communeName;
    }

    public String getQuarterCode() {
        return quarterCode;
    }

    public void setQuarterCode(String quarterCode) {
        this.quarterCode = quarterCode;
    }

    public String getQuarterName() {
        return quarterName;
    }

    public void setQuarterName(String quarterName) {
        this.quarterName = quarterName;
    }

    public String getAvenueCode() {
        return avenueCode;
    }

    public void setAvenueCode(String avenueCode) {
        this.avenueCode = avenueCode;
    }

    public String getAvenueName() {
        return avenueName;
    }

    public void setAvenueName(String avenueName) {
        this.avenueName = avenueName;
    }

    public List<MockEntiteAdministrative> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<MockEntiteAdministrative> districtList) {
        this.districtList = districtList;
    }

    public List<MockEntiteAdministrative> getCommuneList() {
        return communeList;
    }

    public void setCommuneList(List<MockEntiteAdministrative> communeList) {
        this.communeList = communeList;
    }

    public List<MockEntiteAdministrative> getQuarterList() {
        return quarterList;
    }

    public void setQuarterList(List<MockEntiteAdministrative> quarterList) {
        this.quarterList = quarterList;
    }

    public List<MockEntiteAdministrative> getAvenueList() {
        return avenueList;
    }

    public void setAvenueList(List<MockEntiteAdministrative> avenueList) {
        this.avenueList = avenueList;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getChaine() {
        return chaine;
    }

    public void setChaine(String chaine) {
        this.chaine = chaine;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

}
