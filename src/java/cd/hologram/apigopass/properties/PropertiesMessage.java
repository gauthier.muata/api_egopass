/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.properties;

import cd.hologram.apigopass.constants.PropertiesConst;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author moussa.toure
 */
public class PropertiesMessage {

    private final Properties propertiesMessage = new Properties();

    public PropertiesMessage() throws IOException {
        propertiesMessage.load(getClass().getResourceAsStream(PropertiesConst.Message.PROPERTIES_FILE_PATH));
    }
    
    public String getContent(String title) {
        return propertiesMessage.getProperty(title);
    }
}
