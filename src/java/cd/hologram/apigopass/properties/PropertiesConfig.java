/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.properties;

import cd.hologram.apigopass.constants.PropertiesConst;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author moussa.toure
 */
public class PropertiesConfig {

    private final Properties propertiesConfig = new Properties();

    public PropertiesConfig() throws IOException {
        propertiesConfig.load(getClass().getResourceAsStream(PropertiesConst.Config.PROPERTIES_FILE_PATH));
    }

    public String getContent(String title) {
        return propertiesConfig.getProperty(title);
    }
}
