/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.pojo;

import cd.hologram.apigopass.entities.ComplementBien;
import cd.hologram.apigopass.entities.Bien;
import cd.hologram.apigopass.business.DocumentBusiness;
import cd.hologram.apigopass.constants.PropertiesConst;
import cd.hologram.apigopass.mock.Periode;
import cd.hologram.apigopass.properties.PropertiesConfig;
import java.util.*;
import org.json.*;

/**
 *
 * @author emmanuel.tsasa
 */
public class TraitementBien {

    static PropertiesConfig propertiesConfig;

    /**
     * Creates a new instance of GenericResource
     *
     * @throws java.io.IOException
     */
    public static JSONObject getJSONComplement(String Id, Bien bien) {

        JSONArray keysArray;
        JSONObject result = new JSONObject();

        try {

            propertiesConfig = new PropertiesConfig();
            String configKeysComplements = propertiesConfig.getContent(PropertiesConst.Config.CONFIG_KEYS_COMPLEMENTS);

            keysArray = new JSONArray(configKeysComplements);

            JSONObject data = null;
            for (int i = 0; i < keysArray.length(); i++) {
                if (keysArray.getJSONObject(i).getString("Operation").equals(Id)) {
                    data = keysArray.getJSONObject(i);
                    break;
                }
            }

            if (data != null) {

                List<ComplementBien> complementBienList = bien.getComplementBienList();
                JSONArray dataComplementArray = new JSONArray(data.getString("Complements"));

                for (int i = 0; i < dataComplementArray.length(); i++) {
                    JSONObject dataComplement = dataComplementArray.getJSONObject(i);
                    if (complementBienList != null) {
                        for (ComplementBien cb : complementBienList) {
                            if (dataComplement.getString("code").equals(cb.getTypeComplement().getCode())) {
                                result.put(dataComplement.getString("key"), cb.getValeur());
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        return result;
    }

    public static List<JSONObject> getJSONPeriodes(List<Periode> periodeList) {

        List<JSONObject> results = new ArrayList<>();

        try {

            for (Periode periode : periodeList) {
                JSONObject dataPeriod = new JSONObject();
                dataPeriod.put("code", periode.getCode());
                dataPeriod.put("id", periode.getId());
                dataPeriod.put("title", periode.getTitle());
                dataPeriod.put("amount", periode.getAmount());
                dataPeriod.put("devise", periode.getDevise());
                results.add(dataPeriod);
            }
        } catch (Exception e) {
        }
        return results;
    }

    public static Periode getPeriod(String codeAB, String tarifCode, int valeur, String periodId, Date debut) {

        return DocumentBusiness.getPeriodeInformation(codeAB, tarifCode, valeur, periodId, debut);

    }

//    public static void init(){
//        
//        try {
//            propertiesConfig = new PropertiesConfig();
//        } catch (IOException ex) {
//            Logger.getLogger(TraitementBien.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//    }
}
