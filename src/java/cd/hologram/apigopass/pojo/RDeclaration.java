/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.pojo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanuel.tsasa
 */
@Entity
public class RDeclaration implements Serializable {
    
    @Id
    @Column(name = "CODE")
    private String code;
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ASSUJETISSEMENT")
    private String assujetissement;
    @Column(name = "DEBUT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date debut;
    @Column(name = "FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;
    @Column(name = "DATE_LIMITE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLimite;
    @Column(name = "NOTE_CALCUL")
    private String noteCalcul;
    @Column(name = "ETAT")
    private Short etat;
    @Column(name = "AMR")
    private String amr;
    @Column(name = "AGENT_CREAT")
    private String agentCreat;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAssujetissement() {
        return assujetissement;
    }

    public void setAssujetissement(String assujetissement) {
        this.assujetissement = assujetissement;
    }

    public Date getDebut() {
        return debut;
    }

    public void setDebut(Date debut) {
        this.debut = debut;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public Date getDateLimite() {
        return dateLimite;
    }

    public void setDateLimite(Date dateLimite) {
        this.dateLimite = dateLimite;
    }

    public String getNoteCalcul() {
        return noteCalcul;
    }

    public void setNoteCalcul(String noteCalcul) {
        this.noteCalcul = noteCalcul;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAmr() {
        return amr;
    }

    public void setAmr(String amr) {
        this.amr = amr;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    
}
