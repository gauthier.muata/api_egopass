/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.dao;

import cd.hologram.apigopass.business.DataAccess;
import cd.hologram.apigopass.business.DocumentBusiness;
import cd.hologram.apigopass.constants.GeneralConst;
import cd.hologram.apigopass.entities.Acquisition;
import cd.hologram.apigopass.entities.AdressePersonne;
import cd.hologram.apigopass.entities.ArticleBudgetaire;
import cd.hologram.apigopass.entities.Assujeti;
import cd.hologram.apigopass.entities.Bien;
import cd.hologram.apigopass.entities.ComplementBien;
import cd.hologram.apigopass.entities.EntiteAdministrative;
import cd.hologram.apigopass.entities.PeriodeDeclaration;
import cd.hologram.apigopass.entities.Personne;
import cd.hologram.apigopass.entities.RetraitDeclaration;
import cd.hologram.apigopass.mock.BienCreate;
import cd.hologram.apigopass.mock.ComplementsBienCreate;
import cd.hologram.apigopass.mock.ComplementsPersonneMorale;
import cd.hologram.apigopass.mock.ComplementsPersonnePhysique;
import cd.hologram.apigopass.mock.MockAdresse;
import cd.hologram.apigopass.mock.MockPersonCreate;
import cd.hologram.apigopass.mock.Periode;
import cd.hologram.apigopass.mock.PersonCreate;
import cd.hologram.apigopass.utils.CustumException;
import cd.hologram.apigopass.utils.Tools;
import static cd.hologram.apigopass.utils.Tools.getPrivateFields;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.*;
import javax.transaction.*;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author tony.lundja
 */
@Stateless(mappedName = "daoAPI_Gopass")
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class DaoImpl implements IDaoLocal, IDaoRemote {

    @PersistenceContext(unitName = "apigopassPU")
    private EntityManager em;

    private final static String INVALID_ARGUMENT_TEXT = "Argument invalid";

    @Resource
    private EJBContext contextEJB;

    UserTransaction userTransaction;

    private EntityManager getEntityManager() {

        return em;
    }

    private boolean verifyQuery(String query, Class<Object> entity) {
        return query == null || entity == null;
    }

    private boolean verifyLimit(Integer limit) {
        return limit == null || limit < 0;
    }

    private static void setParameters(Query query, boolean like, Object... params) {

        int i = 1;
        for (Object object : params) {
            if (object instanceof Date) {
                query.setParameter(i++, (Date) object, TemporalType.DATE);
            } else {
                if (like) {
                    query.setParameter(i++, "%" + object + "%");
                } else {
                    query.setParameter(i++, object);
                }
            }
        }
    }

    private void setStoreProcedureParameters(StoredProcedureQuery query, HashMap<String, Object> params) {

        Iterator<String> keys = params.keySet().iterator();

        while (keys.hasNext()) {

            String key = keys.next();
            query.registerStoredProcedureParameter(key, Object.class, ParameterMode.IN);
            if (params.get(key) instanceof Date) {
                query.setParameter(key, (Date) params.get(key), TemporalType.DATE);
            }
            query.setParameter(key, params.get(key));

        }

    }

    private boolean isBulkQueryValid(HashMap<String, Object[]> params) {

        boolean isValid = true;

        Iterator<String> keys = params.keySet().iterator();

        while (keys.hasNext()) {

            String key = keys.next();

            if (!key.contains(":")) {
                isValid = false;
            }
        }

        return isValid;

    }

    private String getOutPutFromBulkQuery(
            String storedProcedureName,
            String outputkey,
            HashMap<String, Object> storedProcedureParams,
            HashMap<String, Object[]> bulkQuery) {

        String valueRetour;

        try {

            valueRetour = getStoredProcedureOutput(storedProcedureName, outputkey, storedProcedureParams);

            if (!valueRetour.isEmpty()) {

                int paramSize = bulkQuery.size() + 1;

                for (int i = 0; i <= paramSize; i++) {

                    Iterator<String> keys = bulkQuery.keySet().iterator();

                    while (keys.hasNext()) {

                        String key = keys.next();
                        String[] splitKey = key.split(":");
                        int index = Integer.parseInt(splitKey[0]);
                        String req = splitKey[1];

                        if (index == i) {

                            if (req.contains("%s")) {

                                req = req.replaceAll("%s", valueRetour);
                            }

                            Query query = (Query) getEntityManager().createNativeQuery(req);
                            setParameters(query, false, bulkQuery.get(key));
                            query.executeUpdate();
                            break;
                        }

                    }

                }
            }
        } catch (Exception exeption) {
            throw exeption;
        }

        return valueRetour;

    }

    private void setBulkInsertion(HashMap<String, Object[]> params) {

        int paramSize = params.size() + 1;

        for (int i = 0; i <= paramSize; i++) {

            Iterator<String> keys = params.keySet().iterator();

            while (keys.hasNext()) {

                String key = keys.next();
                String[] splitKey = key.split(":");
                int index = Integer.parseInt(splitKey[0]);
                if (index == i) {
                    Query query = (Query) getEntityManager().createNativeQuery(splitKey[1]);
                    setParameters(query, false, params.get(key));
                    query.executeUpdate();
                    break;
                }

            }

        }

    }

    private void setBulkInsertion(
            String storedProcedureName,
            String outputkey,
            HashMap<String, Object> storedProcedureParams,
            HashMap<String, Object[]> bulkQuery) {

        String valueRetour = getStoredProcedureOutput(storedProcedureName,
                outputkey, storedProcedureParams);

        int paramSize = bulkQuery.size() + 1;

        for (int i = 0; i <= paramSize; i++) {

            Iterator<String> keys = bulkQuery.keySet().iterator();

            while (keys.hasNext()) {

                String key = keys.next();
                String[] splitKey = key.split(":");
                int index = Integer.parseInt(splitKey[0]);
                String req = splitKey[1];

                if (index == i) {

                    if (req.contains("%s")) {
                        req = req.replaceAll("%s", valueRetour.trim());

                    }

                    Query query = (Query) getEntityManager().createNativeQuery(req);
                    setParameters(query, false, bulkQuery.get(key));
                    query.executeUpdate();
                    break;
                }

            }

        }

    }

//    /**
//     *
//     */
    @Override
    public Object find(Class<Object> entity, Object id) {
        Object object = null;
        try {
            if (entity == null || id == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            object = getEntityManager().find(entity, id);
        } catch (Exception e) {
            CustumException.BasicLogException(e);
            throw e;
        }
        return object;
    }

//    /**
//     *
//     */
    @Override
    public Object find(String query, Class<Object> entity, boolean like, Object... params) {
        Object objectList = Collections.EMPTY_LIST;
        try {
            if (verifyQuery(query, entity) || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query nativeQuery = getEntityManager().createNativeQuery(query, entity);
            setParameters(nativeQuery, like, params);
            objectList = nativeQuery.getResultList();
        } catch (Exception e) {
            CustumException.BasicLogException(e);
            throw e;
        }
        return objectList;
    }

//    /**
//     *
//     */
    @Override
    public Object find(String query, Class<Object> entity, Integer limit, boolean like, Object... params) {
        Object objectList = Collections.EMPTY_LIST;
        try {
            if (verifyQuery(query, entity) || verifyLimit(limit) || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            Query nativeQuery = getEntityManager().createNativeQuery(query, entity);
            nativeQuery.setFirstResult(0);
            nativeQuery.setMaxResults(limit);
            setParameters(nativeQuery, like, params);
            objectList = nativeQuery.getResultList();
        } catch (Exception e) {
            CustumException.BasicLogException(e);
            throw e;
        }
        return objectList;
    }

//    /**
//     *
//     */
    @Override
    public Object findJPQL(String queryJPQL, boolean like, Object... params) {
        Object objectList = Collections.EMPTY_LIST;
        try {
            if (queryJPQL == null || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query query = getEntityManager().createQuery(queryJPQL);
            setParameters(query, like, params);
            objectList = query.getResultList();
        } catch (Exception e) {
            CustumException.BasicLogException(e);
            throw e;
        }
        return objectList;
    }

//    /**
//     *
//     */
    @Override
    public Object findJPQL(String queryJPQL, Integer limit, boolean like, Object... params) {
        Object objectList = Collections.EMPTY_LIST;
        try {
            if (queryJPQL == null || verifyLimit(limit) || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query query = getEntityManager().createQuery(queryJPQL);
            query.setFirstResult(0);
            query.setMaxResults(limit);
            setParameters(query, like, params);
            objectList = query.getResultList();
        } catch (Exception e) {
            CustumException.BasicLogException(e);
            throw e;
        }
        return objectList;
    }

//    /**
//     *
//     */
    @Override
    public Object findNamedJPQL(String namedQuery, boolean like, Object... params) {
        Object objectList = Collections.EMPTY_LIST;
        try {
            if (namedQuery == null || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query query = getEntityManager().createNamedQuery(namedQuery);
            setParameters(query, like, params);
            objectList = query.getResultList();
        } catch (Exception e) {
            CustumException.BasicLogException(e);
            throw e;
        }
        return objectList;
    }

//    /**
//     *
//     */
    @Override
    public Object findNamedJPQL(String namedQuery, Integer limit, boolean like, Object... params) {
        Object objectList = Collections.EMPTY_LIST;
        try {
            if (namedQuery == null || verifyLimit(limit) || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query query = getEntityManager().createNamedQuery(namedQuery);

            query.setFirstResult(0);
            query.setMaxResults(limit);

            setParameters(query, like, params);

            objectList = query.getResultList();

        } catch (Exception e) {

            CustumException.BasicLogException(e);
            throw e;

        }

        return objectList;

    }

//    /**
//     *
//     */
    @Override
    public Object find(String storedProcedureName, HashMap<String, Object> params) {

        Object object = null;

        try {

            if (storedProcedureName == null || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            StoredProcedureQuery storedProcedure = getEntityManager().createStoredProcedureQuery(storedProcedureName);

            setStoreProcedureParameters(storedProcedure, params);

            object = storedProcedure.getResultList();

        } catch (Exception e) {

            CustumException.BasicLogException(e);
            throw e;
        }

        return object;
    }

//    /**
//     *
//     */
    @Override
    public boolean executeNativeQuery(String nativeQuery, Object... params) {

        userTransaction = contextEJB.getUserTransaction();

        boolean result = false;

        try {

            if (nativeQuery == null || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            Query query = (Query) getEntityManager().createNativeQuery(nativeQuery);

            setParameters(query, false, params);

            result = query.executeUpdate() != 0;

            userTransaction.commit();

        } catch (Exception e) {

            try {
                userTransaction.setRollbackOnly();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            result = false;
            try {
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;

    }

//    /**
//     *
//     */
    @Override
    public String getStoredProcedureOutput(String storedProcedureName, String outputKey, HashMap<String, Object> storedProcedureParams) {

        String outPut = GeneralConst.EMPTY_STRING;

        try {

            StoredProcedureQuery storedProcedure = getEntityManager().createStoredProcedureQuery(storedProcedureName);

            storedProcedure.registerStoredProcedureParameter(outputKey, String.class, ParameterMode.OUT);

            setStoreProcedureParameters(storedProcedure, storedProcedureParams);

            storedProcedure.execute();

            outPut = String.valueOf(storedProcedure.getOutputParameterValue(outputKey));

        } catch (Exception e) {

            CustumException.BasicLogException(e);
            throw e;
        }

        return outPut;
    }

//    /**
//     *
//     */
    @Override
    public boolean executeStoredProcedure(String storedProcedureName, HashMap<String, Object> storedProcedureParams) {

        userTransaction = contextEJB.getUserTransaction();

        boolean result = false;

        try {

            if (storedProcedureName == null || storedProcedureParams == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            StoredProcedureQuery storedProcedure = getEntityManager().createStoredProcedureQuery(storedProcedureName);

            setStoreProcedureParameters(storedProcedure, storedProcedureParams);

            storedProcedure.execute();

            userTransaction.commit();

        } catch (Exception e) {

            try {
                userTransaction.setRollbackOnly();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            result = false;
            try {
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;
    }

//    /**
//     *
//     */
    @Override
    public boolean executeStoredProcedure(String storedProcedureName, Object... storedProcedureParams) {

        userTransaction = contextEJB.getUserTransaction();

        boolean result = false;

        try {

            if (storedProcedureName == null || storedProcedureParams == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            Query query = (Query) getEntityManager().createNativeQuery(storedProcedureName);

            setParameters(query, false, storedProcedureParams);

            result = query.executeUpdate() != 0;

            userTransaction.commit();

        } catch (Exception e) {

            try {
                userTransaction.setRollbackOnly();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            result = false;
            try {
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;
    }

//    /**
//     *
//     */
    @Override
    public boolean executeBulkQuery(HashMap<String, Object[]> params) {

        userTransaction = contextEJB.getUserTransaction();

        boolean result;

        try {

            if (!isBulkQueryValid(params)) {
                result = false;
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            setBulkInsertion(params);

            userTransaction.commit();

            result = true;

        } catch (Exception e) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            result = false;
            try {
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;
    }

//    /**
//     *
//     */
    @Override
    public boolean executeBulkQuery(String storedProcedureName, HashMap<String, Object> storedProcedureParams, String outputKey, HashMap<String, Object[]> bulkQuery) {

        userTransaction = contextEJB.getUserTransaction();

        boolean result;

        try {

            if (!isBulkQueryValid(bulkQuery)) {
                result = false;
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            setBulkInsertion(storedProcedureName, outputKey, storedProcedureParams, bulkQuery);

            userTransaction.commit();

            result = true;

        } catch (Exception e) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            result = false;
            try {
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;
    }

//    /**
//     *
//     */
    @Override
    public String executeBulkQueryGetOutput(String storedProcedureName, HashMap<String, Object> storedProcedureParams, String outputKey, HashMap<String, Object[]> bulkQuery) {

        userTransaction = contextEJB.getUserTransaction();

        String result;

        try {

            if (!isBulkQueryValid(bulkQuery)) {
                result = GeneralConst.EMPTY_STRING;
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            result = getOutPutFromBulkQuery(storedProcedureName, outputKey, storedProcedureParams, bulkQuery);

            userTransaction.commit();

        } catch (Exception e) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            result = GeneralConst.EMPTY_STRING;
            try {
                userTransaction.rollback();
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;
    }

//    /**
//     *
//     */
    @Override
    public String executeBulkQueryGetOutput(String firstStoredProcedureName,
            HashMap<String, Object> firstStoredProcedureParams,
            String secondStoredProcedureName,
            HashMap<String, Object> secondStoredProcedurParams,
            String firstStoredOutputKey,
            String secondStoredOutputKey,
            String secondParamsWaitingKey,
            HashMap<String, Object[]> firstBulkQuery,
            HashMap<String, Object[]> secondBulkQuery) {

        userTransaction = contextEJB.getUserTransaction();

        String firstResult, secondResult = GeneralConst.EMPTY_STRING;

        try {

            if (!isBulkQueryValid(firstBulkQuery) && !isBulkQueryValid(secondBulkQuery)) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            firstResult = getOutPutFromBulkQuery(firstStoredProcedureName, firstStoredOutputKey, firstStoredProcedureParams, firstBulkQuery);

            if (!firstResult.isEmpty()) {

                secondStoredProcedurParams.put(secondParamsWaitingKey, firstResult);

                secondResult = getOutPutFromBulkQuery(secondStoredProcedureName, secondStoredOutputKey, secondStoredProcedurParams, secondBulkQuery);

            }

            if (!firstResult.isEmpty() && !secondResult.isEmpty()) {
                userTransaction.commit();
            } else {
                userTransaction.rollback();
            }

        } catch (Exception e) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            firstResult = GeneralConst.EMPTY_STRING;
            try {
                userTransaction.rollback();
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return firstResult;
    }

    @Override
    public Object executeFunction(String functionName) {

        Object result = null;
        try {
            if (functionName == null || functionName.isEmpty()) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query requette = em.createNativeQuery(functionName);

            result = (Object) requette.getSingleResult();

        } catch (Exception e) {
            CustumException.BasicLogException(e);
            throw e;
        }
        return result;
    }

    public String generatekey(String table, String idColumn, String prefix, int nbrOrdrSize, String sifix) {
        String key = "";
        try {
            int taillePrefix = prefix.length();
            int intCode = 0;
            try {
                String[] infos = null;

                String queryStr = "SELECT TOP(1) " + infos[1] + "  FROM " + infos[0] + " WHERE SUBSTRING(" + infos[1] + ",1," + taillePrefix + ") = ? ORDER BY " + infos[1] + " DESC";
                Query requette = getEntityManager().createNativeQuery(queryStr);
                requette.setParameter(1, prefix);
                try {
                    String resultat = requette.getSingleResult().toString();
                    resultat = resultat.replace("[", "");
                    resultat = resultat.replace("]", "");
                    intCode = Integer.parseInt(resultat.substring(taillePrefix));
                    intCode += 1;
                    String zero = "";
                    int taille = String.valueOf(intCode).length();
                    for (int i = 0; i < 6 - taille; i++) {
                        zero += "0";
                    }
                    key = prefix + zero + intCode;
                } catch (NumberFormatException e) {
                    key = prefix + "000001";
                    CustumException.BasicLogException(e);
                }
            } catch (Exception e) {
                key = prefix + "000001";
                CustumException.BasicLogException(e);
            }
        } catch (SecurityException e) {
            CustumException.BasicLogException(e);
            throw e;
        }
        return key;
    }

    public boolean createAndUpdate(Object... entities) {
        boolean success;
        userTransaction = contextEJB.getUserTransaction();
        try {
            userTransaction.begin();
            for (Object object : entities) {
                if (entityExist(object)) {
                    getEntityManager().merge(object);
                } else {
                    getEntityManager().persist(object);
                }
            }
            userTransaction.commit();
            success = true;
        } catch (javax.validation.ConstraintViolationException e) {
            e.printStackTrace();
            success = false;
            CustumException.BasicLogException(e);
        } catch (Exception e) {
            try {
                userTransaction.setRollbackOnly();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            success = false;
            try {
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }
        return success;
    }

    public boolean entityExist(Object entity) {
        boolean success = false;
        try {
            Field[] tab = entity.getClass().getDeclaredFields();
            if (tab != null && tab.length > 0) {
                for (Field field : tab) {
                    Annotation[] annotations = field.getAnnotations();
                    if (annotations != null && annotations.length > 0) {
                        for (Annotation annotation : annotations) {
                            if (annotation.toString().equals("@javax.persistence.Id()")) {
                                String fieldName = field.getName();
                                Method method = entity.getClass().getDeclaredMethod("get" + String.valueOf(fieldName.charAt(0)).toUpperCase() + fieldName.substring(1));
                                if (method != null) {
                                    Object object = method.invoke(entity);
                                    Class entityClass = Class.forName(entity.getClass().getCanonicalName());
                                    try {
                                        if (find(entityClass, object) != null) {
                                            success = true;
                                        } else {
                                            success = false;
                                        }
                                    } catch (Exception e) {
                                        CustumException.BasicLogException(e);
                                        throw e;
                                    }
                                }
                                break;
                            }
                            if (annotation.toString().equals("@javax.persistence.EmbeddedId()")) {
                                Method method = entity.getClass().getDeclaredMethod("get" + entity.getClass().getSimpleName() + "PK");

                                if (method != null) {
                                    Object object = method.invoke(entity);
                                    Class entityClass = Class.forName(entity.getClass().getCanonicalName());
                                    if (find(entityClass, object) != null) {
                                        success = true;
                                    } else {
                                        success = false;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            CustumException.BasicLogException(e);

        }
        return success;
    }

    public Object getSingleResult(String Nativequery, Object... params) {

        Object object = null;

        try {

            if (Nativequery == null || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            Query query = getEntityManager().createNativeQuery(Nativequery);
            object = (Object) query.getSingleResult();
            setParameters(query, false, params);

        } catch (Exception e) {

            CustumException.BasicLogException(e);
            throw e;
        }

        return object;
    }

    public Object findStoredProcedure(String procedure, HashMap<String, Object> params) {

        Object object = null;

        try {

            if (procedure == null || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            StoredProcedureQuery storedProcedure = getEntityManager().createStoredProcedureQuery(procedure);

            setStoreProcedureParameters(storedProcedure, params);

            object = storedProcedure.getResultList();

        } catch (Exception e) {

            CustumException.BasicLogException(e);
            throw e;
        }

        return object;

    }

    @Override
    public Integer getSingleResultToInteger(String query) {
        Integer result = Integer.valueOf("0");
        try {
            if (query == null || query.isEmpty()) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query requette = em.createNativeQuery(query);
            result = Integer.valueOf(requette.getSingleResult().toString());

        } catch (Exception e) {
            CustumException.BasicLogException(e);
            throw e;
        }
        return result;
    }

    public String savePersonAndProperties(List<MockPersonCreate> mockPersonCreateList) {

        int counter = 0, counter2 = 0, counterOther = 0;
        String codePersonne = "", codeAdresse;
        MockAdresse mockAdresse;
        List<Field> currentField;

        JSONObject resultJson;

        String result = GeneralConst.EMPTY_STRING;
        userTransaction = contextEJB.getUserTransaction();

        try {

            userTransaction.begin();

            HashMap<String, Object[]> bulkOthers = new HashMap<>();

            for (MockPersonCreate mpc : mockPersonCreateList) {

                counter = 0;
                counter2 = 0;
                counterOther = 0;

                PersonCreate personCreate = mpc.getPersonObject();
                Object dataMock = getObjectComplementPersonne(personCreate);

                if (personCreate.getCode().equals("")) {
                    //CREATION DE LA PERSONNE
                    HashMap<String, Object> bulkPersonne = new HashMap<>();
                    String formeJuridique = personCreate.getType();
                    bulkPersonne.put("NIF", personCreate.getNif());
                    bulkPersonne.put("NOM", personCreate.getName());
                    bulkPersonne.put("POSTNOM", personCreate.getLastname());
                    bulkPersonne.put("PRENOMS", personCreate.getFirstname());
                    bulkPersonne.put("FORME_JURIDIQUE", formeJuridique);
                    bulkPersonne.put("AGENT_CREAT", null);

                    HashMap<String, Object[]> bulkDetailPersonne = new HashMap<>();

                    currentField = getPrivateFields(dataMock.getClass());
                    for (int i = 0; i < currentField.size(); i++) {
                        Field field = currentField.get(i);
                        String complement = field.getName();
                        String valeur = Tools.getValue(dataMock, complement);

                        counter++;
                        bulkDetailPersonne.put(counter + ":EXEC F_NEW_PERSONNE_COMPLEMENT ?1,'%s',?2,?3",
                                new Object[]{complement.split("_")[0], valeur, null});

                    }

                    codePersonne = getOutPutFromBulkQuery("P_NEW_PERSONNE", "CODE", bulkPersonne, bulkDetailPersonne);

                    mockAdresse = personCreate.getAddress();
                    codeAdresse = getAdresseCode(mockAdresse);

                    counterOther++;
                    bulkOthers.put(counterOther + ":EXEC P_AFFECTE_ADRESSE_PERSONNE ?1,?2,?3",
                            new Object[]{codeAdresse, codePersonne, 1});

                } else {
                    //MODIFICATION DE LA PERSONNE

                    codePersonne = personCreate.getCode();

                    counterOther++;
                    bulkOthers.put(counterOther + ":UPDATE T_PERSONNE SET NOM = ?1, POSTNOM = ?2, PRENOMS = ?3, NIF = ?4 WHERE CODE = ?5",
                            new Object[]{
                                personCreate.getName(),
                                personCreate.getLastname(),
                                personCreate.getFirstname(),
                                personCreate.getNif(),
                                codePersonne
                            });

                    currentField = getPrivateFields(dataMock.getClass());
                    for (int i = 0; i < currentField.size(); i++) {
                        Field field = currentField.get(i);
                        String complement = field.getName();
                        String valeur = Tools.getValue(dataMock, complement);

                        counterOther++;
                        bulkOthers.put(counterOther + ":UPDATE T_COMPLEMENT SET VALEUR = ?1 WHERE FK_COMPLEMENT_FORME = ?2 AND PERSONNE = ?3",
                                new Object[]{valeur, complement.split("_")[0], codePersonne});

                    }
                }

                // CREATION DES BIENS
                List<BienCreate> bienCreateList = mpc.getPropertyObjectList();

                for (BienCreate bienCreate : bienCreateList) {

                    if (bienCreate.getId().equals("")) {
                        //CREATION D'UN BIEN
                        mockAdresse = bienCreate.getAddress();
                        codeAdresse = getAdresseCode(mockAdresse);

                        HashMap<String, Object> bulkAdressPers = new HashMap<>();
                        bulkAdressPers.put("ADRESSE", codeAdresse);
                        bulkAdressPers.put("PERSONNE", codePersonne);
                        bulkAdressPers.put("PAR_DEFAUT", 0);

                        String codeAP = getStoredProcedureOutput("P_AFFECTE_ADRESSE_PERSONNE_V2", "code", bulkAdressPers);

                        HashMap<String, Object> bulkBien = new HashMap<>();
                        bulkBien.put("INTITULE", bienCreate.getName());
                        bulkBien.put("DESCRIPTION", bienCreate.getDescription());
                        bulkBien.put("PERSONNE", codePersonne);
                        bulkBien.put("ADRESSE_PERSONNE", codeAP);
                        bulkBien.put("AGENT_CREAT", null);
                        bulkBien.put("DATE_ACQUISITION", bienCreate.getDateAcquisition());

                        HashMap<String, Object[]> bulkDetailBien = new HashMap<>();

                        ComplementsBienCreate cbc = bienCreate.getPropertyComplements();
                        currentField = getPrivateFields(cbc.getClass());
                        for (int i = 0; i < currentField.size(); i++) {
                            Field field = currentField.get(i);
                            String complement = field.getName();
                            String valeur = Tools.getValue(cbc, complement);
                            String unite = "";

                            if (valeur.contains("__")) {
                                String[] dataArray = valeur.split("__");
                                valeur = dataArray[0];
                                unite = dataArray[1];
                            }

                            complement = complement.split("CB")[1];

                            counter2++;
                            bulkDetailBien.put(counter2 + ":EXEC F_NEW_COMPLEMENT_BIEN ?1,'%s',?2,?3,?4",
                                    new Object[]{complement.split("_")[0], valeur, null, unite});
                        }

                        getOutPutFromBulkQuery("P_NEW_BIEN", "ID", bulkBien, bulkDetailBien);

                    } else {
                        //MODIFICATION D'UN BIEN

                        String idBien = bienCreate.getId();

                        counterOther++;
                        bulkOthers.put(counterOther + ":UPDATE T_BIEN SET INTITULE = ?1, DESCRIPTION = ?2 WHERE ID = ?3",
                                new Object[]{bienCreate.getName(), bienCreate.getDescription(), idBien});

                        counterOther++;
                        bulkOthers.put(counterOther + ":UPDATE T_ACQUISITION SET DATE_ACQUISITION = ?1 WHERE BIEN = ?2",
                                new Object[]{bienCreate.getDateAcquisition(), idBien});

                        dataMock = bienCreate.getPropertyComplements();

                        currentField = getPrivateFields(dataMock.getClass());
                        for (int i = 0; i < currentField.size(); i++) {
                            Field field = currentField.get(i);
                            String complement = field.getName();
                            String valeur = Tools.getValue(dataMock, complement);

                            String unite = "";
                            if (valeur.contains("__")) {
                                String[] dataArray = valeur.split("__");
                                valeur = dataArray[0];
                                unite = dataArray[1];
                            }

                            complement = complement.split("CB")[1];

                            counterOther++;
                            bulkOthers.put(counterOther + ":UPDATE T_COMPLEMENT_BIEN SET VALEUR = ?1, DEVISE = ?2 WHERE TYPE_COMPLEMENT = ?3 AND BIEN = ?4",
                                    new Object[]{valeur, unite, complement.split("_")[0], idBien});

                        }
                    }

                }

                setBulkInsertion(bulkOthers);
            }

            userTransaction.commit();

            resultJson = new JSONObject();
            resultJson.put("code", "100");
            resultJson.put("message", "Success");

            result = resultJson.toString();

        } catch (Exception e) {
            try {
                userTransaction.rollback();

                resultJson = new JSONObject();
                resultJson.put("code", "300");
                resultJson.put("message", "Error");

                result = resultJson.toString();

            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            } catch (JSONException ex) {
                Logger.getLogger(DaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                userTransaction.rollback();
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;
    }

    public String savePerson(List<PersonCreate> personCreateList) throws NotSupportedException, JSONException {

        int counter = 0, counter2 = 0, counterOther = 0;
        String codePersonne = "", codeAdresse;
        MockAdresse mockAdresse;
        List<Field> currentField;

        JSONObject resultJson;

        String result = GeneralConst.EMPTY_STRING;
        userTransaction = contextEJB.getUserTransaction();

        try {

            userTransaction.begin();

            HashMap<String, Object[]> bulkOthers = new HashMap<>();

            for (PersonCreate mpc : personCreateList) {

                counter = 0;
                counter2 = 0;
                counterOther = 0;

                Object dataMock = getObjectComplementPersonne(mpc);

                if (mpc.getCode().equals("")) {
                    //CREATION DE LA PERSONNE
                    HashMap<String, Object> bulkPersonne = new HashMap<>();
                    String formeJuridique = mpc.getType();
                    bulkPersonne.put("NIF", mpc.getNif());
                    bulkPersonne.put("NOM", mpc.getName());
                    bulkPersonne.put("POSTNOM", mpc.getLastname());
                    bulkPersonne.put("PRENOMS", mpc.getFirstname());
                    bulkPersonne.put("FORME_JURIDIQUE", formeJuridique);
                    bulkPersonne.put("AGENT_CREAT", GeneralConst.NumberString.TWO);

                    HashMap<String, Object[]> bulkDetailPersonne = new HashMap<>();

                    currentField = getPrivateFields(dataMock.getClass());
                    for (int i = 0; i < currentField.size(); i++) {
                        Field field = currentField.get(i);
                        String complement = field.getName();
                        String valeur = Tools.getValue(dataMock, complement);

                        counter++;
                        bulkDetailPersonne.put(counter + ":EXEC F_NEW_PERSONNE_COMPLEMENT ?1,'%s',?2,?3",
                                new Object[]{complement.split("_")[0], valeur, null});

                    }

                    codePersonne = getOutPutFromBulkQuery("P_NEW_PERSONNE", "CODE", bulkPersonne, bulkDetailPersonne);

                    mockAdresse = mpc.getAddress();
                    codeAdresse = getAdresseCode(mockAdresse);

                    counterOther++;
                    bulkOthers.put(counterOther + ":EXEC P_AFFECTE_ADRESSE_PERSONNE ?1,?2,?3",
                            new Object[]{codeAdresse, codePersonne, 1});

                } else {
                    //MODIFICATION DE LA PERSONNE

                    codePersonne = mpc.getCode();

                    counterOther++;
                    bulkOthers.put(counterOther + ":UPDATE T_PERSONNE SET NOM = ?1, POSTNOM = ?2, PRENOMS = ?3, NIF = ?4 WHERE CODE = ?5",
                            new Object[]{
                                mpc.getName(),
                                mpc.getLastname(),
                                mpc.getFirstname(),
                                mpc.getNif(),
                                codePersonne
                            });

                    currentField = getPrivateFields(dataMock.getClass());
                    for (int i = 0; i < currentField.size(); i++) {
                        Field field = currentField.get(i);
                        String complement = field.getName();
                        String valeur = Tools.getValue(dataMock, complement);

                        counterOther++;
                        bulkOthers.put(counterOther + ":UPDATE T_COMPLEMENT SET VALEUR = ?1 WHERE FK_COMPLEMENT_FORME = ?2 AND PERSONNE = ?3",
                                new Object[]{valeur, complement.split("_")[0], codePersonne});

                    }
                }

                setBulkInsertion(bulkOthers);
            }

            userTransaction.commit();

            resultJson = new JSONObject();
            resultJson.put("personId", codePersonne);

            result = resultJson.toString();

        } catch (Exception e) {
            try {
                userTransaction.rollback();

                resultJson = new JSONObject();
                resultJson.put("code", "300");
                resultJson.put("message", "Error");

                result = resultJson.toString();

            } catch (Exception ex) {
                resultJson = new JSONObject();
                resultJson.put("code", "300");
                resultJson.put("message", "Error");
                Logger.getLogger(DaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            result = resultJson.toString();
        }

        return result;
    }

    public String saveBien(List<BienCreate> bienCreateList) {

        int counter = 0, counterOther = 0;

        MockAdresse mockAdresse;
        List<Field> currentField;

        JSONObject resultJson;

        String result = GeneralConst.EMPTY_STRING;
        userTransaction = contextEJB.getUserTransaction();

        try {

            HashMap<String, Object[]> bulkOthers = new HashMap<>();

            for (BienCreate bienCreate : bienCreateList) {

                if (bienCreate.getId().equals("")) {
                    //CREATION D'UN BIEN
                    mockAdresse = bienCreate.getAddress();
                    String codeAdresse = getAdresseCode(mockAdresse);

                    HashMap<String, Object> bulkAdressPers = new HashMap<>();
                    bulkAdressPers.put("ADRESSE", codeAdresse);
                    bulkAdressPers.put("PERSONNE", bienCreate.getPersonCode());
                    bulkAdressPers.put("PAR_DEFAUT", 0);

                    String codeAP = getStoredProcedureOutput("P_AFFECTE_ADRESSE_PERSONNE_V2", "code", bulkAdressPers);

                    HashMap<String, Object> bulkBien = new HashMap<>();
                    bulkBien.put("INTITULE", bienCreate.getName());
                    bulkBien.put("DESCRIPTION", bienCreate.getDescription());
                    bulkBien.put("PERSONNE", bienCreate.getPersonCode());
                    bulkBien.put("ADRESSE_PERSONNE", codeAP);
                    bulkBien.put("AGENT_CREAT", null);
                    bulkBien.put("DATE_ACQUISITION", bienCreate.getDateAcquisition());

                    HashMap<String, Object[]> bulkDetailBien = new HashMap<>();

                    ComplementsBienCreate cbc = bienCreate.getPropertyComplements();
                    currentField = getPrivateFields(cbc.getClass());
                    for (int i = 0; i < currentField.size(); i++) {
                        Field field = currentField.get(i);
                        String complement = field.getName();
                        String valeur = Tools.getValue(cbc, complement);
                        String unite = "";

                        if (valeur.contains("__")) {
                            String[] dataArray = valeur.split("__");
                            valeur = dataArray[0];
                            unite = dataArray[1];
                        }

                        complement = complement.split("CB")[1];

                        counter++;
                        bulkDetailBien.put(counter + ":EXEC F_NEW_COMPLEMENT_BIEN ?1,'%s',?2,?3,?4",
                                new Object[]{complement.split("_")[0], valeur, null, unite});
                    }

                    getOutPutFromBulkQuery("P_NEW_BIEN", "ID", bulkBien, bulkDetailBien);

                } else {
                    //MODIFICATION D'UN BIEN

                    String idBien = bienCreate.getId();

                    counterOther++;
                    bulkOthers.put(counterOther + ":UPDATE T_BIEN SET INTITULE = ?1, DESCRIPTION = ?2 WHERE ID = ?3",
                            new Object[]{bienCreate.getName(), bienCreate.getDescription(), idBien});

                    counterOther++;
                    bulkOthers.put(counterOther + ":UPDATE T_ACQUISITION SET DATE_ACQUISITION = ?1 WHERE BIEN = ?2",
                            new Object[]{bienCreate.getDateAcquisition(), idBien});

                    Object dataMock = bienCreate.getPropertyComplements();

                    currentField = getPrivateFields(dataMock.getClass());
                    for (int i = 0; i < currentField.size(); i++) {
                        Field field = currentField.get(i);
                        String complement = field.getName();
                        String valeur = Tools.getValue(dataMock, complement);

                        String unite = "";
                        if (valeur.contains("__")) {
                            String[] dataArray = valeur.split("__");
                            valeur = dataArray[0];
                            unite = dataArray[1];
                        }

                        complement = complement.split("CB")[1];

                        counterOther++;
                        bulkOthers.put(counterOther + ":UPDATE T_COMPLEMENT_BIEN SET VALEUR = ?1, DEVISE = ?2 WHERE TYPE_COMPLEMENT = ?3 AND BIEN = ?4",
                                new Object[]{valeur, unite, complement.split("_")[0], idBien});

                    }
                }
                setBulkInsertion(bulkOthers);
            }
        } catch (Exception e) {
            try {
                userTransaction.rollback();

                resultJson = new JSONObject();
                resultJson.put("code", "300");
                resultJson.put("message", "Error");

                result = resultJson.toString();

            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            } catch (JSONException ex) {
                Logger.getLogger(DaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                userTransaction.rollback();
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;
    }

    public String execBulkQueryv4(
            String fistQuery,
            String returnValue,
            HashMap<String, Object> params,
            HashMap<String, Object[]> bulkQuery) {

        userTransaction = contextEJB.getUserTransaction();

        String result;

        try {

            if (!isBulkQueryValid(bulkQuery)) {
                result = GeneralConst.EMPTY_STRING;
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            result = setBulkInsertionv4(fistQuery, returnValue, params, bulkQuery);

            userTransaction.commit();

        } catch (Exception e) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            result = GeneralConst.EMPTY_STRING;
            try {
                userTransaction.rollback();
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;

    }

    private String setBulkInsertionv4(
            String fistQuery,
            String returnValue,
            HashMap<String, Object> params,
            HashMap<String, Object[]> bulkQuery) {

        String valueRetour;

        try {

            valueRetour = getStoredProcedureOutput(fistQuery, returnValue, params);

            if (!valueRetour.isEmpty()) {

                int paramSize = bulkQuery.size() + 1;

                for (int i = 0; i <= paramSize; i++) {

                    Iterator<String> keys = bulkQuery.keySet().iterator();

                    while (keys.hasNext()) {

                        String key = keys.next();
                        String[] splitKey = key.split(":");
                        int index = Integer.parseInt(splitKey[0]);
                        String req = splitKey[1];

                        if (index == i) {

                            if (req.contains("%s")) {

                                req = req.replaceAll("%s", valueRetour);
                            }

                            Query query = (Query) getEntityManager().createNativeQuery(req);
                            setParameters(query, false, bulkQuery.get(key));
                            query.executeUpdate();
                            break;
                        }
                    }
                }
            }
        } catch (Exception exeption) {
            throw exeption;
        }

        return valueRetour;

    }

    public boolean execBulkQuery(
            String fistQuery,
            String returnValue,
            HashMap<String, Object> params,
            HashMap<String, Object[]> bulkQuery) {

        userTransaction = contextEJB.getUserTransaction();

        boolean result;

        try {

            if (!isBulkQueryValid(bulkQuery)) {
                result = false;
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            setBulkInsert(fistQuery, returnValue, params, bulkQuery);

            userTransaction.commit();

            result = true;

        } catch (Exception e) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            result = false;
            try {
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;

    }

    private void setBulkInsert(
            String fistQuery,
            String returnValue,
            HashMap<String, Object> params,
            HashMap<String, Object[]> bulkQuery) {

        String valueRetour = getStoredProcedureOutput(fistQuery, returnValue, params);

        int paramSize = bulkQuery.size() + 1;

        for (int i = 0; i <= paramSize; i++) {

            Iterator<String> keys = bulkQuery.keySet().iterator();

            while (keys.hasNext()) {

                String key = keys.next();
                String[] splitKey = key.split(":");
                int index = Integer.parseInt(splitKey[0]);
                String req = splitKey[1];

                if (index == i) {

                    if (req.contains("%s")) {
                        req = String.format(req, valueRetour);
                    }

                    Query query = (Query) getEntityManager().createNativeQuery(req);
                    setParameters(query, false, bulkQuery.get(key));
                    query.executeUpdate();
                    break;
                }

            }

        }

    }

    public String getAdresseCode(MockAdresse mockAdresse) {

        HashMap<String, Object> bulkAdresse = new HashMap<>();

        EntiteAdministrative avenue = DataAccess.getEntiteAdmByAvenue(mockAdresse.getAvenueCode());
        EntiteAdministrative quartier = avenue.getEntiteMere();
        EntiteAdministrative commune = quartier.getEntiteMere();
        EntiteAdministrative district = commune.getEntiteMere();
        EntiteAdministrative ville = district.getEntiteMere();
        EntiteAdministrative province = ville.getEntiteMere();

        bulkAdresse.put("NUMERO", mockAdresse.getNumber());
        bulkAdresse.put("AVENUE", avenue.getCode());
        bulkAdresse.put("QUARTIER", quartier.getCode());
        bulkAdresse.put("COMMUNE", commune.getCode());
        bulkAdresse.put("DISTRICT", district.getCode());
        bulkAdresse.put("VILLE", ville.getCode());
        bulkAdresse.put("PROVINCE", province.getCode());
        bulkAdresse.put("AGENT_CREAT", null);

        return getStoredProcedureOutput("P_ADRESSE", "ID", bulkAdresse);

    }

    private Object getObjectComplementPersonne(PersonCreate personCreate) {

        Object dataMock;
        String formeJuridique = personCreate.getType();
        if (formeJuridique.equals("04")) {
            ComplementsPersonnePhysique cpp = personCreate.getNaturalPersonComplements();
            dataMock = cpp;
        } else {
            ComplementsPersonneMorale cpm = personCreate.getLegalPersonComplements();
            dataMock = cpm;
        }
        return dataMock;
    }

    public String createBien(PeriodeDeclaration pDeclaration, RetraitDeclaration retraitDeclaration, String center, String codeAgent, String compteBancaire, String banque) {

        int counter;
        String result = GeneralConst.EMPTY_STRING;
        String codeDeclaration = "DEC-".concat(String.valueOf(new Date().getTime())
                .substring(8, 13)).concat(GeneralConst.DASH_SEPARATOR).concat(Tools.generateRandomValue(5));
        userTransaction = contextEJB.getUserTransaction();

        try {

            userTransaction.begin();

            HashMap<String, Object[]> bulkOther = new HashMap<>();
            counter = 0;

            if (pDeclaration.getId() == 0) {

                HashMap<String, Object> bulkPeriod = new HashMap<>();
                bulkPeriod.put("ASSUJETTISSEMENT", pDeclaration.getAssujetissement().getId());
                bulkPeriod.put("DEBUT", pDeclaration.getDebut());
                bulkPeriod.put("FIN", pDeclaration.getFin());
                bulkPeriod.put("DATE_LIMITE", pDeclaration.getDateLimite());
                bulkPeriod.put("NOTE_CALCUL", null);
                bulkPeriod.put("AMR", null);

                String periodId = getStoredProcedureOutput("F_NEW_PERIODE_DECLARATION_2", "ID", bulkPeriod);

                result = periodId;

                counter++;
                bulkOther.put(counter + ":EXEC F_NEW_RETRAIT_DECLARATION_2 ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14,?15,?16",
                        new Object[]{
                            retraitDeclaration.getRequerant().toUpperCase(),
                            retraitDeclaration.getFkAssujetti(),
                            retraitDeclaration.getFkAb(),
                            periodId,
                            codeAgent,
                            retraitDeclaration.getValeurBase(),
                            retraitDeclaration.getMontant(),
                            retraitDeclaration.getDevise(),
                            codeDeclaration,
                            null, null, banque, compteBancaire, null, null, null
                        });

            } else {

                counter++;
                bulkOther.put(counter + ":EXEC F_NEW_RETRAIT_DECLARATION_2 ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14,?15,?16",
                        new Object[]{
                            retraitDeclaration.getRequerant().toUpperCase(),
                            retraitDeclaration.getFkAssujetti(),
                            retraitDeclaration.getFkAb(),
                            pDeclaration.getId(),
                            codeAgent,
                            retraitDeclaration.getValeurBase(),
                            retraitDeclaration.getMontant(),
                            retraitDeclaration.getDevise(),
                            codeDeclaration,
                            null, null, banque, compteBancaire, null, null, null
                        });

                result = pDeclaration.getId().toString();
            }

            setBulkInsertion(bulkOther);
            userTransaction.commit();

        } catch (Exception e) {

            try {
                userTransaction.rollback();

                result = GeneralConst.EMPTY_STRING;

            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }

            try {
                userTransaction.rollback();

                result = GeneralConst.EMPTY_STRING;

                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;
    }

    public String createBien(Assujeti assujeti, String tcb, String tarifCode, String requerant, String amount, String devise, String centre, String codeAgent) {

        int counter = 0, counterAssuj = 0;

        String codeDeclaration = "DEC-".concat(String.valueOf(new Date().getTime())
                .substring(8, 13)).concat(GeneralConst.DASH_SEPARATOR).concat(Tools.generateRandomValue(5));

        String result = GeneralConst.EMPTY_STRING;
        userTransaction = contextEJB.getUserTransaction();

        String idAssuj;

        try {

            userTransaction.begin();

            Bien bien = assujeti.getBien();
            Personne personne = assujeti.getPersonne();
            AdressePersonne adressePersonne = bien.getFkAdressePersonne();
            Acquisition acquisition = bien.getAcquisitionList().get(0);

            List<ComplementBien> complementBienList = bien.getComplementBienList();
            ArticleBudgetaire articleBudgetaire = assujeti.getArticleBudgetaire();
            PeriodeDeclaration periodeDeclaration = assujeti.getPeriodeDeclarationList().get(0);

            Periode periode;

            if (bien.getId().equals(GeneralConst.EMPTY_STRING)) {

                HashMap<String, Object> bulkBien = new HashMap<>();
                bulkBien.put("INTITULE", bien.getIntitule());
                bulkBien.put("DESCRIPTION", bien.getDescription());
                bulkBien.put("PERSONNE", personne.getCode());
                bulkBien.put("ADRESSE_PERSONNE", adressePersonne.getCode());
                bulkBien.put("AGENT_CREAT", null);
                bulkBien.put("DATE_ACQUISITION", acquisition.getDateAcquisition());
                bulkBien.put("PROPRIETAIRE", acquisition.getProprietaire() ? 1 : 0);

                HashMap<String, Object[]> bulkDetailBien = new HashMap<>();

                String unite = GeneralConst.EMPTY_STRING;
                for (ComplementBien complementBien : complementBienList) {
                    counter++;
                    bulkDetailBien.put(counter + ":EXEC F_NEW_COMPLEMENT_BIEN ?1,'%s',?2,?3,?4",
                            new Object[]{complementBien.getTypeComplement().getCode(), complementBien.getValeur(), null, unite});
                }

                String idBien = getOutPutFromBulkQuery("P_NEW_BIEN_2", "ID", bulkBien, bulkDetailBien);

                HashMap<String, Object> bulkAssujetti = new HashMap<>();
                bulkAssujetti.put("BIEN", idBien);
                bulkAssujetti.put("ARTICLE_BUDGETAIRE", articleBudgetaire.getCode());
                bulkAssujetti.put("DUREE", 1);
                bulkAssujetti.put("RENOUVELLEMENT", 1);
                bulkAssujetti.put("DATE_DEBUT", assujeti.getDateDebut());
                bulkAssujetti.put("DATE_FIN", assujeti.getDateFin());
                bulkAssujetti.put("VALEUR", assujeti.getValeur());
                bulkAssujetti.put("PERSONNE", personne.getCode());
                bulkAssujetti.put("ADRESSE", adressePersonne.getCode());
                bulkAssujetti.put("AGENT_CREAT", null);
                bulkAssujetti.put("REFERENCE_CONTRAT", GeneralConst.EMPTY_STRING);
                bulkAssujetti.put("NBR_JOUR_LIMITE", articleBudgetaire.getNbrJourLimite());
                bulkAssujetti.put("PERIODICITE", articleBudgetaire.getPeriodicite().getCode());
                bulkAssujetti.put("TARIF", assujeti.getTarif().getCode());
                bulkAssujetti.put("NUM_ACTE_NOTARIE", GeneralConst.EMPTY_STRING);
                bulkAssujetti.put("DATE_ACTE_NOTARIE", null);
                bulkAssujetti.put("GESTIONNAIRE", null);

                idAssuj = getStoredProcedureOutput("F_NEW_ASSUJETISSEMENT_V2", "ID_ASS", bulkAssujetti);

                HashMap<String, Object> bulkPeriod = new HashMap<>();
                bulkPeriod.put("ASSUJETTISSEMENT", idAssuj);
                bulkPeriod.put("DEBUT", periodeDeclaration.getDebut());
                bulkPeriod.put("FIN", periodeDeclaration.getFin());
                bulkPeriod.put("DATE_LIMITE", periodeDeclaration.getDateLimite());
                bulkPeriod.put("NOTE_CALCUL", null);
                bulkPeriod.put("AMR", null);

                String periodId = getStoredProcedureOutput("F_NEW_PERIODE_DECLARATION_2", "ID", bulkPeriod);

                periode = DocumentBusiness.getPeriodeInformation(articleBudgetaire.getCode(), tarifCode, assujeti.getValeur().intValue(), periodId, periodeDeclaration.getDebut(), amount, devise);

                HashMap<String, Object[]> bulkAssuj = new HashMap<>();

                counterAssuj++;
                bulkAssuj.put(counterAssuj + ":EXEC P_UPDATE_ASSUJETISSEMENT ?1,?2,?3",
                        new Object[]{tcb, idBien, idAssuj});

                counterAssuj++;
                bulkAssuj.put(counterAssuj + ":EXEC F_NEW_RETRAIT_DECLARATION_2 ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14,?15,?16",
                        new Object[]{
                            requerant.toUpperCase(),
                            personne.getCode(),
                            articleBudgetaire.getCode(),
                            periodId,
                            codeAgent,
                            assujeti.getValeur(),
                            periode.getAmount(),
                            periode.getDevise(),
                            codeDeclaration,
                            null, null, null, null, null, null, null
                        });

                setBulkInsertion(bulkAssuj);

            } else {

                idAssuj = assujeti.getId();
                HashMap<String, Object[]> bulkOther = new HashMap<>();
                counter = 0;

                for (ComplementBien complementBien : complementBienList) {
                    if (!complementBien.isExist()) {
                        counter++;
                        bulkOther.put(counter + ":EXEC F_NEW_COMPLEMENT_BIEN ?1,?2,?3,?4,?5",
                                new Object[]{complementBien.getTypeComplement().getCode(), bien.getId(), complementBien.getValeur(), null, GeneralConst.EMPTY_STRING});
                    }
                }

                if (idAssuj == null) {

                    HashMap<String, Object> bulkAssujetti = new HashMap<>();
                    bulkAssujetti.put("BIEN", assujeti.getBien().getId());
                    bulkAssujetti.put("ARTICLE_BUDGETAIRE", articleBudgetaire.getCode());
                    bulkAssujetti.put("DUREE", 1);
                    bulkAssujetti.put("RENOUVELLEMENT", 1);
                    bulkAssujetti.put("DATE_DEBUT", assujeti.getDateDebut());
                    bulkAssujetti.put("DATE_FIN", assujeti.getDateFin());
                    bulkAssujetti.put("VALEUR", assujeti.getValeur());
                    bulkAssujetti.put("PERSONNE", personne.getCode());
                    bulkAssujetti.put("ADRESSE", adressePersonne.getCode());
                    bulkAssujetti.put("AGENT_CREAT", null);
                    bulkAssujetti.put("REFERENCE_CONTRAT", GeneralConst.EMPTY_STRING);
                    bulkAssujetti.put("NBR_JOUR_LIMITE", articleBudgetaire.getNbrJourLimite());
                    bulkAssujetti.put("PERIODICITE", articleBudgetaire.getPeriodicite().getCode());
                    bulkAssujetti.put("TARIF", assujeti.getTarif().getCode());
                    bulkAssujetti.put("NUM_ACTE_NOTARIE", GeneralConst.EMPTY_STRING);
                    bulkAssujetti.put("DATE_ACTE_NOTARIE", null);
                    bulkAssujetti.put("GESTIONNAIRE", null);

                    idAssuj = getStoredProcedureOutput("F_NEW_ASSUJETISSEMENT_V2", "ID_ASS", bulkAssujetti);

                    counter++;
                    bulkOther.put(counter + ":EXEC P_UPDATE_ASSUJETISSEMENT ?1,?2,?3",
                            new Object[]{tcb, assujeti.getBien().getId(), idAssuj});
                }

                if (acquisition.getId().equals(GeneralConst.EMPTY_STRING)) {
                    counter++;
                    bulkOther.put(counter + ":EXEC F_NEW_ACQUISITION_2 ?1,?2,?3,?4",
                            new Object[]{personne.getCode(), bien.getId(), acquisition.getDateAcquisition(), acquisition.getProprietaire()});
                }

                if (periodeDeclaration.getId() == 0) {

                    HashMap<String, Object> bulkPeriod = new HashMap<>();
                    bulkPeriod.put("ASSUJETTISSEMENT", idAssuj);
                    bulkPeriod.put("DEBUT", periodeDeclaration.getDebut());
                    bulkPeriod.put("FIN", periodeDeclaration.getFin());
                    bulkPeriod.put("DATE_LIMITE", periodeDeclaration.getDateLimite());
                    bulkPeriod.put("NOTE_CALCUL", null);
                    bulkPeriod.put("AMR", null);

                    String periodId = getStoredProcedureOutput("F_NEW_PERIODE_DECLARATION_2", "ID", bulkPeriod);
                    periode = DocumentBusiness.getPeriodeInformation(articleBudgetaire.getCode(), tarifCode, assujeti.getValeur().intValue(), periodId, periodeDeclaration.getDebut(), amount, devise);

                    counter++;
                    bulkOther.put(counter + ":EXEC F_NEW_RETRAIT_DECLARATION_2 ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14,?15,?16",
                            new Object[]{
                                requerant.toUpperCase(),
                                personne.getCode(),
                                articleBudgetaire.getCode(),
                                periodId,
                                codeAgent,
                                assujeti.getValeur(),
                                periode.getAmount(),
                                periode.getDevise(),
                                codeDeclaration,
                                null, null, null, null, null, null, null
                            });

                } else {

                    periode = DocumentBusiness.getPeriodeInformation(articleBudgetaire.getCode(), tarifCode, assujeti.getValeur().intValue(), periodeDeclaration.getId().toString(), periodeDeclaration.getDebut(), amount, devise);

                    counter++;
                    bulkOther.put(counter + ":EXEC F_NEW_RETRAIT_DECLARATION_2 ?1,?2,?3,?4,?5,?6,?7,?8,?9",
                            new Object[]{
                                requerant.toUpperCase(),
                                personne.getCode(),
                                articleBudgetaire.getCode(),
                                periodeDeclaration.getId(),
                                codeAgent,
                                assujeti.getValeur(),
                                periode.getAmount(),
                                periode.getDevise(),
                                centre
                            });
                }

                setBulkInsertion(bulkOther);
            }

            result = idAssuj;

            userTransaction.commit();

            //userTransaction.rollback();
        } catch (Exception e) {
            try {
                userTransaction.rollback();

                result = "";

            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }

            try {
                userTransaction.rollback();

                result = "";

                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;
    }

    public String createBienV2(Assujeti assujeti, String tcb, PersonCreate personCreate) {

        int counter = 0, counterAssuj = 0;

        String result = GeneralConst.EMPTY_STRING;
        userTransaction = contextEJB.getUserTransaction();

        try {

            userTransaction.begin();

            HashMap<String, Object[]> bulkAssuj = new HashMap<>();

            if (assujeti != null) {

                Bien bien = assujeti.getBien();
                Personne personne = assujeti.getPersonne();
                Acquisition acquisition = bien.getAcquisitionList().get(0);
                List<ComplementBien> complementBienList = bien.getComplementBienList();
                ArticleBudgetaire articleBudgetaire = assujeti.getArticleBudgetaire();
                AdressePersonne adressePersonne = bien.getFkAdressePersonne();
                PeriodeDeclaration periodeDeclaration = assujeti.getPeriodeDeclarationList().get(0);

                HashMap<String, Object> bulkBien = new HashMap<>();
                bulkBien.put("INTITULE", bien.getIntitule());
                bulkBien.put("DESCRIPTION", bien.getDescription());
                bulkBien.put("PERSONNE", personne.getCode());
                bulkBien.put("ADRESSE_PERSONNE", adressePersonne.getCode());
                bulkBien.put("AGENT_CREAT", null);
                bulkBien.put("DATE_ACQUISITION", acquisition.getDateAcquisition());

                HashMap<String, Object[]> bulkDetailBien = new HashMap<>();

                String unite = "";
                for (ComplementBien complementBien : complementBienList) {
                    counter++;
                    bulkDetailBien.put(counter + ":EXEC F_NEW_COMPLEMENT_BIEN ?1,'%s',?2,?3,?4",
                            new Object[]{complementBien.getTypeComplement().getCode(), complementBien.getValeur(), null, unite});
                }

                String idBien = getOutPutFromBulkQuery("P_NEW_BIEN", "ID", bulkBien, bulkDetailBien);

                HashMap<String, Object> bulkAssujetti = new HashMap<>();
                bulkAssujetti.put("BIEN", idBien);
                bulkAssujetti.put("ARTICLE_BUDGETAIRE", articleBudgetaire.getCode());
                bulkAssujetti.put("DUREE", 1);
                bulkAssujetti.put("RENOUVELLEMENT", 1);
                bulkAssujetti.put("DATE_DEBUT", assujeti.getDateDebut());
                bulkAssujetti.put("DATE_FIN", assujeti.getDateFin());
                bulkAssujetti.put("VALEUR", assujeti.getValeur());
                bulkAssujetti.put("PERSONNE", personne.getCode());
                bulkAssujetti.put("ADRESSE", adressePersonne.getCode());
                bulkAssujetti.put("AGENT_CREAT", null);
                bulkAssujetti.put("REFERENCE_CONTRAT", "");
                bulkAssujetti.put("NBR_JOUR_LIMITE", articleBudgetaire.getNbrJourLimite());
                bulkAssujetti.put("PERIODICITE", articleBudgetaire.getPeriodicite().getCode());
                bulkAssujetti.put("TARIF", articleBudgetaire.getTarif().getCode());
                bulkAssujetti.put("NUM_ACTE_NOTARIE", "");
                bulkAssujetti.put("DATE_ACTE_NOTARIE", null);
                bulkAssujetti.put("GESTIONNAIRE", null);

                String idAssuj = getStoredProcedureOutput("F_NEW_ASSUJETISSEMENT_V2", "ID_ASS", bulkAssujetti);

                counterAssuj++;
                bulkAssuj.put(counterAssuj + ":EXEC F_NEW_PERIODE_DECLARATION ?1,?2,?3,?4,?5,?6,?7",
                        new Object[]{
                            idAssuj,
                            periodeDeclaration.getDebut(),
                            periodeDeclaration.getFin(),
                            periodeDeclaration.getDateLimite(),
                            null,
                            null,
                            null
                        });

                counterAssuj++;
                bulkAssuj.put(counterAssuj + ":EXEC P_UPDATE_ASSUJETISSEMENT ?1,?2,?3",
                        new Object[]{tcb, idBien, idAssuj});

                setBulkInsertion(bulkAssuj);

                result = idAssuj;

            } else {

                result = "";
            }

            userTransaction.commit();

        } catch (Exception e) {
            try {
                userTransaction.rollback();

                result = "";

            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }

            try {
                userTransaction.rollback();
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;
    }

    public String getSingleResultToString(String query, Object... params) {
        String result = "";
        try {
            Query requette = em.createNativeQuery(query);
            setParameters(requette, false, params);
            result = requette.getSingleResult().toString();

        } catch (SecurityException ex) {
            CustumException.LogException(ex);
        }
        return result;
    }

    public BigDecimal getSingleResultToBigDecimal2(String query, Object... params) {
        BigDecimal result = new BigDecimal("0");
        try {
            Query requette = em.createNativeQuery(query);
            setParameters(requette, false, params);
            result = BigDecimal.valueOf(Double.parseDouble(requette.getSingleResult().toString()));

        } catch (SecurityException ex) {
            //CustumException.LogExceptionEJB(ex);
        }
        return result;
    }

    public String execBulkQueryWithOneLevel(
            String fistQuery,
            String returnValue,
            HashMap<String, Object> params,
            HashMap<String, Object[]> bulkQuery) {

        userTransaction = contextEJB.getUserTransaction();

        String result;

        try {

            if (!isBulkQueryValid(bulkQuery)) {
                result = GeneralConst.EMPTY_STRING;
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            result = setBulkInsertionWithOneLevel(fistQuery, returnValue, params, bulkQuery);

            userTransaction.commit();

        } catch (Exception e) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            result = GeneralConst.EMPTY_STRING;
            try {
                userTransaction.rollback();
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;

    }

    private String setBulkInsertionWithOneLevel(
            String fistQuery,
            String returnValue,
            HashMap<String, Object> params,
            HashMap<String, Object[]> bulkQuery) {

        String valueRetour;

        try {

            valueRetour = getStoredProcedureOutput(fistQuery, returnValue, params);

            if (!valueRetour.isEmpty()) {

                int paramSize = bulkQuery.size() + 1;

                for (int i = 0; i <= paramSize; i++) {

                    Iterator<String> keys = bulkQuery.keySet().iterator();

                    while (keys.hasNext()) {

                        String key = keys.next();
                        String[] splitKey = key.split(":");
                        int index = Integer.parseInt(splitKey[0]);
                        String req = splitKey[1];

                        if (index == i) {

                            if (req.contains("%s")) {

                                req = req.replaceAll("%s", valueRetour);
                            }

                            Query query = (Query) getEntityManager().createNativeQuery(req);
                            setParameters(query, false, bulkQuery.get(key));
                            query.executeUpdate();
                            break;
                        }
                    }
                }
            }
        } catch (Exception exeption) {
            throw exeption;
        }

        return valueRetour;

    }
}
