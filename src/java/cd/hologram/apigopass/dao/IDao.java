
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.dao;

import java.util.HashMap;

/**
 *
 * @author tony.lundja
 * @param <T>
 */
public interface IDao<T> {

    public T find(Class<T> entity, T id);

    public T find(String query, Class<T> entity, boolean like, T... params);

    public T find(String query, Class<T> entity, Integer limit, boolean like, T... params);

    public T findJPQL(String queryJPQL, boolean like, T... params);

    public T findJPQL(String queryJPQL, Integer limit, boolean like, T... params);

    public T findNamedJPQL(String namedQuery, boolean like, T... params);

    public T findNamedJPQL(String namedQuery, Integer limit, boolean like, T... params);

    public Object find(String storedProcedureName, HashMap<String, Object> params);

    public boolean executeNativeQuery(String nativeQuery, Object... params);
    
    public Integer getSingleResultToInteger(String query);

    public boolean executeStoredProcedure(String storedProcedureName, HashMap<String, Object> storedProcedureParams);

    public boolean executeStoredProcedure(String storedProcedureName, Object... storedProcedureParams);

    public String getStoredProcedureOutput(String storedProcedureName, String outputKey, HashMap<String, Object> storedProcedureParams);

    public boolean executeBulkQuery(HashMap<String, Object[]> params);

    public boolean executeBulkQuery(
            String storedProcedureName,
            HashMap<String, Object> storedProcedureParams,
            String outputKey,
            HashMap<String, Object[]> bulkQuery);

    public String executeBulkQueryGetOutput(
            String storedProcedureName,
            HashMap<String, Object> storedProcedureParams,
            String outputKey,
            HashMap<String, Object[]> bulkQuery);

    public String executeBulkQueryGetOutput(
            String firstStoredProcedureName,
            HashMap<String, Object> firstStoredProcedureParams,
            String secondStoredProcedureName,
            HashMap<String, Object> secondStoredProcedurParams,
            String firstStoredOutputKey,
            String secondStoredOutputKey,
            String secondParamsWaitingKey,
            HashMap<String, Object[]> firstBulkQuery,
            HashMap<String, Object[]> secondBulkQuery);

    public Object executeFunction(String functionName);

}
