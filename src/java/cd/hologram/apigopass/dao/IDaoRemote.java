/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.dao;

import javax.ejb.Remote;

/**
 *
 * @author tony.lundja
 */
@Remote
public interface IDaoRemote extends IDao<Object> {
    
}
