/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_DETAIL_DEPOT_DECLARATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailDepotDeclaration.findAll", query = "SELECT d FROM DetailDepotDeclaration d"),
    @NamedQuery(name = "DetailDepotDeclaration.findByCode", query = "SELECT d FROM DetailDepotDeclaration d WHERE d.code = :code"),
    @NamedQuery(name = "DetailDepotDeclaration.findByTaux", query = "SELECT d FROM DetailDepotDeclaration d WHERE d.taux = :taux"),
    @NamedQuery(name = "DetailDepotDeclaration.findByQuantite", query = "SELECT d FROM DetailDepotDeclaration d WHERE d.quantite = :quantite"),
    @NamedQuery(name = "DetailDepotDeclaration.findByValeurBase", query = "SELECT d FROM DetailDepotDeclaration d WHERE d.valeurBase = :valeurBase"),
    @NamedQuery(name = "DetailDepotDeclaration.findByEtat", query = "SELECT d FROM DetailDepotDeclaration d WHERE d.etat = :etat")})
public class DetailDepotDeclaration implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Long code;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "TAUX")
    private BigDecimal taux;
    @Column(name = "QUANTITE")
    private BigDecimal quantite;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VALEUR_BASE")
    private BigDecimal valeurBase;
    @Column(name = "ETAT")
    private Integer etat;
    @JoinColumn(name = "ARTICLE_BUDGETAIRE", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private ArticleBudgetaire articleBudgetaire;
    @JoinColumn(name = "DEPOT_DECLARATION", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private DepotDeclaration depotDeclaration;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private Devise devise;
    @JoinColumn(name = "TARIF", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif tarif;

    public DetailDepotDeclaration() {
    }

    public DetailDepotDeclaration(Long code) {
        this.code = code;
    }

    public DetailDepotDeclaration(Long code, BigDecimal taux, BigDecimal valeurBase) {
        this.code = code;
        this.taux = taux;
        this.valeurBase = valeurBase;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public BigDecimal getQuantite() {
        return quantite;
    }

    public void setQuantite(BigDecimal quantite) {
        this.quantite = quantite;
    }

    public BigDecimal getValeurBase() {
        return valeurBase;
    }

    public void setValeurBase(BigDecimal valeurBase) {
        this.valeurBase = valeurBase;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public ArticleBudgetaire getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(ArticleBudgetaire articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public DepotDeclaration getDepotDeclaration() {
        return depotDeclaration;
    }

    public void setDepotDeclaration(DepotDeclaration depotDeclaration) {
        this.depotDeclaration = depotDeclaration;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public Tarif getTarif() {
        return tarif;
    }

    public void setTarif(Tarif tarif) {
        this.tarif = tarif;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailDepotDeclaration)) {
            return false;
        }
        DetailDepotDeclaration other = (DetailDepotDeclaration) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.DetailDepotDeclaration[ code=" + code + " ]";
    }
    
}
