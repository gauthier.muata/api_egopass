/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_PREUVE_PAIEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PreuvePaiement.findAll", query = "SELECT p FROM PreuvePaiement p"),
    @NamedQuery(name = "PreuvePaiement.findByCpi", query = "SELECT p FROM PreuvePaiement p WHERE p.cpi = :cpi"),
    @NamedQuery(name = "PreuvePaiement.findByDossier", query = "SELECT p FROM PreuvePaiement p WHERE p.dossier = :dossier")})
public class PreuvePaiement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CPI")
    private String cpi;
    @Size(max = 25)
    @Column(name = "DOSSIER")
    private String dossier;

    public PreuvePaiement() {
    }

    public PreuvePaiement(String cpi) {
        this.cpi = cpi;
    }

    public String getCpi() {
        return cpi;
    }

    public void setCpi(String cpi) {
        this.cpi = cpi;
    }

    public String getDossier() {
        return dossier;
    }

    public void setDossier(String dossier) {
        this.dossier = dossier;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cpi != null ? cpi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PreuvePaiement)) {
            return false;
        }
        PreuvePaiement other = (PreuvePaiement) object;
        if ((this.cpi == null && other.cpi != null) || (this.cpi != null && !this.cpi.equals(other.cpi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.PreuvePaiement[ cpi=" + cpi + " ]";
    }
    
}
