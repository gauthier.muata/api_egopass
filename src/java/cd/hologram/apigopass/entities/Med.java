/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_MED")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Med.findAll", query = "SELECT m FROM Med m"),
    @NamedQuery(name = "Med.findById", query = "SELECT m FROM Med m WHERE m.id = :id"),
    @NamedQuery(name = "Med.findByEtat", query = "SELECT m FROM Med m WHERE m.etat = :etat"),
    @NamedQuery(name = "Med.findByAgentCreat", query = "SELECT m FROM Med m WHERE m.agentCreat = :agentCreat"),
    @NamedQuery(name = "Med.findByDateCreat", query = "SELECT m FROM Med m WHERE m.dateCreat = :dateCreat"),
    @NamedQuery(name = "Med.findByAgentMaj", query = "SELECT m FROM Med m WHERE m.agentMaj = :agentMaj"),
    @NamedQuery(name = "Med.findByDateMaj", query = "SELECT m FROM Med m WHERE m.dateMaj = :dateMaj"),
    @NamedQuery(name = "Med.findByPersonne", query = "SELECT m FROM Med m WHERE m.personne = :personne"),
    @NamedQuery(name = "Med.findByPeriodeDeclaration", query = "SELECT m FROM Med m WHERE m.periodeDeclaration = :periodeDeclaration"),
    @NamedQuery(name = "Med.findByDateEcheance", query = "SELECT m FROM Med m WHERE m.dateEcheance = :dateEcheance"),
    @NamedQuery(name = "Med.findByNoteCalcul", query = "SELECT m FROM Med m WHERE m.noteCalcul = :noteCalcul"),
    @NamedQuery(name = "Med.findByNotePerception", query = "SELECT m FROM Med m WHERE m.notePerception = :notePerception"),
    @NamedQuery(name = "Med.findByEtatImpression", query = "SELECT m FROM Med m WHERE m.etatImpression = :etatImpression"),
    @NamedQuery(name = "Med.findByTypeMed", query = "SELECT m FROM Med m WHERE m.typeMed = :typeMed"),
    @NamedQuery(name = "Med.findByArticleBudgetaire", query = "SELECT m FROM Med m WHERE m.articleBudgetaire = :articleBudgetaire"),
    @NamedQuery(name = "Med.findByAdresse", query = "SELECT m FROM Med m WHERE m.adresse = :adresse"),
    @NamedQuery(name = "Med.findByExerciceFiscal", query = "SELECT m FROM Med m WHERE m.exerciceFiscal = :exerciceFiscal"),
    @NamedQuery(name = "Med.findByMontant", query = "SELECT m FROM Med m WHERE m.montant = :montant"),
    @NamedQuery(name = "Med.findByMedMere", query = "SELECT m FROM Med m WHERE m.medMere = :medMere"),
    @NamedQuery(name = "Med.findByMontantPenalite", query = "SELECT m FROM Med m WHERE m.montantPenalite = :montantPenalite"),
    @NamedQuery(name = "Med.findByDateImpression", query = "SELECT m FROM Med m WHERE m.dateImpression = :dateImpression"),
    @NamedQuery(name = "Med.findByDateReception", query = "SELECT m FROM Med m WHERE m.dateReception = :dateReception"),
    @NamedQuery(name = "Med.findByNumeroDocument", query = "SELECT c FROM Med c WHERE c.numeroDocument = :numeroDocument")})
public class Med implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Size(max = 25)
    @Column(name = "PERSONNE")
    private String personne;
    @Column(name = "PERIODE_DECLARATION")
    private Integer periodeDeclaration;
    @Column(name = "DATE_ECHEANCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEcheance;
    @Size(max = 25)
    @Column(name = "NOTE_CALCUL")
    private String noteCalcul;
    @Size(max = 25)
    @Column(name = "NOTE_PERCEPTION")
    private String notePerception;
    @Column(name = "ETAT_IMPRESSION")
    private Short etatImpression;
    @Size(max = 20)
    @Column(name = "TYPE_MED")
    private String typeMed;
    @Size(max = 25)
    @Column(name = "ARTICLE_BUDGETAIRE")
    private String articleBudgetaire;
    @Size(max = 50)
    @Column(name = "ADRESSE")
    private String adresse;
    @Size(max = 10)
    @Column(name = "EXERCICE_FISCAL")
    private String exerciceFiscal;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Column(name = "MONTANT_PENALITE")
    private BigDecimal montantPenalite;

    @Column(name = "MED_MERE")
    private String medMere;
    @Column(name = "DATE_IMPRESSION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateImpression;
    @Column(name = "DATE_RECEPTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReception;
    @Size(max = 100)
    @Column(name = "NUMERO_DOCUMENT")
    private String numeroDocument;

    public Med() {
    }

    public Med(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public String getPersonne() {
        return personne;
    }

    public void setPersonne(String personne) {
        this.personne = personne;
    }

    public Integer getPeriodeDeclaration() {
        return periodeDeclaration;
    }

    public void setPeriodeDeclaration(Integer periodeDeclaration) {
        this.periodeDeclaration = periodeDeclaration;
    }

    public Date getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(Date dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public String getNoteCalcul() {
        return noteCalcul;
    }

    public void setNoteCalcul(String noteCalcul) {
        this.noteCalcul = noteCalcul;
    }

    public String getNotePerception() {
        return notePerception;
    }

    public void setNotePerception(String notePerception) {
        this.notePerception = notePerception;
    }

    public Short getEtatImpression() {
        return etatImpression;
    }

    public void setEtatImpression(Short etatImpression) {
        this.etatImpression = etatImpression;
    }

    public String getTypeMed() {
        return typeMed;
    }

    public void setTypeMed(String typeMed) {
        this.typeMed = typeMed;
    }

    public String getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(String articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getExerciceFiscal() {
        return exerciceFiscal;
    }

    public void setExerciceFiscal(String exerciceFiscal) {
        this.exerciceFiscal = exerciceFiscal;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Date getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(Date dateImpression) {
        this.dateImpression = dateImpression;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public String getNumeroDocument() {
        return numeroDocument;
    }

    public void setNumeroDocument(String numeroDocument) {
        this.numeroDocument = numeroDocument;
    }

    public BigDecimal getMontantPenalite() {
        return montantPenalite;
    }

    public void setMontantPenalite(BigDecimal montantPenalite) {
        this.montantPenalite = montantPenalite;
    }

    public String getMedMere() {
        return medMere;
    }

    public void setMedMere(String medMere) {
        this.medMere = medMere;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Med)) {
            return false;
        }
        Med other = (Med) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Med[ id=" + id + " ]";
    }

}
