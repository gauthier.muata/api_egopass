/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_DETAILS_AMR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailsAmr.findAll", query = "SELECT d FROM DetailsAmr d"),
    @NamedQuery(name = "DetailsAmr.findById", query = "SELECT d FROM DetailsAmr d WHERE d.id = :id"),
    @NamedQuery(name = "DetailsAmr.findByTaux", query = "SELECT d FROM DetailsAmr d WHERE d.taux = :taux"),
    @NamedQuery(name = "DetailsAmr.findByMontantDu", query = "SELECT d FROM DetailsAmr d WHERE d.montantDu = :montantDu"),
    @NamedQuery(name = "DetailsAmr.findByEtat", query = "SELECT d FROM DetailsAmr d WHERE d.etat = :etat")})
public class DetailsAmr implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TAUX")
    private BigDecimal taux;
    @Column(name = "MONTANT_DU")
    private BigDecimal montantDu;
    @Column(name = "ETAT")
    private Boolean etat;
    @JoinColumn(name = "AMR", referencedColumnName = "NUMERO")
    @ManyToOne
    private Amr amr;
    @JoinColumn(name = "DETAILS_NC", referencedColumnName = "ID")
    @ManyToOne
    private DetailsNc detailsNc;
    @JoinColumn(name = "PENALITE", referencedColumnName = "CODE")
    @ManyToOne
    private Penalite penalite;

    public DetailsAmr() {
    }

    public DetailsAmr(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public BigDecimal getMontantDu() {
        return montantDu;
    }

    public void setMontantDu(BigDecimal montantDu) {
        this.montantDu = montantDu;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Amr getAmr() {
        return amr;
    }

    public void setAmr(Amr amr) {
        this.amr = amr;
    }

    public DetailsNc getDetailsNc() {
        return detailsNc;
    }

    public void setDetailsNc(DetailsNc detailsNc) {
        this.detailsNc = detailsNc;
    }

    public Penalite getPenalite() {
        return penalite;
    }

    public void setPenalite(Penalite penalite) {
        this.penalite = penalite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailsAmr)) {
            return false;
        }
        DetailsAmr other = (DetailsAmr) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.DetailsAmr[ id=" + id + " ]";
    }
    
}
