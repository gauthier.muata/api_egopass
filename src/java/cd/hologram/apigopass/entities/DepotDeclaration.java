/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_DEPOT_DECLARATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DepotDeclaration.findAll", query = "SELECT d FROM DepotDeclaration d"),
    @NamedQuery(name = "DepotDeclaration.findByCode", query = "SELECT d FROM DepotDeclaration d WHERE d.code = :code"),
    @NamedQuery(name = "DepotDeclaration.findByNumeroDepot", query = "SELECT d FROM DepotDeclaration d WHERE d.numeroDepot = :numeroDepot"),
    @NamedQuery(name = "DepotDeclaration.findByBordereau", query = "SELECT d FROM DepotDeclaration d WHERE d.bordereau = :bordereau"),
    @NamedQuery(name = "DepotDeclaration.findByDateCreat", query = "SELECT d FROM DepotDeclaration d WHERE d.dateCreat = :dateCreat"),
    @NamedQuery(name = "DepotDeclaration.findByNbrImpression", query = "SELECT d FROM DepotDeclaration d WHERE d.nbrImpression = :nbrImpression"),
    @NamedQuery(name = "DepotDeclaration.findByEtat", query = "SELECT d FROM DepotDeclaration d WHERE d.etat = :etat"),
    @NamedQuery(name = "DepotDeclaration.findByNumeroAttestationPaiement", query = "SELECT d FROM DepotDeclaration d WHERE d.numeroAttestationPaiement = :numeroAttestationPaiement")})
public class DepotDeclaration implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 25)
    @Column(name = "NUMERO_DEPOT")
    private String numeroDepot;
    @Column(name = "BORDEREAU")
    private BigInteger bordereau;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Column(name = "NBR_IMPRESSION")
    private Integer nbrImpression;
    @Column(name = "ETAT")
    private Boolean etat;
    @Size(max = 50)
    @Column(name = "NUMERO_ATTESTATION_PAIEMENT")
    private String numeroAttestationPaiement;
    @JoinColumn(name = "AGENT", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agent;
    @JoinColumn(name = "PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne personne;
    @JoinColumn(name = "SERVICE", referencedColumnName = "CODE")
    @ManyToOne
    private Service service;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "depotDeclaration")
    private List<DetailDepotDeclaration> detailDepotDeclarationList;
    @OneToMany(mappedBy = "depotDeclaration")
    private List<NoteCalcul> noteCalculList;

    public DepotDeclaration() {
    }

    public DepotDeclaration(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNumeroDepot() {
        return numeroDepot;
    }

    public void setNumeroDepot(String numeroDepot) {
        this.numeroDepot = numeroDepot;
    }

    public BigInteger getBordereau() {
        return bordereau;
    }

    public void setBordereau(BigInteger bordereau) {
        this.bordereau = bordereau;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Integer getNbrImpression() {
        return nbrImpression;
    }

    public void setNbrImpression(Integer nbrImpression) {
        this.nbrImpression = nbrImpression;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getNumeroAttestationPaiement() {
        return numeroAttestationPaiement;
    }

    public void setNumeroAttestationPaiement(String numeroAttestationPaiement) {
        this.numeroAttestationPaiement = numeroAttestationPaiement;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @XmlTransient
    public List<DetailDepotDeclaration> getDetailDepotDeclarationList() {
        return detailDepotDeclarationList;
    }

    public void setDetailDepotDeclarationList(List<DetailDepotDeclaration> detailDepotDeclarationList) {
        this.detailDepotDeclarationList = detailDepotDeclarationList;
    }

    @XmlTransient
    public List<NoteCalcul> getNoteCalculList() {
        return noteCalculList;
    }

    public void setNoteCalculList(List<NoteCalcul> noteCalculList) {
        this.noteCalculList = noteCalculList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DepotDeclaration)) {
            return false;
        }
        DepotDeclaration other = (DepotDeclaration) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.DepotDeclaration[ code=" + code + " ]";
    }
    
}
