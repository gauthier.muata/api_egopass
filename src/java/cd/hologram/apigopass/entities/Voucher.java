/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrateur
 */
@Entity
@Cacheable(false)
@Table(name = "T_VOUCHER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Voucher.findAll", query = "SELECT v FROM Voucher v"),
    @NamedQuery(name = "Voucher.findById", query = "SELECT v FROM Voucher v WHERE v.id = :id"),
    @NamedQuery(name = "Voucher.findByQte", query = "SELECT v FROM Voucher v WHERE v.qte = :qte"),
    @NamedQuery(name = "Voucher.findByPrixUnitaire", query = "SELECT v FROM Voucher v WHERE v.prixUnitaire = :prixUnitaire"),
    @NamedQuery(name = "Voucher.findByMontant", query = "SELECT v FROM Voucher v WHERE v.montant = :montant"),
    @NamedQuery(name = "Voucher.findByRemise", query = "SELECT v FROM Voucher v WHERE v.remise = :remise"),
    @NamedQuery(name = "Voucher.findByMontantFinal", query = "SELECT v FROM Voucher v WHERE v.montantFinal = :montantFinal"),
    @NamedQuery(name = "Voucher.findByEtat", query = "SELECT v FROM Voucher v WHERE v.etat = :etat"),
    @NamedQuery(name = "Voucher.findByNewId", query = "SELECT v FROM Voucher v WHERE v.newId = :newId")})
public class Voucher implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "QTE")
    private Integer qte;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PRIX_UNITAIRE")
    private BigDecimal prixUnitaire;
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Column(name = "REMISE")
    private BigDecimal remise;
    @Column(name = "MONTANT_FINAL")
    private BigDecimal montantFinal;
    @Column(name = "ETAT")
    private Integer etat;
//    @OneToMany(mappedBy = "fkVaucher")
//    private List<DetailVoucher> detailVoucherList;
    @JoinColumn(name = "AGENT_MAJ", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentMaj;
    @JoinColumn(name = "FK_COMMANDE", referencedColumnName = "ID")
    @ManyToOne
    private Commande fkCommande;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;
    @JoinColumn(name = "FK_SITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site fkSite;
    @JoinColumn(name = "FK_TARIF", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif fkTarif;
    @JoinColumn(name = "FK_AXE", referencedColumnName = "ID")
    @ManyToOne
    private Axe fkAxe;
    @Size(max = 50)
    @Column(name = "NEW_ID")
    private String newId;

    public Voucher() {
    }

    public Voucher(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQte() {
        return qte;
    }

    public void setQte(Integer qte) {
        this.qte = qte;
    }

    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public BigDecimal getRemise() {
        return remise;
    }

    public void setRemise(BigDecimal remise) {
        this.remise = remise;
    }

    public BigDecimal getMontantFinal() {
        return montantFinal;
    }

    public void setMontantFinal(BigDecimal montantFinal) {
        this.montantFinal = montantFinal;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

//    @XmlTransient
//    public List<DetailVoucher> getDetailVoucherList() {
//        return detailVoucherList;
//    }
//
//    public void setDetailVoucherList(List<DetailVoucher> detailVoucherList) {
//        this.detailVoucherList = detailVoucherList;
//    }

    public Agent getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Agent agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Commande getFkCommande() {
        return fkCommande;
    }

    public void setFkCommande(Commande fkCommande) {
        this.fkCommande = fkCommande;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public Site getFkSite() {
        return fkSite;
    }

    public void setFkSite(Site fkSite) {
        this.fkSite = fkSite;
    }

    public Tarif getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(Tarif fkTarif) {
        this.fkTarif = fkTarif;
    }

    public Axe getFkAxe() {
        return fkAxe;
    }

    public void setFkAxe(Axe fkAxe) {
        this.fkAxe = fkAxe;
    }

    public String getNewId() {
        return newId;
    }

    public void setNewId(String newId) {
        this.newId = newId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Voucher)) {
            return false;
        }
        Voucher other = (Voucher) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entites.Voucher[ id=" + id + " ]";
    }

}
