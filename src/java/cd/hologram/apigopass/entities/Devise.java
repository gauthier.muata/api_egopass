/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_DEVISE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Devise.findAll", query = "SELECT d FROM Devise d"),
    @NamedQuery(name = "Devise.findByCode", query = "SELECT d FROM Devise d WHERE d.code = :code"),
    @NamedQuery(name = "Devise.findByIntitule", query = "SELECT d FROM Devise d WHERE d.intitule = :intitule"),
    @NamedQuery(name = "Devise.findByParDefaut", query = "SELECT d FROM Devise d WHERE d.parDefaut = :parDefaut"),
    @NamedQuery(name = "Devise.findByEtat", query = "SELECT d FROM Devise d WHERE d.etat = :etat"),
    @NamedQuery(name = "Devise.findByAgentCreat", query = "SELECT d FROM Devise d WHERE d.agentCreat = :agentCreat"),
    @NamedQuery(name = "Devise.findByDateCreat", query = "SELECT d FROM Devise d WHERE d.dateCreat = :dateCreat"),
    @NamedQuery(name = "Devise.findByAgentMaj", query = "SELECT d FROM Devise d WHERE d.agentMaj = :agentMaj"),
    @NamedQuery(name = "Devise.findByDateMaj", query = "SELECT d FROM Devise d WHERE d.dateMaj = :dateMaj")})
public class Devise implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "CODE")
    private String code;
    @Size(max = 50)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "PAR_DEFAUT")
    private Short parDefaut;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @OneToMany(mappedBy = "devise")
    private List<CompteBancaire> compteBancaireList;
    @OneToMany(mappedBy = "devise")
    private List<Journal> journalList;
    @OneToMany(mappedBy = "devise")
    private List<Taux> tauxList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "devise")
    private List<DetailDepotDeclaration> detailDepotDeclarationList;
    @OneToMany(mappedBy = "devise")
    private List<NotePerception> notePerceptionList;
    @OneToMany(mappedBy = "devise")
    private List<DetailsNc> detailsNcList;

    public Devise() {
    }

    public Devise(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Short getParDefaut() {
        return parDefaut;
    }

    public void setParDefaut(Short parDefaut) {
        this.parDefaut = parDefaut;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    @XmlTransient
    public List<CompteBancaire> getCompteBancaireList() {
        return compteBancaireList;
    }

    public void setCompteBancaireList(List<CompteBancaire> compteBancaireList) {
        this.compteBancaireList = compteBancaireList;
    }

    @XmlTransient
    public List<Journal> getJournalList() {
        return journalList;
    }

    public void setJournalList(List<Journal> journalList) {
        this.journalList = journalList;
    }

    @XmlTransient
    public List<Taux> getTauxList() {
        return tauxList;
    }

    public void setTauxList(List<Taux> tauxList) {
        this.tauxList = tauxList;
    }

    @XmlTransient
    public List<DetailDepotDeclaration> getDetailDepotDeclarationList() {
        return detailDepotDeclarationList;
    }

    public void setDetailDepotDeclarationList(List<DetailDepotDeclaration> detailDepotDeclarationList) {
        this.detailDepotDeclarationList = detailDepotDeclarationList;
    }

    @XmlTransient
    public List<NotePerception> getNotePerceptionList() {
        return notePerceptionList;
    }

    public void setNotePerceptionList(List<NotePerception> notePerceptionList) {
        this.notePerceptionList = notePerceptionList;
    }

    @XmlTransient
    public List<DetailsNc> getDetailsNcList() {
        return detailsNcList;
    }

    public void setDetailsNcList(List<DetailsNc> detailsNcList) {
        this.detailsNcList = detailsNcList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Devise)) {
            return false;
        }
        Devise other = (Devise) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Devise[ code=" + code + " ]";
    }
    
}
