/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PARAM_CONTRAINTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ParamContrainte.findAll", query = "SELECT p FROM ParamContrainte p"),
    @NamedQuery(name = "ParamContrainte.findById", query = "SELECT p FROM ParamContrainte p WHERE p.id = :id"),
    @NamedQuery(name = "ParamContrainte.findByPourcentageInteretMoratoire", query = "SELECT p FROM ParamContrainte p WHERE p.pourcentageInteretMoratoire = :pourcentageInteretMoratoire"),
    @NamedQuery(name = "ParamContrainte.findByPourcentageFraisPoursuiteCtrCmdt", query = "SELECT p FROM ParamContrainte p WHERE p.pourcentageFraisPoursuiteCtrCmdt = :pourcentageFraisPoursuiteCtrCmdt"),
    @NamedQuery(name = "ParamContrainte.findByPourcentagePenalite", query = "SELECT p FROM ParamContrainte p WHERE p.pourcentagePenalite = :pourcentagePenalite"),
    @NamedQuery(name = "ParamContrainte.findByRefFaitGenerateur", query = "SELECT p FROM ParamContrainte p WHERE p.refFaitGenerateur = :refFaitGenerateur"),
    @NamedQuery(name = "ParamContrainte.findByAgentCmdt", query = "SELECT p FROM ParamContrainte p WHERE p.agentCmdt = :agentCmdt"),
    @NamedQuery(name = "ParamContrainte.findByAgentSignataire", query = "SELECT p FROM ParamContrainte p WHERE p.agentSignataire = :agentSignataire"),
    @NamedQuery(name = "ParamContrainte.findByComptePenalite", query = "SELECT p FROM ParamContrainte p WHERE p.comptePenalite = :comptePenalite"),
    @NamedQuery(name = "ParamContrainte.findByCompteFonctionnement", query = "SELECT p FROM ParamContrainte p WHERE p.compteFonctionnement = :compteFonctionnement"),
    @NamedQuery(name = "ParamContrainte.findByEtat", query = "SELECT p FROM ParamContrainte p WHERE p.etat = :etat"),
    @NamedQuery(name = "ParamContrainte.findByAgentCreat", query = "SELECT p FROM ParamContrainte p WHERE p.agentCreat = :agentCreat"),
    @NamedQuery(name = "ParamContrainte.findByDateCreat", query = "SELECT p FROM ParamContrainte p WHERE p.dateCreat = :dateCreat"),
    @NamedQuery(name = "ParamContrainte.findByAgentMaj", query = "SELECT p FROM ParamContrainte p WHERE p.agentMaj = :agentMaj"),
    @NamedQuery(name = "ParamContrainte.findByDateMaj", query = "SELECT p FROM ParamContrainte p WHERE p.dateMaj = :dateMaj"),
    @NamedQuery(name = "ParamContrainte.findByFraisPoursuiteAtd", query = "SELECT p FROM ParamContrainte p WHERE p.fraisPoursuiteAtd = :fraisPoursuiteAtd"),
    @NamedQuery(name = "ParamContrainte.findByCompteTresorUrbain", query = "SELECT p FROM ParamContrainte p WHERE p.compteTresorUrbain = :compteTresorUrbain")})
public class ParamContrainte implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "POURCENTAGE_INTERET_MORATOIRE")
    private BigDecimal pourcentageInteretMoratoire;
    @Column(name = "POURCENTAGE_FRAIS_POURSUITE_CTR_CMDT")
    private BigDecimal pourcentageFraisPoursuiteCtrCmdt;
    @Column(name = "POURCENTAGE_PENALITE")
    private BigDecimal pourcentagePenalite;
    @Size(max = 50)
    @Column(name = "REF_FAIT_GENERATEUR")
    private String refFaitGenerateur;
    @Size(max = 25)
    @Column(name = "AGENT_CMDT")
    private String agentCmdt;
    @Size(max = 25)
    @Column(name = "AGENT_SIGNATAIRE")
    private String agentSignataire;
    @Size(max = 100)
    @Column(name = "COMPTE_PENALITE")
    private String comptePenalite;
    @Size(max = 100)
    @Column(name = "COMPTE_FONCTIONNEMENT")
    private String compteFonctionnement;
    @Column(name = "ETAT")
    private Boolean etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "FRAIS_POURSUITE_ATD")
    private BigDecimal fraisPoursuiteAtd;
    @Size(max = 100)
    @Column(name = "COMPTE_TRESOR_URBAIN")
    private String compteTresorUrbain;
    @OneToMany(mappedBy = "idParam")
    private List<Contrainte> contrainteList;

    public ParamContrainte() {
    }

    public ParamContrainte(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getPourcentageInteretMoratoire() {
        return pourcentageInteretMoratoire;
    }

    public void setPourcentageInteretMoratoire(BigDecimal pourcentageInteretMoratoire) {
        this.pourcentageInteretMoratoire = pourcentageInteretMoratoire;
    }

    public BigDecimal getPourcentageFraisPoursuiteCtrCmdt() {
        return pourcentageFraisPoursuiteCtrCmdt;
    }

    public void setPourcentageFraisPoursuiteCtrCmdt(BigDecimal pourcentageFraisPoursuiteCtrCmdt) {
        this.pourcentageFraisPoursuiteCtrCmdt = pourcentageFraisPoursuiteCtrCmdt;
    }

    public BigDecimal getPourcentagePenalite() {
        return pourcentagePenalite;
    }

    public void setPourcentagePenalite(BigDecimal pourcentagePenalite) {
        this.pourcentagePenalite = pourcentagePenalite;
    }

    public String getRefFaitGenerateur() {
        return refFaitGenerateur;
    }

    public void setRefFaitGenerateur(String refFaitGenerateur) {
        this.refFaitGenerateur = refFaitGenerateur;
    }

    public String getAgentCmdt() {
        return agentCmdt;
    }

    public void setAgentCmdt(String agentCmdt) {
        this.agentCmdt = agentCmdt;
    }

    public String getAgentSignataire() {
        return agentSignataire;
    }

    public void setAgentSignataire(String agentSignataire) {
        this.agentSignataire = agentSignataire;
    }

    public String getComptePenalite() {
        return comptePenalite;
    }

    public void setComptePenalite(String comptePenalite) {
        this.comptePenalite = comptePenalite;
    }

    public String getCompteFonctionnement() {
        return compteFonctionnement;
    }

    public void setCompteFonctionnement(String compteFonctionnement) {
        this.compteFonctionnement = compteFonctionnement;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public BigDecimal getFraisPoursuiteAtd() {
        return fraisPoursuiteAtd;
    }

    public void setFraisPoursuiteAtd(BigDecimal fraisPoursuiteAtd) {
        this.fraisPoursuiteAtd = fraisPoursuiteAtd;
    }

    public String getCompteTresorUrbain() {
        return compteTresorUrbain;
    }

    public void setCompteTresorUrbain(String compteTresorUrbain) {
        this.compteTresorUrbain = compteTresorUrbain;
    }

    @XmlTransient
    public List<Contrainte> getContrainteList() {
        return contrainteList;
    }

    public void setContrainteList(List<Contrainte> contrainteList) {
        this.contrainteList = contrainteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParamContrainte)) {
            return false;
        }
        ParamContrainte other = (ParamContrainte) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.ParamContrainte[ id=" + id + " ]";
    }
    
}
