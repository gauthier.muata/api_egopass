/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_ARCHIVE_ACCUSE_RECEPTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArchiveAccuseReception.findAll", query = "SELECT a FROM ArchiveAccuseReception a"),
    @NamedQuery(name = "ArchiveAccuseReception.findById", query = "SELECT a FROM ArchiveAccuseReception a WHERE a.id = :id"),
    @NamedQuery(name = "ArchiveAccuseReception.findByDocumentReference", query = "SELECT a FROM ArchiveAccuseReception a WHERE a.documentReference = :documentReference"),
    @NamedQuery(name = "ArchiveAccuseReception.findByTypeDocument", query = "SELECT a FROM ArchiveAccuseReception a WHERE a.typeDocument = :typeDocument"),
    @NamedQuery(name = "ArchiveAccuseReception.findByDatecreat", query = "SELECT a FROM ArchiveAccuseReception a WHERE a.datecreat = :datecreat"),
    @NamedQuery(name = "ArchiveAccuseReception.findByEtat", query = "SELECT a FROM ArchiveAccuseReception a WHERE a.etat = :etat"),
    @NamedQuery(name = "ArchiveAccuseReception.findByFkDocument", query = "SELECT a FROM ArchiveAccuseReception a WHERE a.fkDocument = :fkDocument")})
public class ArchiveAccuseReception implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "DOCUMENT_REFERENCE")
    private String documentReference;
    @Size(max = 50)
    @Column(name = "TYPE_DOCUMENT")
    private String typeDocument;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ARCHIVE")
    private String archive;
    @Column(name = "DATECREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreat;
    @Column(name = "ETAT")
    private Integer etat;
    @Size(max = 25)
    @Column(name = "FK_DOCUMENT")
    private String fkDocument;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "OBSERVATION")
    private String observation;

    public ArchiveAccuseReception() {
    }

    public ArchiveAccuseReception(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDocumentReference() {
        return documentReference;
    }

    public void setDocumentReference(String documentReference) {
        this.documentReference = documentReference;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public String getArchive() {
        return archive;
    }

    public void setArchive(String archive) {
        this.archive = archive;
    }

    public Date getDatecreat() {
        return datecreat;
    }

    public void setDatecreat(Date datecreat) {
        this.datecreat = datecreat;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getFkDocument() {
        return fkDocument;
    }

    public void setFkDocument(String fkDocument) {
        this.fkDocument = fkDocument;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArchiveAccuseReception)) {
            return false;
        }
        ArchiveAccuseReception other = (ArchiveAccuseReception) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.ArchiveAccuseReception[ id=" + id + " ]";
    }
    
}
