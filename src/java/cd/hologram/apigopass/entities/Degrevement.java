/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_DEGREVEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Degrevement.findAll", query = "SELECT d FROM Degrevement d"),
    @NamedQuery(name = "Degrevement.findByCode", query = "SELECT d FROM Degrevement d WHERE d.code = :code"),
    @NamedQuery(name = "Degrevement.findByDossier", query = "SELECT d FROM Degrevement d WHERE d.dossier = :dossier"),
    @NamedQuery(name = "Degrevement.findByTypeDegrevement", query = "SELECT d FROM Degrevement d WHERE d.typeDegrevement = :typeDegrevement"),
    @NamedQuery(name = "Degrevement.findByMontantDegreve", query = "SELECT d FROM Degrevement d WHERE d.montantDegreve = :montantDegreve"),
    @NamedQuery(name = "Degrevement.findByMontantPaye", query = "SELECT d FROM Degrevement d WHERE d.montantPaye = :montantPaye")})
public class Degrevement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Long code;
    @Size(max = 25)
    @Column(name = "DOSSIER")
    private String dossier;
    @Size(max = 1)
    @Column(name = "TYPE_DEGREVEMENT")
    private String typeDegrevement;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT_DEGREVE")
    private BigDecimal montantDegreve;
    @Column(name = "MONTANT_PAYE")
    private BigDecimal montantPaye;
    @JoinColumn(name = "BON_A_PAYER", referencedColumnName = "CODE")
    @ManyToOne
    private BonAPayer bonAPayer;

    public Degrevement() {
    }

    public Degrevement(Long code) {
        this.code = code;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getDossier() {
        return dossier;
    }

    public void setDossier(String dossier) {
        this.dossier = dossier;
    }

    public String getTypeDegrevement() {
        return typeDegrevement;
    }

    public void setTypeDegrevement(String typeDegrevement) {
        this.typeDegrevement = typeDegrevement;
    }

    public BigDecimal getMontantDegreve() {
        return montantDegreve;
    }

    public void setMontantDegreve(BigDecimal montantDegreve) {
        this.montantDegreve = montantDegreve;
    }

    public BigDecimal getMontantPaye() {
        return montantPaye;
    }

    public void setMontantPaye(BigDecimal montantPaye) {
        this.montantPaye = montantPaye;
    }

    public BonAPayer getBonAPayer() {
        return bonAPayer;
    }

    public void setBonAPayer(BonAPayer bonAPayer) {
        this.bonAPayer = bonAPayer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Degrevement)) {
            return false;
        }
        Degrevement other = (Degrevement) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Degrevement[ code=" + code + " ]";
    }
    
}
