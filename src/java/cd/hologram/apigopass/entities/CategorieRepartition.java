/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_CATEGORIE_REPARTITION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategorieRepartition.findAll", query = "SELECT c FROM CategorieRepartition c"),
    @NamedQuery(name = "CategorieRepartition.findByCode", query = "SELECT c FROM CategorieRepartition c WHERE c.code = :code"),
    @NamedQuery(name = "CategorieRepartition.findByIntitule", query = "SELECT c FROM CategorieRepartition c WHERE c.intitule = :intitule")})
public class CategorieRepartition implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "INTITULE")
    private String intitule;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categorieRepartition")
    private List<Retribution> retributionList;

    public CategorieRepartition() {
    }

    public CategorieRepartition(String code) {
        this.code = code;
    }

    public CategorieRepartition(String code, String intitule) {
        this.code = code;
        this.intitule = intitule;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    @XmlTransient
    public List<Retribution> getRetributionList() {
        return retributionList;
    }

    public void setRetributionList(List<Retribution> retributionList) {
        this.retributionList = retributionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategorieRepartition)) {
            return false;
        }
        CategorieRepartition other = (CategorieRepartition) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.CategorieRepartition[ code=" + code + " ]";
    }
    
}
