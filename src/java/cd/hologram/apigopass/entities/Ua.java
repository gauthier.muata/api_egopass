/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_UA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ua.findAll", query = "SELECT u FROM Ua u"),
    @NamedQuery(name = "Ua.findByCode", query = "SELECT u FROM Ua u WHERE u.code = :code"),
    @NamedQuery(name = "Ua.findByIntitule", query = "SELECT u FROM Ua u WHERE u.intitule = :intitule"),
    @NamedQuery(name = "Ua.findByAgentCreat", query = "SELECT u FROM Ua u WHERE u.agentCreat = :agentCreat"),
    @NamedQuery(name = "Ua.findByDateCreat", query = "SELECT u FROM Ua u WHERE u.dateCreat = :dateCreat"),
    @NamedQuery(name = "Ua.findByAgentMaj", query = "SELECT u FROM Ua u WHERE u.agentMaj = :agentMaj"),
    @NamedQuery(name = "Ua.findByDateMaj", query = "SELECT u FROM Ua u WHERE u.dateMaj = :dateMaj"),
    @NamedQuery(name = "Ua.findByEtat", query = "SELECT u FROM Ua u WHERE u.etat = :etat"),
    @NamedQuery(name = "Ua.findByMail", query = "SELECT u FROM Ua u WHERE u.mail = :mail")})
public class Ua implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODE")
    private String code;
    @Size(max = 150)
    @Column(name = "INTITULE")
    private String intitule;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 200)
    @Column(name = "MAIL")
    private String mail;
    @OneToMany(mappedBy = "ua")
    private List<Agent> agentList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ua")
    private List<UaSite> uaSiteList;

    public Ua() {
    }

    public Ua(String code) {
        this.code = code;
    }

    public Ua(String code, Date dateMaj) {
        this.code = code;
        this.dateMaj = dateMaj;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @XmlTransient
    public List<Agent> getAgentList() {
        return agentList;
    }

    public void setAgentList(List<Agent> agentList) {
        this.agentList = agentList;
    }

    @XmlTransient
    public List<UaSite> getUaSiteList() {
        return uaSiteList;
    }

    public void setUaSiteList(List<UaSite> uaSiteList) {
        this.uaSiteList = uaSiteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ua)) {
            return false;
        }
        Ua other = (Ua) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Ua[ code=" + code + " ]";
    }
    
}
