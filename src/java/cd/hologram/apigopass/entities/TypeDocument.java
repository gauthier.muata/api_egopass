/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_TYPE_DOCUMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeDocument.findAll", query = "SELECT t FROM TypeDocument t"),
    @NamedQuery(name = "TypeDocument.findByCode", query = "SELECT t FROM TypeDocument t WHERE t.code = :code"),
    @NamedQuery(name = "TypeDocument.findByIntitule", query = "SELECT t FROM TypeDocument t WHERE t.intitule = :intitule"),
    @NamedQuery(name = "TypeDocument.findByFenetre", query = "SELECT t FROM TypeDocument t WHERE t.fenetre = :fenetre"),
    @NamedQuery(name = "TypeDocument.findByEtat", query = "SELECT t FROM TypeDocument t WHERE t.etat = :etat"),
    @NamedQuery(name = "TypeDocument.findByEcheance", query = "SELECT t FROM TypeDocument t WHERE t.echeance = :echeance"),
    @NamedQuery(name = "TypeDocument.findByEditable", query = "SELECT t FROM TypeDocument t WHERE t.editable = :editable"),
    @NamedQuery(name = "TypeDocument.findByHtmlPrint", query = "SELECT t FROM TypeDocument t WHERE t.htmlPrint = :htmlPrint"),
    @NamedQuery(name = "TypeDocument.findByControlSolde", query = "SELECT t FROM TypeDocument t WHERE t.controlSolde = :controlSolde")})
public class TypeDocument implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CODE")
    private String code;
    @Size(max = 100)
    @Column(name = "INTITULE")
    private String intitule;
    @Size(max = 20)
    @Column(name = "FENETRE")
    private String fenetre;
    @Column(name = "ETAT")
    private Short etat;
    @Column(name = "ECHEANCE")
    private Integer echeance;
    @Column(name = "EDITABLE")
    private Boolean editable;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ANCHOR")
    private String anchor;
    @Column(name = "HTML_PRINT")
    private Boolean htmlPrint;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ANCHOR_STYLE")
    private String anchorStyle;
    @Column(name = "CONTROL_SOLDE")
    private Boolean controlSolde;

    public TypeDocument() {
    }

    public TypeDocument(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getFenetre() {
        return fenetre;
    }

    public void setFenetre(String fenetre) {
        this.fenetre = fenetre;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public Integer getEcheance() {
        return echeance;
    }

    public void setEcheance(Integer echeance) {
        this.echeance = echeance;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public String getAnchor() {
        return anchor;
    }

    public void setAnchor(String anchor) {
        this.anchor = anchor;
    }

    public Boolean getHtmlPrint() {
        return htmlPrint;
    }

    public void setHtmlPrint(Boolean htmlPrint) {
        this.htmlPrint = htmlPrint;
    }

    public String getAnchorStyle() {
        return anchorStyle;
    }

    public void setAnchorStyle(String anchorStyle) {
        this.anchorStyle = anchorStyle;
    }

    public Boolean getControlSolde() {
        return controlSolde;
    }

    public void setControlSolde(Boolean controlSolde) {
        this.controlSolde = controlSolde;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeDocument)) {
            return false;
        }
        TypeDocument other = (TypeDocument) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.TypeDocument[ code=" + code + " ]";
    }
    
}
