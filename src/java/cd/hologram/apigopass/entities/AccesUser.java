/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_ACCES_USER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccesUser.findAll", query = "SELECT a FROM AccesUser a"),
    @NamedQuery(name = "AccesUser.findById", query = "SELECT a FROM AccesUser a WHERE a.id = :id"),
    @NamedQuery(name = "AccesUser.findByAgent", query = "SELECT a FROM AccesUser a WHERE a.agent = :agent"),
    @NamedQuery(name = "AccesUser.findByDroit", query = "SELECT a FROM AccesUser a WHERE a.droit = :droit"),
    @NamedQuery(name = "AccesUser.findByAgentCreat", query = "SELECT a FROM AccesUser a WHERE a.agentCreat = :agentCreat"),
    @NamedQuery(name = "AccesUser.findByDateCreat", query = "SELECT a FROM AccesUser a WHERE a.dateCreat = :dateCreat"),
    @NamedQuery(name = "AccesUser.findByAgentMaj", query = "SELECT a FROM AccesUser a WHERE a.agentMaj = :agentMaj"),
    @NamedQuery(name = "AccesUser.findByDateMaj", query = "SELECT a FROM AccesUser a WHERE a.dateMaj = :dateMaj"),
    @NamedQuery(name = "AccesUser.findByEtat", query = "SELECT a FROM AccesUser a WHERE a.etat = :etat")})
public class AccesUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 20)
    @Column(name = "AGENT")
    private String agent;
    @Size(max = 50)
    @Column(name = "DROIT")
    private String droit;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ETAT")
    private short etat;

    public AccesUser() {
    }

    public AccesUser(Integer id) {
        this.id = id;
    }

    public AccesUser(Integer id, short etat) {
        this.id = id;
        this.etat = etat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getDroit() {
        return droit;
    }

    public void setDroit(String droit) {
        this.droit = droit;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public short getEtat() {
        return etat;
    }

    public void setEtat(short etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccesUser)) {
            return false;
        }
        AccesUser other = (AccesUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.AccesUser[ id=" + id + " ]";
    }
    
}
