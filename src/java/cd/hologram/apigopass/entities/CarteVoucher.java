/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juslin TSHIAMUA
 */
@Entity
@Cacheable(false)
@Table(name = "T_CARTE_VOUCHER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CarteVoucher.findAll", query = "SELECT c FROM CarteVoucher c"),
    @NamedQuery(name = "CarteVoucher.findById", query = "SELECT c FROM CarteVoucher c WHERE c.id = :id"),
    @NamedQuery(name = "CarteVoucher.findByNumeroCarte", query = "SELECT c FROM CarteVoucher c WHERE c.numeroCarte = :numeroCarte"),
    @NamedQuery(name = "CarteVoucher.findByObservation", query = "SELECT c FROM CarteVoucher c WHERE c.observation = :observation"),
    @NamedQuery(name = "CarteVoucher.findByEtat", query = "SELECT c FROM CarteVoucher c WHERE c.etat = :etat")})
public class CarteVoucher implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 25)
    @Column(name = "NUMERO_CARTE")
    private String numeroCarte;
    @Size(max = 50)
    @Column(name = "OBSERVATION")
    private String observation;
    @Column(name = "ETAT")
    private Integer etat;
    @JoinColumn(name = "FK_COMMANDE_CARTE", referencedColumnName = "ID")
    @ManyToOne
    private CommandeCarte fkCommandeCarte;
    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne fkPersonne;
    @OneToMany(mappedBy = "fkCarte")
    private List<CarteDetailVoucher> carteDetailVoucherList;

    public CarteVoucher() {
    }

    public CarteVoucher(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumeroCarte() {
        return numeroCarte;
    }

    public void setNumeroCarte(String numeroCarte) {
        this.numeroCarte = numeroCarte;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public CommandeCarte getFkCommandeCarte() {
        return fkCommandeCarte;
    }

    public void setFkCommandeCarte(CommandeCarte fkCommandeCarte) {
        this.fkCommandeCarte = fkCommandeCarte;
    }

    public Personne getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(Personne fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    @XmlTransient
    public List<CarteDetailVoucher> getCarteDetailVoucherList() {
        return carteDetailVoucherList;
    }

    public void setCarteDetailVoucherList(List<CarteDetailVoucher> carteDetailVoucherList) {
        this.carteDetailVoucherList = carteDetailVoucherList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CarteVoucher)) {
            return false;
        }
        CarteVoucher other = (CarteVoucher) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.CarteVoucher[ id=" + id + " ]";
    }
    
}
