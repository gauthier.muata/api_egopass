/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Lenovo
 */
@Entity
@Table(name = "T_Type_Ea")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeEa.findAll", query = "SELECT t FROM TypeEa t"),
    @NamedQuery(name = "TypeEa.findByIdTypeEntite", query = "SELECT t FROM TypeEa t WHERE t.idTypeEntite = :idTypeEntite"),
    @NamedQuery(name = "TypeEa.findByEtat", query = "SELECT t FROM TypeEa t WHERE t.etat = :etat")})
public class TypeEa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "IdTypeEntite")
    private String idTypeEntite;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "IntituleTypeEntite")
    private String intituleTypeEntite;
    @Size(max = 50)
    @Column(name = "Etat")
    private String etat;
    @OneToMany(mappedBy = "fkTypeEntite")
    private List<Entite> entiteList;

    public TypeEa() {
    }

    public TypeEa(String idTypeEntite) {
        this.idTypeEntite = idTypeEntite;
    }

    public String getIdTypeEntite() {
        return idTypeEntite;
    }

    public void setIdTypeEntite(String idTypeEntite) {
        this.idTypeEntite = idTypeEntite;
    }

    public String getIntituleTypeEntite() {
        return intituleTypeEntite;
    }

    public void setIntituleTypeEntite(String intituleTypeEntite) {
        this.intituleTypeEntite = intituleTypeEntite;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<Entite> getEntiteList() {
        return entiteList;
    }

    public void setEntiteList(List<Entite> entiteList) {
        this.entiteList = entiteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTypeEntite != null ? idTypeEntite.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeEa)) {
            return false;
        }
        TypeEa other = (TypeEa) object;
        if ((this.idTypeEntite == null && other.idTypeEntite != null) || (this.idTypeEntite != null && !this.idTypeEntite.equals(other.idTypeEntite))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.TypeEa[ idTypeEntite=" + idTypeEntite + " ]";
    }
    
}
