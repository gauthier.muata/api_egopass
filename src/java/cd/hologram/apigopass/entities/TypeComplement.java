/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_TYPE_COMPLEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeComplement.findAll", query = "SELECT t FROM TypeComplement t"),
    @NamedQuery(name = "TypeComplement.findByCode", query = "SELECT t FROM TypeComplement t WHERE t.code = :code"),
    @NamedQuery(name = "TypeComplement.findByIntitule", query = "SELECT t FROM TypeComplement t WHERE t.intitule = :intitule"),
    @NamedQuery(name = "TypeComplement.findByType", query = "SELECT t FROM TypeComplement t WHERE t.type = :type"),
    @NamedQuery(name = "TypeComplement.findByEtat", query = "SELECT t FROM TypeComplement t WHERE t.etat = :etat"),
    @NamedQuery(name = "TypeComplement.findByObjetInteraction", query = "SELECT t FROM TypeComplement t WHERE t.objetInteraction = :objetInteraction"),
    @NamedQuery(name = "TypeComplement.findByTaxable", query = "SELECT t FROM TypeComplement t WHERE t.taxable = :taxable"),
    @NamedQuery(name = "TypeComplement.findByBaseVariable", query = "SELECT t FROM TypeComplement t WHERE t.baseVariable = :baseVariable"),
    @NamedQuery(name = "TypeComplement.findByObligatoire", query = "SELECT t FROM TypeComplement t WHERE t.obligatoire = :obligatoire"),
    @NamedQuery(name = "TypeComplement.findByProprietaire", query = "SELECT t FROM TypeComplement t WHERE t.proprietaire = :proprietaire"),
    @NamedQuery(name = "TypeComplement.findByValeurPredefinie", query = "SELECT t FROM TypeComplement t WHERE t.valeurPredefinie = :valeurPredefinie")})
public class TypeComplement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 75)
    @Column(name = "INTITULE")
    private String intitule;
    @Size(max = 10)
    @Column(name = "TYPE")
    private String type;
    @Column(name = "ETAT")
    private Boolean etat;
    @Size(max = 50)
    @Column(name = "OBJET_INTERACTION")
    private String objetInteraction;
    @Column(name = "TAXABLE")
    private Boolean taxable;
    @Column(name = "BASE_VARIABLE")
    private Boolean baseVariable;
    @Column(name = "OBLIGATOIRE")
    private Boolean obligatoire;
    @Column(name = "PROPRIETAIRE")
    private Boolean proprietaire;
    @Column(name = "VALEUR_PREDEFINIE")
    private Boolean valeurPredefinie;
    @JoinColumn(name = "FK_CATEGORIE_TYPE_COMPELEMENT", referencedColumnName = "CODE")
    @ManyToOne
    private CategorieTypeComplement fkCategorieTypeCompelement;
    @OneToMany(mappedBy = "fkTypeComplement")
    private List<ValeurPredefinie> valeurPredefinieList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "complement")
    private List<ComplementForme> complementFormeList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "complement")
    private List<TypeComplementBien> typeComplementBienList;

    public TypeComplement() {
    }

    public TypeComplement(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getObjetInteraction() {
        return objetInteraction;
    }

    public void setObjetInteraction(String objetInteraction) {
        this.objetInteraction = objetInteraction;
    }

    public Boolean getTaxable() {
        return taxable;
    }

    public void setTaxable(Boolean taxable) {
        this.taxable = taxable;
    }

    public Boolean getBaseVariable() {
        return baseVariable;
    }

    public void setBaseVariable(Boolean baseVariable) {
        this.baseVariable = baseVariable;
    }

    public Boolean getObligatoire() {
        return obligatoire;
    }

    public void setObligatoire(Boolean obligatoire) {
        this.obligatoire = obligatoire;
    }

    public Boolean getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Boolean proprietaire) {
        this.proprietaire = proprietaire;
    }

    public Boolean getValeurPredefinie() {
        return valeurPredefinie;
    }

    public void setValeurPredefinie(Boolean valeurPredefinie) {
        this.valeurPredefinie = valeurPredefinie;
    }

    public CategorieTypeComplement getFkCategorieTypeCompelement() {
        return fkCategorieTypeCompelement;
    }

    public void setFkCategorieTypeCompelement(CategorieTypeComplement fkCategorieTypeCompelement) {
        this.fkCategorieTypeCompelement = fkCategorieTypeCompelement;
    }

    @XmlTransient
    public List<ValeurPredefinie> getValeurPredefinieList() {
        return valeurPredefinieList;
    }

    public void setValeurPredefinieList(List<ValeurPredefinie> valeurPredefinieList) {
        this.valeurPredefinieList = valeurPredefinieList;
    }

    @XmlTransient
    public List<ComplementForme> getComplementFormeList() {
        return complementFormeList;
    }

    public void setComplementFormeList(List<ComplementForme> complementFormeList) {
        this.complementFormeList = complementFormeList;
    }

    @XmlTransient
    public List<TypeComplementBien> getTypeComplementBienList() {
        return typeComplementBienList;
    }

    public void setTypeComplementBienList(List<TypeComplementBien> typeComplementBienList) {
        this.typeComplementBienList = typeComplementBienList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeComplement)) {
            return false;
        }
        TypeComplement other = (TypeComplement) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.TypeComplement[ code=" + code + " ]";
    }
    
}
