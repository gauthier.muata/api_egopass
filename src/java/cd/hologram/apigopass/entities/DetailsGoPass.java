/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrateur
 */
@Entity
@Cacheable(false)
@Table(name = "T_DETAILS_GOPASSE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailsGoPass.findAll", query = "SELECT d FROM DetailsGoPass d"),
    @NamedQuery(name = "DetailsGoPass.findByIdDetails", query = "SELECT d FROM DetailsGoPass d WHERE d.idDetails = :idDetails"),
    @NamedQuery(name = "DetailsGoPass.findByPassager", query = "SELECT g FROM DetailsGoPass g WHERE g.passager = :passager"),
    @NamedQuery(name = "DetailsGoPass.findByPieceIdentite", query = "SELECT g FROM DetailsGoPass g WHERE g.pieceIdentite = :pieceIdentite"),
    @NamedQuery(name = "DetailsGoPass.findBySexePassager", query = "SELECT g FROM DetailsGoPass g WHERE g.sexePassager = :sexePassager"),
    @NamedQuery(name = "DetailsGoPass.findByCodeGoPass", query = "SELECT g FROM DetailsGoPass g WHERE g.codeGoPass = :codeGoPass"),
    @NamedQuery(name = "DetailsGoPass.findByDateGeneration", query = "SELECT g FROM DetailsGoPass g WHERE g.dateGeneration = :dateGeneration"),
    @NamedQuery(name = "DetailsGoPass.findByDateUtilisation", query = "SELECT g FROM DetailsGoPass g WHERE g.dateUtilisation = :dateUtilisation"),
    @NamedQuery(name = "DetailsGoPass.findByDateActivation", query = "SELECT g FROM DetailsGoPass g WHERE g.dateActivation = :dateActivation"),
    @NamedQuery(name = "DetailsGoPass.findByDateVol", query = "SELECT g FROM DetailsGoPass g WHERE g.dateVol = :dateVol"),
    @NamedQuery(name = "DetailsGoPass.findByEtat", query = "SELECT g FROM DetailsGoPass g WHERE g.etat = :etat"),
    @NamedQuery(name = "DetailsGoPass.findByNumeroVol", query = "SELECT g FROM DetailsGoPass g WHERE g.numeroVol = :numeroVol"),
    @NamedQuery(name = "DetailsGoPass.findByCodeTicketGenerate", query = "SELECT g FROM DetailsGoPass g WHERE g.codeTicketGenerate = :codeTicketGenerate")})
public class DetailsGoPass implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdDetails")
    private Integer idDetails;
    @Size(max = 250)
    @Column(name = "Passager")
    private String passager;
    @Size(max = 50)
    @Column(name = "PieceIdentite")
    private String pieceIdentite;
    @Size(max = 50)
    @Column(name = "CodeGoPass")
    private String codeGoPass;
    @Size(max = 50)
    @Column(name = "Sexe_Passager")
    private String sexePassager;
    @Column(name = "Date_Generation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateGeneration;
    @Column(name = "Date_Utilisation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUtilisation;
    @Column(name = "Date_Activation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateActivation;
    @Column(name = "Date_Vol")
    private String dateVol;
    @Column(name = "ETAT")
    private Integer etat;
    @Size(max = 50)
    @Column(name = "CodeTicket_Generate")
    private String codeTicketGenerate;
    @Size(max = 50)
    @Column(name = "Numero_Vol")
    private String numeroVol;
    @JoinColumn(name = "Fk_Gopasse", referencedColumnName = "IdGopasse")
    @ManyToOne
    private GoPass fkGopasse;
    @JoinColumn(name = "Agent_Creat", referencedColumnName = "CODE")
    @ManyToOne
    private Agent fkAgentCreat;
    @JoinColumn(name = "Agent_Utilisation", referencedColumnName = "CODE")
    @ManyToOne
    private Agent fkAgentUtilisation;
    @JoinColumn(name = "Fk_Compagnie", referencedColumnName = "IdCompagnie")
    @ManyToOne
    private Compagnie fkCompagnie;
    @JoinColumn(name = "Fk_Aeroport_Provenance", referencedColumnName = "IdAeroport")
    @ManyToOne
    private Aeroport fkAeroportProvenance;
    @JoinColumn(name = "Fk_Aeroport_Destination", referencedColumnName = "IdAeroport")
    @ManyToOne
    private Aeroport fkAeroportDestination;

    public DetailsGoPass() {

    }

    public Integer getIdDetails() {
        return idDetails;
    }

    public void setIdDetails(Integer idDetails) {
        this.idDetails = idDetails;
    }

    public String getPassager() {
        return passager;
    }

    public void setPassager(String passager) {
        this.passager = passager;
    }

    public String getPieceIdentite() {
        return pieceIdentite;
    }

    public void setPieceIdentite(String pieceIdentite) {
        this.pieceIdentite = pieceIdentite;
    }

    public String getSexePassager() {
        return sexePassager;
    }

    public void setSexePassager(String sexePassager) {
        this.sexePassager = sexePassager;
    }

    public String getCodeGoPass() {
        return codeGoPass;
    }

    public void setCodeGoPass(String codeGoPass) {
        this.codeGoPass = codeGoPass;
    }

    public Date getDateGeneration() {
        return dateGeneration;
    }

    public void setDateGeneration(Date dateGeneration) {
        this.dateGeneration = dateGeneration;
    }

    public Date getDateUtilisation() {
        return dateUtilisation;
    }

    public void setDateUtilisation(Date dateUtilisation) {
        this.dateUtilisation = dateUtilisation;
    }

    public Date getDateActivation() {
        return dateActivation;
    }

    public void setDateActivation(Date dateActivation) {
        this.dateActivation = dateActivation;
    }

    public String getDateVol() {
        return dateVol;
    }

    public void setDateVol(String dateVol) {
        this.dateVol = dateVol;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getCodeTicketGenerate() {
        return codeTicketGenerate;
    }

    public void setCodeTicketGenerate(String codeTicketGenerate) {
        this.codeTicketGenerate = codeTicketGenerate;
    }

    public GoPass getFkGopasse() {
        return fkGopasse;
    }

    public void setFkGopasse(GoPass fkGopasse) {
        this.fkGopasse = fkGopasse;
    }

    public Agent getFkAgentCreat() {
        return fkAgentCreat;
    }

    public void setFkAgentCreat(Agent fkAgentCreat) {
        this.fkAgentCreat = fkAgentCreat;
    }

    public Agent getFkAgentUtilisation() {
        return fkAgentUtilisation;
    }

    public void setFkAgentUtilisation(Agent fkAgentUtilisation) {
        this.fkAgentUtilisation = fkAgentUtilisation;
    }

    public Compagnie getFkCompagnie() {
        return fkCompagnie;
    }

    public void setFkCompagnie(Compagnie fkCompagnie) {
        this.fkCompagnie = fkCompagnie;
    }

    public Aeroport getFkAeroportProvenance() {
        return fkAeroportProvenance;
    }

    public void setFkAeroportProvenance(Aeroport fkAeroportProvenance) {
        this.fkAeroportProvenance = fkAeroportProvenance;
    }

    public Aeroport getFkAeroportDestination() {
        return fkAeroportDestination;
    }

    public void setFkAeroportDestination(Aeroport fkAeroportDestination) {
        this.fkAeroportDestination = fkAeroportDestination;
    }

    public String getNumeroVol() {
        return numeroVol;
    }

    public void setNumeroVol(String numeroVol) {
        this.numeroVol = numeroVol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetails != null ? idDetails.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailsGoPass)) {
            return false;
        }
        DetailsGoPass other = (DetailsGoPass) object;
        if ((this.idDetails == null && other.idDetails != null) || (this.idDetails != null && !this.idDetails.equals(other.idDetails))) {
            return false;
        }
        return true;
    }

}
