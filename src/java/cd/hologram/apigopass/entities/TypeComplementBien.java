/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_TYPE_COMPLEMENT_BIEN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeComplementBien.findAll", query = "SELECT t FROM TypeComplementBien t"),
    @NamedQuery(name = "TypeComplementBien.findByEtat", query = "SELECT t FROM TypeComplementBien t WHERE t.etat = :etat"),
    @NamedQuery(name = "TypeComplementBien.findByCode", query = "SELECT t FROM TypeComplementBien t WHERE t.code = :code")})
public class TypeComplementBien implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "ETAT")
    private Boolean etat;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @OneToMany(mappedBy = "typeComplementBien")
    private List<AbComplementBien> abComplementBienList;
    @JoinColumn(name = "TYPE_BIEN", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private TypeBien typeBien;
    @JoinColumn(name = "COMPLEMENT", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private TypeComplement complement;
    @OneToMany(mappedBy = "typeComplement")
    private List<ComplementBien> complementBienList;

    public TypeComplementBien() {
    }

    public TypeComplementBien(String code) {
        this.code = code;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlTransient
    public List<AbComplementBien> getAbComplementBienList() {
        return abComplementBienList;
    }

    public void setAbComplementBienList(List<AbComplementBien> abComplementBienList) {
        this.abComplementBienList = abComplementBienList;
    }

    public TypeBien getTypeBien() {
        return typeBien;
    }

    public void setTypeBien(TypeBien typeBien) {
        this.typeBien = typeBien;
    }

    public TypeComplement getComplement() {
        return complement;
    }

    public void setComplement(TypeComplement complement) {
        this.complement = complement;
    }

    @XmlTransient
    public List<ComplementBien> getComplementBienList() {
        return complementBienList;
    }

    public void setComplementBienList(List<ComplementBien> complementBienList) {
        this.complementBienList = complementBienList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeComplementBien)) {
            return false;
        }
        TypeComplementBien other = (TypeComplementBien) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.TypeComplementBien[ code=" + code + " ]";
    }
    
}
