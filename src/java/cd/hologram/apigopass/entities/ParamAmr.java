/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PARAM_AMR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ParamAmr.findAll", query = "SELECT p FROM ParamAmr p"),
    @NamedQuery(name = "ParamAmr.findById", query = "SELECT p FROM ParamAmr p WHERE p.id = :id"),
    @NamedQuery(name = "ParamAmr.findByCompteDgrkCdf", query = "SELECT p FROM ParamAmr p WHERE p.compteDgrkCdf = :compteDgrkCdf"),
    @NamedQuery(name = "ParamAmr.findByCompteDgrkUsd", query = "SELECT p FROM ParamAmr p WHERE p.compteDgrkUsd = :compteDgrkUsd"),
    @NamedQuery(name = "ParamAmr.findByReceveurRecetteFiscale", query = "SELECT p FROM ParamAmr p WHERE p.receveurRecetteFiscale = :receveurRecetteFiscale"),
    @NamedQuery(name = "ParamAmr.findByEtat", query = "SELECT p FROM ParamAmr p WHERE p.etat = :etat")})
public class ParamAmr implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 100)
    @Column(name = "COMPTE_DGRK_CDF")
    private String compteDgrkCdf;
    @Size(max = 100)
    @Column(name = "COMPTE_DGRK_USD")
    private String compteDgrkUsd;
    @Size(max = 50)
    @Column(name = "RECEVEUR_RECETTE_FISCALE")
    private String receveurRecetteFiscale;
    @Column(name = "ETAT")
    private Boolean etat;

    public ParamAmr() {
    }

    public ParamAmr(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompteDgrkCdf() {
        return compteDgrkCdf;
    }

    public void setCompteDgrkCdf(String compteDgrkCdf) {
        this.compteDgrkCdf = compteDgrkCdf;
    }

    public String getCompteDgrkUsd() {
        return compteDgrkUsd;
    }

    public void setCompteDgrkUsd(String compteDgrkUsd) {
        this.compteDgrkUsd = compteDgrkUsd;
    }

    public String getReceveurRecetteFiscale() {
        return receveurRecetteFiscale;
    }

    public void setReceveurRecetteFiscale(String receveurRecetteFiscale) {
        this.receveurRecetteFiscale = receveurRecetteFiscale;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParamAmr)) {
            return false;
        }
        ParamAmr other = (ParamAmr) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.ParamAmr[ id=" + id + " ]";
    }
    
}
