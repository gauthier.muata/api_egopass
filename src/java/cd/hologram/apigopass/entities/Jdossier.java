/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_JDOSSIER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Jdossier.findAll", query = "SELECT j FROM Jdossier j"),
    @NamedQuery(name = "Jdossier.findById", query = "SELECT j FROM Jdossier j WHERE j.id = :id"),
    @NamedQuery(name = "Jdossier.findByDossier", query = "SELECT j FROM Jdossier j WHERE j.dossier = :dossier"),
    @NamedQuery(name = "Jdossier.findByReference", query = "SELECT j FROM Jdossier j WHERE j.reference = :reference"),
    @NamedQuery(name = "Jdossier.findByLibelle", query = "SELECT j FROM Jdossier j WHERE j.libelle = :libelle"),
    @NamedQuery(name = "Jdossier.findByDebit", query = "SELECT j FROM Jdossier j WHERE j.debit = :debit"),
    @NamedQuery(name = "Jdossier.findByCredit", query = "SELECT j FROM Jdossier j WHERE j.credit = :credit"),
    @NamedQuery(name = "Jdossier.findBySolde", query = "SELECT j FROM Jdossier j WHERE j.solde = :solde"),
    @NamedQuery(name = "Jdossier.findByAgentCreat", query = "SELECT j FROM Jdossier j WHERE j.agentCreat = :agentCreat"),
    @NamedQuery(name = "Jdossier.findByDateCreat", query = "SELECT j FROM Jdossier j WHERE j.dateCreat = :dateCreat"),
    @NamedQuery(name = "Jdossier.findByAgentMaj", query = "SELECT j FROM Jdossier j WHERE j.agentMaj = :agentMaj"),
    @NamedQuery(name = "Jdossier.findByDateMaj", query = "SELECT j FROM Jdossier j WHERE j.dateMaj = :dateMaj"),
    @NamedQuery(name = "Jdossier.findByEtat", query = "SELECT j FROM Jdossier j WHERE j.etat = :etat"),
    @NamedQuery(name = "Jdossier.findBySoldeCreaditImpot", query = "SELECT j FROM Jdossier j WHERE j.soldeCreaditImpot = :soldeCreaditImpot"),
    @NamedQuery(name = "Jdossier.findByEstCreditImpot", query = "SELECT j FROM Jdossier j WHERE j.estCreditImpot = :estCreditImpot"),
    @NamedQuery(name = "Jdossier.findByDevise", query = "SELECT j FROM Jdossier j WHERE j.devise = :devise")})
public class Jdossier implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 20)
    @Column(name = "DOSSIER")
    private String dossier;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "REFERENCE")
    private String reference;
    @Size(max = 150)
    @Column(name = "LIBELLE")
    private String libelle;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "DEBIT")
    private BigDecimal debit;
    @Column(name = "CREDIT")
    private BigDecimal credit;
    @Column(name = "SOLDE")
    private BigDecimal solde;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;
    @Column(name = "SOLDE_CREADIT_IMPOT")
    private BigDecimal soldeCreaditImpot;
    @Column(name = "EST_CREDIT_IMPOT")
    private Boolean estCreditImpot;
    @Size(max = 5)
    @Column(name = "DEVISE")
    private String devise;

    public Jdossier() {
    }

    public Jdossier(Integer id) {
        this.id = id;
    }

    public Jdossier(Integer id, String reference) {
        this.id = id;
        this.reference = reference;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDossier() {
        return dossier;
    }

    public void setDossier(String dossier) {
        this.dossier = dossier;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public BigDecimal getDebit() {
        return debit;
    }

    public void setDebit(BigDecimal debit) {
        this.debit = debit;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    public BigDecimal getSolde() {
        return solde;
    }

    public void setSolde(BigDecimal solde) {
        this.solde = solde;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public BigDecimal getSoldeCreaditImpot() {
        return soldeCreaditImpot;
    }

    public void setSoldeCreaditImpot(BigDecimal soldeCreaditImpot) {
        this.soldeCreaditImpot = soldeCreaditImpot;
    }

    public Boolean getEstCreditImpot() {
        return estCreditImpot;
    }

    public void setEstCreditImpot(Boolean estCreditImpot) {
        this.estCreditImpot = estCreditImpot;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jdossier)) {
            return false;
        }
        Jdossier other = (Jdossier) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Jdossier[ id=" + id + " ]";
    }
    
}
