/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PROCEDURE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Procedure.findAll", query = "SELECT p FROM Procedure p"),
    @NamedQuery(name = "Procedure.findByCode", query = "SELECT p FROM Procedure p WHERE p.code = :code"),
    @NamedQuery(name = "Procedure.findByIntitule", query = "SELECT p FROM Procedure p WHERE p.intitule = :intitule"),
    @NamedQuery(name = "Procedure.findByEnCreation", query = "SELECT p FROM Procedure p WHERE p.enCreation = :enCreation"),
    @NamedQuery(name = "Procedure.findByAgentCreat", query = "SELECT p FROM Procedure p WHERE p.agentCreat = :agentCreat"),
    @NamedQuery(name = "Procedure.findByDateCreat", query = "SELECT p FROM Procedure p WHERE p.dateCreat = :dateCreat"),
    @NamedQuery(name = "Procedure.findByAgentMaj", query = "SELECT p FROM Procedure p WHERE p.agentMaj = :agentMaj"),
    @NamedQuery(name = "Procedure.findByDateMaj", query = "SELECT p FROM Procedure p WHERE p.dateMaj = :dateMaj"),
    @NamedQuery(name = "Procedure.findByEtat", query = "SELECT p FROM Procedure p WHERE p.etat = :etat")})
public class Procedure implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CODE")
    private String code;
    @Size(max = 50)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "EN_CREATION")
    private Short enCreation;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;

    public Procedure() {
    }

    public Procedure(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Short getEnCreation() {
        return enCreation;
    }

    public void setEnCreation(Short enCreation) {
        this.enCreation = enCreation;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Procedure)) {
            return false;
        }
        Procedure other = (Procedure) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Procedure[ code=" + code + " ]";
    }
    
}
