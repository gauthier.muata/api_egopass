/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_AGENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agent.findAll", query = "SELECT a FROM Agent a"),
    @NamedQuery(name = "Agent.findByCode", query = "SELECT a FROM Agent a WHERE a.code = :code"),
    @NamedQuery(name = "Agent.findByNom", query = "SELECT a FROM Agent a WHERE a.nom = :nom"),
    @NamedQuery(name = "Agent.findByPrenoms", query = "SELECT a FROM Agent a WHERE a.prenoms = :prenoms"),
    @NamedQuery(name = "Agent.findByEtape", query = "SELECT a FROM Agent a WHERE a.etape = :etape"),
    @NamedQuery(name = "Agent.findByLogin", query = "SELECT a FROM Agent a WHERE a.login = :login"),
    @NamedQuery(name = "Agent.findByMdp", query = "SELECT a FROM Agent a WHERE a.mdp = :mdp"),
    @NamedQuery(name = "Agent.findByAgentCreat", query = "SELECT a FROM Agent a WHERE a.agentCreat = :agentCreat"),
    @NamedQuery(name = "Agent.findByDateCreat", query = "SELECT a FROM Agent a WHERE a.dateCreat = :dateCreat"),
    @NamedQuery(name = "Agent.findByAgentMaj", query = "SELECT a FROM Agent a WHERE a.agentMaj = :agentMaj"),
    @NamedQuery(name = "Agent.findByDateMaj", query = "SELECT a FROM Agent a WHERE a.dateMaj = :dateMaj"),
    @NamedQuery(name = "Agent.findByEtat", query = "SELECT a FROM Agent a WHERE a.etat = :etat"),
    @NamedQuery(name = "Agent.findByMatricule", query = "SELECT a FROM Agent a WHERE a.matricule = :matricule"),
    @NamedQuery(name = "Agent.findByCompteExpire", query = "SELECT a FROM Agent a WHERE a.compteExpire = :compteExpire"),
    @NamedQuery(name = "Agent.findByDateExpiration", query = "SELECT a FROM Agent a WHERE a.dateExpiration = :dateExpiration"),
    @NamedQuery(name = "Agent.findByDistrict", query = "SELECT a FROM Agent a WHERE a.district = :district"),
    @NamedQuery(name = "Agent.findByChangePwd", query = "SELECT a FROM Agent a WHERE a.changePwd = :changePwd"),
    @NamedQuery(name = "Agent.findByGrade", query = "SELECT a FROM Agent a WHERE a.grade = :grade"),
    @NamedQuery(name = "Agent.findByPersonne", query = "SELECT a FROM Agent a WHERE a.personne = :personne"),
    @NamedQuery(name = "Agent.findByConnecte", query = "SELECT a FROM Agent a WHERE a.connecte = :connecte"),
    @NamedQuery(name = "Agent.findByDateDerniereConnexion", query = "SELECT a FROM Agent a WHERE a.dateDerniereConnexion = :dateDerniereConnexion"),
    @NamedQuery(name = "Agent.findByDateDerniereDeconnexion", query = "SELECT a FROM Agent a WHERE a.dateDerniereDeconnexion = :dateDerniereDeconnexion"),
    @NamedQuery(name = "Agent.findByDateSuppression", query = "SELECT a FROM Agent a WHERE a.dateSuppression = :dateSuppression"),
    @NamedQuery(name = "Agent.findByAgentSuppression", query = "SELECT a FROM Agent a WHERE a.agentSuppression = :agentSuppression")})
public class Agent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Integer code;
    @Size(max = 50)
    @Column(name = "NOM")
    private String nom;
    @Size(max = 50)
    @Column(name = "PRENOMS")
    private String prenoms;
    @Size(max = 5)
    @Column(name = "ETAPE")
    private String etape;
    @Size(max = 50)
    @Column(name = "LOGIN")
    private String login;
    @Size(max = 250)
    @Column(name = "MDP")
    private String mdp;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ETAT")
    private short etat;
    @Size(max = 50)
    @Column(name = "MATRICULE")
    private String matricule;
    @Column(name = "COMPTE_EXPIRE")
    private Short compteExpire;
    @Column(name = "DATE_EXPIRATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateExpiration;
    @Size(max = 50)
    @Column(name = "DISTRICT")
    private String district;
    @Size(max = 50)
    @Column(name = "CHANGE_PWD")
    private String changePwd;
    @Size(max = 50)
    @Column(name = "GRADE")
    private String grade;
    @Size(max = 25)
    @Column(name = "PERSONNE")
    private String personne;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "SIGNATURE")
    private String signature;
    @Column(name = "CONNECTE")
    private Integer connecte;
    @Column(name = "DATE_DERNIERE_CONNEXION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDerniereConnexion;
    @Column(name = "DATE_DERNIERE_DECONNEXION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDerniereDeconnexion;
    @Column(name = "DATE_SUPPRESSION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateSuppression;
    @Column(name = "AGENT_SUPPRESSION")
    private Integer agentSuppression;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "OBSERVATION_SUPRESSION")
    private String observationSupression;
    @OneToMany(mappedBy = "agentTraitement")
    private List<DetailsReclamation> detailsReclamationList;
    @OneToMany(mappedBy = "agentCreat")
    private List<Reclamation> reclamationList;
    @OneToMany(mappedBy = "agent")
    private List<DepotDeclaration> depotDeclarationList;
    @OneToMany(mappedBy = "agentMaj")
    private List<Voucher> voucherList;
    @JoinColumn(name = "FONCTION", referencedColumnName = "CODE")
    @ManyToOne
    private Fonction fonction;
    @JoinColumn(name = "SERVICE", referencedColumnName = "CODE")
    @ManyToOne
    private Service service;
    @JoinColumn(name = "SITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site site;
    @JoinColumn(name = "UA", referencedColumnName = "CODE")
    @ManyToOne
    private Ua ua;
    @OneToMany(mappedBy = "codeagent")
    private List<AgentSite> agentSiteList;
    @OneToMany(mappedBy = "agentCreat")
    private List<DernierAvertissement> dernierAvertissementList;
    @OneToMany(mappedBy = "agentCreat")
    private List<RecoursJuridictionnel> recoursJuridictionnelList;
    @OneToMany(mappedBy = "agentCreat")
    private List<LettreSaisie> lettreSaisieList;
    @OneToMany(mappedBy = "agent")
    private List<Gestionnaire> gestionnaireList;
    @OneToMany(mappedBy = "agentCreat")
    private List<ExtraitDeRole> extraitDeRoleList;
    @OneToMany(mappedBy = "agentCreat")
    private List<Role> roleList;
    @OneToMany(mappedBy = "agentValidation")
    private List<DetailEchelonnement> detailEchelonnementList;
    @OneToMany(mappedBy = "agentPaiement")
    private List<Commande> commandeList;
    @OneToMany(mappedBy = "agentUpdate")
    private List<CommandeCarte> commandeCarteList;
    @OneToMany(mappedBy = "agentMaj")
    private List<GoPass> goPassList;
    @OneToMany(mappedBy = "fkAgentCreat")
    private List<DetailsGoPass> detailsGoPassList;
    @OneToMany(mappedBy = "fkAgentUtilisation")
    private List<DetailsGoPass> detailsGoPassList1;

    public Agent() {
    }

    public Agent(Integer code) {
        this.code = code;
    }

    public Agent(Integer code, short etat) {
        this.code = code;
        this.etat = etat;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenoms() {
        return prenoms;
    }

    public void setPrenoms(String prenoms) {
        this.prenoms = prenoms;
    }

    public String getEtape() {
        return etape;
    }

    public void setEtape(String etape) {
        this.etape = etape;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public short getEtat() {
        return etat;
    }

    public void setEtat(short etat) {
        this.etat = etat;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public Short getCompteExpire() {
        return compteExpire;
    }

    public void setCompteExpire(Short compteExpire) {
        this.compteExpire = compteExpire;
    }

    public Date getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getChangePwd() {
        return changePwd;
    }

    public void setChangePwd(String changePwd) {
        this.changePwd = changePwd;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getPersonne() {
        return personne;
    }

    public void setPersonne(String personne) {
        this.personne = personne;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Integer getConnecte() {
        return connecte;
    }

    public void setConnecte(Integer connecte) {
        this.connecte = connecte;
    }

    public Date getDateDerniereConnexion() {
        return dateDerniereConnexion;
    }

    public void setDateDerniereConnexion(Date dateDerniereConnexion) {
        this.dateDerniereConnexion = dateDerniereConnexion;
    }

    public Date getDateDerniereDeconnexion() {
        return dateDerniereDeconnexion;
    }

    public void setDateDerniereDeconnexion(Date dateDerniereDeconnexion) {
        this.dateDerniereDeconnexion = dateDerniereDeconnexion;
    }

    public Date getDateSuppression() {
        return dateSuppression;
    }

    public void setDateSuppression(Date dateSuppression) {
        this.dateSuppression = dateSuppression;
    }

    public Integer getAgentSuppression() {
        return agentSuppression;
    }

    public void setAgentSuppression(Integer agentSuppression) {
        this.agentSuppression = agentSuppression;
    }

    public String getObservationSupression() {
        return observationSupression;
    }

    public void setObservationSupression(String observationSupression) {
        this.observationSupression = observationSupression;
    }

    @XmlTransient
    public List<DetailsReclamation> getDetailsReclamationList() {
        return detailsReclamationList;
    }

    public void setDetailsReclamationList(List<DetailsReclamation> detailsReclamationList) {
        this.detailsReclamationList = detailsReclamationList;
    }

    @XmlTransient
    public List<Reclamation> getReclamationList() {
        return reclamationList;
    }

    public void setReclamationList(List<Reclamation> reclamationList) {
        this.reclamationList = reclamationList;
    }

    @XmlTransient
    public List<DepotDeclaration> getDepotDeclarationList() {
        return depotDeclarationList;
    }

    public void setDepotDeclarationList(List<DepotDeclaration> depotDeclarationList) {
        this.depotDeclarationList = depotDeclarationList;
    }

    @XmlTransient
    public List<Voucher> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<Voucher> voucherList) {
        this.voucherList = voucherList;
    }

    public Fonction getFonction() {
        return fonction;
    }

    public void setFonction(Fonction fonction) {
        this.fonction = fonction;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public Ua getUa() {
        return ua;
    }

    public void setUa(Ua ua) {
        this.ua = ua;
    }

    @XmlTransient
    public List<AgentSite> getAgentSiteList() {
        return agentSiteList;
    }

    public void setAgentSiteList(List<AgentSite> agentSiteList) {
        this.agentSiteList = agentSiteList;
    }

    @XmlTransient
    public List<DernierAvertissement> getDernierAvertissementList() {
        return dernierAvertissementList;
    }

    public void setDernierAvertissementList(List<DernierAvertissement> dernierAvertissementList) {
        this.dernierAvertissementList = dernierAvertissementList;
    }

    @XmlTransient
    public List<RecoursJuridictionnel> getRecoursJuridictionnelList() {
        return recoursJuridictionnelList;
    }

    public void setRecoursJuridictionnelList(List<RecoursJuridictionnel> recoursJuridictionnelList) {
        this.recoursJuridictionnelList = recoursJuridictionnelList;
    }

    @XmlTransient
    public List<LettreSaisie> getLettreSaisieList() {
        return lettreSaisieList;
    }

    public void setLettreSaisieList(List<LettreSaisie> lettreSaisieList) {
        this.lettreSaisieList = lettreSaisieList;
    }

    @XmlTransient
    public List<Gestionnaire> getGestionnaireList() {
        return gestionnaireList;
    }

    public void setGestionnaireList(List<Gestionnaire> gestionnaireList) {
        this.gestionnaireList = gestionnaireList;
    }

    @XmlTransient
    public List<ExtraitDeRole> getExtraitDeRoleList() {
        return extraitDeRoleList;
    }

    public void setExtraitDeRoleList(List<ExtraitDeRole> extraitDeRoleList) {
        this.extraitDeRoleList = extraitDeRoleList;
    }

    @XmlTransient
    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    @XmlTransient
    public List<DetailEchelonnement> getDetailEchelonnementList() {
        return detailEchelonnementList;
    }

    public void setDetailEchelonnementList(List<DetailEchelonnement> detailEchelonnementList) {
        this.detailEchelonnementList = detailEchelonnementList;
    }

    @XmlTransient
    public List<Commande> getCommandeList() {
        return commandeList;
    }

    public void setCommandeList(List<Commande> commandeList) {
        this.commandeList = commandeList;
    }

    @XmlTransient
    public List<GoPass> getGoPassList() {
        return goPassList;
    }

    public void setGoPassList(List<GoPass> goPassList) {
        this.goPassList = goPassList;
    }

    @XmlTransient
    public List<DetailsGoPass> getDetailsGoPassList() {
        return detailsGoPassList;
    }

    public void setDetailsGoPassList(List<DetailsGoPass> detailsGoPassList) {
        this.detailsGoPassList = detailsGoPassList;
    }

    @XmlTransient
    public List<DetailsGoPass> getDetailsGoPassList1() {
        return detailsGoPassList1;
    }

    public void setDetailsGoPassList1(List<DetailsGoPass> detailsGoPassList1) {
        this.detailsGoPassList1 = detailsGoPassList1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agent)) {
            return false;
        }
        Agent other = (Agent) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @XmlTransient
    public List<CommandeCarte> getCommandeCarteList() {
        return commandeCarteList;
    }

    public void setCommandeCarteList(List<CommandeCarte> commandeCarteList) {
        this.commandeCarteList = commandeCarteList;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Agent[ code=" + code + " ]";
    }

}
