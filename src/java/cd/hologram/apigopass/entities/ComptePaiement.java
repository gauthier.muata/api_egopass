/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_COMPTE_PAIEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComptePaiement.findAll", query = "SELECT c FROM ComptePaiement c"),
    @NamedQuery(name = "ComptePaiement.findById", query = "SELECT c FROM ComptePaiement c WHERE c.id = :id"),
    @NamedQuery(name = "ComptePaiement.findByCompteBancaire", query = "SELECT c FROM ComptePaiement c WHERE c.compteBancaire = :compteBancaire"),
    @NamedQuery(name = "ComptePaiement.findByTypeDocument", query = "SELECT c FROM ComptePaiement c WHERE c.typeDocument = :typeDocument"),
    @NamedQuery(name = "ComptePaiement.findByEtat", query = "SELECT c FROM ComptePaiement c WHERE c.etat = :etat")})
public class ComptePaiement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 25)
    @Column(name = "COMPTE_BANCAIRE")
    private String compteBancaire;
    @Size(max = 10)
    @Column(name = "TYPE_DOCUMENT")
    private String typeDocument;
    @Column(name = "ETAT")
    private Boolean etat;

    public ComptePaiement() {
    }

    public ComptePaiement(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComptePaiement)) {
            return false;
        }
        ComptePaiement other = (ComptePaiement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.ComptePaiement[ id=" + id + " ]";
    }
    
}
