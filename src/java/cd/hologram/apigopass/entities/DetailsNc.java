/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_DETAILS_NC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailsNc.findAll", query = "SELECT d FROM DetailsNc d"),
    @NamedQuery(name = "DetailsNc.findById", query = "SELECT d FROM DetailsNc d WHERE d.id = :id"),
    @NamedQuery(name = "DetailsNc.findByTauxArticleBudgetaire", query = "SELECT d FROM DetailsNc d WHERE d.tauxArticleBudgetaire = :tauxArticleBudgetaire"),
    @NamedQuery(name = "DetailsNc.findByValeurBase", query = "SELECT d FROM DetailsNc d WHERE d.valeurBase = :valeurBase"),
    @NamedQuery(name = "DetailsNc.findByMontantDu", query = "SELECT d FROM DetailsNc d WHERE d.montantDu = :montantDu"),
    @NamedQuery(name = "DetailsNc.findByQte", query = "SELECT d FROM DetailsNc d WHERE d.qte = :qte"),
    @NamedQuery(name = "DetailsNc.findByPenaliser", query = "SELECT d FROM DetailsNc d WHERE d.penaliser = :penaliser"),
    @NamedQuery(name = "DetailsNc.findByEtat", query = "SELECT d FROM DetailsNc d WHERE d.etat = :etat"),
    @NamedQuery(name = "DetailsNc.findByTypeTaux", query = "SELECT d FROM DetailsNc d WHERE d.typeTaux = :typeTaux"),
    @NamedQuery(name = "DetailsNc.findByEtatRepartie", query = "SELECT d FROM DetailsNc d WHERE d.etatRepartie = :etatRepartie"),
    @NamedQuery(name = "DetailsNc.findByTarif", query = "SELECT d FROM DetailsNc d WHERE d.tarif = :tarif")})
public class DetailsNc implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TAUX_ARTICLE_BUDGETAIRE")
    private BigDecimal tauxArticleBudgetaire;
    @Column(name = "VALEUR_BASE")
    private BigDecimal valeurBase;
    @Column(name = "MONTANT_DU")
    private BigDecimal montantDu;
    @Column(name = "QTE")
    private Integer qte;
    @Column(name = "PENALISER")
    private Short penaliser;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 1)
    @Column(name = "TYPE_TAUX")
    private String typeTaux;
    @Column(name = "ETAT_REPARTIE")
    private Boolean etatRepartie;
    @Size(max = 50)
    @Column(name = "TARIF")
    private String tarif;
    @OneToMany(mappedBy = "detailsNc")
    private List<Repartition> repartitionList;
    @OneToMany(mappedBy = "detailsNc")
    private List<DetailsAmr> detailsAmrList;
    @JoinColumn(name = "ARTICLE_BUDGETAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleBudgetaire articleBudgetaire;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;
    @JoinColumn(name = "NOTE_CALCUL", referencedColumnName = "NUMERO")
    @ManyToOne
    private NoteCalcul noteCalcul;
    @JoinColumn(name = "NOTE_PERCEPTION", referencedColumnName = "NUMERO")
    @ManyToOne
    private NotePerception notePerception;
    @JoinColumn(name = "PERIODE_DECLARATION", referencedColumnName = "ID")
    @ManyToOne
    private PeriodeDeclaration periodeDeclaration;

    public DetailsNc() {
    }

    public DetailsNc(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getTauxArticleBudgetaire() {
        return tauxArticleBudgetaire;
    }

    public void setTauxArticleBudgetaire(BigDecimal tauxArticleBudgetaire) {
        this.tauxArticleBudgetaire = tauxArticleBudgetaire;
    }

    public BigDecimal getValeurBase() {
        return valeurBase;
    }

    public void setValeurBase(BigDecimal valeurBase) {
        this.valeurBase = valeurBase;
    }

    public BigDecimal getMontantDu() {
        return montantDu;
    }

    public void setMontantDu(BigDecimal montantDu) {
        this.montantDu = montantDu;
    }

    public Integer getQte() {
        return qte;
    }

    public void setQte(Integer qte) {
        this.qte = qte;
    }

    public Short getPenaliser() {
        return penaliser;
    }

    public void setPenaliser(Short penaliser) {
        this.penaliser = penaliser;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getTypeTaux() {
        return typeTaux;
    }

    public void setTypeTaux(String typeTaux) {
        this.typeTaux = typeTaux;
    }

    public Boolean getEtatRepartie() {
        return etatRepartie;
    }

    public void setEtatRepartie(Boolean etatRepartie) {
        this.etatRepartie = etatRepartie;
    }

    public String getTarif() {
        return tarif;
    }

    public void setTarif(String tarif) {
        this.tarif = tarif;
    }

    @XmlTransient
    public List<Repartition> getRepartitionList() {
        return repartitionList;
    }

    public void setRepartitionList(List<Repartition> repartitionList) {
        this.repartitionList = repartitionList;
    }

    @XmlTransient
    public List<DetailsAmr> getDetailsAmrList() {
        return detailsAmrList;
    }

    public void setDetailsAmrList(List<DetailsAmr> detailsAmrList) {
        this.detailsAmrList = detailsAmrList;
    }

    public ArticleBudgetaire getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(ArticleBudgetaire articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public NoteCalcul getNoteCalcul() {
        return noteCalcul;
    }

    public void setNoteCalcul(NoteCalcul noteCalcul) {
        this.noteCalcul = noteCalcul;
    }

    public NotePerception getNotePerception() {
        return notePerception;
    }

    public void setNotePerception(NotePerception notePerception) {
        this.notePerception = notePerception;
    }

    public PeriodeDeclaration getPeriodeDeclaration() {
        return periodeDeclaration;
    }

    public void setPeriodeDeclaration(PeriodeDeclaration periodeDeclaration) {
        this.periodeDeclaration = periodeDeclaration;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailsNc)) {
            return false;
        }
        DetailsNc other = (DetailsNc) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.DetailsNc[ id=" + id + " ]";
    }
    
}
