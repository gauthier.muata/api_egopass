/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juslin TSHIAMUA
 */
@Entity
@Cacheable(false)
@Table(name = "T_CONCILIATION_VOUCHERS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConciliationVouchers.findAll", query = "SELECT c FROM ConciliationVouchers c"),
    @NamedQuery(name = "ConciliationVouchers.findById", query = "SELECT c FROM ConciliationVouchers c WHERE c.id = :id"),
    @NamedQuery(name = "ConciliationVouchers.findByReference", query = "SELECT c FROM ConciliationVouchers c WHERE c.reference = :reference"),
    @NamedQuery(name = "ConciliationVouchers.findByMontantCmd", query = "SELECT c FROM ConciliationVouchers c WHERE c.montantCmd = :montantCmd"),
    @NamedQuery(name = "ConciliationVouchers.findByMontantPaye", query = "SELECT c FROM ConciliationVouchers c WHERE c.montantPaye = :montantPaye")})
public class ConciliationVouchers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 25)
    @Column(name = "REFERENCE")
    private String reference;
    @Column(name = "MONTANT_CMD")
    private BigDecimal montantCmd;
    @Column(name = "MONTANT_PAYE")
    private BigDecimal montantPaye;
    @JoinColumn(name = "FK_COMMANDE", referencedColumnName = "ID")
    @ManyToOne
    private Commande fkCommande;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public BigDecimal getMontantCmd() {
        return montantCmd;
    }

    public void setMontantCmd(BigDecimal montantCmd) {
        this.montantCmd = montantCmd;
    }

    public BigDecimal getMontantPaye() {
        return montantPaye;
    }

    public void setMontantPaye(BigDecimal montantPaye) {
        this.montantPaye = montantPaye;
    }

    public Commande getFkCommande() {
        return fkCommande;
    }

    public void setFkCommande(Commande fkCommande) {
        this.fkCommande = fkCommande;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commande)) {
            return false;
        }
        ConciliationVouchers other = (ConciliationVouchers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.ConciliationVouchers[ id=" + id + " ]";
    }

}
