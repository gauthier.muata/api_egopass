/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_TYPE_BIEN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeBien.findAll", query = "SELECT t FROM TypeBien t"),
    @NamedQuery(name = "TypeBien.findByCode", query = "SELECT t FROM TypeBien t WHERE t.code = :code"),
    @NamedQuery(name = "TypeBien.findByIntitule", query = "SELECT t FROM TypeBien t WHERE t.intitule = :intitule"),
    @NamedQuery(name = "TypeBien.findByContractuel", query = "SELECT t FROM TypeBien t WHERE t.contractuel = :contractuel"),
    @NamedQuery(name = "TypeBien.findByEtat", query = "SELECT t FROM TypeBien t WHERE t.etat = :etat")})
public class TypeBien implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CODE")
    private String code;
    @Size(max = 150)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "CONTRACTUEL")
    private Boolean contractuel;
    @Column(name = "ETAT")
    private Boolean etat;
    @OneToMany(mappedBy = "typeBien")
    private List<Bien> bienList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "typeBien")
    private List<TypeComplementBien> typeComplementBienList;
    @OneToMany(mappedBy = "typeBien")
    private List<TypeBienService> typeBienServiceList;

    public TypeBien() {
    }

    public TypeBien(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Boolean getContractuel() {
        return contractuel;
    }

    public void setContractuel(Boolean contractuel) {
        this.contractuel = contractuel;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<Bien> getBienList() {
        return bienList;
    }

    public void setBienList(List<Bien> bienList) {
        this.bienList = bienList;
    }

    @XmlTransient
    public List<TypeComplementBien> getTypeComplementBienList() {
        return typeComplementBienList;
    }

    public void setTypeComplementBienList(List<TypeComplementBien> typeComplementBienList) {
        this.typeComplementBienList = typeComplementBienList;
    }

    @XmlTransient
    public List<TypeBienService> getTypeBienServiceList() {
        return typeBienServiceList;
    }

    public void setTypeBienServiceList(List<TypeBienService> typeBienServiceList) {
        this.typeBienServiceList = typeBienServiceList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeBien)) {
            return false;
        }
        TypeBien other = (TypeBien) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.TypeBien[ code=" + code + " ]";
    }

}
