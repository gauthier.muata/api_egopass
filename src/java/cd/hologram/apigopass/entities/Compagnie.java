/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juslin TSHIAMUA
 */
@Entity
@Cacheable(false)
@Table(name = "T_COMPAGNIE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compagnie.findAll", query = "SELECT c FROM Compagnie c"),
    @NamedQuery(name = "Compagnie.findByIdCompagnie", query = "SELECT c FROM Compagnie c WHERE c.idCompagnie = :idCompagnie"),
    @NamedQuery(name = "Compagnie.findByCodeAita", query = "SELECT c FROM Compagnie c WHERE c.codeAita = :codeAita"),
    @NamedQuery(name = "Compagnie.findByCodeOaci", query = "SELECT c FROM Compagnie c WHERE c.codeOaci = :codeOaci"),
    @NamedQuery(name = "Compagnie.findByIntitule", query = "SELECT c FROM Compagnie c WHERE c.intitule = :intitule"),
    @NamedQuery(name = "Compagnie.findBySigle", query = "SELECT c FROM Compagnie c WHERE c.sigle = :sigle"),
    @NamedQuery(name = "Compagnie.findByLogo", query = "SELECT c FROM Compagnie c WHERE c.logo = :logo")})
public class Compagnie implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdCompagnie")
    private Integer idCompagnie;
    @Size(max = 50)
    @Column(name = "CodeOaci")
    private String codeOaci;
    @Size(max = 50)
    @Column(name = "CodeAita")
    private String codeAita;
    @Size(max = 250)
    @Column(name = "Intitule")
    private String intitule;
    @Size(max = 50)
    @Column(name = "Sigle")
    private String sigle;
    @Size(max = 250)
    @Column(name = "Logo")
    private String logo;
    @Column(name = "ETAT")
    private Integer etat;
    @OneToMany(mappedBy = "fkCompagnie")
    private List<DetailsGoPass> detailsGoPassList;

    public Compagnie() {
        
    }

    public Compagnie(Integer idCompagnie) {
        this.idCompagnie = idCompagnie;
    }

    public Integer getIdCompagnie() {
        return idCompagnie;
    }

    public void setIdCompagnie(Integer idCompagnie) {
        this.idCompagnie = idCompagnie;
    }

    public String getCodeOaci() {
        return codeOaci;
    }

    public void setCodeOaci(String codeOaci) {
        this.codeOaci = codeOaci;
    }

    public String getCodeAita() {
        return codeAita;
    }

    public void setCodeAita(String codeAita) {
        this.codeAita = codeAita;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getSigle() {
        return sigle;
    }

    public void setSigle(String sigle) {
        this.sigle = sigle;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<DetailsGoPass> getDetailsGoPassList() {
        return detailsGoPassList;
    }

    public void setDetailsGoPassList(List<DetailsGoPass> detailsGoPassList) {
        this.detailsGoPassList = detailsGoPassList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCompagnie != null ? idCompagnie.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compagnie)) {
            return false;
        }
        Compagnie other = (Compagnie) object;
        if ((this.idCompagnie == null && other.idCompagnie != null) || (this.idCompagnie != null && !this.idCompagnie.equals(other.idCompagnie))) {
            return false;
        }
        return true;
    }
}
