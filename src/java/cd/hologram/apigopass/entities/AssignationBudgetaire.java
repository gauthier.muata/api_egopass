/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrateur
 */
@Entity
@Table(name = "T_ASSIGNATION_BUDGETAIRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AssignationBudgetaire.findAll", query = "SELECT a FROM AssignationBudgetaire a"),
    @NamedQuery(name = "AssignationBudgetaire.findById", query = "SELECT a FROM AssignationBudgetaire a WHERE a.id = :id"),
    @NamedQuery(name = "AssignationBudgetaire.findByFkExercice", query = "SELECT a FROM AssignationBudgetaire a WHERE a.fkExercice = :fkExercice"),
    @NamedQuery(name = "AssignationBudgetaire.findByMontantGlobal", query = "SELECT a FROM AssignationBudgetaire a WHERE a.montantGlobal = :montantGlobal"),
    @NamedQuery(name = "AssignationBudgetaire.findByMontantReel", query = "SELECT a FROM AssignationBudgetaire a WHERE a.montantReel = :montantReel"),
    @NamedQuery(name = "AssignationBudgetaire.findByFkDevise", query = "SELECT a FROM AssignationBudgetaire a WHERE a.fkDevise = :fkDevise"),
    @NamedQuery(name = "AssignationBudgetaire.findByDateCreate", query = "SELECT a FROM AssignationBudgetaire a WHERE a.dateCreate = :dateCreate"),
    @NamedQuery(name = "AssignationBudgetaire.findByAgentCreate", query = "SELECT a FROM AssignationBudgetaire a WHERE a.agentCreate = :agentCreate"),
    @NamedQuery(name = "AssignationBudgetaire.findByEtat", query = "SELECT a FROM AssignationBudgetaire a WHERE a.etat = :etat")})
public class AssignationBudgetaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 10)
    @Column(name = "FK_EXERCICE")
    private String fkExercice;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT_GLOBAL")
    private BigDecimal montantGlobal;
    @Column(name = "MONTANT_REEL")
    private BigDecimal montantReel;
    @Size(max = 5)
    @Column(name = "FK_DEVISE")
    private String fkDevise;
    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ARCHIVE")
    private String archive;
    @Column(name = "ETAT")
    private Integer etat;
    @OneToMany(mappedBy = "fkAssignation")
    private List<DetailsAssignationBudgetaire> detailsAssignationBudgetaireList;

    public AssignationBudgetaire() {
    }

    public AssignationBudgetaire(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkExercice() {
        return fkExercice;
    }

    public void setFkExercice(String fkExercice) {
        this.fkExercice = fkExercice;
    }

    public BigDecimal getMontantGlobal() {
        return montantGlobal;
    }

    public void setMontantGlobal(BigDecimal montantGlobal) {
        this.montantGlobal = montantGlobal;
    }

    public BigDecimal getMontantReel() {
        return montantReel;
    }

    public void setMontantReel(BigDecimal montantReel) {
        this.montantReel = montantReel;
    }

    public String getFkDevise() {
        return fkDevise;
    }

    public void setFkDevise(String fkDevise) {
        this.fkDevise = fkDevise;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }

    public String getArchive() {
        return archive;
    }

    public void setArchive(String archive) {
        this.archive = archive;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<DetailsAssignationBudgetaire> getDetailsAssignationBudgetaireList() {
        return detailsAssignationBudgetaireList;
    }

    public void setDetailsAssignationBudgetaireList(List<DetailsAssignationBudgetaire> detailsAssignationBudgetaireList) {
        this.detailsAssignationBudgetaireList = detailsAssignationBudgetaireList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AssignationBudgetaire)) {
            return false;
        }
        AssignationBudgetaire other = (AssignationBudgetaire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.AssignationBudgetaire[ id=" + id + " ]";
    }

}
