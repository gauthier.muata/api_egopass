/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_RECOURS_JURIDICTIONNEL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RecoursJuridictionnel.findAll", query = "SELECT r FROM RecoursJuridictionnel r"),
    @NamedQuery(name = "RecoursJuridictionnel.findById", query = "SELECT r FROM RecoursJuridictionnel r WHERE r.id = :id"),
    @NamedQuery(name = "RecoursJuridictionnel.findByDateCreat", query = "SELECT r FROM RecoursJuridictionnel r WHERE r.dateCreat = :dateCreat"),
    @NamedQuery(name = "RecoursJuridictionnel.findByMotifRecours", query = "SELECT r FROM RecoursJuridictionnel r WHERE r.motifRecours = :motifRecours"),
    @NamedQuery(name = "RecoursJuridictionnel.findByNumeroEnregistrementGreffe", query = "SELECT r FROM RecoursJuridictionnel r WHERE r.numeroEnregistrementGreffe = :numeroEnregistrementGreffe"),
    @NamedQuery(name = "RecoursJuridictionnel.findByDateDepotRecours", query = "SELECT r FROM RecoursJuridictionnel r WHERE r.dateDepotRecours = :dateDepotRecours"),
    @NamedQuery(name = "RecoursJuridictionnel.findByEtat", query = "SELECT r FROM RecoursJuridictionnel r WHERE r.etat = :etat")})
public class RecoursJuridictionnel implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ID")
    private String id;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 250)
    @Column(name = "MOTIF_RECOURS")
    private String motifRecours;
    @Size(max = 50)
    @Column(name = "NUMERO_ENREGISTREMENT_GREFFE")
    private String numeroEnregistrementGreffe;
    @Column(name = "DATE_DEPOT_RECOURS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDepotRecours;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DATE_AUDIENCE")
    private String dateAudience;
    @Column(name = "ETAT")
    private Integer etat;
    @JoinColumn(name = "AGENT_CREAT", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentCreat;
    @JoinColumn(name = "FK_RECLAMATION", referencedColumnName = "ID")
    @ManyToOne
    private Reclamation fkReclamation;

    public RecoursJuridictionnel() {
    }

    public RecoursJuridictionnel(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getMotifRecours() {
        return motifRecours;
    }

    public void setMotifRecours(String motifRecours) {
        this.motifRecours = motifRecours;
    }

    public String getNumeroEnregistrementGreffe() {
        return numeroEnregistrementGreffe;
    }

    public void setNumeroEnregistrementGreffe(String numeroEnregistrementGreffe) {
        this.numeroEnregistrementGreffe = numeroEnregistrementGreffe;
    }

    public Date getDateDepotRecours() {
        return dateDepotRecours;
    }

    public void setDateDepotRecours(Date dateDepotRecours) {
        this.dateDepotRecours = dateDepotRecours;
    }

    public String getDateAudience() {
        return dateAudience;
    }

    public void setDateAudience(String dateAudience) {
        this.dateAudience = dateAudience;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Agent getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Agent agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Reclamation getFkReclamation() {
        return fkReclamation;
    }

    public void setFkReclamation(Reclamation fkReclamation) {
        this.fkReclamation = fkReclamation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RecoursJuridictionnel)) {
            return false;
        }
        RecoursJuridictionnel other = (RecoursJuridictionnel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.RecoursJuridictionnel[ id=" + id + " ]";
    }
    
}
