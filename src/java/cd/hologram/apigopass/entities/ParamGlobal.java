/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrateur
 */
@Entity
@Cacheable(false)
@Table(name = "T_PARAM_GLOBAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ParamGlobal.findAll", query = "SELECT p FROM ParamGlobal p"),
    @NamedQuery(name = "ParamGlobal.findById", query = "SELECT p FROM ParamGlobal p WHERE p.id = :id"),
    @NamedQuery(name = "ParamGlobal.findByIntitule", query = "SELECT p FROM ParamGlobal p WHERE p.intitule = :intitule")})
public class ParamGlobal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "INTITULE")
    private String intitule;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "VALEUR")
    private String valeur;

    public ParamGlobal() {
    }

    public ParamGlobal(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParamGlobal)) {
            return false;
        }
        ParamGlobal other = (ParamGlobal) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.ParamGlobal[ id=" + id + " ]";
    }

}
