/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_EMPREINTES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TEmpreintes.findAll", query = "SELECT t FROM TEmpreintes t"),
    @NamedQuery(name = "TEmpreintes.findByFingerId", query = "SELECT t FROM TEmpreintes t WHERE t.tEmpreintesPK.fingerId = :fingerId"),
    @NamedQuery(name = "TEmpreintes.findByPersonne", query = "SELECT t FROM TEmpreintes t WHERE t.tEmpreintesPK.personne = :personne")})
public class TEmpreintes implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TEmpreintesPK tEmpreintesPK;
    @Lob
    @Column(name = "IMAGE_EMPREINTE")
    private byte[] imageEmpreinte;

    public TEmpreintes() {
    }

    public TEmpreintes(TEmpreintesPK tEmpreintesPK) {
        this.tEmpreintesPK = tEmpreintesPK;
    }

    public TEmpreintes(int fingerId, String personne) {
        this.tEmpreintesPK = new TEmpreintesPK(fingerId, personne);
    }

    public TEmpreintesPK getTEmpreintesPK() {
        return tEmpreintesPK;
    }

    public void setTEmpreintesPK(TEmpreintesPK tEmpreintesPK) {
        this.tEmpreintesPK = tEmpreintesPK;
    }

    public byte[] getImageEmpreinte() {
        return imageEmpreinte;
    }

    public void setImageEmpreinte(byte[] imageEmpreinte) {
        this.imageEmpreinte = imageEmpreinte;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tEmpreintesPK != null ? tEmpreintesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TEmpreintes)) {
            return false;
        }
        TEmpreintes other = (TEmpreintes) object;
        if ((this.tEmpreintesPK == null && other.tEmpreintesPK != null) || (this.tEmpreintesPK != null && !this.tEmpreintesPK.equals(other.tEmpreintesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.TEmpreintes[ tEmpreintesPK=" + tEmpreintesPK + " ]";
    }
    
}
