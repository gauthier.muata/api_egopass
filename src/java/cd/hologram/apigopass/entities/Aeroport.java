/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juslin TSHIAMUA
 */
@Entity
@Cacheable(false)
@Table(name = "T_AEROPORT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aeroport.findAll", query = "SELECT a FROM Aeroport a"),
    @NamedQuery(name = "Aeroport.findByIdAeroport", query = "SELECT a FROM Aeroport a WHERE a.idAeroport = :idAeroport"),
    @NamedQuery(name = "Aeroport.findByCodeAeroport", query = "SELECT a FROM Aeroport a WHERE a.codeAeroport = :codeAeroport"),
    @NamedQuery(name = "Aeroport.findByIntitule", query = "SELECT a FROM Aeroport a WHERE a.intitule = :intitule"),
    @NamedQuery(name = "Aeroport.findByEtat", query = "SELECT a FROM Aeroport a WHERE a.etat = :etat")})
public class Aeroport implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdAeroport")
    private Integer idAeroport;
    @Size(max = 50)
    @Column(name = "CodeAeroport")
    private String codeAeroport;
    @Size(max = 250)
    @Column(name = "Intitule")
    private String intitule;
    @Column(name = "ETAT")
    private Integer etat;
    @JoinColumn(name = "Fk_entite", referencedColumnName = "IdEntite")
    @ManyToOne
    private Entite fkEntite;
    @OneToMany(mappedBy = "fkAeroportProvenance")
    List<DetailsGoPass> detailsGoPassesList;
    @OneToMany(mappedBy = "fkAeroportDestination")
    List<DetailsGoPass> detailsGoPassesList1;

    public Aeroport() {
    }

    public Aeroport(Integer idAeroport) {
        this.idAeroport = idAeroport;
    }

    public Integer getIdAeroport() {
        return idAeroport;
    }

    public void setIdAeroport(Integer idAeroport) {
        this.idAeroport = idAeroport;
    }

    public String getCodeAeroport() {
        return codeAeroport;
    }

    public void setCodeAeroport(String codeAeroport) {
        this.codeAeroport = codeAeroport;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Entite getFkEntite() {
        return fkEntite;
    }

    public void setFkEntite(Entite fkEntite) {
        this.fkEntite = fkEntite;
    }

    @XmlTransient
    public List<DetailsGoPass> getDetailsGoPassesList() {
        return detailsGoPassesList;
    }

    public void setDetailsGoPassesList(List<DetailsGoPass> detailsGoPassesList) {
        this.detailsGoPassesList = detailsGoPassesList;
    }

    @XmlTransient
    public List<DetailsGoPass> getDetailsGoPassesList1() {
        return detailsGoPassesList1;
    }

    public void setDetailsGoPassesList1(List<DetailsGoPass> detailsGoPassesList1) {
        this.detailsGoPassesList1 = detailsGoPassesList1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAeroport != null ? idAeroport.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aeroport)) {
            return false;
        }
        Aeroport other = (Aeroport) object;
        if ((this.idAeroport == null && other.idAeroport != null) || (this.idAeroport != null && !this.idAeroport.equals(other.idAeroport))) {
            return false;
        }
        return true;
    }
}
