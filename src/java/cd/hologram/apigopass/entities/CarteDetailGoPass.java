/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juslin TSHIAMUA
 */
@Entity
@Cacheable(false)
@Table(name = "T_CARTE_DETAIL_GOPASS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CarteDetailGoPass.findAll", query = "SELECT c FROM CarteDetailGoPass c"),
    @NamedQuery(name = "CarteDetailGoPass.findById", query = "SELECT c FROM CarteDetailGoPass c WHERE c.id = :id"),
    @NamedQuery(name = "CarteDetailGoPass.findByDateCreate", query = "SELECT c FROM CarteDetailGoPass c WHERE c.dateCreate = :dateCreate"),
    @NamedQuery(name = "CarteDetailGoPass.findByEtat", query = "SELECT c FROM CarteDetailGoPass c WHERE c.etat = :etat")})
public class CarteDetailGoPass implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "ETAT")
    private Integer etat;
    @JoinColumn(name = "FK_CARTE_GOPASS", referencedColumnName = "ID")
    @ManyToOne
    private CarteGoPass fkCarteGoPass;
    @JoinColumn(name = "FK_DETAIL_GOPASS", referencedColumnName = "IdDetails")
    @ManyToOne
    private DetailsGoPass fkDetailsGoPass;

    public CarteDetailGoPass() {
    }

    public CarteDetailGoPass(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public CarteGoPass getFkCarteGoPass() {
        return fkCarteGoPass;
    }

    public void setFkCarteGoPass(CarteGoPass fkCarteGoPass) {
        this.fkCarteGoPass = fkCarteGoPass;
    }

    public DetailsGoPass getFkDetailsGoPass() {
        return fkDetailsGoPass;
    }

    public void setFkDetailsGoPass(DetailsGoPass fkDetailsGoPass) {
        this.fkDetailsGoPass = fkDetailsGoPass;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CarteDetailGoPass)) {
            return false;
        }
        CarteDetailGoPass other = (CarteDetailGoPass) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.CarteDetailGoPass[ id=" + id + " ]";
    }

}
