/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_NATURE_ARTICLE_BUDGETAIRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NatureArticleBudgetaire.findAll", query = "SELECT n FROM NatureArticleBudgetaire n"),
    @NamedQuery(name = "NatureArticleBudgetaire.findByCode", query = "SELECT n FROM NatureArticleBudgetaire n WHERE n.code = :code"),
    @NamedQuery(name = "NatureArticleBudgetaire.findByIntitule", query = "SELECT n FROM NatureArticleBudgetaire n WHERE n.intitule = :intitule"),
    @NamedQuery(name = "NatureArticleBudgetaire.findByEtat", query = "SELECT n FROM NatureArticleBudgetaire n WHERE n.etat = :etat")})
public class NatureArticleBudgetaire implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "CODE")
    private String code;
    @Size(max = 50)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "ETAT")
    private Short etat;
    @OneToMany(mappedBy = "nature")
    private List<ArticleBudgetaire> articleBudgetaireList;

    public NatureArticleBudgetaire() {
    }

    public NatureArticleBudgetaire(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<ArticleBudgetaire> getArticleBudgetaireList() {
        return articleBudgetaireList;
    }

    public void setArticleBudgetaireList(List<ArticleBudgetaire> articleBudgetaireList) {
        this.articleBudgetaireList = articleBudgetaireList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NatureArticleBudgetaire)) {
            return false;
        }
        NatureArticleBudgetaire other = (NatureArticleBudgetaire) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.NatureArticleBudgetaire[ code=" + code + " ]";
    }
    
}
