/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juslin TSHIAMUA
 */
@Entity
@Cacheable(false)
@Table(name = "T_GO_PASSE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GoPass.findAll", query = "SELECT g FROM GoPass g"),
    @NamedQuery(name = "GoPass.findByIdGopasse", query = "SELECT g FROM GoPass g WHERE g.idGopasse = :idGopasse"),
    @NamedQuery(name = "GoPass.findByNumeroGopasse", query = "SELECT g FROM GoPass g WHERE g.numeroGopasse = :numeroGopasse"),
    @NamedQuery(name = "GoPass.findByQuantite", query = "SELECT g FROM GoPass g WHERE g.quantite = :quantite"),
    @NamedQuery(name = "GoPass.findByPrixUnitaire", query = "SELECT g FROM GoPass g WHERE g.prixUnitaire = :prixUnitaire"),
    @NamedQuery(name = "GoPass.findByMontant", query = "SELECT g FROM GoPass g WHERE g.montant = :montant"),
    @NamedQuery(name = "GoPass.findByDevise", query = "SELECT g FROM GoPass g WHERE g.devise = :devise"),
    @NamedQuery(name = "GoPass.findByTypeGopasse", query = "SELECT g FROM GoPass g WHERE g.typeGopasse = :typeGopasse"),
    @NamedQuery(name = "GoPass.findByEtat", query = "SELECT g FROM GoPass g WHERE g.etat = :etat"),
    @NamedQuery(name = "GoPass.findByDateMaj", query = "SELECT g FROM GoPass g WHERE g.dateMaj = :dateMaj"),
    @NamedQuery(name = "GoPass.findByNewId", query = "SELECT g FROM GoPass g WHERE g.newId = :newId")})
public class GoPass implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdGopasse")
    private Integer idGopasse;
    @Size(max = 50)
    @Column(name = "NumeroGopasse")
    private String numeroGopasse;
    @Column(name = "Quantite")
    private Integer quantite;
    @Column(name = "PrixUnitaire")
    private BigDecimal prixUnitaire;
    @Column(name = "Montant")
    private BigDecimal montant;
    @Size(max = 50)
    @Column(name = "Devise")
    private String devise;
    @Size(max = 50)
    @Column(name = "TypeGopasse")
    private String typeGopasse;
    @Column(name = "ETAT")
    private Integer etat;
    @Column(name = "Date_Maj")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Size(max = 50)
    @Column(name = "New_Id")
    private String newId;
    @JoinColumn(name = "Fk_Tarif", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif fkTarif;
    @JoinColumn(name = "Agent_Maj", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentMaj;
    @JoinColumn(name = "Fk_Commande", referencedColumnName = "ID")
    @ManyToOne
    private Commande fkCommande;

    public GoPass() {
        
    }

    public GoPass(Integer idGoPasse) {
        this.idGopasse = idGoPasse;
    }

    public Integer getIdGopasse() {
        return idGopasse;
    }

    public void setIdGopasse(Integer idGopasse) {
        this.idGopasse = idGopasse;
    }

    public String getNumeroGopasse() {
        return numeroGopasse;
    }

    public void setNumeroGopasse(String numeroGopasse) {
        this.numeroGopasse = numeroGopasse;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getTypeGopasse() {
        return typeGopasse;
    }

    public void setTypeGopasse(String typeGopasse) {
        this.typeGopasse = typeGopasse;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public String getNewId() {
        return newId;
    }

    public void setNewId(String newId) {
        this.newId = newId;
    }

    public Tarif getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(Tarif fkTarif) {
        this.fkTarif = fkTarif;
    }

    public Agent getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Agent agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Commande getFkCommande() {
        return fkCommande;
    }

    public void setFkCommande(Commande fkCommande) {
        this.fkCommande = fkCommande;
    }
}
