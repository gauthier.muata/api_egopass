/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_IDENTITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Identite.findAll", query = "SELECT i FROM Identite i"),
    @NamedQuery(name = "Identite.findById", query = "SELECT i FROM Identite i WHERE i.id = :id"),
    @NamedQuery(name = "Identite.findByNom", query = "SELECT i FROM Identite i WHERE i.nom = :nom"),
    @NamedQuery(name = "Identite.findByPrenoms", query = "SELECT i FROM Identite i WHERE i.prenoms = :prenoms"),
    @NamedQuery(name = "Identite.findByCarteId", query = "SELECT i FROM Identite i WHERE i.carteId = :carteId"),
    @NamedQuery(name = "Identite.findByCarteType", query = "SELECT i FROM Identite i WHERE i.carteType = :carteType"),
    @NamedQuery(name = "Identite.findByAdresse", query = "SELECT i FROM Identite i WHERE i.adresse = :adresse"),
    @NamedQuery(name = "Identite.findByNationalite", query = "SELECT i FROM Identite i WHERE i.nationalite = :nationalite"),
    @NamedQuery(name = "Identite.findByType", query = "SELECT i FROM Identite i WHERE i.type = :type"),
    @NamedQuery(name = "Identite.findByAgentCreat", query = "SELECT i FROM Identite i WHERE i.agentCreat = :agentCreat"),
    @NamedQuery(name = "Identite.findByDateCreat", query = "SELECT i FROM Identite i WHERE i.dateCreat = :dateCreat"),
    @NamedQuery(name = "Identite.findByAgentMaj", query = "SELECT i FROM Identite i WHERE i.agentMaj = :agentMaj"),
    @NamedQuery(name = "Identite.findByDateMaj", query = "SELECT i FROM Identite i WHERE i.dateMaj = :dateMaj"),
    @NamedQuery(name = "Identite.findByEtat", query = "SELECT i FROM Identite i WHERE i.etat = :etat")})
public class Identite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "NOM")
    private String nom;
    @Size(max = 50)
    @Column(name = "PRENOMS")
    private String prenoms;
    @Size(max = 20)
    @Column(name = "CARTE_ID")
    private String carteId;
    @Size(max = 20)
    @Column(name = "CARTE_TYPE")
    private String carteType;
    @Size(max = 75)
    @Column(name = "ADRESSE")
    private String adresse;
    @Size(max = 3)
    @Column(name = "NATIONALITE")
    private String nationalite;
    @Column(name = "TYPE")
    private Short type;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;

    public Identite() {
    }

    public Identite(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenoms() {
        return prenoms;
    }

    public void setPrenoms(String prenoms) {
        this.prenoms = prenoms;
    }

    public String getCarteId() {
        return carteId;
    }

    public void setCarteId(String carteId) {
        this.carteId = carteId;
    }

    public String getCarteType() {
        return carteType;
    }

    public void setCarteType(String carteType) {
        this.carteType = carteType;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Identite)) {
            return false;
        }
        Identite other = (Identite) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Identite[ id=" + id + " ]";
    }
    
}
