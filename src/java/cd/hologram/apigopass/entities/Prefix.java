/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PREFIX")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prefix.findAll", query = "SELECT p FROM Prefix p"),
    @NamedQuery(name = "Prefix.findByCode", query = "SELECT p FROM Prefix p WHERE p.code = :code"),
    @NamedQuery(name = "Prefix.findByNombreMaximum", query = "SELECT p FROM Prefix p WHERE p.nombreMaximum = :nombreMaximum"),
    @NamedQuery(name = "Prefix.findByEtat", query = "SELECT p FROM Prefix p WHERE p.etat = :etat")})
public class Prefix implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "CODE")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NOMBRE_MAXIMUM")
    private int nombreMaximum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ETAT")
    private boolean etat;

    public Prefix() {
    }

    public Prefix(String code) {
        this.code = code;
    }

    public Prefix(String code, int nombreMaximum, boolean etat) {
        this.code = code;
        this.nombreMaximum = nombreMaximum;
        this.etat = etat;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getNombreMaximum() {
        return nombreMaximum;
    }

    public void setNombreMaximum(int nombreMaximum) {
        this.nombreMaximum = nombreMaximum;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prefix)) {
            return false;
        }
        Prefix other = (Prefix) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Prefix[ code=" + code + " ]";
    }
    
}
