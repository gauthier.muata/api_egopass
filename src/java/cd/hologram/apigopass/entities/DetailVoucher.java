/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juslin TSHIAMUA
 */
@Entity
@Cacheable(false)
@Table(name = "T_DETAIL_VOUCHER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailVoucher.findAll", query = "SELECT d FROM DetailVoucher d"),
    @NamedQuery(name = "DetailVoucher.findById", query = "SELECT d FROM DetailVoucher d WHERE d.id = :id"),
    @NamedQuery(name = "DetailVoucher.findByCodeVoucher", query = "SELECT d FROM DetailVoucher d WHERE d.codeVoucher = :codeVoucher"),
    @NamedQuery(name = "DetailVoucher.findByDateGeneration", query = "SELECT d FROM DetailVoucher d WHERE d.dateGeneration = :dateGeneration"),
    @NamedQuery(name = "DetailVoucher.findByDateUtilisation", query = "SELECT d FROM DetailVoucher d WHERE d.dateUtilisation = :dateUtilisation"),
    @NamedQuery(name = "DetailVoucher.findByEtat", query = "SELECT d FROM DetailVoucher d WHERE d.etat = :etat"),
    @NamedQuery(name = "DetailVoucher.findByCodeTicketGenerate", query = "SELECT d FROM DetailVoucher d WHERE d.codeTicketGenerate = :codeTicketGenerate")})
public class DetailVoucher implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "CODE_VOUCHER")
    private String codeVoucher;
    @Column(name = "DATE_GENERATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateGeneration;
    @Column(name = "DATE_UTILISATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUtilisation;
    @Column(name = "ETAT")
    private Integer etat;
    @Size(max = 50)
    @Column(name = "CODE_TICKET_GENERATE")
    private String codeTicketGenerate;
    @JoinColumn(name = "FK_VAUCHER", referencedColumnName = "ID")
    @ManyToOne
    private Voucher fkVaucher;
    @JoinColumn(name = "AGENT_UTILISATION", referencedColumnName = "CODE")
    @ManyToOne
    private Agent fkAgent;
    @OneToMany(mappedBy = "fkDetailVoucher")
    private List<CarteDetailVoucher> carteDetailVoucherList;
    @JoinColumn(name = "FK_SITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site fkSite;

    public DetailVoucher() {
    }

    public DetailVoucher(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodeVoucher() {
        return codeVoucher;
    }

    public void setCodeVoucher(String codeVoucher) {
        this.codeVoucher = codeVoucher;
    }

    public Date getDateGeneration() {
        return dateGeneration;
    }

    public void setDateGeneration(Date dateGeneration) {
        this.dateGeneration = dateGeneration;
    }

    public Date getDateUtilisation() {
        return dateUtilisation;
    }

    public void setDateUtilisation(Date dateUtilisation) {
        this.dateUtilisation = dateUtilisation;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getCodeTicketGenerate() {
        return codeTicketGenerate;
    }

    public void setCodeTicketGenerate(String codeTicketGenerate) {
        this.codeTicketGenerate = codeTicketGenerate;
    }

    public Agent getFkAgent() {
        return fkAgent;
    }

    public void setFkAgent(Agent fkAgent) {
        this.fkAgent = fkAgent;
    }

    public Voucher getFkVaucher() {
        return fkVaucher;
    }

    public void setFkVaucher(Voucher fkVaucher) {
        this.fkVaucher = fkVaucher;
    }

    public Site getFkSite() {
        return fkSite;
    }

    public void setFkSite(Site fkSite) {
        this.fkSite = fkSite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailVoucher)) {
            return false;
        }
        DetailVoucher other = (DetailVoucher) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @XmlTransient
    public List<CarteDetailVoucher> getCarteDetailVoucherList() {
        return carteDetailVoucherList;
    }

    public void setCarteDetailVoucherList(List<CarteDetailVoucher> carteDetailVoucherList) {
        this.carteDetailVoucherList = carteDetailVoucherList;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.DetailVoucher[ id=" + id + " ]";
    }

}
