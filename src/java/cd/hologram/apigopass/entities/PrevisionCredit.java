/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrateur
 */
@Entity
@Cacheable(false)
@Table(name = "T_PREVISION_CREDIT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PrevisionCredit.findAll", query = "SELECT p FROM PrevisionCredit p"),
    @NamedQuery(name = "PrevisionCredit.findByCode", query = "SELECT p FROM PrevisionCredit p WHERE p.code = :code"),
    @NamedQuery(name = "PrevisionCredit.findByType", query = "SELECT p FROM PrevisionCredit p WHERE p.type = :type"),
    @NamedQuery(name = "PrevisionCredit.findByNbreTourInitial", query = "SELECT p FROM PrevisionCredit p WHERE p.nbreTourInitial = :nbreTourInitial"),
    @NamedQuery(name = "PrevisionCredit.findByNbreTourReel", query = "SELECT p FROM PrevisionCredit p WHERE p.nbreTourReel = :nbreTourReel"),
    @NamedQuery(name = "PrevisionCredit.findByMontantTotal", query = "SELECT p FROM PrevisionCredit p WHERE p.montantTotal = :montantTotal"),
    @NamedQuery(name = "PrevisionCredit.findByEtat", query = "SELECT p FROM PrevisionCredit p WHERE p.etat = :etat"),
    @NamedQuery(name = "PrevisionCredit.findByFkPersonne", query = "SELECT p FROM PrevisionCredit p WHERE p.fkPersonne = :fkPersonne"),
    @NamedQuery(name = "PrevisionCredit.findByDateCreate", query = "SELECT p FROM PrevisionCredit p WHERE p.dateCreate = :dateCreate"),
    @NamedQuery(name = "PrevisionCredit.findByAgentCreate", query = "SELECT p FROM PrevisionCredit p WHERE p.agentCreate = :agentCreate"),
    @NamedQuery(name = "PrevisionCredit.findByDateMaj", query = "SELECT p FROM PrevisionCredit p WHERE p.dateMaj = :dateMaj"),
    @NamedQuery(name = "PrevisionCredit.findByAgentMaj", query = "SELECT p FROM PrevisionCredit p WHERE p.agentMaj = :agentMaj"),
    @NamedQuery(name = "PrevisionCredit.findByFkSite", query = "SELECT p FROM PrevisionCredit p WHERE p.fkSite = :fkSite")})
public class PrevisionCredit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 25)
    @Column(name = "TYPE")
    private String type;
    @Column(name = "NBRE_TOUR_INITIAL")
    private Integer nbreTourInitial;
    @Column(name = "NBRE_TOUR_REEL")
    private Integer nbreTourReel;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT_TOTAL")
    private BigDecimal montantTotal;
    @Column(name = "ETAT")
    private Integer etat;
    @Size(max = 50)
    @Column(name = "FK_PERSONNE")
    private String fkPersonne;
    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "AGENT_MAJ")
    private Integer agentMaj;
    @Size(max = 50)
    @Column(name = "FK_SITE")
    private String fkSite;
    @JoinColumn(name = "ASSUJETTISSEMENT", referencedColumnName = "CODE")
    @ManyToOne
    private Assujettissement assujettissement;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;
    @OneToMany(mappedBy = "previsionCredit")
    private List<DetailPrevisionCredit> detailPrevisionCreditList;

    public PrevisionCredit() {
    }

    public PrevisionCredit(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getNbreTourInitial() {
        return nbreTourInitial;
    }

    public void setNbreTourInitial(Integer nbreTourInitial) {
        this.nbreTourInitial = nbreTourInitial;
    }

    public Integer getNbreTourReel() {
        return nbreTourReel;
    }

    public void setNbreTourReel(Integer nbreTourReel) {
        this.nbreTourReel = nbreTourReel;
    }

    public BigDecimal getMontantTotal() {
        return montantTotal;
    }

    public void setMontantTotal(BigDecimal montantTotal) {
        this.montantTotal = montantTotal;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(String fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Integer getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Integer agentMaj) {
        this.agentMaj = agentMaj;
    }

    public String getFkSite() {
        return fkSite;
    }

    public void setFkSite(String fkSite) {
        this.fkSite = fkSite;
    }

    public Assujettissement getAssujettissement() {
        return assujettissement;
    }

    public void setAssujettissement(Assujettissement assujettissement) {
        this.assujettissement = assujettissement;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    @XmlTransient
    public List<DetailPrevisionCredit> getDetailPrevisionCreditList() {
        return detailPrevisionCreditList;
    }

    public void setDetailPrevisionCreditList(List<DetailPrevisionCredit> detailPrevisionCreditList) {
        this.detailPrevisionCreditList = detailPrevisionCreditList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PrevisionCredit)) {
            return false;
        }
        PrevisionCredit other = (PrevisionCredit) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.PrevisionCredit[ code=" + code + " ]";
    }
    
}
