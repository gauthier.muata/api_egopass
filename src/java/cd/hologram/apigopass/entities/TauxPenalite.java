/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_TAUX_PENALITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TauxPenalite.findAll", query = "SELECT t FROM TauxPenalite t"),
    @NamedQuery(name = "TauxPenalite.findByCode", query = "SELECT t FROM TauxPenalite t WHERE t.code = :code"),
    @NamedQuery(name = "TauxPenalite.findByTypePenalite", query = "SELECT t FROM TauxPenalite t WHERE t.typePenalite = :typePenalite"),
    @NamedQuery(name = "TauxPenalite.findByPenalite", query = "SELECT t FROM TauxPenalite t WHERE t.penalite = :penalite"),
    @NamedQuery(name = "TauxPenalite.findByNatureArticleBudgetaire", query = "SELECT t FROM TauxPenalite t WHERE t.natureArticleBudgetaire = :natureArticleBudgetaire"),
    @NamedQuery(name = "TauxPenalite.findByPalier", query = "SELECT t FROM TauxPenalite t WHERE t.palier = :palier"),
    @NamedQuery(name = "TauxPenalite.findByAgentCreat", query = "SELECT t FROM TauxPenalite t WHERE t.agentCreat = :agentCreat"),
    @NamedQuery(name = "TauxPenalite.findByDateCreat", query = "SELECT t FROM TauxPenalite t WHERE t.dateCreat = :dateCreat"),
    @NamedQuery(name = "TauxPenalite.findByAgentMaj", query = "SELECT t FROM TauxPenalite t WHERE t.agentMaj = :agentMaj"),
    @NamedQuery(name = "TauxPenalite.findByDateMaj", query = "SELECT t FROM TauxPenalite t WHERE t.dateMaj = :dateMaj"),
    @NamedQuery(name = "TauxPenalite.findByEcheance", query = "SELECT t FROM TauxPenalite t WHERE t.echeance = :echeance")})
public class TauxPenalite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODE")
    private String code;
    @Size(max = 20)
    @Column(name = "TYPE_PENALITE")
    private String typePenalite;
    @Size(max = 20)
    @Column(name = "PENALITE")
    private String penalite;
    @Size(max = 20)
    @Column(name = "NATURE_ARTICLE_BUDGETAIRE")
    private String natureArticleBudgetaire;
    @Column(name = "PALIER")
    private Boolean palier;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ECHEANCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date echeance;

    public TauxPenalite() {
    }

    public TauxPenalite(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTypePenalite() {
        return typePenalite;
    }

    public void setTypePenalite(String typePenalite) {
        this.typePenalite = typePenalite;
    }

    public String getPenalite() {
        return penalite;
    }

    public void setPenalite(String penalite) {
        this.penalite = penalite;
    }

    public String getNatureArticleBudgetaire() {
        return natureArticleBudgetaire;
    }

    public void setNatureArticleBudgetaire(String natureArticleBudgetaire) {
        this.natureArticleBudgetaire = natureArticleBudgetaire;
    }

    public Boolean getPalier() {
        return palier;
    }

    public void setPalier(Boolean palier) {
        this.palier = palier;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Date getEcheance() {
        return echeance;
    }

    public void setEcheance(Date echeance) {
        this.echeance = echeance;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TauxPenalite)) {
            return false;
        }
        TauxPenalite other = (TauxPenalite) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.TauxPenalite[ code=" + code + " ]";
    }
    
}
