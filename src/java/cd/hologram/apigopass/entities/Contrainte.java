/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_CONTRAINTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contrainte.findAll", query = "SELECT c FROM Contrainte c"),
    @NamedQuery(name = "Contrainte.findById", query = "SELECT c FROM Contrainte c WHERE c.id = :id"),
    @NamedQuery(name = "Contrainte.findByNumeroDocument", query = "SELECT c FROM Contrainte c WHERE c.numeroDocument = :numeroDocument"),
    @NamedQuery(name = "Contrainte.findByFaitGenerateur", query = "SELECT c FROM Contrainte c WHERE c.faitGenerateur = :faitGenerateur"),
    @NamedQuery(name = "Contrainte.findByAgentCommandement", query = "SELECT c FROM Contrainte c WHERE c.agentCommandement = :agentCommandement"),
    @NamedQuery(name = "Contrainte.findByMontantRecouvrement", query = "SELECT c FROM Contrainte c WHERE c.montantRecouvrement = :montantRecouvrement"),
    @NamedQuery(name = "Contrainte.findByMontantPenalite", query = "SELECT c FROM Contrainte c WHERE c.montantPenalite = :montantPenalite"),
    @NamedQuery(name = "Contrainte.findByMontantTresorUrbain", query = "SELECT c FROM Contrainte c WHERE c.montantTresorUrbain = :montantTresorUrbain"),
    @NamedQuery(name = "Contrainte.findByMontantDirectionRecettes", query = "SELECT c FROM Contrainte c WHERE c.montantDirectionRecettes = :montantDirectionRecettes"),
    @NamedQuery(name = "Contrainte.findByMontantFraisPoursuite", query = "SELECT c FROM Contrainte c WHERE c.montantFraisPoursuite = :montantFraisPoursuite"),
    @NamedQuery(name = "Contrainte.findByEtatImpression", query = "SELECT c FROM Contrainte c WHERE c.etatImpression = :etatImpression"),
    @NamedQuery(name = "Contrainte.findByAgentCreat", query = "SELECT c FROM Contrainte c WHERE c.agentCreat = :agentCreat"),
    @NamedQuery(name = "Contrainte.findByDateCreat", query = "SELECT c FROM Contrainte c WHERE c.dateCreat = :dateCreat"),
    @NamedQuery(name = "Contrainte.findByAgentMaj", query = "SELECT c FROM Contrainte c WHERE c.agentMaj = :agentMaj"),
    @NamedQuery(name = "Contrainte.findByDateMaj", query = "SELECT c FROM Contrainte c WHERE c.dateMaj = :dateMaj"),
    @NamedQuery(name = "Contrainte.findByEtat", query = "SELECT c FROM Contrainte c WHERE c.etat = :etat"),
    @NamedQuery(name = "Contrainte.findByDateReception", query = "SELECT c FROM Contrainte c WHERE c.dateReception = :dateReception"),
    @NamedQuery(name = "Contrainte.findByDateEcheance", query = "SELECT c FROM Contrainte c WHERE c.dateEcheance = :dateEcheance")})
public class Contrainte implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    @Size(max = 20)
    @Column(name = "NUMERO_DOCUMENT")
    private String numeroDocument;
    @Size(max = 50)
    @Column(name = "FAIT_GENERATEUR")
    private String faitGenerateur;
    @Size(max = 50)
    @Column(name = "AGENT_COMMANDEMENT")
    private String agentCommandement;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT_RECOUVREMENT")
    private BigDecimal montantRecouvrement;
    @Column(name = "MONTANT_PENALITE")
    private BigDecimal montantPenalite;
    @Column(name = "MONTANT_TRESOR_URBAIN")
    private BigDecimal montantTresorUrbain;
    @Column(name = "MONTANT_DIRECTION_RECETTES")
    private BigDecimal montantDirectionRecettes;
    @Column(name = "MONTANT_FRAIS_POURSUITE")
    private BigDecimal montantFraisPoursuite;
    @Column(name = "ETAT_IMPRESSION")
    private Boolean etatImpression;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;
    @Column(name = "DATE_RECEPTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReception;
    @Column(name = "DATE_ECHEANCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEcheance;
    @OneToMany(mappedBy = "idContrainte")
    private List<Commandement> commandementList;
    @JoinColumn(name = "ID_AMR", referencedColumnName = "NUMERO")
    @ManyToOne
    private Amr idAmr;
    @JoinColumn(name = "ID_PARAM", referencedColumnName = "ID")
    @ManyToOne
    private ParamContrainte idParam;

    public Contrainte() {
    }

    public Contrainte(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumeroDocument() {
        return numeroDocument;
    }

    public void setNumeroDocument(String numeroDocument) {
        this.numeroDocument = numeroDocument;
    }

    public String getFaitGenerateur() {
        return faitGenerateur;
    }

    public void setFaitGenerateur(String faitGenerateur) {
        this.faitGenerateur = faitGenerateur;
    }

    public String getAgentCommandement() {
        return agentCommandement;
    }

    public void setAgentCommandement(String agentCommandement) {
        this.agentCommandement = agentCommandement;
    }

    public BigDecimal getMontantRecouvrement() {
        return montantRecouvrement;
    }

    public void setMontantRecouvrement(BigDecimal montantRecouvrement) {
        this.montantRecouvrement = montantRecouvrement;
    }

    public BigDecimal getMontantPenalite() {
        return montantPenalite;
    }

    public void setMontantPenalite(BigDecimal montantPenalite) {
        this.montantPenalite = montantPenalite;
    }

    public BigDecimal getMontantTresorUrbain() {
        return montantTresorUrbain;
    }

    public void setMontantTresorUrbain(BigDecimal montantTresorUrbain) {
        this.montantTresorUrbain = montantTresorUrbain;
    }

    public BigDecimal getMontantDirectionRecettes() {
        return montantDirectionRecettes;
    }

    public void setMontantDirectionRecettes(BigDecimal montantDirectionRecettes) {
        this.montantDirectionRecettes = montantDirectionRecettes;
    }

    public BigDecimal getMontantFraisPoursuite() {
        return montantFraisPoursuite;
    }

    public void setMontantFraisPoursuite(BigDecimal montantFraisPoursuite) {
        this.montantFraisPoursuite = montantFraisPoursuite;
    }

    public Boolean getEtatImpression() {
        return etatImpression;
    }

    public void setEtatImpression(Boolean etatImpression) {
        this.etatImpression = etatImpression;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public Date getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(Date dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    @XmlTransient
    public List<Commandement> getCommandementList() {
        return commandementList;
    }

    public void setCommandementList(List<Commandement> commandementList) {
        this.commandementList = commandementList;
    }

    public Amr getIdAmr() {
        return idAmr;
    }

    public void setIdAmr(Amr idAmr) {
        this.idAmr = idAmr;
    }

    public ParamContrainte getIdParam() {
        return idParam;
    }

    public void setIdParam(ParamContrainte idParam) {
        this.idParam = idParam;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contrainte)) {
            return false;
        }
        Contrainte other = (Contrainte) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Contrainte[ id=" + id + " ]";
    }
    
}
