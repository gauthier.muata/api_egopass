/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author moussa.toure
 */
@Embeddable
public class TEmpreintesPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "FINGER_ID")
    private int fingerId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "PERSONNE")
    private String personne;

    public TEmpreintesPK() {
    }

    public TEmpreintesPK(int fingerId, String personne) {
        this.fingerId = fingerId;
        this.personne = personne;
    }

    public int getFingerId() {
        return fingerId;
    }

    public void setFingerId(int fingerId) {
        this.fingerId = fingerId;
    }

    public String getPersonne() {
        return personne;
    }

    public void setPersonne(String personne) {
        this.personne = personne;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) fingerId;
        hash += (personne != null ? personne.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TEmpreintesPK)) {
            return false;
        }
        TEmpreintesPK other = (TEmpreintesPK) object;
        if (this.fingerId != other.fingerId) {
            return false;
        }
        if ((this.personne == null && other.personne != null) || (this.personne != null && !this.personne.equals(other.personne))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.TEmpreintesPK[ fingerId=" + fingerId + ", personne=" + personne + " ]";
    }
    
}
