/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author WILLY
 */
@Entity
@Cacheable(false)
@Table(name = "T_DIVISION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Division.findAll", query = "SELECT d FROM Division d"),
    @NamedQuery(name = "Division.findByCode", query = "SELECT d FROM Division d WHERE d.code = :code"),
    @NamedQuery(name = "Division.findByIntitule", query = "SELECT d FROM Division d WHERE d.intitule = :intitule"),
    @NamedQuery(name = "Division.findBySigle", query = "SELECT d FROM Division d WHERE d.sigle = :sigle"),
    @NamedQuery(name = "Division.findByEtat", query = "SELECT d FROM Division d WHERE d.etat = :etat")})

public class Division implements Serializable {

    @OneToMany(mappedBy = "fkDivision")
    private List<Site> siteList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODE")
    private String code;
    @Size(max = 20)
    @Column(name = "SIGLE")
    private String sigle;
    @Size(max = 100)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "ETAT")
    private Short etat;
    @OneToMany(mappedBy = "fkDivision")
    private List<UtilisateurDivision> utilisateurDivisionList;
    @JoinColumn(name = "FK_ENTITE", referencedColumnName = "CODE")
    @ManyToOne
    private EntiteAdministrative fkEntite;

    public Division() {
    }

    public Division(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSigle() {
        return sigle;
    }

    public void setSigle(String sigle) {
        this.sigle = sigle;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<UtilisateurDivision> getUtilisateurDivisionList() {
        return utilisateurDivisionList;
    }

    public void setUtilisateurDivisionList(List<UtilisateurDivision> utilisateurDivisionList) {
        this.utilisateurDivisionList = utilisateurDivisionList;
    }

    public EntiteAdministrative getFkEntite() {
        return fkEntite;
    }

    public void setFkEntite(EntiteAdministrative fkEntite) {
        this.fkEntite = fkEntite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Division)) {
            return false;
        }
        Division other = (Division) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Division[ code=" + code + " ]";
    }

    @XmlTransient
    public List<Site> getSiteList() {
        return siteList;
    }

    public void setSiteList(List<Site> siteList) {
        this.siteList = siteList;
    }

}
