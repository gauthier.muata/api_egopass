/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_DOC_PROCESS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocProcess.findAll", query = "SELECT d FROM DocProcess d"),
    @NamedQuery(name = "DocProcess.findById", query = "SELECT d FROM DocProcess d WHERE d.id = :id"),
    @NamedQuery(name = "DocProcess.findByDossier", query = "SELECT d FROM DocProcess d WHERE d.dossier = :dossier"),
    @NamedQuery(name = "DocProcess.findByReference", query = "SELECT d FROM DocProcess d WHERE d.reference = :reference"),
    @NamedQuery(name = "DocProcess.findByOrdre", query = "SELECT d FROM DocProcess d WHERE d.ordre = :ordre"),
    @NamedQuery(name = "DocProcess.findByWorkFlow", query = "SELECT d FROM DocProcess d WHERE d.workFlow = :workFlow"),
    @NamedQuery(name = "DocProcess.findByDocument", query = "SELECT d FROM DocProcess d WHERE d.document = :document"),
    @NamedQuery(name = "DocProcess.findByProcess", query = "SELECT d FROM DocProcess d WHERE d.process = :process"),
    @NamedQuery(name = "DocProcess.findByObligatoire", query = "SELECT d FROM DocProcess d WHERE d.obligatoire = :obligatoire"),
    @NamedQuery(name = "DocProcess.findByArchive", query = "SELECT d FROM DocProcess d WHERE d.archive = :archive"),
    @NamedQuery(name = "DocProcess.findByArchiveObligatoire", query = "SELECT d FROM DocProcess d WHERE d.archiveObligatoire = :archiveObligatoire"),
    @NamedQuery(name = "DocProcess.findByInitiateur", query = "SELECT d FROM DocProcess d WHERE d.initiateur = :initiateur"),
    @NamedQuery(name = "DocProcess.findByObservation", query = "SELECT d FROM DocProcess d WHERE d.observation = :observation"),
    @NamedQuery(name = "DocProcess.findByAgentCreat", query = "SELECT d FROM DocProcess d WHERE d.agentCreat = :agentCreat"),
    @NamedQuery(name = "DocProcess.findByDateCreat", query = "SELECT d FROM DocProcess d WHERE d.dateCreat = :dateCreat"),
    @NamedQuery(name = "DocProcess.findByAgentMaj", query = "SELECT d FROM DocProcess d WHERE d.agentMaj = :agentMaj"),
    @NamedQuery(name = "DocProcess.findByDateMaj", query = "SELECT d FROM DocProcess d WHERE d.dateMaj = :dateMaj"),
    @NamedQuery(name = "DocProcess.findByEtat", query = "SELECT d FROM DocProcess d WHERE d.etat = :etat")})
public class DocProcess implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 20)
    @Column(name = "DOSSIER")
    private String dossier;
    @Size(max = 20)
    @Column(name = "REFERENCE")
    private String reference;
    @Size(max = 3)
    @Column(name = "ORDRE")
    private String ordre;
    @Size(max = 25)
    @Column(name = "WORK_FLOW")
    private String workFlow;
    @Size(max = 25)
    @Column(name = "DOCUMENT")
    private String document;
    @Size(max = 25)
    @Column(name = "PROCESS")
    private String process;
    @Column(name = "OBLIGATOIRE")
    private Short obligatoire;
    @Column(name = "ARCHIVE")
    private Short archive;
    @Column(name = "ARCHIVE_OBLIGATOIRE")
    private Short archiveObligatoire;
    @Column(name = "INITIATEUR")
    private Integer initiateur;
    @Size(max = 255)
    @Column(name = "OBSERVATION")
    private String observation;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;

    public DocProcess() {
    }

    public DocProcess(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDossier() {
        return dossier;
    }

    public void setDossier(String dossier) {
        this.dossier = dossier;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getOrdre() {
        return ordre;
    }

    public void setOrdre(String ordre) {
        this.ordre = ordre;
    }

    public String getWorkFlow() {
        return workFlow;
    }

    public void setWorkFlow(String workFlow) {
        this.workFlow = workFlow;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public Short getObligatoire() {
        return obligatoire;
    }

    public void setObligatoire(Short obligatoire) {
        this.obligatoire = obligatoire;
    }

    public Short getArchive() {
        return archive;
    }

    public void setArchive(Short archive) {
        this.archive = archive;
    }

    public Short getArchiveObligatoire() {
        return archiveObligatoire;
    }

    public void setArchiveObligatoire(Short archiveObligatoire) {
        this.archiveObligatoire = archiveObligatoire;
    }

    public Integer getInitiateur() {
        return initiateur;
    }

    public void setInitiateur(Integer initiateur) {
        this.initiateur = initiateur;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocProcess)) {
            return false;
        }
        DocProcess other = (DocProcess) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.DocProcess[ id=" + id + " ]";
    }
    
}
