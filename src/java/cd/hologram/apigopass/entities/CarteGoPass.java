/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juslin TSHIAMUA
 */
@Entity
@Cacheable(false)
@Table(name = "T_CARTE_GOPASS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CarteGoPass.findAll", query = "SELECT c FROM CarteGoPass c"),
    @NamedQuery(name = "CarteGoPass.findById", query = "SELECT c FROM CarteGoPass c WHERE c.id = :id"),
    @NamedQuery(name = "CarteGoPass.findByNumeroCarte", query = "SELECT c FROM CarteGoPass c WHERE c.numeroCarte = :numeroCarte"),
    @NamedQuery(name = "CarteGoPass.findByObservation", query = "SELECT c FROM CarteGoPass c WHERE c.observation = :observation"),
    @NamedQuery(name = "CarteGoPass.findByEtat", query = "SELECT c FROM CarteGoPass c WHERE c.etat = :etat"),
    @NamedQuery(name = "CarteGoPass.findByDateGenerate", query = "SELECT c FROM CarteGoPass c WHERE c.dateGenerate = :dateGenerate")})
public class CarteGoPass implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 25)
    @Column(name = "NUMERO_CARTE")
    private String numeroCarte;
    @Size(max = 50)
    @Column(name = "OBSERVATION")
    private String observation;
    @Column(name = "ETAT")
    private Integer etat;
    @Column(name = "DATE_GENERATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateGenerate;
    @JoinColumn(name = "FK_COMMANDE_CARTE", referencedColumnName = "ID")
    @ManyToOne
    private CommandeCarte fkCommandeCarte;
    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne fkPersonne;
    @OneToMany(mappedBy = "fkCarteGoPass")
    private List<CarteDetailGoPass> carteDetailGoPassList;

    public CarteGoPass() {
    }

    public CarteGoPass(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumeroCarte() {
        return numeroCarte;
    }

    public void setNumeroCarte(String numeroCarte) {
        this.numeroCarte = numeroCarte;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Date getDateGenerate() {
        return dateGenerate;
    }

    public void setDateGenerate(Date dateGenerate) {
        this.dateGenerate = dateGenerate;
    }

    public CommandeCarte getFkCommandeCarte() {
        return fkCommandeCarte;
    }

    public void setFkCommandeCarte(CommandeCarte fkCommandeCarte) {
        this.fkCommandeCarte = fkCommandeCarte;
    }

    public Personne getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(Personne fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    @XmlTransient
    public List<CarteDetailGoPass> getCarteDetailGoPassList() {
        return carteDetailGoPassList;
    }

    public void setCarteDetailGoPassList(List<CarteDetailGoPass> carteDetailGoPassList) {
        this.carteDetailGoPassList = carteDetailGoPassList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CarteGoPass)) {
            return false;
        }
        CarteGoPass other = (CarteGoPass) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.CarteGoPass[ id=" + id + " ]";
    }

}
