/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emmanuel.tsasa
 */
@Entity
@Table(name = "T_TYPE_BIEN_SERVICE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeBienService.findAll", query = "SELECT t FROM TypeBienService t"),
    @NamedQuery(name = "TypeBienService.findById", query = "SELECT t FROM TypeBienService t WHERE t.id = :id"),
    @NamedQuery(name = "TypeBienService.findByType", query = "SELECT t FROM TypeBienService t WHERE t.type = :type"),
    @NamedQuery(name = "TypeBienService.findByEtat", query = "SELECT t FROM TypeBienService t WHERE t.etat = :etat")})
public class TypeBienService implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    
    
    @Column(name = "TYPE")
    private Integer type;
    
    @Column(name = "ETAT")
    private Short etat;
    @JoinColumn(name = "SERVICE", referencedColumnName = "CODE")
    @ManyToOne
    private Service service;
    @JoinColumn(name = "TYPE_BIEN", referencedColumnName = "CODE")
    @ManyToOne
    private TypeBien typeBien;

    public TypeBienService() {
    }

    public TypeBienService(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    
    
    
    

    public TypeBien getTypeBien() {
        return typeBien;
    }

    public void setTypeBien(TypeBien typeBien) {
        this.typeBien = typeBien;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeBienService)) {
            return false;
        }
        TypeBienService other = (TypeBienService) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.TypeBienService[ id=" + id + " ]";
    }
    
}
