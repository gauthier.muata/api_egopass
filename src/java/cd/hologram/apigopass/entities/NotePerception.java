/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_NOTE_PERCEPTION")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotePerception.findAll", query = "SELECT n FROM NotePerception n"),
    @NamedQuery(name = "NotePerception.findByNumero", query = "SELECT n FROM NotePerception n WHERE n.numero = :numero"),
    @NamedQuery(name = "NotePerception.findByNetAPayer", query = "SELECT n FROM NotePerception n WHERE n.netAPayer = :netAPayer"),
    @NamedQuery(name = "NotePerception.findByPayer", query = "SELECT n FROM NotePerception n WHERE n.payer = :payer"),
    @NamedQuery(name = "NotePerception.findBySolde", query = "SELECT n FROM NotePerception n WHERE n.solde = :solde"),
    @NamedQuery(name = "NotePerception.findBySite", query = "SELECT n FROM NotePerception n WHERE n.site = :site"),
    @NamedQuery(name = "NotePerception.findByDateEcheancePaiement", query = "SELECT n FROM NotePerception n WHERE n.dateEcheancePaiement = :dateEcheancePaiement"),
    @NamedQuery(name = "NotePerception.findByCompteBancaire", query = "SELECT n FROM NotePerception n WHERE n.compteBancaire = :compteBancaire"),
    @NamedQuery(name = "NotePerception.findByDateReception", query = "SELECT n FROM NotePerception n WHERE n.dateReception = :dateReception"),
    @NamedQuery(name = "NotePerception.findByTimbre", query = "SELECT n FROM NotePerception n WHERE n.timbre = :timbre"),
    @NamedQuery(name = "NotePerception.findByPapierSecurise", query = "SELECT n FROM NotePerception n WHERE n.papierSecurise = :papierSecurise"),
    @NamedQuery(name = "NotePerception.findByEtat", query = "SELECT n FROM NotePerception n WHERE n.etat = :etat"),
    @NamedQuery(name = "NotePerception.findByAgentCreat", query = "SELECT n FROM NotePerception n WHERE n.agentCreat = :agentCreat"),
    @NamedQuery(name = "NotePerception.findByDateCreat", query = "SELECT n FROM NotePerception n WHERE n.dateCreat = :dateCreat"),
    @NamedQuery(name = "NotePerception.findByFractionnee", query = "SELECT n FROM NotePerception n WHERE n.fractionnee = :fractionnee"),
    @NamedQuery(name = "NotePerception.findByTaxationOffice", query = "SELECT n FROM NotePerception n WHERE n.taxationOffice = :taxationOffice"),
    @NamedQuery(name = "NotePerception.findByNbrImpression", query = "SELECT n FROM NotePerception n WHERE n.nbrImpression = :nbrImpression"),
    @NamedQuery(name = "NotePerception.findByAmr", query = "SELECT n FROM NotePerception n WHERE n.amr = :amr"),
    @NamedQuery(name = "NotePerception.findByQuittance", query = "SELECT n FROM NotePerception n WHERE n.quittance = :quittance"),
    @NamedQuery(name = "NotePerception.findByTauxApplique", query = "SELECT n FROM NotePerception n WHERE n.tauxApplique = :tauxApplique"),
    @NamedQuery(name = "NotePerception.findByMontantInitial", query = "SELECT n FROM NotePerception n WHERE n.montantInitial = :montantInitial")})
public class NotePerception implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "NUMERO")
    private String numero;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "NET_A_PAYER")
    private BigDecimal netAPayer;
    @Column(name = "PAYER")
    private BigDecimal payer;
    @Column(name = "SOLDE")
    private BigDecimal solde;
    @Size(max = 25)
    @Column(name = "SITE")
    private String site;
    @Size(max = 10)
    @Column(name = "DATE_ECHEANCE_PAIEMENT")
    private String dateEcheancePaiement;
    @Size(max = 25)
    @Column(name = "COMPTE_BANCAIRE")
    private String compteBancaire;
    @Size(max = 10)
    @Column(name = "DATE_RECEPTION")
    private String dateReception;
    @Size(max = 50)
    @Column(name = "TIMBRE")
    private String timbre;
    @Size(max = 50)
    @Column(name = "PAPIER_SECURISE")
    private String papierSecurise;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Size(max = 10)
    @Column(name = "DATE_CREAT")
    private String dateCreat;
    @Column(name = "FRACTIONNEE")
    private Short fractionnee;
    @Column(name = "TAXATION_OFFICE")
    private Short taxationOffice;
    @Column(name = "NBR_IMPRESSION")
    private Integer nbrImpression;
    @Size(max = 25)
    @Column(name = "AMR")
    private String amr;
    @Size(max = 25)
    @Column(name = "QUITTANCE")
    private String quittance;
    @Column(name = "TAUX_APPLIQUE")
    private BigDecimal tauxApplique;
    @Column(name = "MONTANT_INITIAL")
    private BigDecimal montantInitial;
    @OneToMany(mappedBy = "notePerception")
    private List<AcquitLiberatoire> acquitLiberatoireList;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;
    @JoinColumn(name = "NOTE_CALCUL", referencedColumnName = "NUMERO")
    @ManyToOne
    private NoteCalcul noteCalcul;
    @OneToMany(mappedBy = "npMere")
    private List<NotePerception> notePerceptionList;
    @JoinColumn(name = "NP_MERE", referencedColumnName = "NUMERO")
    @ManyToOne
    private NotePerception npMere;
    @JoinColumn(name = "RECEPTIONNISTE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne receptionniste;
    @OneToMany(mappedBy = "notePerception")
    private List<DetailsNc> detailsNcList;

    public NotePerception() {
    }

    public NotePerception(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public BigDecimal getNetAPayer() {
        return netAPayer;
    }

    public void setNetAPayer(BigDecimal netAPayer) {
        this.netAPayer = netAPayer;
    }

    public BigDecimal getPayer() {
        return payer;
    }

    public void setPayer(BigDecimal payer) {
        this.payer = payer;
    }

    public BigDecimal getSolde() {
        return solde;
    }

    public void setSolde(BigDecimal solde) {
        this.solde = solde;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getDateEcheancePaiement() {
        return dateEcheancePaiement;
    }

    public void setDateEcheancePaiement(String dateEcheancePaiement) {
        this.dateEcheancePaiement = dateEcheancePaiement;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public String getDateReception() {
        return dateReception;
    }

    public void setDateReception(String dateReception) {
        this.dateReception = dateReception;
    }

    public String getTimbre() {
        return timbre;
    }

    public void setTimbre(String timbre) {
        this.timbre = timbre;
    }

    public String getPapierSecurise() {
        return papierSecurise;
    }

    public void setPapierSecurise(String papierSecurise) {
        this.papierSecurise = papierSecurise;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(String dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Short getFractionnee() {
        return fractionnee;
    }

    public void setFractionnee(Short fractionnee) {
        this.fractionnee = fractionnee;
    }

    public Short getTaxationOffice() {
        return taxationOffice;
    }

    public void setTaxationOffice(Short taxationOffice) {
        this.taxationOffice = taxationOffice;
    }

    public Integer getNbrImpression() {
        return nbrImpression;
    }

    public void setNbrImpression(Integer nbrImpression) {
        this.nbrImpression = nbrImpression;
    }

    public String getAmr() {
        return amr;
    }

    public void setAmr(String amr) {
        this.amr = amr;
    }

    public String getQuittance() {
        return quittance;
    }

    public void setQuittance(String quittance) {
        this.quittance = quittance;
    }

    public BigDecimal getTauxApplique() {
        return tauxApplique;
    }

    public void setTauxApplique(BigDecimal tauxApplique) {
        this.tauxApplique = tauxApplique;
    }

    public BigDecimal getMontantInitial() {
        return montantInitial;
    }

    public void setMontantInitial(BigDecimal montantInitial) {
        this.montantInitial = montantInitial;
    }

    @XmlTransient
    public List<AcquitLiberatoire> getAcquitLiberatoireList() {
        return acquitLiberatoireList;
    }

    public void setAcquitLiberatoireList(List<AcquitLiberatoire> acquitLiberatoireList) {
        this.acquitLiberatoireList = acquitLiberatoireList;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public NoteCalcul getNoteCalcul() {
        return noteCalcul;
    }

    public void setNoteCalcul(NoteCalcul noteCalcul) {
        this.noteCalcul = noteCalcul;
    }

    @XmlTransient
    public List<NotePerception> getNotePerceptionList() {
        return notePerceptionList;
    }

    public void setNotePerceptionList(List<NotePerception> notePerceptionList) {
        this.notePerceptionList = notePerceptionList;
    }

    public NotePerception getNpMere() {
        return npMere;
    }

    public void setNpMere(NotePerception npMere) {
        this.npMere = npMere;
    }

    public Personne getReceptionniste() {
        return receptionniste;
    }

    public void setReceptionniste(Personne receptionniste) {
        this.receptionniste = receptionniste;
    }

    @XmlTransient
    public List<DetailsNc> getDetailsNcList() {
        return detailsNcList;
    }

    public void setDetailsNcList(List<DetailsNc> detailsNcList) {
        this.detailsNcList = detailsNcList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numero != null ? numero.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotePerception)) {
            return false;
        }
        NotePerception other = (NotePerception) object;
        if ((this.numero == null && other.numero != null) || (this.numero != null && !this.numero.equals(other.numero))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.NotePerception[ numero=" + numero + " ]";
    }
    
}
