/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_RECLAMATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reclamation.findAll", query = "SELECT r FROM Reclamation r"),
    @NamedQuery(name = "Reclamation.findById", query = "SELECT r FROM Reclamation r WHERE r.id = :id"),
    @NamedQuery(name = "Reclamation.findByDateCreat", query = "SELECT r FROM Reclamation r WHERE r.dateCreat = :dateCreat"),
    @NamedQuery(name = "Reclamation.findByDateReceptionCourier", query = "SELECT r FROM Reclamation r WHERE r.dateReceptionCourier = :dateReceptionCourier"),
    @NamedQuery(name = "Reclamation.findByEtat", query = "SELECT r FROM Reclamation r WHERE r.etat = :etat"),
    @NamedQuery(name = "Reclamation.findByTypeReclamation", query = "SELECT r FROM Reclamation r WHERE r.typeReclamation = :typeReclamation"),
    @NamedQuery(name = "Reclamation.findByAgentValidation", query = "SELECT r FROM Reclamation r WHERE r.agentValidation = :agentValidation"),
    @NamedQuery(name = "Reclamation.findByReferenceCourierReclamation", query = "SELECT r FROM Reclamation r WHERE r.referenceCourierReclamation = :referenceCourierReclamation"),
    @NamedQuery(name = "Reclamation.findByEtatReclamation", query = "SELECT r FROM Reclamation r WHERE r.etatReclamation = :etatReclamation"),
    @NamedQuery(name = "Reclamation.findByEstTraiter", query = "SELECT r FROM Reclamation r WHERE r.estTraiter = :estTraiter")})
public class Reclamation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ID")
    private String id;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Column(name = "DATE_RECEPTION_COURIER")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReceptionCourier;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "OBSERVATION")
    private String observation;
    @Column(name = "ETAT")
    private Integer etat;
    @Size(max = 100)
    @Column(name = "TYPE_RECLAMATION")
    private String typeReclamation;
    @Column(name = "AGENT_VALIDATION")
    private Integer agentValidation;
    @Size(max = 50)
    @Column(name = "REFERENCE_COURIER_RECLAMATION")
    private String referenceCourierReclamation;
    @Column(name = "ETAT_RECLAMATION")
    private Integer etatReclamation;
    @Column(name = "EST_TRAITER")
    private Integer estTraiter;
    @OneToMany(mappedBy = "fkReclamation")
    private List<DetailsReclamation> detailsReclamationList;
    @JoinColumn(name = "AGENT_CREAT", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentCreat;
    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne fkPersonne;
    @OneToMany(mappedBy = "fkReclamation")
    private List<RecoursJuridictionnel> recoursJuridictionnelList;

    public Reclamation() {
    }

    public Reclamation(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Date getDateReceptionCourier() {
        return dateReceptionCourier;
    }

    public void setDateReceptionCourier(Date dateReceptionCourier) {
        this.dateReceptionCourier = dateReceptionCourier;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getTypeReclamation() {
        return typeReclamation;
    }

    public void setTypeReclamation(String typeReclamation) {
        this.typeReclamation = typeReclamation;
    }

    public Integer getAgentValidation() {
        return agentValidation;
    }

    public void setAgentValidation(Integer agentValidation) {
        this.agentValidation = agentValidation;
    }

    public String getReferenceCourierReclamation() {
        return referenceCourierReclamation;
    }

    public void setReferenceCourierReclamation(String referenceCourierReclamation) {
        this.referenceCourierReclamation = referenceCourierReclamation;
    }

    public Integer getEtatReclamation() {
        return etatReclamation;
    }

    public void setEtatReclamation(Integer etatReclamation) {
        this.etatReclamation = etatReclamation;
    }

    public Integer getEstTraiter() {
        return estTraiter;
    }

    public void setEstTraiter(Integer estTraiter) {
        this.estTraiter = estTraiter;
    }

    @XmlTransient
    public List<DetailsReclamation> getDetailsReclamationList() {
        return detailsReclamationList;
    }

    public void setDetailsReclamationList(List<DetailsReclamation> detailsReclamationList) {
        this.detailsReclamationList = detailsReclamationList;
    }

    public Agent getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Agent agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Personne getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(Personne fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    @XmlTransient
    public List<RecoursJuridictionnel> getRecoursJuridictionnelList() {
        return recoursJuridictionnelList;
    }

    public void setRecoursJuridictionnelList(List<RecoursJuridictionnel> recoursJuridictionnelList) {
        this.recoursJuridictionnelList = recoursJuridictionnelList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reclamation)) {
            return false;
        }
        Reclamation other = (Reclamation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Reclamation[ id=" + id + " ]";
    }
    
}
