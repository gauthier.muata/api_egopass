/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PROFIL_EQUIPEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProfilEquipement.findAll", query = "SELECT p FROM ProfilEquipement p"),
    @NamedQuery(name = "ProfilEquipement.findById", query = "SELECT p FROM ProfilEquipement p WHERE p.id = :id"),
    @NamedQuery(name = "ProfilEquipement.findByFkUser", query = "SELECT p FROM ProfilEquipement p WHERE p.fkUser = :fkUser"),
    @NamedQuery(name = "ProfilEquipement.findByAgentCreat", query = "SELECT p FROM ProfilEquipement p WHERE p.agentCreat = :agentCreat"),
    @NamedQuery(name = "ProfilEquipement.findByDateCreat", query = "SELECT p FROM ProfilEquipement p WHERE p.dateCreat = :dateCreat"),
    @NamedQuery(name = "ProfilEquipement.findByEtat", query = "SELECT p FROM ProfilEquipement p WHERE p.etat = :etat")})
public class ProfilEquipement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 25)
    @Column(name = "FK_USER")
    private String fkUser;
    @Size(max = 50)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Size(max = 50)
    @Column(name = "DATE_CREAT")
    private String dateCreat;
    @Column(name = "ETAT")
    private Boolean etat;
    @JoinColumn(name = "FK_EQUIPEMENT", referencedColumnName = "ID")
    @ManyToOne
    private Equipement fkEquipement;
    @JoinColumn(name = "FK_PROFIL_CFG", referencedColumnName = "ID")
    @ManyToOne
    private ProfilCfg fkProfilCfg;

    public ProfilEquipement() {
    }

    public ProfilEquipement(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkUser() {
        return fkUser;
    }

    public void setFkUser(String fkUser) {
        this.fkUser = fkUser;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(String dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Equipement getFkEquipement() {
        return fkEquipement;
    }

    public void setFkEquipement(Equipement fkEquipement) {
        this.fkEquipement = fkEquipement;
    }

    public ProfilCfg getFkProfilCfg() {
        return fkProfilCfg;
    }

    public void setFkProfilCfg(ProfilCfg fkProfilCfg) {
        this.fkProfilCfg = fkProfilCfg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProfilEquipement)) {
            return false;
        }
        ProfilEquipement other = (ProfilEquipement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.ProfilEquipement[ id=" + id + " ]";
    }
    
}
