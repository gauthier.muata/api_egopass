/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_VALEUR_PREDEFINIE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ValeurPredefinie.findAll", query = "SELECT v FROM ValeurPredefinie v"),
    @NamedQuery(name = "ValeurPredefinie.findByCode", query = "SELECT v FROM ValeurPredefinie v WHERE v.code = :code"),
    @NamedQuery(name = "ValeurPredefinie.findByValeur", query = "SELECT v FROM ValeurPredefinie v WHERE v.valeur = :valeur"),
    @NamedQuery(name = "ValeurPredefinie.findByEtat", query = "SELECT v FROM ValeurPredefinie v WHERE v.etat = :etat")})
public class ValeurPredefinie implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CODE")
    private String code;
    @Size(max = 500)
    @Column(name = "VALEUR")
    private String valeur;
    @Column(name = "ETAT")
    private Boolean etat;
    @JoinColumn(name = "FK_TYPE_COMPLEMENT", referencedColumnName = "CODE")
    @ManyToOne
    private TypeComplement fkTypeComplement;

    public ValeurPredefinie() {
    }

    public ValeurPredefinie(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public TypeComplement getFkTypeComplement() {
        return fkTypeComplement;
    }

    public void setFkTypeComplement(TypeComplement fkTypeComplement) {
        this.fkTypeComplement = fkTypeComplement;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValeurPredefinie)) {
            return false;
        }
        ValeurPredefinie other = (ValeurPredefinie) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.ValeurPredefinie[ code=" + code + " ]";
    }
    
}
