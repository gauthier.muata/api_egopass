/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juslin TSHIAMUA
 */
@Entity
@Cacheable(false)
@Table(name = "T_AXE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Axe.findAll", query = "SELECT a FROM Axe a"),
    @NamedQuery(name = "Axe.findById", query = "SELECT a FROM Axe a WHERE a.id = :id"),
    @NamedQuery(name = "Axe.findByIntitule", query = "SELECT a FROM Axe a WHERE a.intitule = :intitule"),
    @NamedQuery(name = "Axe.findByEtat", query = "SELECT a FROM Axe a WHERE a.etat = :etat"),
    @NamedQuery(name = "Axe.findBySigle", query = "SELECT a FROM Axe a WHERE a.sigle = :sigle")})
public class Axe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 200)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "ETAT")
    private Integer etat;
    @Size(max = 10)
    @Column(name = "SIGLE")
    private String sigle;
    @OneToMany(mappedBy = "fkAxe")
    private List<Voucher> voucherList;
    @OneToMany(mappedBy = "fkAxe")
    private List<AxePeage> axePeageList;

    public Axe() {
    }

    public Axe(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getSigle() {
        return sigle;
    }

    public void setSigle(String sigle) {
        this.sigle = sigle;
    }

    @XmlTransient
    public List<Voucher> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<Voucher> voucherList) {
        this.voucherList = voucherList;
    }

    @XmlTransient
    public List<AxePeage> getAxePeageList() {
        return axePeageList;
    }

    public void setAxePeageList(List<AxePeage> axePeageList) {
        this.axePeageList = axePeageList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Axe)) {
            return false;
        }
        Axe other = (Axe) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.entites.Axe[ id=" + id + " ]";
    }

}
