/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrateur
 */
@Entity
@Table(name = "T_DETAILS_ASSIGNATION_BUDGETAIRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailsAssignationBudgetaire.findAll", query = "SELECT d FROM DetailsAssignationBudgetaire d"),
    @NamedQuery(name = "DetailsAssignationBudgetaire.findById", query = "SELECT d FROM DetailsAssignationBudgetaire d WHERE d.id = :id"),
    @NamedQuery(name = "DetailsAssignationBudgetaire.findByMontantAssigne", query = "SELECT d FROM DetailsAssignationBudgetaire d WHERE d.montantAssigne = :montantAssigne"),
    @NamedQuery(name = "DetailsAssignationBudgetaire.findByEtat", query = "SELECT d FROM DetailsAssignationBudgetaire d WHERE d.etat = :etat"),
    @NamedQuery(name = "DetailsAssignationBudgetaire.findByDateMaj", query = "SELECT d FROM DetailsAssignationBudgetaire d WHERE d.dateMaj = :dateMaj"),
    @NamedQuery(name = "DetailsAssignationBudgetaire.findByAgentMaj", query = "SELECT d FROM DetailsAssignationBudgetaire d WHERE d.agentMaj = :agentMaj")})
public class DetailsAssignationBudgetaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT_ASSIGNE")
    private BigDecimal montantAssigne;
    @Column(name = "ETAT")
    private Integer etat;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "AGENT_MAJ")
    private Integer agentMaj;
    @JoinColumn(name = "FK_ARTICLE_BUDGETAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleBudgetaire fkArticleBudgetaire;
    @JoinColumn(name = "FK_ASSIGNATION", referencedColumnName = "ID")
    @ManyToOne
    private AssignationBudgetaire fkAssignation;
    @JoinColumn(name = "FK_DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise fkDevise;

    public DetailsAssignationBudgetaire() {
    }

    public DetailsAssignationBudgetaire(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getMontantAssigne() {
        return montantAssigne;
    }

    public void setMontantAssigne(BigDecimal montantAssigne) {
        this.montantAssigne = montantAssigne;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Integer getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Integer agentMaj) {
        this.agentMaj = agentMaj;
    }

    public ArticleBudgetaire getFkArticleBudgetaire() {
        return fkArticleBudgetaire;
    }

    public void setFkArticleBudgetaire(ArticleBudgetaire fkArticleBudgetaire) {
        this.fkArticleBudgetaire = fkArticleBudgetaire;
    }

    public AssignationBudgetaire getFkAssignation() {
        return fkAssignation;
    }

    public void setFkAssignation(AssignationBudgetaire fkAssignation) {
        this.fkAssignation = fkAssignation;
    }

    public Devise getFkDevise() {
        return fkDevise;
    }

    public void setFkDevise(Devise fkDevise) {
        this.fkDevise = fkDevise;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailsAssignationBudgetaire)) {
            return false;
        }
        DetailsAssignationBudgetaire other = (DetailsAssignationBudgetaire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.DetailsAssignationBudgetaire[ id=" + id + " ]";
    }

}
