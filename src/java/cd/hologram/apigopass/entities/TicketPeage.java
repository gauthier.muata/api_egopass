/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrateur
 */
@Entity
@Cacheable(false)
@Table(name = "T_TICKET_PEAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TicketPeage.findAll", query = "SELECT t FROM TicketPeage t"),
    @NamedQuery(name = "TicketPeage.findByCode", query = "SELECT t FROM TicketPeage t WHERE t.code = :code"),
    @NamedQuery(name = "TicketPeage.findByMontant", query = "SELECT t FROM TicketPeage t WHERE t.montant = :montant"),
    @NamedQuery(name = "TicketPeage.findByTypePaiement", query = "SELECT t FROM TicketPeage t WHERE t.typePaiement = :typePaiement"),
    @NamedQuery(name = "TicketPeage.findByDateProd", query = "SELECT t FROM TicketPeage t WHERE t.dateProd = :dateProd"),
    @NamedQuery(name = "TicketPeage.findByDateSync", query = "SELECT t FROM TicketPeage t WHERE t.dateSync = :dateSync"),
    @NamedQuery(name = "TicketPeage.findByAgentCreat", query = "SELECT t FROM TicketPeage t WHERE t.agentCreat = :agentCreat"),
    @NamedQuery(name = "TicketPeage.findByEtat", query = "SELECT t FROM TicketPeage t WHERE t.etat = :etat"),
    @NamedQuery(name = "TicketPeage.findByFkOperationSite", query = "SELECT t FROM TicketPeage t WHERE t.fkOperationSite = :fkOperationSite"),
    @NamedQuery(name = "TicketPeage.findByDateMaj", query = "SELECT t FROM TicketPeage t WHERE t.dateMaj = :dateMaj"),
    @NamedQuery(name = "TicketPeage.findByAgentMaj", query = "SELECT t FROM TicketPeage t WHERE t.agentMaj = :agentMaj"),
    @NamedQuery(name = "TicketPeage.findByFkTarif", query = "SELECT t FROM TicketPeage t WHERE t.fkTarif = :fkTarif"),
    @NamedQuery(name = "TicketPeage.findByPlaqueChassis", query = "SELECT t FROM TicketPeage t WHERE t.plaqueChassis = :plaqueChassis"),
    @NamedQuery(name = "TicketPeage.findByShiftTravail", query = "SELECT t FROM TicketPeage t WHERE t.shiftTravail = :shiftTravail")})
public class TicketPeage implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CODE")
    private String code;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Size(max = 20)
    @Column(name = "TYPE_PAIEMENT")
    private String typePaiement;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "MOTIF_ANNULATION")
    private String motifAnnulation;
    @Column(name = "DATE_PROD")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateProd;
    @Column(name = "DATE_SYNC")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateSync;
    @Column(name = "AGENT_CREAT")
    private Integer agentCreat;
    @Column(name = "ETAT")
    private Integer etat;
    @Column(name = "FK_OPERATION_SITE")
    private Integer fkOperationSite;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "AGENT_MAJ")
    private Integer agentMaj;
    @Size(max = 50)
    @Column(name = "FK_TARIF")
    private String fkTarif;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "OBSERVATION")
    private String observation;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "TICKET_DOCUMENT")
    private String ticketDocument;
    @Size(max = 50)
    @Column(name = "PLAQUE_CHASSIS")
    private String plaqueChassis;
    @Size(max = 10)
    @Column(name = "SHIFT_TRAVAIL")
    private String shiftTravail;
    @JoinColumn(name = "ARTICLE_BUDGETAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleBudgetaire articleBudgetaire;
    @JoinColumn(name = "BIEN", referencedColumnName = "ID")
    @ManyToOne
    private Bien bien;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;
    @JoinColumn(name = "PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne personne;
    @JoinColumn(name = "SITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site site;

    public TicketPeage() {
    }

    public TicketPeage(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public String getTypePaiement() {
        return typePaiement;
    }

    public void setTypePaiement(String typePaiement) {
        this.typePaiement = typePaiement;
    }

    public String getMotifAnnulation() {
        return motifAnnulation;
    }

    public void setMotifAnnulation(String motifAnnulation) {
        this.motifAnnulation = motifAnnulation;
    }

    public Date getDateProd() {
        return dateProd;
    }

    public void setDateProd(Date dateProd) {
        this.dateProd = dateProd;
    }

    public Date getDateSync() {
        return dateSync;
    }

    public void setDateSync(Date dateSync) {
        this.dateSync = dateSync;
    }

    public Integer getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Integer agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Integer getFkOperationSite() {
        return fkOperationSite;
    }

    public void setFkOperationSite(Integer fkOperationSite) {
        this.fkOperationSite = fkOperationSite;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Integer getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Integer agentMaj) {
        this.agentMaj = agentMaj;
    }

    public String getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(String fkTarif) {
        this.fkTarif = fkTarif;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getTicketDocument() {
        return ticketDocument;
    }

    public void setTicketDocument(String ticketDocument) {
        this.ticketDocument = ticketDocument;
    }

    public String getPlaqueChassis() {
        return plaqueChassis;
    }

    public void setPlaqueChassis(String plaqueChassis) {
        this.plaqueChassis = plaqueChassis;
    }

    public String getShiftTravail() {
        return shiftTravail;
    }

    public void setShiftTravail(String shiftTravail) {
        this.shiftTravail = shiftTravail;
    }

    public ArticleBudgetaire getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(ArticleBudgetaire articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public Bien getBien() {
        return bien;
    }

    public void setBien(Bien bien) {
        this.bien = bien;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TicketPeage)) {
            return false;
        }
        TicketPeage other = (TicketPeage) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entites.TicketPeage[ code=" + code + " ]";
    }
    
}
