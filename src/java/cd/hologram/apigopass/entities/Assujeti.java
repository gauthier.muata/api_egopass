/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_ASSUJETI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Assujeti.findAll", query = "SELECT a FROM Assujeti a"),
    @NamedQuery(name = "Assujeti.findById", query = "SELECT a FROM Assujeti a WHERE a.id = :id"),
    @NamedQuery(name = "Assujeti.findByDuree", query = "SELECT a FROM Assujeti a WHERE a.duree = :duree"),
    @NamedQuery(name = "Assujeti.findByRenouvellement", query = "SELECT a FROM Assujeti a WHERE a.renouvellement = :renouvellement"),
    @NamedQuery(name = "Assujeti.findByDateDebut", query = "SELECT a FROM Assujeti a WHERE a.dateDebut = :dateDebut"),
    @NamedQuery(name = "Assujeti.findByDateFin", query = "SELECT a FROM Assujeti a WHERE a.dateFin = :dateFin"),
    @NamedQuery(name = "Assujeti.findByValeur", query = "SELECT a FROM Assujeti a WHERE a.valeur = :valeur"),
    @NamedQuery(name = "Assujeti.findByNumActeNotarie", query = "SELECT a FROM Assujeti a WHERE a.numActeNotarie = :numActeNotarie"),
    @NamedQuery(name = "Assujeti.findByDateActeNotarie", query = "SELECT a FROM Assujeti a WHERE a.dateActeNotarie = :dateActeNotarie"),
    @NamedQuery(name = "Assujeti.findByEtat", query = "SELECT a FROM Assujeti a WHERE a.etat = :etat"),
    @NamedQuery(name = "Assujeti.findByReferenceContrat", query = "SELECT a FROM Assujeti a WHERE a.referenceContrat = :referenceContrat"),
    @NamedQuery(name = "Assujeti.findByPeriodicite", query = "SELECT a FROM Assujeti a WHERE a.periodicite = :periodicite"),
    @NamedQuery(name = "Assujeti.findByNbrJourLimite", query = "SELECT a FROM Assujeti a WHERE a.nbrJourLimite = :nbrJourLimite"),
    @NamedQuery(name = "Assujeti.findByUniteValeur", query = "SELECT a FROM Assujeti a WHERE a.uniteValeur = :uniteValeur"),
    @NamedQuery(name = "Assujeti.findByGestionnaire", query = "SELECT a FROM Assujeti a WHERE a.gestionnaire = :gestionnaire"),
    @NamedQuery(name = "Assujeti.findByComplementBien", query = "SELECT a FROM Assujeti a WHERE a.complementBien = :complementBien")})
public class Assujeti implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    @Column(name = "DUREE")
    private Short duree;
    @Column(name = "RENOUVELLEMENT")
    private Short renouvellement;
    @Size(max = 10)
    @Column(name = "DATE_DEBUT")
    private String dateDebut;
    @Size(max = 10)
    @Column(name = "DATE_FIN")
    private String dateFin;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALEUR")
    private BigDecimal valeur;
    @Size(max = 50)
    @Column(name = "NUM_ACTE_NOTARIE")
    private String numActeNotarie;
    @Size(max = 10)
    @Column(name = "DATE_ACTE_NOTARIE")
    private String dateActeNotarie;
    @Column(name = "ETAT")
    private Boolean etat;
    @Size(max = 50)
    @Column(name = "REFERENCE_CONTRAT")
    private String referenceContrat;
    @Size(max = 25)
    @Column(name = "PERIODICITE")
    private String periodicite;
    @Column(name = "NBR_JOUR_LIMITE")
    private Integer nbrJourLimite;
    @Size(max = 25)
    @Column(name = "GESTIONNAIRE")
    private String gestionnaire;
    @Size(max = 25)
    @Column(name = "COMPLEMENT_BIEN")
    private String complementBien;
    @JoinColumn(name = "FK_ADRESSE_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private AdressePersonne fkAdressePersonne;
    @JoinColumn(name = "ARTICLE_BUDGETAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleBudgetaire articleBudgetaire;
    @JoinColumn(name = "BIEN", referencedColumnName = "ID")
    @ManyToOne
    private Bien bien;
    @JoinColumn(name = "PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne personne;
    @JoinColumn(name = "TARIF", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif tarif;
    @OneToMany(mappedBy = "assujetissement")
    private List<PeriodeDeclaration> periodeDeclarationList;
    @Size(max = 100)
    @Column(name = "NOUVELLES_ECHEANCES")
    private String nouvellesEcheances;
    @Size(max = 50)
    @Column(name = "UNITE_VALEUR")
    private String uniteValeur;

    public Assujeti() {
    }

    public Assujeti(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Short getDuree() {
        return duree;
    }

    public void setDuree(Short duree) {
        this.duree = duree;
    }

    public Short getRenouvellement() {
        return renouvellement;
    }

    public void setRenouvellement(Short renouvellement) {
        this.renouvellement = renouvellement;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public String getDateFin() {
        return dateFin;
    }

    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    public BigDecimal getValeur() {
        return valeur;
    }

    public void setValeur(BigDecimal valeur) {
        this.valeur = valeur;
    }

    public String getNumActeNotarie() {
        return numActeNotarie;
    }

    public void setNumActeNotarie(String numActeNotarie) {
        this.numActeNotarie = numActeNotarie;
    }

    public String getDateActeNotarie() {
        return dateActeNotarie;
    }

    public void setDateActeNotarie(String dateActeNotarie) {
        this.dateActeNotarie = dateActeNotarie;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getReferenceContrat() {
        return referenceContrat;
    }

    public void setReferenceContrat(String referenceContrat) {
        this.referenceContrat = referenceContrat;
    }

    public String getPeriodicite() {
        return periodicite;
    }

    public void setPeriodicite(String periodicite) {
        this.periodicite = periodicite;
    }

    public Integer getNbrJourLimite() {
        return nbrJourLimite;
    }

    public void setNbrJourLimite(Integer nbrJourLimite) {
        this.nbrJourLimite = nbrJourLimite;
    }

    public String getGestionnaire() {
        return gestionnaire;
    }

    public void setGestionnaire(String gestionnaire) {
        this.gestionnaire = gestionnaire;
    }

    public String getComplementBien() {
        return complementBien;
    }

    public void setComplementBien(String complementBien) {
        this.complementBien = complementBien;
    }

    public AdressePersonne getFkAdressePersonne() {
        return fkAdressePersonne;
    }

    public void setFkAdressePersonne(AdressePersonne fkAdressePersonne) {
        this.fkAdressePersonne = fkAdressePersonne;
    }

    public ArticleBudgetaire getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(ArticleBudgetaire articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public Bien getBien() {
        return bien;
    }

    public void setBien(Bien bien) {
        this.bien = bien;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    public Tarif getTarif() {
        return tarif;
    }

    public void setTarif(Tarif tarif) {
        this.tarif = tarif;
    }

    @XmlTransient
    public List<PeriodeDeclaration> getPeriodeDeclarationList() {
        return periodeDeclarationList;
    }

    public void setPeriodeDeclarationList(List<PeriodeDeclaration> periodeDeclarationList) {
        this.periodeDeclarationList = periodeDeclarationList;
    }
    
    public String getUniteValeur() {
        return uniteValeur;
    }

    public void setUniteValeur(String uniteValeur) {
        this.uniteValeur = uniteValeur;
    }
    
    public String getNouvellesEcheances() {
        return nouvellesEcheances;
    }

    public void setNouvellesEcheances(String nouvellesEcheances) {
        this.nouvellesEcheances = nouvellesEcheances;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Assujeti)) {
            return false;
        }
        Assujeti other = (Assujeti) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Assujeti[ id=" + id + " ]";
    }
    
}
