/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_PERIODE_DECLARATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PeriodeDeclaration.findAll", query = "SELECT p FROM PeriodeDeclaration p"),
    @NamedQuery(name = "PeriodeDeclaration.findById", query = "SELECT p FROM PeriodeDeclaration p WHERE p.id = :id"),
    @NamedQuery(name = "PeriodeDeclaration.findByDebut", query = "SELECT p FROM PeriodeDeclaration p WHERE p.debut = :debut"),
    @NamedQuery(name = "PeriodeDeclaration.findByFin", query = "SELECT p FROM PeriodeDeclaration p WHERE p.fin = :fin"),
    @NamedQuery(name = "PeriodeDeclaration.findByDateLimite", query = "SELECT p FROM PeriodeDeclaration p WHERE p.dateLimite = :dateLimite"),
    @NamedQuery(name = "PeriodeDeclaration.findByNoteCalcul", query = "SELECT p FROM PeriodeDeclaration p WHERE p.noteCalcul = :noteCalcul"),
    @NamedQuery(name = "PeriodeDeclaration.findByEtat", query = "SELECT p FROM PeriodeDeclaration p WHERE p.etat = :etat"),
    @NamedQuery(name = "PeriodeDeclaration.findByAmr", query = "SELECT p FROM PeriodeDeclaration p WHERE p.amr = :amr"),
    @NamedQuery(name = "PeriodeDeclaration.findByAgentCreat", query = "SELECT p FROM PeriodeDeclaration p WHERE p.agentCreat = :agentCreat")})
public class PeriodeDeclaration implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "DEBUT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date debut;
    @Column(name = "FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;
    @Column(name = "DATE_LIMITE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLimite;
    @Column(name = "DATE_LIMITE_PAIEMENT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLimitePaiement;
    @Size(max = 25)
    @Column(name = "NOTE_CALCUL")
    private String noteCalcul;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 25)
    @Column(name = "AMR")
    private String amr;
    @Size(max = 50)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @JoinColumn(name = "ASSUJETISSEMENT", referencedColumnName = "ID")
    @ManyToOne
    private Assujeti assujetissement;
    @OneToMany(mappedBy = "periodeDeclaration")
    private List<DetailsNc> detailsNcList;
    @Column(name = "OBSERVATION")
    private String observation;
    @Column(name = "AGENT_MAJ")
    private int agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    public PeriodeDeclaration() {
    }

    public PeriodeDeclaration(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDebut() {
        return debut;
    }

    public void setDebut(Date debut) {
        this.debut = debut;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public Date getDateLimite() {
        return dateLimite;
    }

    public void setDateLimite(Date dateLimite) {
        this.dateLimite = dateLimite;
    }

    public String getNoteCalcul() {
        return noteCalcul;
    }

    public void setNoteCalcul(String noteCalcul) {
        this.noteCalcul = noteCalcul;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAmr() {
        return amr;
    }

    public void setAmr(String amr) {
        this.amr = amr;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Assujeti getAssujetissement() {
        return assujetissement;
    }

    public void setAssujetissement(Assujeti assujetissement) {
        this.assujetissement = assujetissement;
    }
    
    public Date getDateLimitePaiement() {
        return dateLimitePaiement;
    }

    public void setDateLimitePaiement(Date dateLimitePaiement) {
        this.dateLimitePaiement = dateLimitePaiement;
    }
    
    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }
    
    public int getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(int agentMaj) {
        this.agentMaj = agentMaj;
    }
    
    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    @XmlTransient
    public List<DetailsNc> getDetailsNcList() {
        return detailsNcList;
    }

    public void setDetailsNcList(List<DetailsNc> detailsNcList) {
        this.detailsNcList = detailsNcList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PeriodeDeclaration)) {
            return false;
        }
        PeriodeDeclaration other = (PeriodeDeclaration) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.PeriodeDeclaration[ id=" + id + " ]";
    }
    
}
