/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_SERVICE_COMPTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServiceCompte.findAll", query = "SELECT s FROM ServiceCompte s"),
    @NamedQuery(name = "ServiceCompte.findById", query = "SELECT s FROM ServiceCompte s WHERE s.id = :id"),
    @NamedQuery(name = "ServiceCompte.findByEtat", query = "SELECT s FROM ServiceCompte s WHERE s.etat = :etat"),
    @NamedQuery(name = "ServiceCompte.findByAgentCreat", query = "SELECT s FROM ServiceCompte s WHERE s.agentCreat = :agentCreat"),
    @NamedQuery(name = "ServiceCompte.findByDateCreat", query = "SELECT s FROM ServiceCompte s WHERE s.dateCreat = :dateCreat"),
    @NamedQuery(name = "ServiceCompte.findByAgentMaj", query = "SELECT s FROM ServiceCompte s WHERE s.agentMaj = :agentMaj"),
    @NamedQuery(name = "ServiceCompte.findByDateMaj", query = "SELECT s FROM ServiceCompte s WHERE s.dateMaj = :dateMaj")})
public class ServiceCompte implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "ID")
    private String id;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @JoinColumn(name = "COMPTE_BANCAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private CompteBancaire compteBancaire;
    @JoinColumn(name = "SERVICE", referencedColumnName = "CODE")
    @ManyToOne
    private Service service;

    public ServiceCompte() {
    }

    public ServiceCompte(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public CompteBancaire getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(CompteBancaire compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServiceCompte)) {
            return false;
        }
        ServiceCompte other = (ServiceCompte) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.ServiceCompte[ id=" + id + " ]";
    }
    
}
