/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrateur
 */
@Entity
@Table(name = "CLE_REPARTITION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CleRepartition.findAll", query = "SELECT c FROM CleRepartition c"),
    @NamedQuery(name = "CleRepartition.findByCode", query = "SELECT c FROM CleRepartition c WHERE c.code = :code"),
    @NamedQuery(name = "CleRepartition.findByEntiteBeneficiaire", query = "SELECT c FROM CleRepartition c WHERE c.entiteBeneficiaire = :entiteBeneficiaire"),
    @NamedQuery(name = "CleRepartition.findByEtat", query = "SELECT c FROM CleRepartition c WHERE c.etat = :etat"),
    @NamedQuery(name = "CleRepartition.findByIsTresorPart", query = "SELECT c FROM CleRepartition c WHERE c.isTresorPart = :isTresorPart"),
    @NamedQuery(name = "CleRepartition.findByTaux", query = "SELECT c FROM CleRepartition c WHERE c.taux = :taux")})
public class CleRepartition implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Integer code;
    @Size(max = 255)
    @Column(name = "ENTITE_BENEFICIAIRE")
    private String entiteBeneficiaire;
    @Column(name = "ETAT")
    private Boolean etat;
    @Column(name = "IS_TRESOR_PART")
    private Boolean isTresorPart;
    @Column(name = "TAUX")
    private BigInteger taux;
    @JoinColumn(name = "FK_ARTICLE_BUDGETAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleBudgetaire fkArticleBudgetaire;

    public CleRepartition() {
    }

    public CleRepartition(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getEntiteBeneficiaire() {
        return entiteBeneficiaire;
    }

    public void setEntiteBeneficiaire(String entiteBeneficiaire) {
        this.entiteBeneficiaire = entiteBeneficiaire;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Boolean getIsTresorPart() {
        return isTresorPart;
    }

    public void setIsTresorPart(Boolean isTresorPart) {
        this.isTresorPart = isTresorPart;
    }

    public BigInteger getTaux() {
        return taux;
    }

    public void setTaux(BigInteger taux) {
        this.taux = taux;
    }

    public ArticleBudgetaire getFkArticleBudgetaire() {
        return fkArticleBudgetaire;
    }

    public void setFkArticleBudgetaire(ArticleBudgetaire fkArticleBudgetaire) {
        this.fkArticleBudgetaire = fkArticleBudgetaire;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CleRepartition)) {
            return false;
        }
        CleRepartition other = (CleRepartition) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.CleRepartition[ code=" + code + " ]";
    }
    
}
