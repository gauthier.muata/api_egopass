/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_TAUX")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Taux.findAll", query = "SELECT t FROM Taux t"),
    @NamedQuery(name = "Taux.findById", query = "SELECT t FROM Taux t WHERE t.id = :id"),
    @NamedQuery(name = "Taux.findByDeviseDest", query = "SELECT t FROM Taux t WHERE t.deviseDest = :deviseDest"),
    @NamedQuery(name = "Taux.findByTaux", query = "SELECT t FROM Taux t WHERE t.taux = :taux"),
    @NamedQuery(name = "Taux.findByAgentCreat", query = "SELECT t FROM Taux t WHERE t.agentCreat = :agentCreat"),
    @NamedQuery(name = "Taux.findByDateCreat", query = "SELECT t FROM Taux t WHERE t.dateCreat = :dateCreat"),
    @NamedQuery(name = "Taux.findByAgentMaj", query = "SELECT t FROM Taux t WHERE t.agentMaj = :agentMaj"),
    @NamedQuery(name = "Taux.findByDateMaj", query = "SELECT t FROM Taux t WHERE t.dateMaj = :dateMaj")})
public class Taux implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 5)
    @Column(name = "DEVISE_DEST")
    private String deviseDest;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TAUX")
    private BigDecimal taux;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;

    public Taux() {
    }

    public Taux(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeviseDest() {
        return deviseDest;
    }

    public void setDeviseDest(String deviseDest) {
        this.deviseDest = deviseDest;
    }

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Taux)) {
            return false;
        }
        Taux other = (Taux) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Taux[ id=" + id + " ]";
    }
    
}
