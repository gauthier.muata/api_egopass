/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_ADRESSE_PERSONNE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdressePersonne.findAll", query = "SELECT a FROM AdressePersonne a"),
    @NamedQuery(name = "AdressePersonne.findByCode", query = "SELECT a FROM AdressePersonne a WHERE a.code = :code"),
    @NamedQuery(name = "AdressePersonne.findByEtat", query = "SELECT a FROM AdressePersonne a WHERE a.etat = :etat"),
    @NamedQuery(name = "AdressePersonne.findByParDefaut", query = "SELECT a FROM AdressePersonne a WHERE a.parDefaut = :parDefaut")})
public class AdressePersonne implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Column(name = "ETAT")
    private Boolean etat;
    @Column(name = "PAR_DEFAUT")
    private Boolean parDefaut;
    @OneToMany(mappedBy = "fkAdressePersonne")
    private List<Assujeti> assujetiList;
    @OneToMany(mappedBy = "fkAdressePersonne")
    private List<Bien> bienList;
    @JoinColumn(name = "ADRESSE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Adresse adresse;
    @JoinColumn(name = "PERSONNE", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private Personne personne;

    public AdressePersonne() {
    }

    public AdressePersonne(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Boolean getParDefaut() {
        return parDefaut;
    }

    public void setParDefaut(Boolean parDefaut) {
        this.parDefaut = parDefaut;
    }

    @XmlTransient
    public List<Assujeti> getAssujetiList() {
        return assujetiList;
    }

    public void setAssujetiList(List<Assujeti> assujetiList) {
        this.assujetiList = assujetiList;
    }

    @XmlTransient
    public List<Bien> getBienList() {
        return bienList;
    }

    public void setBienList(List<Bien> bienList) {
        this.bienList = bienList;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdressePersonne)) {
            return false;
        }
        AdressePersonne other = (AdressePersonne) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.AdressePersonne[ code=" + code + " ]";
    }
    
}
