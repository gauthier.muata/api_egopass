/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_CATEGORIE_TYPE_COMPLEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategorieTypeComplement.findAll", query = "SELECT c FROM CategorieTypeComplement c"),
    @NamedQuery(name = "CategorieTypeComplement.findByCode", query = "SELECT c FROM CategorieTypeComplement c WHERE c.code = :code"),
    @NamedQuery(name = "CategorieTypeComplement.findByIntitule", query = "SELECT c FROM CategorieTypeComplement c WHERE c.intitule = :intitule")})
public class CategorieTypeComplement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "CODE")
    private String code;
    @Size(max = 50)
    @Column(name = "INTITULE")
    private String intitule;
    @OneToMany(mappedBy = "fkCategorieTypeCompelement")
    private List<TypeComplement> typeComplementList;

    public CategorieTypeComplement() {
    }

    public CategorieTypeComplement(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    @XmlTransient
    public List<TypeComplement> getTypeComplementList() {
        return typeComplementList;
    }

    public void setTypeComplementList(List<TypeComplement> typeComplementList) {
        this.typeComplementList = typeComplementList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategorieTypeComplement)) {
            return false;
        }
        CategorieTypeComplement other = (CategorieTypeComplement) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.CategorieTypeComplement[ code=" + code + " ]";
    }
    
}
