/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_COMPLEMENT_FORME")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComplementForme.findAll", query = "SELECT c FROM ComplementForme c"),
    @NamedQuery(name = "ComplementForme.findByCode", query = "SELECT c FROM ComplementForme c WHERE c.code = :code"),
    @NamedQuery(name = "ComplementForme.findByEtat", query = "SELECT c FROM ComplementForme c WHERE c.etat = :etat")})
public class ComplementForme implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODE")
    private String code;
    @Column(name = "ETAT")
    private Boolean etat;
    @OneToMany(mappedBy = "fkComplementForme")
    private List<Complement> complementList;
    @JoinColumn(name = "FORME_JURIDIQUE", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private FormeJuridique formeJuridique;
    @JoinColumn(name = "COMPLEMENT", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private TypeComplement complement;

    public ComplementForme() {
    }

    public ComplementForme(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<Complement> getComplementList() {
        return complementList;
    }

    public void setComplementList(List<Complement> complementList) {
        this.complementList = complementList;
    }

    public FormeJuridique getFormeJuridique() {
        return formeJuridique;
    }

    public void setFormeJuridique(FormeJuridique formeJuridique) {
        this.formeJuridique = formeJuridique;
    }

    public TypeComplement getComplement() {
        return complement;
    }

    public void setComplement(TypeComplement complement) {
        this.complement = complement;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComplementForme)) {
            return false;
        }
        ComplementForme other = (ComplementForme) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.ComplementForme[ code=" + code + " ]";
    }
    
}
