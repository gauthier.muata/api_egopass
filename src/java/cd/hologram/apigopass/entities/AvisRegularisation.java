/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_AVIS_REGULARISATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AvisRegularisation.findAll", query = "SELECT a FROM AvisRegularisation a"),
    @NamedQuery(name = "AvisRegularisation.findById", query = "SELECT a FROM AvisRegularisation a WHERE a.id = :id"),
    @NamedQuery(name = "AvisRegularisation.findByDateCreat", query = "SELECT a FROM AvisRegularisation a WHERE a.dateCreat = :dateCreat"),
    @NamedQuery(name = "AvisRegularisation.findByPersonne", query = "SELECT a FROM AvisRegularisation a WHERE a.personne = :personne"),
    @NamedQuery(name = "AvisRegularisation.findByAgentCreat", query = "SELECT a FROM AvisRegularisation a WHERE a.agentCreat = :agentCreat"),
    @NamedQuery(name = "AvisRegularisation.findByDateReception", query = "SELECT a FROM AvisRegularisation a WHERE a.dateReception = :dateReception"),
    @NamedQuery(name = "AvisRegularisation.findByDateEcheance", query = "SELECT a FROM AvisRegularisation a WHERE a.dateEcheance = :dateEcheance"),
    @NamedQuery(name = "AvisRegularisation.findByEtat", query = "SELECT a FROM AvisRegularisation a WHERE a.etat = :etat")})
public class AvisRegularisation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "PERSONNE")
    private String personne;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_RECEPTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReception;
    @Column(name = "DATE_ECHEANCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEcheance;
    @Column(name = "ETAT")
    private Integer etat;

    public AvisRegularisation() {
    }

    public AvisRegularisation(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getPersonne() {
        return personne;
    }

    public void setPersonne(String personne) {
        this.personne = personne;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public Date getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(Date dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AvisRegularisation)) {
            return false;
        }
        AvisRegularisation other = (AvisRegularisation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.AvisRegularisation[ id=" + id + " ]";
    }
    
}
