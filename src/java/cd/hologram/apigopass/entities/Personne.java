/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_PERSONNE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Personne.findAll", query = "SELECT p FROM Personne p"),
    @NamedQuery(name = "Personne.findByCode", query = "SELECT p FROM Personne p WHERE p.code = :code"),
    @NamedQuery(name = "Personne.findByNom", query = "SELECT p FROM Personne p WHERE p.nom = :nom"),
    @NamedQuery(name = "Personne.findByPostnom", query = "SELECT p FROM Personne p WHERE p.postnom = :postnom"),
    @NamedQuery(name = "Personne.findByPrenoms", query = "SELECT p FROM Personne p WHERE p.prenoms = :prenoms"),
    @NamedQuery(name = "Personne.findByEtat", query = "SELECT p FROM Personne p WHERE p.etat = :etat"),
    @NamedQuery(name = "Personne.findByNif", query = "SELECT p FROM Personne p WHERE p.nif = :nif"),
    @NamedQuery(name = "Personne.findByAgentCreat", query = "SELECT p FROM Personne p WHERE p.agentCreat = :agentCreat"),
    @NamedQuery(name = "Personne.findByPhotoProfil", query = "SELECT p FROM Personne p WHERE p.photoProfil = :photoProfil")})
public class Personne implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 75)
    @Column(name = "NOM")
    private String nom;
    @Size(max = 75)
    @Column(name = "POSTNOM")
    private String postnom;
    @Size(max = 75)
    @Column(name = "PRENOMS")
    private String prenoms;
    @Column(name = "ETAT")
    private Boolean etat;
    @Size(max = 20)
    @Column(name = "NIF")
    private String nif;
    @Size(max = 50)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Size(max = 1073741823)
    @Column(name = "PHOTO_PROFIL")
    private String photoProfil;
    @Lob
    @Column(name = "PHOTO")
    private byte[] photo;
    @Lob
    @Column(name = "EMPREINTE")
    private byte[] empreinte;
    @OneToMany(mappedBy = "personne")
    private List<Acquisition> acquisitionList;
    @OneToMany(mappedBy = "receptionniste")
    private List<AcquitLiberatoire> acquitLiberatoireList;
    @OneToMany(mappedBy = "personne")
    private List<Journal> journalList;
    @OneToMany(mappedBy = "fkPersonne")
    private List<Reclamation> reclamationList;
    @OneToMany(mappedBy = "personne")
    private List<Complement> complementList;
    @OneToMany(mappedBy = "personne")
    private List<Assujeti> assujetiList;
    @OneToMany(mappedBy = "personne")
    private List<DepotDeclaration> depotDeclarationList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personne")
    private List<AdressePersonne> adressePersonneList;
    @OneToMany(mappedBy = "fkPersonne")
    private List<BonAPayer> bonAPayerList;
    @JoinColumn(name = "FORME_JURIDIQUE", referencedColumnName = "CODE")
    @ManyToOne
    private FormeJuridique formeJuridique;
    @OneToMany(mappedBy = "receptionniste")
    private List<NotePerception> notePerceptionList;
    @OneToMany(mappedBy = "personne")
    private List<FichePriseCharge> fichePriseChargeList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "personne")
    private LoginWeb loginWeb;

    public Personne() {
    }

    public Personne(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPostnom() {
        return postnom;
    }

    public void setPostnom(String postnom) {
        this.postnom = postnom;
    }

    public String getPrenoms() {
        return prenoms;
    }

    public void setPrenoms(String prenoms) {
        this.prenoms = prenoms;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getPhotoProfil() {
        return photoProfil;
    }

    public void setPhotoProfil(String photoProfil) {
        this.photoProfil = photoProfil;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public byte[] getEmpreinte() {
        return empreinte;
    }

    public void setEmpreinte(byte[] empreinte) {
        this.empreinte = empreinte;
    }

    @XmlTransient
    public List<Acquisition> getAcquisitionList() {
        return acquisitionList;
    }

    public void setAcquisitionList(List<Acquisition> acquisitionList) {
        this.acquisitionList = acquisitionList;
    }

    @XmlTransient
    public List<AcquitLiberatoire> getAcquitLiberatoireList() {
        return acquitLiberatoireList;
    }

    public void setAcquitLiberatoireList(List<AcquitLiberatoire> acquitLiberatoireList) {
        this.acquitLiberatoireList = acquitLiberatoireList;
    }

    @XmlTransient
    public List<Journal> getJournalList() {
        return journalList;
    }

    public void setJournalList(List<Journal> journalList) {
        this.journalList = journalList;
    }

    @XmlTransient
    public List<Reclamation> getReclamationList() {
        return reclamationList;
    }

    public void setReclamationList(List<Reclamation> reclamationList) {
        this.reclamationList = reclamationList;
    }

    @XmlTransient
    public List<Complement> getComplementList() {
        return complementList;
    }

    public void setComplementList(List<Complement> complementList) {
        this.complementList = complementList;
    }

    @XmlTransient
    public List<Assujeti> getAssujetiList() {
        return assujetiList;
    }

    public void setAssujetiList(List<Assujeti> assujetiList) {
        this.assujetiList = assujetiList;
    }

    @XmlTransient
    public List<DepotDeclaration> getDepotDeclarationList() {
        return depotDeclarationList;
    }

    public void setDepotDeclarationList(List<DepotDeclaration> depotDeclarationList) {
        this.depotDeclarationList = depotDeclarationList;
    }

    @XmlTransient
    public List<AdressePersonne> getAdressePersonneList() {
        return adressePersonneList;
    }

    public void setAdressePersonneList(List<AdressePersonne> adressePersonneList) {
        this.adressePersonneList = adressePersonneList;
    }

    @XmlTransient
    public List<BonAPayer> getBonAPayerList() {
        return bonAPayerList;
    }

    public void setBonAPayerList(List<BonAPayer> bonAPayerList) {
        this.bonAPayerList = bonAPayerList;
    }

    public FormeJuridique getFormeJuridique() {
        return formeJuridique;
    }

    public void setFormeJuridique(FormeJuridique formeJuridique) {
        this.formeJuridique = formeJuridique;
    }

    @XmlTransient
    public List<NotePerception> getNotePerceptionList() {
        return notePerceptionList;
    }

    public void setNotePerceptionList(List<NotePerception> notePerceptionList) {
        this.notePerceptionList = notePerceptionList;
    }

    @XmlTransient
    public List<FichePriseCharge> getFichePriseChargeList() {
        return fichePriseChargeList;
    }

    public void setFichePriseChargeList(List<FichePriseCharge> fichePriseChargeList) {
        this.fichePriseChargeList = fichePriseChargeList;
    }

    public LoginWeb getLoginWeb() {
        return loginWeb;
    }

    public void setLoginWeb(LoginWeb loginWeb) {
        this.loginWeb = loginWeb;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Personne)) {
            return false;
        }
        Personne other = (Personne) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

        @Override
    public String toString() {
        return   nom + ' ' + postnom + ' ' + prenoms; 
    }
    
}
