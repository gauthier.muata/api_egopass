/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juslin TSHIAMUA
 */
@Entity
@Cacheable(false)
@Table(name = "T_COMMANDE_CARTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CommandeCarte.findAll", query = "SELECT c FROM CommandeCarte c"),
    @NamedQuery(name = "CommandeCarte.findById", query = "SELECT c FROM CommandeCarte c WHERE c.id = :id"),
    @NamedQuery(name = "CommandeCarte.findByQte", query = "SELECT c FROM CommandeCarte c WHERE c.qte = :qte"),
    @NamedQuery(name = "CommandeCarte.findByPrixUnitaire", query = "SELECT c FROM CommandeCarte c WHERE c.prixUnitaire = :prixUnitaire"),
    @NamedQuery(name = "CommandeCarte.findByPrixTotal", query = "SELECT c FROM CommandeCarte c WHERE c.prixTotal = :prixTotal"),
    @NamedQuery(name = "CommandeCarte.findByDateCreate", query = "SELECT c FROM CommandeCarte c WHERE c.dateCreate = :dateCreate"),
    @NamedQuery(name = "CommandeCarte.findByDateUpdate", query = "SELECT c FROM CommandeCarte c WHERE c.dateUpdate = :dateUpdate"),
    @NamedQuery(name = "CommandeCarte.findByEtat", query = "SELECT c FROM CommandeCarte c WHERE c.etat = :etat"),
    @NamedQuery(name = "CommandeCarte.findByReference", query = "SELECT c FROM CommandeCarte c WHERE c.reference = :reference"),
    @NamedQuery(name = "CommandeCarte.findByToken", query = "SELECT c FROM CommandeCarte c WHERE c.token = :token"),
    @NamedQuery(name = "CommandeCarte.findByDevise", query = "SELECT c FROM CommandeCarte c WHERE c.devise = :devise")})
public class CommandeCarte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "QTE")
    private Integer qte;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PRIX_UNITAIRE")
    private BigDecimal prixUnitaire;
    @Column(name = "PRIX_TOTAL")
    private BigDecimal prixTotal;
    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "ETAT")
    private Integer etat;
    @Size(max = 25)
    @Column(name = "REFERENCE")
    private String reference;
    @Size(max = 5)
    @Column(name = "DEVISE")
    private String devise;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "TOKEN")
    private String token;
    @OneToMany(mappedBy = "fkCommandeCarte")
    private List<CarteGoPass> carteGoPassList;
    @JoinColumn(name = "AGENT_UPDATE", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentUpdate;
    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne fkPersonne;

    public CommandeCarte() {
    }

    public CommandeCarte(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQte() {
        return qte;
    }

    public void setQte(Integer qte) {
        this.qte = qte;
    }

    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public BigDecimal getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(BigDecimal prixTotal) {
        this.prixTotal = prixTotal;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @XmlTransient
    public List<CarteGoPass> getCarteGoPassList() {
        return carteGoPassList;
    }

    public void setCarteGoPassList(List<CarteGoPass> carteGoPassList) {
        this.carteGoPassList = carteGoPassList;
    }

    public Agent getAgentUpdate() {
        return agentUpdate;
    }

    public void setAgentUpdate(Agent agentUpdate) {
        this.agentUpdate = agentUpdate;
    }

    public Personne getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(Personne fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CommandeCarte)) {
            return false;
        }
        CommandeCarte other = (CommandeCarte) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.CommandeCarte[ id=" + id + " ]";
    }

}
