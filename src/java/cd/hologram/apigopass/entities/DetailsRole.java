/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrateur
 */
@Entity
@Table(name = "T_DETAILS_ROLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailsRole.findAll", query = "SELECT d FROM DetailsRole d"),
    @NamedQuery(name = "DetailsRole.findById", query = "SELECT d FROM DetailsRole d WHERE d.id = :id"),
    @NamedQuery(name = "DetailsRole.findByDocumentRef", query = "SELECT d FROM DetailsRole d WHERE d.documentRef = :documentRef"),
    @NamedQuery(name = "DetailsRole.findByFkTypeDocument", query = "SELECT d FROM DetailsRole d WHERE d.fkTypeDocument = :fkTypeDocument"),
    @NamedQuery(name = "DetailsRole.findByOrdonnance", query = "SELECT d FROM DetailsRole d WHERE d.ordonnance = :ordonnance"),
    @NamedQuery(name = "DetailsRole.findByFkMed", query = "SELECT d FROM DetailsRole d WHERE d.fkMed = :fkMed")})
public class DetailsRole implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "DOCUMENT_REF")
    private String documentRef;
    @Size(max = 10)
    @Column(name = "FK_TYPE_DOCUMENT")
    private String fkTypeDocument;
    @Column(name = "ORDONNANCE")
    private Integer ordonnance;
    @Size(max = 20)
    @Column(name = "FK_MED")
    private String fkMed;
    @JoinColumn(name = "FK_EXTRAIT_ROLE", referencedColumnName = "ID")
    @ManyToOne
    private ExtraitDeRole fkExtraitRole;
    @JoinColumn(name = "FK_ROLE", referencedColumnName = "ID")
    @ManyToOne
    private Role fkRole;

    public DetailsRole() {
    }

    public DetailsRole(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDocumentRef() {
        return documentRef;
    }

    public void setDocumentRef(String documentRef) {
        this.documentRef = documentRef;
    }

    public String getFkTypeDocument() {
        return fkTypeDocument;
    }

    public void setFkTypeDocument(String fkTypeDocument) {
        this.fkTypeDocument = fkTypeDocument;
    }

    public Integer getOrdonnance() {
        return ordonnance;
    }

    public void setOrdonnance(Integer ordonnance) {
        this.ordonnance = ordonnance;
    }

    public String getFkMed() {
        return fkMed;
    }

    public void setFkMed(String fkMed) {
        this.fkMed = fkMed;
    }

    public ExtraitDeRole getFkExtraitRole() {
        return fkExtraitRole;
    }

    public void setFkExtraitRole(ExtraitDeRole fkExtraitRole) {
        this.fkExtraitRole = fkExtraitRole;
    }

    public Role getFkRole() {
        return fkRole;
    }

    public void setFkRole(Role fkRole) {
        this.fkRole = fkRole;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailsRole)) {
            return false;
        }
        DetailsRole other = (DetailsRole) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.DetailsRole[ id=" + id + " ]";
    }
    
}
