/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juslin TSHIAMUA
 */
@Entity
@Cacheable(false)
@Table(name = "T_COMMANDE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commande.findAll", query = "SELECT c FROM Commande c"),
    @NamedQuery(name = "Commande.findById", query = "SELECT c FROM Commande c WHERE c.id = :id"),
    @NamedQuery(name = "Commande.findByDateCreat", query = "SELECT c FROM Commande c WHERE c.dateCreat = :dateCreat"),
    @NamedQuery(name = "Commande.findByMontant", query = "SELECT c FROM Commande c WHERE c.montant = :montant"),
    @NamedQuery(name = "Commande.findByEtat", query = "SELECT c FROM Commande c WHERE c.etat = :etat"),
    @NamedQuery(name = "Commande.findByReference", query = "SELECT c FROM Commande c WHERE c.reference = :reference"),
    @NamedQuery(name = "Commande.findByNotePaiement", query = "SELECT c FROM Commande c WHERE c.notePaiement = :notePaiement"),
    @NamedQuery(name = "Commande.findByPreuvePaiement", query = "SELECT c FROM Commande c WHERE c.preuvePaiement = :preuvePaiement"),
    @NamedQuery(name = "Commande.findByDatePaiement", query = "SELECT c FROM Commande c WHERE c.datePaiement = :datePaiement"),
    @NamedQuery(name = "Commande.findByReferencePaiement", query = "SELECT c FROM Commande c WHERE c.referencePaiement = :referencePaiement"),
    @NamedQuery(name = "Commande.findByToken", query = "SELECT c FROM Commande c WHERE c.token = :token"),
    @NamedQuery(name = "Commande.findByFichierZip", query = "SELECT c FROM Commande c WHERE c.fichierZip = :fichierZip")})
public class Commande implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Column(name = "ETAT")
    private Integer etat;
    @Size(max = 25)
    @Column(name = "REFERENCE")
    private String reference;
    @JoinColumn(name = "FK_AB", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleBudgetaire fkAb;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;
    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne fkPersonne;
    @OneToMany(mappedBy = "fkCommande")
    private List<Voucher> voucherList;
    @OneToMany(mappedBy = "fkCommande")
    private List<GoPass> goPassList;
    @OneToMany(mappedBy = "fkCommande")
    private List<ConciliationVouchers> conciliationVouchersList;
    @JoinColumn(name = "FK_COMPTE_BANCAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private CompteBancaire fkCompteBancaire;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "NOTE_PAIEMENT")
    private String notePaiement;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "PREUVE_PAIEMENT")
    private String preuvePaiement;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "TOKEN")
    private String token;
    @Column(name = "DATE_PAIEMENT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datePaiement;
    @Size(max = 25)
    @Column(name = "REFERENCE_PAIEMENT")
    private String referencePaiement;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "FICHIER_ZIP")
    private String fichierZip;
    @JoinColumn(name = "AGENT_PAIEMENT", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentPaiement;
    @JoinColumn(name = "SITE_PAIEMENT", referencedColumnName = "CODE")
    @ManyToOne
    private Site sitePaiement;
    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;

    public Commande() {
    }

    public Commande(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public ArticleBudgetaire getFkAb() {
        return fkAb;
    }

    public void setFkAb(ArticleBudgetaire fkAb) {
        this.fkAb = fkAb;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public Personne getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(Personne fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    @XmlTransient
    public List<Voucher> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<Voucher> voucherList) {
        this.voucherList = voucherList;
    }

    @XmlTransient
    public List<GoPass> getGoPassList() {
        return goPassList;
    }

    public void setGoPassList(List<GoPass> goPassList) {
        this.goPassList = goPassList;
    }

    public List<ConciliationVouchers> getConciliationVouchersList() {
        return conciliationVouchersList;
    }

    public void setConciliationVouchersList(List<ConciliationVouchers> conciliationVouchersList) {
        this.conciliationVouchersList = conciliationVouchersList;
    }

    public CompteBancaire getFkCompteBancaire() {
        return fkCompteBancaire;
    }

    public void setFkCompteBancaire(CompteBancaire fkCompteBancaire) {
        this.fkCompteBancaire = fkCompteBancaire;
    }

    public String getNotePaiement() {
        return notePaiement;
    }

    public void setNotePaiement(String notePaiement) {
        this.notePaiement = notePaiement;
    }

    public String getPreuvePaiement() {
        return preuvePaiement;
    }

    public void setPreuvePaiement(String preuvePaiement) {
        this.preuvePaiement = preuvePaiement;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFichierZip() {
        return fichierZip;
    }

    public void setFichierZip(String fichierZip) {
        this.fichierZip = fichierZip;
    }

    public Date getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    public String getReferencePaiement() {
        return referencePaiement;
    }

    public void setReferencePaiement(String referencePaiement) {
        this.referencePaiement = referencePaiement;
    }

    public Agent getAgentPaiement() {
        return agentPaiement;
    }

    public void setAgentPaiement(Agent agentPaiement) {
        this.agentPaiement = agentPaiement;
    }

    public Site getSitePaiement() {
        return sitePaiement;
    }

    public void setSitePaiement(Site sitePaiement) {
        this.sitePaiement = sitePaiement;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commande)) {
            return false;
        }
        Commande other = (Commande) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Commande[ id=" + id + " ]";
    }

}
