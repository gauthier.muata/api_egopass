/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_COMPLEMENT_BIEN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComplementBien.findAll", query = "SELECT c FROM ComplementBien c"),
    @NamedQuery(name = "ComplementBien.findById", query = "SELECT c FROM ComplementBien c WHERE c.id = :id"),
    @NamedQuery(name = "ComplementBien.findByValeur", query = "SELECT c FROM ComplementBien c WHERE c.valeur = :valeur"),
    @NamedQuery(name = "ComplementBien.findByEtat", query = "SELECT c FROM ComplementBien c WHERE c.etat = :etat"),
    @NamedQuery(name = "ComplementBien.findByDevise", query = "SELECT c FROM ComplementBien c WHERE c.devise = :devise")})
public class ComplementBien implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    @Size(max = 150)
    @Column(name = "VALEUR")
    private String valeur;
    @Column(name = "ETAT")
    private Boolean etat;
    @Size(max = 5)
    @Column(name = "DEVISE")
    private String devise;
    @JoinColumn(name = "BIEN", referencedColumnName = "ID")
    @ManyToOne
    private Bien bien;
    @JoinColumn(name = "TYPE_COMPLEMENT", referencedColumnName = "CODE")
    @ManyToOne
    private TypeComplementBien typeComplement;
    
    @Transient
    private boolean exist;
    
    public ComplementBien() {
    }

    public ComplementBien(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public Bien getBien() {
        return bien;
    }

    public void setBien(Bien bien) {
        this.bien = bien;
    }

    public TypeComplementBien getTypeComplement() {
        return typeComplement;
    }

    public void setTypeComplement(TypeComplementBien typeComplement) {
        this.typeComplement = typeComplement;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComplementBien)) {
            return false;
        }
        ComplementBien other = (ComplementBien) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return id;
    }

    public boolean isExist() {
        return exist;
    }

    public void setExist(boolean exist) {
        this.exist = exist;
    }
    
    
}
