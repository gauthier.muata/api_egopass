/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_ENTITE_ADMINISTRATIVE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EntiteAdministrative.findAll", query = "SELECT e FROM EntiteAdministrative e"),
    @NamedQuery(name = "EntiteAdministrative.findByCode", query = "SELECT e FROM EntiteAdministrative e WHERE e.code = :code"),
    @NamedQuery(name = "EntiteAdministrative.findByIntitule", query = "SELECT e FROM EntiteAdministrative e WHERE e.intitule = :intitule"),
    @NamedQuery(name = "EntiteAdministrative.findByTypeEntite", query = "SELECT e FROM EntiteAdministrative e WHERE e.typeEntite = :typeEntite"),
    @NamedQuery(name = "EntiteAdministrative.findByEtat", query = "SELECT e FROM EntiteAdministrative e WHERE e.etat = :etat")})
public class EntiteAdministrative implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 75)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "TYPE_ENTITE")
    private Integer typeEntite;
    @Column(name = "ETAT")
    private Short etat;
    @JoinColumn(name = "ENTITE_MERE", referencedColumnName = "CODE")
    @ManyToOne
    private EntiteAdministrative entiteMere;
    @OneToMany(mappedBy = "entiteMere")
    private List<EntiteAdministrative> entiteAdministrativeList;

    public EntiteAdministrative() {
    }

    public EntiteAdministrative(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Integer getTypeEntite() {
        return typeEntite;
    }

    public void setTypeEntite(Integer typeEntite) {
        this.typeEntite = typeEntite;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<EntiteAdministrative> getEntiteAdministrativeList() {
        return entiteAdministrativeList;
    }

    public void setEntiteAdministrativeList(List<EntiteAdministrative> entiteAdministrativeList) {
        this.entiteAdministrativeList = entiteAdministrativeList;
    }

    public EntiteAdministrative getEntiteMere() {
        return entiteMere;
    }

    public void setEntiteMere(EntiteAdministrative entiteMere) {
        this.entiteMere = entiteMere;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EntiteAdministrative)) {
            return false;
        }
        EntiteAdministrative other = (EntiteAdministrative) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.EntiteAdministrative[ code=" + code + " ]";
    }

}
