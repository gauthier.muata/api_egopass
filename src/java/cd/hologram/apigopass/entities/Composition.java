/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_COMPOSITION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Composition.findAll", query = "SELECT c FROM Composition c"),
    @NamedQuery(name = "Composition.findByCode", query = "SELECT c FROM Composition c WHERE c.code = :code"),
    @NamedQuery(name = "Composition.findByEtat", query = "SELECT c FROM Composition c WHERE c.etat = :etat"),
    @NamedQuery(name = "Composition.findByChambre", query = "SELECT c FROM Composition c WHERE c.chambre = :chambre"),
    @NamedQuery(name = "Composition.findByPermanant", query = "SELECT c FROM Composition c WHERE c.permanant = :permanant")})
public class Composition implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODE")
    private String code;
    @Column(name = "ETAT")
    private Boolean etat;
    @Size(max = 5)
    @Column(name = "CHAMBRE")
    private String chambre;
    @Column(name = "PERMANANT")
    private Boolean permanant;

    public Composition() {
    }

    public Composition(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getChambre() {
        return chambre;
    }

    public void setChambre(String chambre) {
        this.chambre = chambre;
    }

    public Boolean getPermanant() {
        return permanant;
    }

    public void setPermanant(Boolean permanant) {
        this.permanant = permanant;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Composition)) {
            return false;
        }
        Composition other = (Composition) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Composition[ code=" + code + " ]";
    }
    
}
