/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Cacheable(false)
@Entity
@Table(name = "T_RETRAIT_DECLARATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RetraitDeclaration.findAll", query = "SELECT r FROM RetraitDeclaration r"),
    @NamedQuery(name = "RetraitDeclaration.findById", query = "SELECT r FROM RetraitDeclaration r WHERE r.id = :id"),
    @NamedQuery(name = "RetraitDeclaration.findByRequerant", query = "SELECT r FROM RetraitDeclaration r WHERE r.requerant = :requerant"),
    @NamedQuery(name = "RetraitDeclaration.findByFkAssujetti", query = "SELECT r FROM RetraitDeclaration r WHERE r.fkAssujetti = :fkAssujetti"),
    @NamedQuery(name = "RetraitDeclaration.findByCodeDeclaration", query = "SELECT r FROM RetraitDeclaration r WHERE r.codeDeclaration = :codeDeclaration"),
    @NamedQuery(name = "RetraitDeclaration.findByFkAb", query = "SELECT r FROM RetraitDeclaration r WHERE r.fkAb = :fkAb"),
    @NamedQuery(name = "RetraitDeclaration.findByFkPeriode", query = "SELECT r FROM RetraitDeclaration r WHERE r.fkPeriode = :fkPeriode"),
    @NamedQuery(name = "RetraitDeclaration.findByFkAgentCreate", query = "SELECT r FROM RetraitDeclaration r WHERE r.fkAgentCreate = :fkAgentCreate"),
    @NamedQuery(name = "RetraitDeclaration.findByDateCreate", query = "SELECT r FROM RetraitDeclaration r WHERE r.dateCreate = :dateCreate"),
    @NamedQuery(name = "RetraitDeclaration.findByDateEcheancePaiement", query = "SELECT r FROM RetraitDeclaration r WHERE r.dateEcheancePaiement = :dateEcheancePaiement"),
    @NamedQuery(name = "RetraitDeclaration.findByRetraitDeclarationMere", query = "SELECT r FROM RetraitDeclaration r WHERE r.retraitDeclarationMere = :retraitDeclarationMere"),
    @NamedQuery(name = "RetraitDeclaration.findByEstPenalise", query = "SELECT r FROM RetraitDeclaration r WHERE r.estPenalise = :estPenalise"),
    @NamedQuery(name = "RetraitDeclaration.findByFkBanque", query = "SELECT r FROM RetraitDeclaration r WHERE r.fkBanque = :fkBanque"),
    @NamedQuery(name = "RetraitDeclaration.findByFkCompteBancaire", query = "SELECT r FROM RetraitDeclaration r WHERE r.fkCompteBancaire = :fkCompteBancaire")})
public class RetraitDeclaration implements Serializable {

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALEUR_BASE")
    private BigDecimal valeurBase;
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Size(max = 20)
    @Column(name = "DEVISE")
    private String devise;
    @Column(name = "ETAT")
    private Integer etat;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 200)
    @Column(name = "REQUERANT")
    private String requerant;
    @Size(max = 25)
    @Column(name = "FK_ASSUJETTI")
    private String fkAssujetti;
    @Size(max = 25)
    @Column(name = "CODE_DECLARATION")
    private String codeDeclaration;
    @Size(max = 25)
    @Column(name = "FK_AB")
    private String fkAb;
    @Size(max = 25)
    @Column(name = "FK_PERIODE")
    private String fkPeriode;
    @Size(max = 10)
    @Column(name = "FK_AGENT_CREATE")
    private String fkAgentCreate;
    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;

    @Column(name = "DATE_ECHEANCE_PAIEMENT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEcheancePaiement;

    @Column(name = "RETRAIT_DECLARATION_MERE")
    private Integer retraitDeclarationMere;

    @Column(name = "EST_PENALISE")
    private Integer estPenalise;

    @Column(name = "FK_BANQUE")
    private String fkBanque;

    @Column(name = "FK_COMPTE_BANCAIRE")
    private String fkCompteBancaire;

    @Column(name = "PENALITE_REMISE")
    private BigDecimal penaliteRemise;
    
    @Column(name = "TAUX_REMISE")
    private Integer tauxRemise;
    
    @Column(name = "FK_SITE")
    private String fkSite;
    
    @Column(name = "NEW_ID")
    private String newId;

    public RetraitDeclaration() {
    }

    public RetraitDeclaration(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRequerant() {
        return requerant;
    }

    public void setRequerant(String requerant) {
        this.requerant = requerant;
    }

    public String getFkAssujetti() {
        return fkAssujetti;
    }

    public void setFkAssujetti(String fkAssujetti) {
        this.fkAssujetti = fkAssujetti;
    }

    public String getCodeDeclaration() {
        return codeDeclaration;
    }

    public void setCodeDeclaration(String codeDeclaration) {
        this.codeDeclaration = codeDeclaration;
    }

    public String getFkAb() {
        return fkAb;
    }

    public void setFkAb(String fkAb) {
        this.fkAb = fkAb;
    }

    public String getFkPeriode() {
        return fkPeriode;
    }

    public void setFkPeriode(String fkPeriode) {
        this.fkPeriode = fkPeriode;
    }

    public String getFkAgentCreate() {
        return fkAgentCreate;
    }

    public void setFkAgentCreate(String fkAgentCreate) {
        this.fkAgentCreate = fkAgentCreate;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateEcheancePaiement() {
        return dateEcheancePaiement;
    }

    public void setDateEcheancePaiement(Date dateEcheancePaiement) {
        this.dateEcheancePaiement = dateEcheancePaiement;
    }

    public Integer getRetraitDeclarationMere() {
        return retraitDeclarationMere;
    }

    public void setRetraitDeclarationMere(Integer retraitDeclarationMere) {
        this.retraitDeclarationMere = retraitDeclarationMere;
    }

    public Integer getEstPenalise() {
        return estPenalise;
    }

    public void setEstPenalise(Integer estPenalise) {
        this.estPenalise = estPenalise;
    }

    public String getFkBanque() {
        return fkBanque;
    }

    public void setFkBanque(String fkBanque) {
        this.fkBanque = fkBanque;
    }

    public String getFkCompteBancaire() {
        return fkCompteBancaire;
    }

    public void setFkCompteBancaire(String fkCompteBancaire) {
        this.fkCompteBancaire = fkCompteBancaire;
    }

    public BigDecimal getPenaliteRemise() {
        return penaliteRemise;
    }

    public void setPenaliteRemise(BigDecimal penaliteRemise) {
        this.penaliteRemise = penaliteRemise;
    }

    public Integer getTauxRemise() {
        return tauxRemise;
    }

    public void setTauxRemise(Integer tauxRemise) {
        this.tauxRemise = tauxRemise;
    }

    public String getFkSite() {
        return fkSite;
    }

    public void setFkSite(String fkSite) {
        this.fkSite = fkSite;
    }

    public String getNewId() {
        return newId;
    }

    public void setNewId(String newId) {
        this.newId = newId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RetraitDeclaration)) {
            return false;
        }
        RetraitDeclaration other = (RetraitDeclaration) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.RetraitDeclaration[ id=" + id + " ]";
    }

    public BigDecimal getValeurBase() {
        return valeurBase;
    }

    public void setValeurBase(BigDecimal valeurBase) {
        this.valeurBase = valeurBase;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

}
