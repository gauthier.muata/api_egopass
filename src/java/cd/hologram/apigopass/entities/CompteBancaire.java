/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_COMPTE_BANCAIRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompteBancaire.findAll", query = "SELECT c FROM CompteBancaire c"),
    @NamedQuery(name = "CompteBancaire.findByCode", query = "SELECT c FROM CompteBancaire c WHERE c.code = :code"),
    @NamedQuery(name = "CompteBancaire.findByIntitule", query = "SELECT c FROM CompteBancaire c WHERE c.intitule = :intitule"),
    @NamedQuery(name = "CompteBancaire.findByEtat", query = "SELECT c FROM CompteBancaire c WHERE c.etat = :etat"),
    @NamedQuery(name = "CompteBancaire.findByAgentCreat", query = "SELECT c FROM CompteBancaire c WHERE c.agentCreat = :agentCreat"),
    @NamedQuery(name = "CompteBancaire.findByDateCreat", query = "SELECT c FROM CompteBancaire c WHERE c.dateCreat = :dateCreat"),
    @NamedQuery(name = "CompteBancaire.findByAgentMaj", query = "SELECT c FROM CompteBancaire c WHERE c.agentMaj = :agentMaj"),
    @NamedQuery(name = "CompteBancaire.findByDateMaj", query = "SELECT c FROM CompteBancaire c WHERE c.dateMaj = :dateMaj")})
public class CompteBancaire implements Serializable {

    @OneToMany(mappedBy = "fkCompteBancaire")
    private List<Commande> commandeList;
    @OneToMany(mappedBy = "compteBancaire")
    private List<ServiceCompte> serviceCompteList;
    @OneToMany(mappedBy = "compteBancaire")
    private List<Journal> journalList;
    @OneToMany(mappedBy = "fkCompte")
    private List<BonAPayer> bonAPayerList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CODE")
    private String code;
    @Size(max = 75)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @JoinColumn(name = "BANQUE", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private Banque banque;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;

    public CompteBancaire() {
    }

    public CompteBancaire(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Banque getBanque() {
        return banque;
    }

    public void setBanque(Banque banque) {
        this.banque = banque;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompteBancaire)) {
            return false;
        }
        CompteBancaire other = (CompteBancaire) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @XmlTransient
    public List<Commande> getCommandeList() {
        return commandeList;
    }

    public void setCommandeList(List<Commande> commandeList) {
        this.commandeList = commandeList;
    }

    @XmlTransient
    public List<ServiceCompte> getServiceCompteList() {
        return serviceCompteList;
    }

    public void setServiceCompteList(List<ServiceCompte> serviceCompteList) {
        this.serviceCompteList = serviceCompteList;
    }

    @XmlTransient
    public List<Journal> getJournalList() {
        return journalList;
    }

    public void setJournalList(List<Journal> journalList) {
        this.journalList = journalList;
    }

    @XmlTransient
    public List<BonAPayer> getBonAPayerList() {
        return bonAPayerList;
    }

    public void setBonAPayerList(List<BonAPayer> bonAPayerList) {
        this.bonAPayerList = bonAPayerList;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.CompteBancaire[ code=" + code + " ]";
    }

}
