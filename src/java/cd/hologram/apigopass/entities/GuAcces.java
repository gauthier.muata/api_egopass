/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_GU_ACCES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GuAcces.findAll", query = "SELECT g FROM GuAcces g"),
    @NamedQuery(name = "GuAcces.findById", query = "SELECT g FROM GuAcces g WHERE g.id = :id"),
    @NamedQuery(name = "GuAcces.findByGroupe", query = "SELECT g FROM GuAcces g WHERE g.groupe = :groupe"),
    @NamedQuery(name = "GuAcces.findByDroits", query = "SELECT g FROM GuAcces g WHERE g.droits = :droits"),
    @NamedQuery(name = "GuAcces.findByAgentCreat", query = "SELECT g FROM GuAcces g WHERE g.agentCreat = :agentCreat"),
    @NamedQuery(name = "GuAcces.findByDateCreat", query = "SELECT g FROM GuAcces g WHERE g.dateCreat = :dateCreat"),
    @NamedQuery(name = "GuAcces.findByAgentMaj", query = "SELECT g FROM GuAcces g WHERE g.agentMaj = :agentMaj"),
    @NamedQuery(name = "GuAcces.findByDateMaj", query = "SELECT g FROM GuAcces g WHERE g.dateMaj = :dateMaj"),
    @NamedQuery(name = "GuAcces.findByEtat", query = "SELECT g FROM GuAcces g WHERE g.etat = :etat")})
public class GuAcces implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "GROUPE")
    private Integer groupe;
    @Size(max = 100)
    @Column(name = "DROITS")
    private String droits;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;

    public GuAcces() {
    }

    public GuAcces(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGroupe() {
        return groupe;
    }

    public void setGroupe(Integer groupe) {
        this.groupe = groupe;
    }

    public String getDroits() {
        return droits;
    }

    public void setDroits(String droits) {
        this.droits = droits;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuAcces)) {
            return false;
        }
        GuAcces other = (GuAcces) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.GuAcces[ id=" + id + " ]";
    }
    
}
