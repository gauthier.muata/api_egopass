/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_RETRIBUTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Retribution.findAll", query = "SELECT r FROM Retribution r"),
    @NamedQuery(name = "Retribution.findByCode", query = "SELECT r FROM Retribution r WHERE r.code = :code"),
    @NamedQuery(name = "Retribution.findByValeur", query = "SELECT r FROM Retribution r WHERE r.valeur = :valeur"),
    @NamedQuery(name = "Retribution.findByType", query = "SELECT r FROM Retribution r WHERE r.type = :type"),
    @NamedQuery(name = "Retribution.findByEtat", query = "SELECT r FROM Retribution r WHERE r.etat = :etat")})
public class Retribution implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "VALEUR")
    private BigDecimal valeur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "TYPE")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ETAT")
    private boolean etat;
    @OneToMany(mappedBy = "retribution")
    private List<Repartition> repartitionList;
    @JoinColumn(name = "CATEGORIE_REPARTITION", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private CategorieRepartition categorieRepartition;
    @JoinColumn(name = "PALIER", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private Palier palier;
    @JoinColumn(name = "SERVICE", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private Service service;

    public Retribution() {
    }

    public Retribution(String code) {
        this.code = code;
    }

    public Retribution(String code, BigDecimal valeur, String type, boolean etat) {
        this.code = code;
        this.valeur = valeur;
        this.type = type;
        this.etat = etat;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getValeur() {
        return valeur;
    }

    public void setValeur(BigDecimal valeur) {
        this.valeur = valeur;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<Repartition> getRepartitionList() {
        return repartitionList;
    }

    public void setRepartitionList(List<Repartition> repartitionList) {
        this.repartitionList = repartitionList;
    }

    public CategorieRepartition getCategorieRepartition() {
        return categorieRepartition;
    }

    public void setCategorieRepartition(CategorieRepartition categorieRepartition) {
        this.categorieRepartition = categorieRepartition;
    }

    public Palier getPalier() {
        return palier;
    }

    public void setPalier(Palier palier) {
        this.palier = palier;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Retribution)) {
            return false;
        }
        Retribution other = (Retribution) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Retribution[ code=" + code + " ]";
    }
    
}
