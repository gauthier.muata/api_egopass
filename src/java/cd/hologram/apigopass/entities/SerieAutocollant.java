/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_SERIE_AUTOCOLLANT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SerieAutocollant.findAll", query = "SELECT s FROM SerieAutocollant s"),
    @NamedQuery(name = "SerieAutocollant.findById", query = "SELECT s FROM SerieAutocollant s WHERE s.id = :id"),
    @NamedQuery(name = "SerieAutocollant.findByAgentCreat", query = "SELECT s FROM SerieAutocollant s WHERE s.agentCreat = :agentCreat"),
    @NamedQuery(name = "SerieAutocollant.findByAgentMaj", query = "SELECT s FROM SerieAutocollant s WHERE s.agentMaj = :agentMaj"),
    @NamedQuery(name = "SerieAutocollant.findByBorneInferieur", query = "SELECT s FROM SerieAutocollant s WHERE s.borneInferieur = :borneInferieur"),
    @NamedQuery(name = "SerieAutocollant.findByBorneSuperieur", query = "SELECT s FROM SerieAutocollant s WHERE s.borneSuperieur = :borneSuperieur"),
    @NamedQuery(name = "SerieAutocollant.findByDateAffectation", query = "SELECT s FROM SerieAutocollant s WHERE s.dateAffectation = :dateAffectation"),
    @NamedQuery(name = "SerieAutocollant.findByDateDerniereUtilisation", query = "SELECT s FROM SerieAutocollant s WHERE s.dateDerniereUtilisation = :dateDerniereUtilisation"),
    @NamedQuery(name = "SerieAutocollant.findByDateReception", query = "SELECT s FROM SerieAutocollant s WHERE s.dateReception = :dateReception"),
    @NamedQuery(name = "SerieAutocollant.findByDerniereCle", query = "SELECT s FROM SerieAutocollant s WHERE s.derniereCle = :derniereCle"),
    @NamedQuery(name = "SerieAutocollant.findByEtat", query = "SELECT s FROM SerieAutocollant s WHERE s.etat = :etat"),
    @NamedQuery(name = "SerieAutocollant.findByIdSerieMere", query = "SELECT s FROM SerieAutocollant s WHERE s.idSerieMere = :idSerieMere"),
    @NamedQuery(name = "SerieAutocollant.findByProvince", query = "SELECT s FROM SerieAutocollant s WHERE s.province = :province"),
    @NamedQuery(name = "SerieAutocollant.findByQteInitiale", query = "SELECT s FROM SerieAutocollant s WHERE s.qteInitiale = :qteInitiale"),
    @NamedQuery(name = "SerieAutocollant.findByQteManquant", query = "SELECT s FROM SerieAutocollant s WHERE s.qteManquant = :qteManquant"),
    @NamedQuery(name = "SerieAutocollant.findByQteRestante", query = "SELECT s FROM SerieAutocollant s WHERE s.qteRestante = :qteRestante"),
    @NamedQuery(name = "SerieAutocollant.findByQteUtilisee", query = "SELECT s FROM SerieAutocollant s WHERE s.qteUtilisee = :qteUtilisee"),
    @NamedQuery(name = "SerieAutocollant.findBySite", query = "SELECT s FROM SerieAutocollant s WHERE s.site = :site"),
    @NamedQuery(name = "SerieAutocollant.findBySuffixe", query = "SELECT s FROM SerieAutocollant s WHERE s.suffixe = :suffixe"),
    @NamedQuery(name = "SerieAutocollant.findByType", query = "SELECT s FROM SerieAutocollant s WHERE s.type = :type")})
public class SerieAutocollant implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 255)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Size(max = 255)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "BORNE_INFERIEUR")
    private Integer borneInferieur;
    @Column(name = "BORNE_SUPERIEUR")
    private Integer borneSuperieur;
    @Column(name = "DATE_AFFECTATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAffectation;
    @Column(name = "DATE_DERNIERE_UTILISATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDerniereUtilisation;
    @Column(name = "DATE_RECEPTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReception;
    @Column(name = "DERNIERE_CLE")
    private Integer derniereCle;
    @Column(name = "ETAT")
    private Integer etat;
    @Column(name = "ID_SERIE_MERE")
    private Integer idSerieMere;
    @Size(max = 255)
    @Column(name = "PROVINCE")
    private String province;
    @Column(name = "QTE_INITIALE")
    private Integer qteInitiale;
    @Column(name = "QTE_MANQUANT")
    private Integer qteManquant;
    @Column(name = "QTE_RESTANTE")
    private Integer qteRestante;
    @Column(name = "QTE_UTILISEE")
    private Integer qteUtilisee;
    @Size(max = 255)
    @Column(name = "SITE")
    private String site;
    @Size(max = 255)
    @Column(name = "SUFFIXE")
    private String suffixe;
    @Size(max = 255)
    @Column(name = "TYPE")
    private String type;

    public SerieAutocollant() {
    }

    public SerieAutocollant(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Integer getBorneInferieur() {
        return borneInferieur;
    }

    public void setBorneInferieur(Integer borneInferieur) {
        this.borneInferieur = borneInferieur;
    }

    public Integer getBorneSuperieur() {
        return borneSuperieur;
    }

    public void setBorneSuperieur(Integer borneSuperieur) {
        this.borneSuperieur = borneSuperieur;
    }

    public Date getDateAffectation() {
        return dateAffectation;
    }

    public void setDateAffectation(Date dateAffectation) {
        this.dateAffectation = dateAffectation;
    }

    public Date getDateDerniereUtilisation() {
        return dateDerniereUtilisation;
    }

    public void setDateDerniereUtilisation(Date dateDerniereUtilisation) {
        this.dateDerniereUtilisation = dateDerniereUtilisation;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public Integer getDerniereCle() {
        return derniereCle;
    }

    public void setDerniereCle(Integer derniereCle) {
        this.derniereCle = derniereCle;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Integer getIdSerieMere() {
        return idSerieMere;
    }

    public void setIdSerieMere(Integer idSerieMere) {
        this.idSerieMere = idSerieMere;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Integer getQteInitiale() {
        return qteInitiale;
    }

    public void setQteInitiale(Integer qteInitiale) {
        this.qteInitiale = qteInitiale;
    }

    public Integer getQteManquant() {
        return qteManquant;
    }

    public void setQteManquant(Integer qteManquant) {
        this.qteManquant = qteManquant;
    }

    public Integer getQteRestante() {
        return qteRestante;
    }

    public void setQteRestante(Integer qteRestante) {
        this.qteRestante = qteRestante;
    }

    public Integer getQteUtilisee() {
        return qteUtilisee;
    }

    public void setQteUtilisee(Integer qteUtilisee) {
        this.qteUtilisee = qteUtilisee;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getSuffixe() {
        return suffixe;
    }

    public void setSuffixe(String suffixe) {
        this.suffixe = suffixe;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SerieAutocollant)) {
            return false;
        }
        SerieAutocollant other = (SerieAutocollant) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.SerieAutocollant[ id=" + id + " ]";
    }
    
}
