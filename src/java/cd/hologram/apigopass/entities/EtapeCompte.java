/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_ETAPE_COMPTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EtapeCompte.findAll", query = "SELECT e FROM EtapeCompte e"),
    @NamedQuery(name = "EtapeCompte.findById", query = "SELECT e FROM EtapeCompte e WHERE e.id = :id"),
    @NamedQuery(name = "EtapeCompte.findByEtape", query = "SELECT e FROM EtapeCompte e WHERE e.etape = :etape"),
    @NamedQuery(name = "EtapeCompte.findByCompte", query = "SELECT e FROM EtapeCompte e WHERE e.compte = :compte"),
    @NamedQuery(name = "EtapeCompte.findByEtat", query = "SELECT e FROM EtapeCompte e WHERE e.etat = :etat")})
public class EtapeCompte implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 25)
    @Column(name = "ETAPE")
    private String etape;
    @Size(max = 25)
    @Column(name = "COMPTE")
    private String compte;
    @Column(name = "ETAT")
    private Boolean etat;

    public EtapeCompte() {
    }

    public EtapeCompte(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEtape() {
        return etape;
    }

    public void setEtape(String etape) {
        this.etape = etape;
    }

    public String getCompte() {
        return compte;
    }

    public void setCompte(String compte) {
        this.compte = compte;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EtapeCompte)) {
            return false;
        }
        EtapeCompte other = (EtapeCompte) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.EtapeCompte[ id=" + id + " ]";
    }
    
}
