/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Lenovo
 */
@Entity
@Table(name = "T_ENTITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Entite.findAll", query = "SELECT e FROM Entite e"),
    @NamedQuery(name = "Entite.findByIdEntite", query = "SELECT e FROM Entite e WHERE e.idEntite = :idEntite"),
    @NamedQuery(name = "Entite.findByIntituleEntite", query = "SELECT e FROM Entite e WHERE e.intituleEntite = :intituleEntite"),
    @NamedQuery(name = "Entite.findByDenominationHabitant", query = "SELECT e FROM Entite e WHERE e.denominationHabitant = :denominationHabitant"),
    @NamedQuery(name = "Entite.findByEtat", query = "SELECT e FROM Entite e WHERE e.etat = :etat"),
    @NamedQuery(name = "Entite.findByIsVisible", query = "SELECT e FROM Entite e WHERE e.isVisible = :isVisible")})
public class Entite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "IdEntite")
    private String idEntite;
    @Size(max = 150)
    @Column(name = "IntituleEntite")
    private String intituleEntite;
    @Size(max = 150)
    @Column(name = "DenominationHabitant")
    private String denominationHabitant;
    @Column(name = "Etat")
    private Integer etat;
    @Column(name = "IsVisible")
    private Integer isVisible;
    @OneToMany(mappedBy = "fkEntiteMere")
    private List<Entite> entiteList;
    @JoinColumn(name = "Fk_EntiteMere", referencedColumnName = "IdEntite")
    @ManyToOne
    private Entite fkEntiteMere;
    @JoinColumn(name = "Fk_TypeEntite", referencedColumnName = "IdTypeEntite")
    @ManyToOne
    private TypeEa fkTypeEntite;
    @OneToMany(mappedBy = "fkEntite")
    private List<Aeroport> aeroportList;

    public Entite() {
    }

    public Entite(String idEntite) {
        this.idEntite = idEntite;
    }

    public String getIdEntite() {
        return idEntite;
    }

    public void setIdEntite(String idEntite) {
        this.idEntite = idEntite;
    }

    public String getIntituleEntite() {
        return intituleEntite;
    }

    public void setIntituleEntite(String intituleEntite) {
        this.intituleEntite = intituleEntite;
    }

    public String getDenominationHabitant() {
        return denominationHabitant;
    }

    public void setDenominationHabitant(String denominationHabitant) {
        this.denominationHabitant = denominationHabitant;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Integer getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(Integer isVisible) {
        this.isVisible = isVisible;
    }

    @XmlTransient
    public List<Entite> getEntiteList() {
        return entiteList;
    }

    public void setEntiteList(List<Entite> entiteList) {
        this.entiteList = entiteList;
    }

    public Entite getFkEntiteMere() {
        return fkEntiteMere;
    }

    public void setFkEntiteMere(Entite fkEntiteMere) {
        this.fkEntiteMere = fkEntiteMere;
    }

    public TypeEa getFkTypeEntite() {
        return fkTypeEntite;
    }

    public void setFkTypeEntite(TypeEa fkTypeEntite) {
        this.fkTypeEntite = fkTypeEntite;
    }

    @XmlTransient
    public List<Aeroport> getAeroportList() {
        return aeroportList;
    }

    public void setAeroportList(List<Aeroport> aeroportList) {
        this.aeroportList = aeroportList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEntite != null ? idEntite.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Entite)) {
            return false;
        }
        Entite other = (Entite) object;
        if ((this.idEntite == null && other.idEntite != null) || (this.idEntite != null && !this.idEntite.equals(other.idEntite))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.Entite[ idEntite=" + idEntite + " ]";
    }

}
