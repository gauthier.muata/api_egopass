/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrateur
 */
@Entity
@Cacheable(false)
@Table(name = "T_REMISE_SOUS_PROVISION_PEAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RemiseSousProvisionPeage.findAll", query = "SELECT r FROM RemiseSousProvisionPeage r"),
    @NamedQuery(name = "RemiseSousProvisionPeage.findById", query = "SELECT r FROM RemiseSousProvisionPeage r WHERE r.id = :id"),
    @NamedQuery(name = "RemiseSousProvisionPeage.findByTaux", query = "SELECT r FROM RemiseSousProvisionPeage r WHERE r.taux = :taux"),
    @NamedQuery(name = "RemiseSousProvisionPeage.findByType", query = "SELECT r FROM RemiseSousProvisionPeage r WHERE r.type = :type"),
    @NamedQuery(name = "RemiseSousProvisionPeage.findByEtat", query = "SELECT r FROM RemiseSousProvisionPeage r WHERE r.etat = :etat"),
    @NamedQuery(name = "RemiseSousProvisionPeage.findByFkAxe", query = "SELECT r FROM RemiseSousProvisionPeage r WHERE r.fkAxe = :fkAxe")})
public class RemiseSousProvisionPeage implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "TAUX")
    private Long taux;
    @Size(max = 50)
    @Column(name = "TYPE")
    private String type;
    @Column(name = "ETAT")
    private Integer etat;
    @Column(name = "FK_AXE")
    private Integer fkAxe;
    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne fkPersonne;
    @JoinColumn(name = "FK_SITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site fkSite;
    @JoinColumn(name = "FK_TARIF", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif fkTarif;

    public RemiseSousProvisionPeage() {
    }

    public RemiseSousProvisionPeage(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getTaux() {
        return taux;
    }

    public void setTaux(Long taux) {
        this.taux = taux;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Integer getFkAxe() {
        return fkAxe;
    }

    public void setFkAxe(Integer fkAxe) {
        this.fkAxe = fkAxe;
    }

    public Personne getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(Personne fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    public Site getFkSite() {
        return fkSite;
    }

    public void setFkSite(Site fkSite) {
        this.fkSite = fkSite;
    }

    public Tarif getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(Tarif fkTarif) {
        this.fkTarif = fkTarif;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RemiseSousProvisionPeage)) {
            return false;
        }
        RemiseSousProvisionPeage other = (RemiseSousProvisionPeage) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.RemiseSousProvisionPeage[ id=" + id + " ]";
    }
    
}
