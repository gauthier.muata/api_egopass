/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_TYPE_COMPLEMENT_DOCUMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeComplementDocument.findAll", query = "SELECT t FROM TypeComplementDocument t"),
    @NamedQuery(name = "TypeComplementDocument.findByCode", query = "SELECT t FROM TypeComplementDocument t WHERE t.code = :code"),
    @NamedQuery(name = "TypeComplementDocument.findByOrdre", query = "SELECT t FROM TypeComplementDocument t WHERE t.ordre = :ordre"),
    @NamedQuery(name = "TypeComplementDocument.findByTypeComplement", query = "SELECT t FROM TypeComplementDocument t WHERE t.typeComplement = :typeComplement"),
    @NamedQuery(name = "TypeComplementDocument.findByTypeDocument", query = "SELECT t FROM TypeComplementDocument t WHERE t.typeDocument = :typeDocument"),
    @NamedQuery(name = "TypeComplementDocument.findByObligatoire", query = "SELECT t FROM TypeComplementDocument t WHERE t.obligatoire = :obligatoire"),
    @NamedQuery(name = "TypeComplementDocument.findByEtat", query = "SELECT t FROM TypeComplementDocument t WHERE t.etat = :etat")})
public class TypeComplementDocument implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "ORDRE")
    private String ordre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "TYPE_COMPLEMENT")
    private String typeComplement;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "TYPE_DOCUMENT")
    private String typeDocument;
    @Column(name = "OBLIGATOIRE")
    private Boolean obligatoire;
    @Column(name = "ETAT")
    private Boolean etat;

    public TypeComplementDocument() {
    }

    public TypeComplementDocument(String code) {
        this.code = code;
    }

    public TypeComplementDocument(String code, String ordre, String typeComplement, String typeDocument) {
        this.code = code;
        this.ordre = ordre;
        this.typeComplement = typeComplement;
        this.typeDocument = typeDocument;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOrdre() {
        return ordre;
    }

    public void setOrdre(String ordre) {
        this.ordre = ordre;
    }

    public String getTypeComplement() {
        return typeComplement;
    }

    public void setTypeComplement(String typeComplement) {
        this.typeComplement = typeComplement;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public Boolean getObligatoire() {
        return obligatoire;
    }

    public void setObligatoire(Boolean obligatoire) {
        this.obligatoire = obligatoire;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeComplementDocument)) {
            return false;
        }
        TypeComplementDocument other = (TypeComplementDocument) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.apidgrk.entities.TypeComplementDocument[ code=" + code + " ]";
    }
    
}
