/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apigopass.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrateur
 */
@Entity
@Cacheable(false)
@Table(name = "T_SITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Site.findAll", query = "SELECT s FROM Site s"),
    @NamedQuery(name = "Site.findByCode", query = "SELECT s FROM Site s WHERE s.code = :code"),
    @NamedQuery(name = "Site.findByIntitule", query = "SELECT s FROM Site s WHERE s.intitule = :intitule"),
    @NamedQuery(name = "Site.findByEtat", query = "SELECT s FROM Site s WHERE s.etat = :etat"),
    @NamedQuery(name = "Site.findByPersonne", query = "SELECT s FROM Site s WHERE s.personne = :personne"),
    @NamedQuery(name = "Site.findByCentre", query = "SELECT s FROM Site s WHERE s.centre = :centre"),
    @NamedQuery(name = "Site.findByPeage", query = "SELECT s FROM Site s WHERE s.peage = :peage"),
    @NamedQuery(name = "Site.findByFkDivision", query = "SELECT s FROM Site s WHERE s.fkDivision = :fkDivision"),
    @NamedQuery(name = "Site.findBySigle", query = "SELECT s FROM Site s WHERE s.sigle = :sigle"),
    @NamedQuery(name = "Site.findByFkTypeEntite", query = "SELECT s FROM Site s WHERE s.fkTypeEntite = :fkTypeEntite")})
public class Site implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CODE")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 25)
    @Column(name = "PERSONNE")
    private String personne;
    @Size(max = 50)
    @Column(name = "CENTRE")
    private String centre;
    @Column(name = "PEAGE")
    private Boolean peage;
    @Size(max = 10)
    @Column(name = "SIGLE")
    private String sigle;
    @Column(name = "FK_TYPE_ENTITE")
    private Integer fkTypeEntite;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "site")
    private List<SiteBanque> siteBanqueList;
    @OneToMany(mappedBy = "fkSite")
    private List<Voucher> voucherList;
    @OneToMany(mappedBy = "site")
    private List<Agent> agentList;
    @OneToMany(mappedBy = "codesite")
    private List<AgentSite> agentSiteList;
    @JoinColumn(name = "ADRESSE", referencedColumnName = "ID")
    @ManyToOne
    private Adresse adresse;
    @OneToMany(mappedBy = "fkSite")
    private List<UtilisateurDivision> utilisateurDivisionList;
    @OneToMany(mappedBy = "fkSite")
    private List<Equipement> equipementList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "site")
    private List<UaSite> uaSiteList;
    @OneToMany(mappedBy = "site")
    private List<TicketPeage> ticketPeageList;
    @JoinColumn(name = "FK_DIVISION", referencedColumnName = "CODE")
    @ManyToOne
    private Division fkDivision;
    @OneToMany(mappedBy = "fkSite")
    private List<RemiseSousProvisionPeage> remiseSousProvisionPeageList;
//    @OneToMany(mappedBy = "fkSite")
//    private List<DetailVoucher> detailVoucherList;

    public Site() {
    }

    public Site(String code) {
        this.code = code;
    }

    public Site(String code, String intitule) {
        this.code = code;
        this.intitule = intitule;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getPersonne() {
        return personne;
    }

    public void setPersonne(String personne) {
        this.personne = personne;
    }

    public String getCentre() {
        return centre;
    }

    public void setCentre(String centre) {
        this.centre = centre;
    }

    public Boolean getPeage() {
        return peage;
    }

    public void setPeage(Boolean peage) {
        this.peage = peage;
    }

    public String getSigle() {
        return sigle;
    }

    public void setSigle(String sigle) {
        this.sigle = sigle;
    }

    public Integer getFkTypeEntite() {
        return fkTypeEntite;
    }

    public void setFkTypeEntite(Integer fkTypeEntite) {
        this.fkTypeEntite = fkTypeEntite;
    }

    @XmlTransient
    public List<SiteBanque> getSiteBanqueList() {
        return siteBanqueList;
    }

    public void setSiteBanqueList(List<SiteBanque> siteBanqueList) {
        this.siteBanqueList = siteBanqueList;
    }

    @XmlTransient
    public List<Voucher> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<Voucher> voucherList) {
        this.voucherList = voucherList;
    }

    @XmlTransient
    public List<Agent> getAgentList() {
        return agentList;
    }

    public void setAgentList(List<Agent> agentList) {
        this.agentList = agentList;
    }

    @XmlTransient
    public List<AgentSite> getAgentSiteList() {
        return agentSiteList;
    }

    public void setAgentSiteList(List<AgentSite> agentSiteList) {
        this.agentSiteList = agentSiteList;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    @XmlTransient
    public List<UtilisateurDivision> getUtilisateurDivisionList() {
        return utilisateurDivisionList;
    }

    public void setUtilisateurDivisionList(List<UtilisateurDivision> utilisateurDivisionList) {
        this.utilisateurDivisionList = utilisateurDivisionList;
    }

    @XmlTransient
    public List<Equipement> getEquipementList() {
        return equipementList;
    }

    public void setEquipementList(List<Equipement> equipementList) {
        this.equipementList = equipementList;
    }

    @XmlTransient
    public List<UaSite> getUaSiteList() {
        return uaSiteList;
    }

    public void setUaSiteList(List<UaSite> uaSiteList) {
        this.uaSiteList = uaSiteList;
    }

    @XmlTransient
    public List<TicketPeage> getTicketPeageList() {
        return ticketPeageList;
    }

    public void setTicketPeageList(List<TicketPeage> ticketPeageList) {
        this.ticketPeageList = ticketPeageList;
    }

    @XmlTransient
    public List<RemiseSousProvisionPeage> getRemiseSousProvisionPeageList() {
        return remiseSousProvisionPeageList;
    }

    public void setRemiseSousProvisionPeageList(List<RemiseSousProvisionPeage> remiseSousProvisionPeageList) {
        this.remiseSousProvisionPeageList = remiseSousProvisionPeageList;
    }

    public Division getFkDivision() {
        return fkDivision;
    }

    public void setFkDivision(Division fkDivision) {
        this.fkDivision = fkDivision;
    }

//    public List<DetailVoucher> getDetailVoucherList() {
//        return detailVoucherList;
//    }
//
//    public void setDetailVoucherList(List<DetailVoucher> detailVoucherList) {
//        this.detailVoucherList = detailVoucherList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Site)) {
            return false;
        }
        Site other = (Site) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entites.Site[ code=" + code + " ]";
    }

}
